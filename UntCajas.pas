unit UntCajas;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, dxSkinsCore, dxSkinsDefaultPainters,
  dxSkinscxPCPainter, cxPCdxBarPopupMenu, cxStyles, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxEdit, Data.DB, cxDBData, cxClasses,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGridLevel,
  cxGridCustomView, cxGrid, cxPC, Data.Win.ADODB, Vcl.Menus, Vcl.StdCtrls,
  cxButtons, cxContainer, Vcl.ExtCtrls, Vcl.DBCtrls, cxLabel, cxTextEdit,
  cxDBEdit, cxInplaceContainer, cxVGrid, cxDBVGrid, cxNavigator, cxDBNavigator,
  cxCheckBox, Vcl.ComCtrls, Vcl.ToolWin, Datasnap.DBClient, JvMenus,
  Datasnap.Provider, cxMaskEdit, Vcl.DBLookup, cxSpinEdit;

type
  TfrmCajas = class(TForm)
    qry_Cajas: TADOQuery;
    dsCajas: TDataSource;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    ColID: TcxGridDBColumn;
    ColNombrePC: TcxGridDBColumn;
    ColSupervisor: TcxGridDBColumn;
    cxStyleRepository1: TcxStyleRepository;
    cxStyle1: TcxStyle;
    cxStyle2: TcxStyle;
    cxstyl1: TcxStyle;
    cxtbshtEstadisticas1: TcxTabSheet;
    atncfld_CajasID: TAutoIncField;
    wdstrngfld_CajasNombrePC: TWideStringField;
    wdstrngfld_CajasSupervisor: TWideStringField;
    intgrfld_CajasImprimieCopia: TIntegerField;
    ColImprimeCopia: TcxGridDBColumn;
    blnfld_CajasSeleccionaSeriales: TBooleanField;
    blnfld_CajasEmiteComprobantes: TBooleanField;
    cxstyl2: TcxStyle;
    tlb1: TToolBar;
    btnRegistro: TToolButton;
    btnAnterior: TToolButton;
    btnSiguiente: TToolButton;
    btnGuardar: TToolButton;
    btnCancelar: TToolButton;
    btn2: TToolButton;
    btnSalir: TToolButton;
    dsetCajas: TClientDataSet;
    dtstprvdrCajas: TDataSetProvider;
    jvpmn1: TJvPopupMenu;
    mniBuscar1: TMenuItem;
    mniAgregar1: TMenuItem;
    mniModificar1: TMenuItem;
    mniEliminar1: TMenuItem;
    cxlbl6: TcxLabel;
    grp1: TGroupBox;
    cxlbl1: TcxLabel;
    cxlbl2: TcxLabel;
    edtIDCaja: TcxDBMaskEdit;
    edtNombreCaja: TcxDBMaskEdit;
    edtSupervisor: TcxDBMaskEdit;
    cxdbspndt1: TcxDBSpinEdit;
    cxlbl3: TcxLabel;
    chkSeleccionesSeriales: TcxDBCheckBox;
    chkEmiteNCF: TcxDBCheckBox;
    mniN1: TMenuItem;
    mniN2: TMenuItem;
    mniN3: TMenuItem;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure btnSalirClick(Sender: TObject);
    procedure mniEliminar1Click(Sender: TObject);
    procedure btnGuardarClick(Sender: TObject);
    procedure mniAgregar1Click(Sender: TObject);
    procedure cxGrid1DBTableView1CellDblClick(Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure btnCancelarClick(Sender: TObject);
    procedure Botones;
    procedure Salir;
    procedure btnAnteriorClick(Sender: TObject);
    procedure btnSiguienteClick(Sender: TObject);
    procedure mniModificar1Click(Sender: TObject);

  private
    { Private declarations }
    var_Posteado : Boolean;
  public
    { Public declarations }
  end;

var
  frmCajas: TfrmCajas;

implementation

{$R *.dfm}

uses UntData;

procedure TfrmCajas.btnSalirClick(Sender: TObject);
begin
  Self.Close;
end;

procedure TfrmCajas.btnSiguienteClick(Sender: TObject);
begin
  dsetCajas.Next;
end;

procedure TfrmCajas.btnAnteriorClick(Sender: TObject);
begin
  dsetCajas.Prior;
end;

procedure TfrmCajas.btnCancelarClick(Sender: TObject);
begin
  var_Posteado:=True;
  dsetCajas.CancelUpdates;
  Botones;
end;

procedure TfrmCajas.btnGuardarClick(Sender: TObject);
begin
  var_Posteado:=True;
  dsetCajas.ApplyUpdates(0);
  Botones;
end;

procedure TfrmCajas.cxGrid1DBTableView1CellDblClick(
  Sender: TcxCustomGridTableView; ACellViewInfo: TcxGridTableDataCellViewInfo;
  AButton: TMouseButton; AShift: TShiftState; var AHandled: Boolean);
begin
  cxPageControl1.ActivePageIndex:=1;
end;

procedure TfrmCajas.FormClose(Sender: TObject; var Action: TCloseAction);
begin
if var_Posteado=False then
  begin
    if MessageBox(Handle,'Los Datos no han sido guardados, se perderan, est� Seguro de Salir?','Caja',MB_ICONQUESTION or MB_YESNO)=ID_YES then
    begin
      dsetCajas.CancelUpdates;
      Action:=caFree;
      frmCajas:=nil;
    end
    else
    begin
      exit;
    end;
  end;
  Action:=caFree;
  frmCajas:=nil;
end;

procedure TfrmCajas.FormShow(Sender: TObject);
begin
  var_Posteado:=True;
  dsetCajas.Open;
  Botones;
end;

procedure TfrmCajas.mniAgregar1Click(Sender: TObject);
begin
  var_Posteado:=False;
  cxPageControl1.ActivePageIndex:=1;
  dsetCajas.Append;
  edtNombreCaja.SetFocus;
  Botones;
end;

procedure TfrmCajas.mniEliminar1Click(Sender: TObject);
begin
  if MessageBox(Handle,'Desear Eliminar el registro?','Cajas',MB_ICONQUESTION or MB_YESNO)=ID_YES then
  begin
    dsetCajas.Delete;
    dsetCajas.ApplyUpdates(1);
  end;
end;

procedure TfrmCajas.mniModificar1Click(Sender: TObject);
begin
  var_Posteado:=False;
  cxPageControl1.ActivePageIndex:=1;
  dsetCajas.Edit;
  edtNombreCaja.SetFocus;
  Botones;
end;

procedure TfrmCajas.Botones;
begin
  if var_Posteado=False then
  begin
    btnRegistro.Visible:=False;
    btnAnterior.Visible:=False;
    btnSiguiente.Visible:=False;
    btnSalir.Visible:=False;
    btnCancelar.Visible:=True;
    btnGuardar.Visible:=True;
  end
  else
  begin
    btnRegistro.Visible:=True;
    btnAnterior.Visible:=True;
    btnSiguiente.Visible:=True;
    btnSalir.Visible:=True;
    btnCancelar.Visible:=False;
    btnGuardar.Visible:=False;
  end;
end;

procedure TfrmCajas.Salir;
begin
  if var_Posteado=False then
  begin
    if MessageBox(Handle,'Los Datos NO han sido guardados, se perderan, est� Seguro de Salir?','Cajas',MB_ICONQUESTION or MB_YESNO) =IDYES then
    begin
      dsetCajas.CancelUpdates;
    end
    else
    begin
      exit;
    end;
  end;
  self.Close();
end;
end.
