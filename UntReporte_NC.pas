unit UntReporte_NC;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Vcl.Menus, dxSkinsCore, dxSkinsDefaultPainters,
  cxControls, cxStyles, dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, Data.DB, cxDBData, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGridLevel, cxClasses, cxGridCustomView,
  cxGrid, Vcl.StdCtrls, JvExStdCtrls, JvCombobox, Vcl.ComCtrls, JvExComCtrls,
  JvDateTimePicker, cxButtons, Datasnap.DBClient, Datasnap.Provider,
  cxContainer, Data.Win.ADODB, cxTextEdit, cxMaskEdit, cxDropDownEdit,
  cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox, JvComponentBase,
  JvDBGridExport, Vcl.Grids, Vcl.DBGrids, JvExDBGrids, JvDBGrid, Vcl.ActnList;

type
  TfrmReportes_NC = class(TForm)
    btnImprimir: TcxButton;
    dtmpckrHasta: TJvDateTimePicker;
    btnSalir: TcxButton;
    lbl1: TLabel;
    dtmpckrDesde: TJvDateTimePicker;
    lblDesde: TLabel;
    cbbUsuario: TcxLookupComboBox;
    qryUsuarios: TADOQuery;
    dsUsuarios: TDataSource;
    lbl2: TLabel;
    btnRefrescar: TcxButton;
    btnExportarExcel: TcxButton;
    sp1: TADOStoredProc;
    ds1: TDataSource;
    jvdbgrd1: TJvDBGrid;
    flsvdlg1: TFileSaveDialog;
    jvdbgrdxclxprt1: TJvDBGridExcelExport;
    actlst1: TActionList;
    actRefrescar: TAction;
    actExportar: TAction;
    procedure btnImprimirClick(Sender: TObject);
    procedure btnSalirClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure cbbVendedorPropertiesChange(Sender: TObject);
    procedure actRefrescarExecute(Sender: TObject);
    procedure actExportarExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmReportes_NC: TfrmReportes_NC;
  var_UsuarioID : Integer;


implementation

{$R *.dfm}

uses UntDataReportes, UntData;


procedure TfrmReportes_NC.actExportarExecute(Sender: TObject);
var
  archivo : String;
begin
  flsvdlg1.Execute;
  archivo:=flsvdlg1.FileName;
  if Length(archivo)>0 then begin
    jvdbgrdxclxprt1.FileName:=archivo;
    jvdbgrdxclxprt1.ExportGrid;
  end;
end;
procedure TfrmReportes_NC.actRefrescarExecute(Sender: TObject);
begin
  if cbbUsuario.Text<>'' then
  begin
    btnExportarExcel.Enabled:=True;
  end
  else
  begin
    btnExportarExcel.Enabled:=False;
  end;
  with sp1 do begin
    Close;
    Parameters.ParamByName('@Fecha_Conciliacion_Desde').Value:=
    dtmpckrDesde.DateTime;
    Parameters.ParamByName('@Fecha_Conciliacion_Hasta').Value:=dtmpckrHasta.DateTime;
    Parameters.ParamByName('@Conciliacion_UsuarioID').Value:=var_UsuarioID;
    Close;
    Open;
  end;
end;

procedure TfrmReportes_NC.btnImprimirClick(Sender: TObject);
var
  var_fecha_desde : String;
  var_fecha_hasta : string;
begin
  if Length(cbbUsuario.Text)=0 then
  begin
    MessageBox(Handle,'Debe seleccionar el usuario','Reporte Notas de Cr�dito conciliadas',MB_ICONERROR or MB_OK);
    Exit;
  end;

  with  DatReportes.sp_RPT_NC_Conciliadas do
  begin
    var_fecha_desde:=FormatDateTime('yyyy/mm/dd',
      dtmpckrDesde.DateTime);
    var_fecha_hasta:=FormatDateTime('yyyy/mm/dd',
      dtmpckrHasta.DateTime);

    Parameters.ParamByName('@Fecha_Conciliacion_Desde').Value:=var_fecha_desde;
    Parameters.ParamByName('@Fecha_Conciliacion_Hasta').Value:=var_fecha_hasta;
    Parameters.ParamByName('@Conciliacion_UsuarioID').Value:=var_UsuarioID;
    Close;
    Open;
    if DatReportes.sp_RPT_NC_Conciliadas.RecordCount=0 then begin
      MessageBox(Handle,'No existe informaci�n','Reporte Notas de Cr�dito conciliadas',MB_ICONERROR or MB_OK);
      Exit;
    end;
    //var_EstadodeCaja:='***** CAJA NO Tiene Registro ****';
    //if FieldByName('EstadodeCaja').Value='C' then var_EstadodeCaja:='CAJA CERRADA';
    //if FieldByName('EstadodeCaja').Value='A' then var_EstadodeCaja:='CAJA ABIERTA';

    with DatReportes.RptNC_Conciliadas do
    begin
      Template.DatabaseSettings.Name:='NC conciliadas';
      Template.LoadFromDatabase;
      //DatReportes.plblEstadodeCaja.Text:=var_EstadodeCaja;
      //DatReportes.plblfecha.Text:='FECHA DESDE: '+FormatDateTime('yyyy/mm/dd',jvdtmpckrDesde.Date)+
      //'  HASTA: '+FormatDateTime('yyyy/mm/dd',jvdtmpckrDesde.Date);

      PrinterSetup.Copies:=1;
      PrinterSetup.PrinterName:='Default';
      DeviceType:='Screen';
      ShowPrintDialog:=False;

//      fn_RegistrodeCambio(CUsuario.Usuario,'Reportes','Imprimiendo Cuadre de Caja.: '+cbbCaja.Text);
      Print;
    end;
  end;
end;

procedure TfrmReportes_NC.btnSalirClick(Sender: TObject);
begin
  Self.Close;
end;

procedure TfrmReportes_NC.cbbVendedorPropertiesChange(Sender: TObject);
begin
  with cbbUsuario.Properties.DataController do
  begin
    var_UsuarioID:=Values[FocusedRecordIndex,1];
  end;
end;

procedure TfrmReportes_NC.FormShow(Sender: TObject);
begin
  dtmpckrDesde.Date:=Now;
  dtmpckrHasta.Date:=Now;
  qryUsuarios.Open;
end;

end.
