unit UntDataReportes;

interface

uses
  System.SysUtils, System.Classes, ppCtrls, ppDB, ppBands, ppPrnabl, ppClass,
  ppCache, Data.DB, Data.Win.ADODB, ppParameter, ppDesignLayer, ppProd,
  ppReport, ppComm, ppRelatv, ppDBPipe, ppStrtch, ppRichTx, ppVar, ppBarCod;

type
  TDatReportes = class(TDataModule)
    pdbplnCuadreDeCaja: TppDBPipeline;
    dsCuadre_De_Caja: TDataSource;
    prprtReport: TppReport;
    spRPTCuadreDeCaja: TADOStoredProc;
    strngfldRPTCuadreDeCajaCAJA: TStringField;
    QryArticuloRPTCuadreDeCajaAPERTURA: TFMTBCDField;
    QryArticuloRPTCuadreDeCajaEFECTIVO: TFMTBCDField;
    QryArticuloRPTCuadreDeCajaTARJETA: TFMTBCDField;
    QryArticuloRPTCuadreDeCajaCHEQUE: TFMTBCDField;
    QryArticuloRPTCuadreDeCajaNCREDITO: TFMTBCDField;
    QryArticuloRPTCuadreDeCajaTCAJA: TFMTBCDField;
    prmtrlst1: TppParameterList;
    strngfldRPTCuadreDeCajaESTADODECAJA: TStringField;
    bcdfldDatReportesMONTODECIERRE: TBCDField;
    QryArticuloRPTCuadreDeCajaDIFERENCIA: TFMTBCDField;
    sp_RPT_NC_Conciliadas: TADOStoredProc;
    dsRPT_NC_Conciliadas: TDataSource;
    dtmfld_RPT_NC_ConciliadasFecha: TDateTimeField;
    intgrfld_RPT_NC_ConciliadasNumeroReq: TIntegerField;
    intgrfld_RPT_NC_ConciliadasNumeroNC: TIntegerField;
    wdstrngfld_RPT_NC_ConciliadasArticuloID: TWideStringField;
    wdstrngfld_RPT_NC_ConciliadasArticuloDescripcion: TWideStringField;
    wdstrngfld_RPT_NC_ConciliadasSerie: TWideStringField;
    wdstrngfld_RPT_NC_ConciliadasRazon: TWideStringField;
    wdstrngfld_RPT_NC_ConciliadasNombre: TWideStringField;
    dtmfld_RPT_NC_ConciliadasFecha_Conciliacion: TDateTimeField;
    pdbplnNC_Conciliadas: TppDBPipeline;
    ptlbnd1: TppTitleBand;
    plbl1: TppLabel;
    plblfecha: TppLabel;
    pln1: TppLine;
    phdrbnd1: TppHeaderBand;
    plbl3: TppLabel;
    pdbtxtCAJA: TppDBText;
    plbl2: TppLabel;
    plblEstadodeCaja: TppLabel;
    plbl13: TppLabel;
    pdtlbnd1: TppDetailBand;
    plbl7: TppLabel;
    pdbtxtEFECTIVO: TppDBText;
    plbl8: TppLabel;
    pdbtxtAPERTURA: TppDBText;
    plbl6: TppLabel;
    pdbtxtTARJETA: TppDBText;
    plbl9: TppLabel;
    pdbtxtNCREDITO: TppDBText;
    plbl10: TppLabel;
    pdbtxtTCAJA: TppDBText;
    plbl4: TppLabel;
    pdbtxtCHEQUE: TppDBText;
    plbl5: TppLabel;
    pln2: TppLine;
    prchtxt1: TppRichText;
    prchtxt2: TppRichText;
    plbl15: TppLabel;
    plbl11: TppLabel;
    pdbtxtDIFERENCIA: TppDBText;
    pdbtxtTCIERRE: TppDBText;
    pftrbnd1: TppFooterBand;
    psystmvrbl1: TppSystemVariable;
    psystmvrbl2: TppSystemVariable;
    pdsgnlyrs1: TppDesignLayers;
    pdsgnlyr1: TppDesignLayer;
    RptNC_Conciliadas: TppReport;
    prmtrlst2: TppParameterList;
    phdrbnd2: TppHeaderBand;
    plbl14: TppLabel;
    plbl16: TppLabel;
    plbl17: TppLabel;
    plbl18: TppLabel;
    plbl19: TppLabel;
    plbl20: TppLabel;
    plbl21: TppLabel;
    plbl12: TppLabel;
    pdtlbnd2: TppDetailBand;
    pdbtxtFecha: TppDBText;
    pdbtxtNumeroReq: TppDBText;
    pdbtxtArticuloID: TppDBText;
    pdbtxtArticuloDescripcion3: TppDBText;
    pdbtxtSerie1: TppDBText;
    pdbtxtRazon: TppDBText;
    pftrbnd2: TppFooterBand;
    plbl25: TppLabel;
    pdbtxtNombre: TppDBText;
    pdsgnlyrs2: TppDesignLayers;
    pdsgnlyr2: TppDesignLayer;
    RptEtiqueta: TppReport;
    phdrbnd3: TppHeaderBand;
    pdtlbnd3: TppDetailBand;
    pdbrcdCodigoBarras: TppDBBarCode;
    pshp1: TppShape;
    plbl22: TppLabel;
    plbl23: TppLabel;
    pdbtxtPrecio: TppDBText;
    pdbtxtDescripcionEtiqueta: TppDBText;
    pftrbnd3: TppFooterBand;
    pdsgnlyrs3: TppDesignLayers;
    pdsgnlyr3: TppDesignLayer;
    prmtrlst3: TppParameterList;
    pdbplnEtiqueta: TppDBPipeline;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  DatReportes: TDatReportes;

implementation

uses
  UntData, UntEtiquetas;

{%CLASSGROUP 'System.Classes.TPersistent'}

{$R *.dfm}

end.
