unit UntArticulos;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, dxSkinsCore, dxSkinsDefaultPainters,
  dxSkinscxPCPainter, cxPCdxBarPopupMenu, cxStyles, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxEdit, Data.DB, cxDBData, cxCheckBox, Vcl.ExtCtrls,
  Vcl.DBCtrls, cxVGrid, cxDBVGrid, cxInplaceContainer, cxGridLevel,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxClasses,
  cxGridCustomView, cxGrid, cxPC, Data.Win.ADODB, cxNavigator,
  Datasnap.Provider, Datasnap.DBClient, JvDBControls, Vcl.Menus, Vcl.StdCtrls,
  cxButtons, JvMenus, Vcl.ToolWin, Vcl.ActnMan, Vcl.ActnCtrls, Vcl.Ribbon,
  Vcl.RibbonLunaStyleActnCtrls, Vcl.ComCtrls, Vcl.ActnList, JvDataSource,
  cxBlobEdit, cxContainer, cxMaskEdit, cxDBEdit, cxTextEdit, cxLabel,
  dxGDIPlusClasses, dxSkinBlack, dxSkinBlue, dxSkinBlueprint, dxSkinCaramel,
  dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinDevExpressDarkStyle,
  dxSkinDevExpressStyle, dxSkinFoggy, dxSkinGlassOceans, dxSkinHighContrast,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black, dxSkinOffice2007Blue,
  dxSkinOffice2007Green, dxSkinOffice2007Pink, dxSkinOffice2007Silver,
  dxSkinOffice2010Black, dxSkinOffice2010Blue, dxSkinOffice2010Silver,
  dxSkinPumpkin, dxSkinSeven, dxSkinSevenClassic, dxSkinSharp, dxSkinSharpPlus,
  dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinTheAsphaltWorld, dxSkinValentine, dxSkinVS2010, dxSkinWhiteprint,
  dxSkinXmas2008Blue;

type
  TfrmArticulos = class(TForm)
    cxpgcntrl1: TcxPageControl;
    cxtbsht1: TcxTabSheet;
    cxgrd1: TcxGrid;
    cxgrdbtblvwGrid1DBTableView1: TcxGridDBTableView;
    cxgrdlvlGrid1Level1: TcxGridLevel;
    dsetArticulos: TClientDataSet;
    dtstprvdrArticulos: TDataSetProvider;
    cxstylrpstry1: TcxStyleRepository;
    cxstyl1: TcxStyle;
    cxtbsht2: TcxTabSheet;
    jvpmn1: TJvPopupMenu;
    mniAgregar1: TMenuItem;
    mniModificar1: TMenuItem;
    mniEliminar1: TMenuItem;
    tlb1: TToolBar;
    btnRegistro: TToolButton;
    btnAnterior: TToolButton;
    btnSiguiente: TToolButton;
    btnGuardar: TToolButton;
    btnCancelar: TToolButton;
    btn2: TToolButton;
    btnSalir: TToolButton;
    ds1: TJvDataSource;
    cxstyl2: TcxStyle;
    dsArticulos: TDataSource;
    GridGrid1DBTableView1CodigoArt: TcxGridDBColumn;
    GridGrid1DBTableView1DescripcionArt: TcxGridDBColumn;
    GridGrid1DBTableView1GrupoArt: TcxGridDBColumn;
    GridGrid1DBTableView1CodigoBarras: TcxGridDBColumn;
    GridGrid1DBTableView1EnMano: TcxGridDBColumn;
    cxdbtxtdt1: TcxDBTextEdit;
    cxdbtxtdt2: TcxDBTextEdit;
    cxdbtxtdt3: TcxDBTextEdit;
    cxdbtxtdt4: TcxDBTextEdit;
    cxdbmskdt1: TcxDBMaskEdit;
    cxlbl1: TcxLabel;
    cxlbl2: TcxLabel;
    cxlbl3: TcxLabel;
    cxlbl4: TcxLabel;
    cxlbl5: TcxLabel;
    cxdbtxtdt5: TcxDBTextEdit;
    cxdbmskdt2: TcxDBMaskEdit;
    cxlbl6: TcxLabel;
    cxlbl7: TcxLabel;
    grp1: TGroupBox;
    cxdbchckbx6: TcxDBCheckBox;
    cxdbchckbx7: TcxDBCheckBox;
    cxdbchckbx8: TcxDBCheckBox;
    cxdbchckbx9: TcxDBCheckBox;
    cxdbchckbx10: TcxDBCheckBox;
    mniBuscar1: TMenuItem;
    tblArticulos: TADOTable;
    atncfldArticulospIdRegistro: TAutoIncField;
    wdstrngfldArticulosCodigoArt: TWideStringField;
    wdstrngfldArticulosDescripcionArt: TWideStringField;
    intgrfldArticulosGrupoArt: TIntegerField;
    wdstrngfldArticulosCodigoBarras: TWideStringField;
    bcdfldArticulosEnMano: TBCDField;
    strngfldArticulosCompraArt: TStringField;
    strngfldArticulosVentaArt: TStringField;
    strngfldArticulosInventarioArt: TStringField;
    strngfldArticulosITBIS: TStringField;
    wdstrngfldArticulosManejaSeriales: TWideStringField;
    wdstrngfldArticulosDescripcionEtiqueta: TWideStringField;
    intgrfldArticulosDocumento_Actualizacion: TIntegerField;
    QryArticuloArticulosPrecio: TFMTBCDField;
    intgrfldArticulosDoc_actualizacion: TIntegerField;
    wdstrngfldArticulosCodigo_Almacen: TWideStringField;
    dtmfldArticulosFechaActualizacion: TDateTimeField;
    dtmfldArticulosFechaModificacion: TDateTimeField;
    wdstrngfldArticulosUsuario: TWideStringField;
    mniN1: TMenuItem;
    mniN2: TMenuItem;
    mniN3: TMenuItem;
    cxdbchckbxAceptaPagoTarjetaCredito: TcxDBCheckBox;
    strngfldArticulosAcepta_Pago_TarjetaCredito: TStringField;
    ColAlmacen: TcxGridDBColumn;
    atncfldArticulospIdRegistro1: TAutoIncField;
    wdstrngfldArticulosCodigoArt1: TWideStringField;
    wdstrngfldArticulosDescripcionArt1: TWideStringField;
    intgrfldArticulosGrupoArt1: TIntegerField;
    wdstrngfldArticulosCodigoBarras1: TWideStringField;
    bcdfldArticulosEnMano1: TBCDField;
    strngfldArticulosCompraArt1: TStringField;
    strngfldArticulosVentaArt1: TStringField;
    strngfldArticulosInventarioArt1: TStringField;
    strngfldArticulosITBIS1: TStringField;
    wdstrngfldArticulosManejaSeriales1: TWideStringField;
    wdstrngfldArticulosDescripcionEtiqueta1: TWideStringField;
    intgrfldArticulosDocumento_Actualizacion1: TIntegerField;
    fmtbcdfldArticulosPrecio: TFMTBCDField;
    intgrfldArticulosDoc_actualizacion1: TIntegerField;
    wdstrngfldArticulosCodigo_Almacen1: TWideStringField;
    dtmfldArticulosFechaActualizacion1: TDateTimeField;
    dtmfldArticulosFechaModificacion1: TDateTimeField;
    wdstrngfldArticulosUsuario1: TWideStringField;
    tblArticulosAcepta_Pago_TarjetaCredito: TStringField;
    procedure FormCreate(Sender: TObject);
    procedure btn4Click(Sender: TObject);
    procedure btnAnteriorClick(Sender: TObject);
    procedure btnSiguienteClick(Sender: TObject);
    procedure btnGuardarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure mniAgregar1Click(Sender: TObject);
    procedure mniModificar1Click(Sender: TObject);
    procedure mniEliminar1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Botones;
    procedure btnCancelarClick(Sender: TObject);
    procedure dsetArticulosPostError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure mniBuscar1Click(Sender: TObject);
    procedure btnSalirClick(Sender: TObject);
    procedure qryArticulosPostError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure dtstprvdrArticulosUpdateError(Sender: TObject;
      DataSet: TCustomClientDataSet; E: EUpdateError; UpdateKind: TUpdateKind;
      var Response: TResolverResponse);
    procedure dtstprvdrArticulosUpdateData(Sender: TObject;
      DataSet: TCustomClientDataSet);
    procedure cxgrdbtblvwGrid1DBTableView1CellDblClick(
      Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
  private
    { Private declarations }
    var_Posteado : Boolean;
  public
    { Public declarations }
  end;

var
  frmArticulos: TfrmArticulos;
  var_Posteado : Boolean;

implementation

{$R *.dfm}

uses UntData, UntMain;

procedure TfrmArticulos.btn4Click(Sender: TObject);
begin
  dsetArticulos.ApplyUpdates(0);
end;

procedure TfrmArticulos.btnAnteriorClick(Sender: TObject);
begin
  ds1.DataSet.Prior;
end;

procedure TfrmArticulos.btnCancelarClick(Sender: TObject);
begin
  var_Posteado:=True;
  dsetArticulos.CancelUpdates;
  Botones;
end;

procedure TfrmArticulos.btnGuardarClick(Sender: TObject);
begin
  var_Posteado:=True;
  dsetArticulos.ApplyUpdates(1);
  Botones;
end;

procedure TfrmArticulos.btnSalirClick(Sender: TObject);
begin
  Self.Close;
end;

procedure TfrmArticulos.btnSiguienteClick(Sender: TObject);
begin
  ds1.DataSet.Next;
end;

procedure TfrmArticulos.cxgrdbtblvwGrid1DBTableView1CellDblClick(
  Sender: TcxCustomGridTableView; ACellViewInfo: TcxGridTableDataCellViewInfo;
  AButton: TMouseButton; AShift: TShiftState; var AHandled: Boolean);
begin
  cxpgcntrl1.activePageIndex:=1;
end;

procedure TfrmArticulos.dsetArticulosPostError(DataSet: TDataSet;
  E: EDatabaseError; var Action: TDataAction);
begin
  ShowMessage('Error');
end;

procedure TfrmArticulos.dtstprvdrArticulosUpdateData(Sender: TObject;
  DataSet: TCustomClientDataSet);
begin
  //MessageBox(Handle,'Registro actualizado Satisfactoriamente','Articulos',MB_ICONINFORMATION or MB_OK)
end;

procedure TfrmArticulos.dtstprvdrArticulosUpdateError(Sender: TObject;
  DataSet: TCustomClientDataSet; E: EUpdateError; UpdateKind: TUpdateKind;
  var Response: TResolverResponse);
begin
  ShowMessage(E.Message);
end;

procedure TfrmArticulos.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  if var_Posteado=False then
  begin
    if MessageBox(Handle,'Los Datos no han sido guardados, se perderan, est� Seguro de Salir?','Articulo',MB_ICONQUESTION or MB_YESNO) =IDYES then
    begin
      dsetArticulos.Cancel;
      Action:=caFree;
      frmArticulos:=nil;
    end
    else
    begin
      exit;
    end;
  end;
  Action:=caFree;
  frmArticulos:=nil;
end;

procedure TfrmArticulos.FormCreate(Sender: TObject);
begin
  dsetArticulos.Active;
end;

procedure TfrmArticulos.FormShow(Sender: TObject);
begin
  var_Posteado:=True;
  dsetArticulos.Open;
end;

procedure TfrmArticulos.mniAgregar1Click(Sender: TObject);
begin
  var_Posteado:=False;
  cxpgcntrl1.ActivePageIndex:=1;
  dsetArticulos.Append;
  Botones;
end;

procedure TfrmArticulos.mniBuscar1Click(Sender: TObject);
begin
  dsetArticulos.Filter:='DescripcionART like Bateria';
  dsetArticulos.Filtered:=True;
  dsetArticulos.FindFirst;
end;

procedure TfrmArticulos.mniEliminar1Click(Sender: TObject);
begin
  if dsetArticulos.FieldByName('EnMano').Value>0 then begin
    MessageBox(Handle,'ERROR, Articulo no puede ser eliminado, tiene existencia en Almac�n?','Articulo',MB_ICONERROR or MB_OK);
    Exit;
  end;
  if MessageBox(Handle,'Seguro desear ELIMINAR el registro?','Articulo',MB_ICONQUESTION or MB_YESNO)=ID_YES then
  begin
    dsetArticulos.Delete;
    dsetArticulos.ApplyUpdates(1);
  end;
end;

procedure TfrmArticulos.mniModificar1Click(Sender: TObject);
begin
  var_Posteado:=False;
  cxpgcntrl1.ActivePageIndex:=1;
  dsetArticulos.Edit;
  Botones;
end;

procedure TfrmArticulos.qryArticulosPostError(DataSet: TDataSet;
  E: EDatabaseError; var Action: TDataAction);
begin
  ShowMessage('Error');
end;

procedure TfrmArticulos.Botones;
begin
  if var_Posteado=False then
  begin
    btnRegistro.Visible:=False;
    btnAnterior.Visible:=False;
    btnSiguiente.Visible:=False;
    btnSalir.Visible:=False;
    btnCancelar.Visible:=True;
    btnGuardar.Visible:=True;
  end
  else
  begin
    btnRegistro.Visible:=True;
    btnAnterior.Visible:=True;
    btnSiguiente.Visible:=True;
    btnSalir.Visible:=True;
    btnCancelar.Visible:=False;
    btnGuardar.Visible:=False;
  end;
end;
end.
