object frmVendedores: TfrmVendedores
  Left = 0
  Top = 0
  Width = 986
  Height = 656
  Align = alClient
  AutoScroll = True
  BorderIcons = [biSystemMenu, biMinimize, biMaximize, biHelp]
  Caption = 'VENDEDORES'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poDesigned
  Visible = True
  WindowState = wsMaximized
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object cxPageControl1: TcxPageControl
    Left = 0
    Top = 65
    Width = 968
    Height = 544
    Align = alClient
    TabOrder = 1
    Properties.ActivePage = cxTabSheet1
    ClientRectBottom = 538
    ClientRectLeft = 3
    ClientRectRight = 962
    ClientRectTop = 26
    object cxTabSheet1: TcxTabSheet
      Caption = 'Listado'
      ImageIndex = 0
      object cxGrid1: TcxGrid
        Left = 0
        Top = 0
        Width = 986
        Height = 509
        Align = alCustom
        Anchors = [akLeft, akTop, akBottom]
        TabOrder = 0
        object cxGrid1DBTableView1: TcxGridDBTableView
          OnCellDblClick = cxGrid1DBTableView1CellDblClick
          DataController.DataSource = dsVendedores
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          OptionsSelection.MultiSelect = True
          Styles.Header = cxStyle1
          object cxGrid1DBTableView1Id: TcxGridDBColumn
            DataBinding.FieldName = 'Id'
            HeaderAlignmentHorz = taCenter
            Styles.Header = cxStyle1
            Width = 68
          end
          object cxGrid1DBTableView1CodVendedor: TcxGridDBColumn
            DataBinding.FieldName = 'CodVendedor'
            HeaderAlignmentHorz = taCenter
            Styles.Header = cxStyle1
            Width = 114
          end
          object cxGrid1DBTableView1Nombre: TcxGridDBColumn
            DataBinding.FieldName = 'Nombre'
            HeaderAlignmentHorz = taCenter
            Styles.Header = cxStyle1
            Width = 193
          end
          object cxGrid1DBTableView1Apellido: TcxGridDBColumn
            DataBinding.FieldName = 'Apellido'
            HeaderAlignmentHorz = taCenter
            Styles.Header = cxStyle1
            Width = 269
          end
        end
        object cxGrid1Level1: TcxGridLevel
          GridView = cxGrid1DBTableView1
        end
      end
    end
    object cxtbshtRegistro: TcxTabSheet
      Caption = 'Registro'
      ImageIndex = 1
      object lbl1: TLabel
        Left = 9
        Top = 8
        Width = 11
        Height = 13
        Caption = 'ID'
      end
      object lbl2: TLabel
        Left = 136
        Top = 8
        Width = 33
        Height = 13
        Caption = 'C'#243'digo'
      end
      object lbl3: TLabel
        Left = 11
        Top = 64
        Width = 37
        Height = 13
        Caption = 'Nombre'
      end
      object lbl4: TLabel
        Left = 231
        Top = 64
        Width = 37
        Height = 13
        Caption = 'Apellido'
      end
      object edt1: TcxDBTextEdit
        Left = 9
        Top = 24
        DataBinding.DataField = 'Id'
        DataBinding.DataSource = dsVendedores
        TabOrder = 0
        Width = 121
      end
      object edt2: TcxDBTextEdit
        Left = 136
        Top = 24
        DataBinding.DataField = 'CodVendedor'
        DataBinding.DataSource = dsVendedores
        TabOrder = 1
        Width = 121
      end
      object edt3: TcxDBTextEdit
        Left = 11
        Top = 80
        DataBinding.DataField = 'Nombre'
        DataBinding.DataSource = dsVendedores
        TabOrder = 2
        Width = 214
      end
      object edt4: TcxDBTextEdit
        Left = 231
        Top = 80
        DataBinding.DataField = 'Apellido'
        DataBinding.DataSource = dsVendedores
        TabOrder = 3
        Width = 214
      end
    end
  end
  object tlb3: TToolBar
    Left = 0
    Top = 0
    Width = 968
    Height = 65
    ButtonHeight = 52
    ButtonWidth = 51
    Caption = 'tlb1'
    Flat = False
    Images = frmMain.ilToolBar32
    ShowCaptions = True
    TabOrder = 0
    Transparent = False
    object btn10: TToolButton
      Left = 0
      Top = 0
      Action = dtstpr1
      ImageIndex = 3
    end
    object btnDataSetPrior1: TToolButton
      Left = 51
      Top = 0
      Action = dtstnxt1
      ImageIndex = 0
    end
    object btn14: TToolButton
      Left = 102
      Top = 0
      Width = 55
      Caption = 'btn5'
      ImageIndex = 5
      Style = tbsSeparator
    end
    object btnDataSetEdit1: TToolButton
      Left = 157
      Top = 0
      Action = act1
      ImageIndex = 9
    end
    object btn13: TToolButton
      Left = 208
      Top = 0
      Action = act3
      ImageIndex = 10
    end
    object btn12: TToolButton
      Left = 259
      Top = 0
      Action = act5
      ImageIndex = 8
    end
    object btn15: TToolButton
      Left = 310
      Top = 0
      Action = act4
      ImageIndex = 2
    end
    object btn11: TToolButton
      Left = 361
      Top = 0
      Hint = 'Cerrar'
      Action = act2
      ImageIndex = 5
    end
    object btn9: TToolButton
      Left = 412
      Top = 0
      Width = 55
      Caption = 'btn9'
      ImageIndex = 5
      Style = tbsSeparator
    end
    object btn16: TToolButton
      Left = 467
      Top = 0
      Action = wndwcls1
      Caption = 'C&errar'
      ImageIndex = 1
    end
  end
  object dsVendedores: TDataSource
    AutoEdit = False
    DataSet = tblVendedores
    Left = 112
    Top = 240
  end
  object cxStyleRepository1: TcxStyleRepository
    Left = 568
    Top = 16
    PixelsPerInch = 120
    object cxStyle1: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clSkyBlue
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
    end
  end
  object tblVendedores: TADOTable
    Connection = DatMain.con1
    CursorType = ctStatic
    TableName = 'vendedores'
    Left = 48
    Top = 240
  end
  object actmgr1: TActionManager
    Left = 656
    Top = 16
    StyleName = 'Platform Default'
    object dtstpr1: TDataSetPrior
      Category = 'dataset'
      Caption = '&Anterior'
      Hint = 'Prior'
      ImageIndex = 1
    end
    object dtstnxt1: TDataSetNext
      Category = 'dataset'
      Caption = '&Siguiente'
      Hint = 'Next'
      ImageIndex = 2
    end
    object act1: TDataSetEdit
      Category = 'dataset'
      Caption = '&Editar'
      Hint = 'Edit'
      ImageIndex = 6
    end
    object act3: TDataSetInsert
      Category = 'dataset'
      Caption = '&Agregar'
      Hint = 'Insert'
      ImageIndex = 4
    end
    object wndwcls1: TWindowClose
      Category = 'Window'
      Caption = 'C&lose'
      Enabled = False
      Hint = 'Close'
    end
    object act2: TDataSetCancel
      Category = 'Dataset'
      Caption = '&Cancelar'
      Hint = 'Cancel'
      ImageIndex = 8
    end
    object act4: TDataSetPost
      Category = 'Dataset'
      Caption = 'A&ceptar'
      Hint = 'Post'
      ImageIndex = 7
    end
    object act5: TDataSetDelete
      Category = 'Dataset'
      Caption = '&Eliminar'
      Hint = 'Delete'
      ImageIndex = 5
    end
  end
end
