program POS;





uses
  Vcl.Forms,
  UntMain in 'UntMain.pas' {frmMain},
  UntData in 'UntData.pas' {DatMain: TDataModule},
  UntFacturacion in 'UntFacturacion.pas' {frmFacturacion},
  UntConsultaArticulos in 'UntConsultaArticulos.pas' {frmConsultaArticulo},
  UntUsuarios in 'UntUsuarios.pas' {frmUsuarios},
  UntVendedores in 'UntVendedores.pas' {frmVendedores},
  UntCantidad in 'UntCantidad.pas' {frmCantidad},
  UntCustomLogin in 'UntCustomLogin.pas' {frmCustomLogin},
  UntClientes in 'UntClientes.pas' {frmClientes},
  UntSeriales in 'UntSeriales.pas' {frmSeriales},
  UntCajas in 'UntCajas.pas' {frmCajas},
  UntBuscar in 'UntBuscar.pas' {frmBuscar},
  UntAbrirCaja in 'UntAbrirCaja.pas' {frmAbrirCaja},
  UntCerrarCaja in 'UntCerrarCaja.pas' {frmCerrarCaja},
  Vcl.Themes,
  Vcl.Styles,
  untRegistrodeCambios in 'untRegistrodeCambios.pas' {frmRegistrodeCambios},
  UntStartUPLogin in 'UntStartUPLogin.pas' {frmStartUPLogin},
  UntPago in 'UntPago.pas' {frmPago},
  UntArticulos in 'UntArticulos.pas' {frmArticulos},
  UntConsultas in 'UntConsultas.pas' {frmConsultas},
  untAbout in 'untAbout.pas' {frmAbout},
  UntReporteVentas in 'UntReporteVentas.pas' {frmReporteVentas},
  UntComun in 'UntComun.pas' {frmComun},
  UntNotasdeCredito in 'UntNotasdeCredito.pas' {frmNotasdeCredito},
  UntConsultaSeriales in 'UntConsultaSeriales.pas' {frmConsultaSeriales},
  UntPreview in 'UntPreview.pas' {frmPreview1},
  UntConsultaFlujodeCaja in 'UntConsultaFlujodeCaja.pas' {frmConsultaFlujodeCaja},
  UntDataReportes in 'UntDataReportes.pas' {DatReportes: TDataModule},
  UntReporteCuadreDeCaja in 'UntReporteCuadreDeCaja.pas' {frmReporteCuadreDeCaja},
  UntTarjetaDescuento in 'UntTarjetaDescuento.pas' {frmTarjetaDescuento},
  UntGeneraNCFaFactura in 'UntGeneraNCFaFactura.pas' {frmGeneraNCFaFactura},
  UntRNC in 'UntRNC.pas' {frmRNC},
  UntSucursal in 'UntSucursal.pas' {frmSucursal},
  UntEstadodeCajas in 'UntEstadodeCajas.pas' {frmEstadodeCajas},
  UntConsultaArticulosTienda in 'UntConsultaArticulosTienda.pas' {frmConsultaArticulosTienda},
  UntCargarNCF in 'UntCargarNCF.pas' {frmCargarNCF},
  UntKardex in 'UntKardex.pas' {frmKardex},
  UntInventarioEntradas in 'UntInventarioEntradas.pas' {frmInventarioEntrada},
  UntConsultaVentasxVendedores in 'UntConsultaVentasxVendedores.pas' {frmConsultaVentasXVendedores},
  UntReporte_NC in 'UntReporte_NC.pas' {frmReportes_NC},
  UntEtiquetas in 'UntEtiquetas.pas' {frmEtiquetas},
  WebPosData in '..\..\webposinterface\WebPosData.pas';

{$R *.res}

begin
  Application.ProcessMessages;
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.Title := 'JMI POS';
  Application.CreateForm(TfrmMain, frmMain);
  Application.CreateForm(TfrmAbrirCaja, frmAbrirCaja);
  Application.CreateForm(TDatMain, DatMain);
  Application.CreateForm(TDatReportes, DatReportes);
  Application.CreateForm(TfrmPreview1, frmPreview1);
  Application.CreateForm(TfrmReportes_NC, frmReportes_NC);
  Application.Run;

end.
