unit UntUsuarios;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, dxSkinsCore, dxSkinsDefaultPainters,
  dxSkinscxPCPainter, cxPCdxBarPopupMenu, cxStyles, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxEdit, Data.DB, cxDBData, Vcl.ExtCtrls, Vcl.DBCtrls,
  dxBarDBNav, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  Data.Win.ADODB, cxGridLevel, cxClasses, cxGridCustomView, cxGrid, cxPC,
  cxVGrid, cxDBVGrid, cxInplaceContainer, Vcl.Menus, Vcl.StdCtrls, cxButtons,
  cxCheckBox, Vcl.Mask, JvDBDotNetControls, JvExMask, JvToolEdit, JvMaskEdit,
  JvDBControls, JvExControls, JvLabel, JvExExtCtrls, JvExtComponent, JvPanel,
  Vcl.ComCtrls, JvExComCtrls, JvDateTimePicker, JvDBDateTimePicker, Vcl.ToolWin,
  Datasnap.DBClient, Datasnap.Provider, JvMenus, cxContainer, cxTextEdit,
  cxMaskEdit, cxDropDownEdit, cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox,
  JvADOQuery, JvDataSource, cxDBEdit, Vcl.ActnMan, Vcl.ActnCtrls, Vcl.Ribbon,
  Vcl.RibbonLunaStyleActnCtrls, Vcl.ActnList, Vcl.DBActns,
  Vcl.PlatformDefaultStyleActnCtrls, Vcl.StdActns, cxMemo, cxListBox,
  cxSpinEdit, cxTimeEdit, cxCalendar;

type
  TfrmUsuarios = class(TForm)
    cxStyleRepository1: TcxStyleRepository;
    cxStyle1: TcxStyle;
    cxstyl1: TcxStyle;
    tblUsuariosGrupos: TADOTable;
    dsUsuariosGrupos: TJvDataSource;
    atncfldUsuariosGrupospIDReigstro: TAutoIncField;
    wdstrngfldUsuariosGruposNombre: TWideStringField;
    wdstrngfldUsuariosGruposDescripcion: TWideStringField;
    tlb3: TToolBar;
    btn10: TToolButton;
    btnDataSetPrior1: TToolButton;
    btnDataSetEdit1: TToolButton;
    btn13: TToolButton;
    btn14: TToolButton;
    btn15: TToolButton;
    btn11: TToolButton;
    btn12: TToolButton;
    btn16: TToolButton;
    btn9: TToolButton;
    cxpgcntrl1: TcxPageControl;
    tbsnUsuarios: TcxTabSheet;
    tbsnGrupos: TcxTabSheet;
    tbsnPrivilegios: TcxTabSheet;
    cxpgcntrl3: TcxPageControl;
    tbsn7: TcxTabSheet;
    cxgrd3: TcxGrid;
    cxgrdbtblvw2: TcxGridDBTableView;
    cxgrdlvl2: TcxGridLevel;
    tbsn8: TcxTabSheet;
    jvpnl2: TJvPanel;
    jvlbl9: TJvLabel;
    jvlbl10: TJvLabel;
    jvlbl11: TJvLabel;
    jvlbl12: TJvLabel;
    jvlbl13: TJvLabel;
    jvlbl14: TJvLabel;
    jvlbl15: TJvLabel;
    jvlbl16: TJvLabel;
    cbb1: TcxDBLookupComboBox;
    cbb3: TcxDBLookupComboBox;
    cbb4: TcxDBLookupComboBox;
    edt1: TcxDBTextEdit;
    actmgr1: TActionManager;
    dtstpr1: TDataSetPrior;
    dtstnxt1: TDataSetNext;
    act1: TDataSetEdit;
    act3: TDataSetInsert;
    wndwcls1: TWindowClose;
    act2: TDataSetCancel;
    act4: TDataSetPost;
    act5: TDataSetDelete;
    cxpgcntrl2: TcxPageControl;
    tbsn1: TcxTabSheet;
    cxgrd1: TcxGrid;
    cxgrdbtblvw1: TcxGridDBTableView;
    cxgrdlvl1: TcxGridLevel;
    cxpgcntrl4: TcxPageControl;
    tbsn3: TcxTabSheet;
    cxgrd2: TcxGrid;
    cxgrdbtblvw3: TcxGridDBTableView;
    cxgrdlvl3: TcxGridLevel;
    Gridcxgrdbtblvw1Nombre: TcxGridDBColumn;
    Gridcxgrdbtblvw1Descripcion: TcxGridDBColumn;
    tblPrivilegios: TADOTable;
    dsPrivilegios: TJvDataSource;
    atncfldPrivilegiospIDRegistro: TAutoIncField;
    wdstrngfldPrivilegiosCodigo: TWideStringField;
    wdstrngfldPrivilegiosDescripcion: TWideStringField;
    Gridcxgrdbtblvw3Codigo: TcxGridDBColumn;
    Gridcxgrdbtblvw3Descripcion: TcxGridDBColumn;
    tblGrupos_Privilegios: TADOTable;
    dsGrupos_Privilegios: TJvDataSource;
    atncfldGrupos_PrivilegiospIDRegistro: TAutoIncField;
    intgrfldGrupos_PrivilegiosGrupoID: TIntegerField;
    intgrfldGrupos_PrivilegiosPrivilegioID: TIntegerField;
    strngfldGrupos_PrivilegiosGrupo: TStringField;
    strngfldGrupos_PrivilegiosPrivilegio: TStringField;
    tblUsuarios: TADOTable;
    edt3: TcxDBTextEdit;
    dsUsuarios: TJvDataSource;
    edt4: TcxDBTextEdit;
    Gridcxgrdbtblvw2Nombre: TcxGridDBColumn;
    Gridcxgrdbtblvw2Usuario: TcxGridDBColumn;
    Gridcxgrdbtblvw2Activo: TcxGridDBColumn;
    Gridcxgrdbtblvw2IdAlmacen: TcxGridDBColumn;
    Gridcxgrdbtblvw2GrupoID: TcxGridDBColumn;
    atncfldUsuariospIdRegistro: TAutoIncField;
    intgrfldUsuariosfkSucursal: TIntegerField;
    wdstrngfldUsuariosNombre: TWideStringField;
    wdstrngfldUsuariosApellido1: TWideStringField;
    wdstrngfldUsuariosApellido2: TWideStringField;
    wdstrngfldUsuariosUsuario: TWideStringField;
    blnfldUsuariosActivo: TBooleanField;
    wdstrngfldUsuariosClave: TWideStringField;
    dtmfldUsuariosFecha: TDateTimeField;
    blnfldUsuariosBloqueo: TBooleanField;
    wdstrngfldUsuariosDescripcionCaja: TWideStringField;
    wdstrngfldUsuariosIdAlmacen: TWideStringField;
    wdstrngfldUsuariosDescripcionAlmacen: TWideStringField;
    strngfldUsuariosConectado: TStringField;
    wdstrngfldUsuariosNombrePC: TWideStringField;
    intgrfldUsuariosGrupoID: TIntegerField;
    strngfldUsuariosGrupo: TStringField;
    strngfldUsuariosAlmacen: TStringField;
    strngfldUsuariosSucursal: TStringField;
    Gridcxgrdbtblvw2Almacen: TcxGridDBColumn;
    Gridcxgrdbtblvw2Sucursal: TcxGridDBColumn;
    qryWork1: TADOQuery;
    spActualizaGrupoPrivilegio: TADOStoredProc;
    pnlPrivilegios: TPanel;
    lbl2: TLabel;
    lbl3: TLabel;
    lstAsignados: TcxListBox;
    lstDisponibles: TcxListBox;
    edtID: TcxDBTextEdit;
    lbl1: TLabel;
    edtDescripcionCaja: TcxDBTextEdit;
    edtFecha: TcxDBDateEdit;
    chkActivo: TcxDBCheckBox;
    procedure btnSalirClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure mniEliminar1Click(Sender: TObject);
    procedure btnAnteriorClick(Sender: TObject);
    procedure btnSiguienteClick(Sender: TObject);
    procedure dsetUsuariosPostError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure dtstprvdrUsuariosUpdateError(Sender: TObject;
      DataSet: TCustomClientDataSet; E: EUpdateError; UpdateKind: TUpdateKind;
      var Response: TResolverResponse);
    procedure lstDisponiblesDblClick(Sender: TObject);
    procedure edtIDPropertiesChange(Sender: TObject);
    procedure CompletaAsignados;
    procedure CompletaNoAsignados;
    procedure lstAsignadosDblClick(Sender: TObject);
    procedure cxgrdbtblvw2CellDblClick(Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);

  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmUsuarios: TfrmUsuarios;
  var_Posteado : Boolean;

implementation

{$R *.dfm}

uses UntData;

procedure TfrmUsuarios.btnAnteriorClick(Sender: TObject);
begin
  dsUsuarios.DataSet.Prior;
end;

procedure TfrmUsuarios.btnSalirClick(Sender: TObject);
begin
  Self.Close;
end;

procedure TfrmUsuarios.btnSiguienteClick(Sender: TObject);
begin
  dsUsuarios.DataSet.Next;
end;

procedure TfrmUsuarios.dsetUsuariosPostError(DataSet: TDataSet;
  E: EDatabaseError; var Action: TDataAction);
begin
  ShowMessage(E.Message);
end;

procedure TfrmUsuarios.dtstprvdrUsuariosUpdateError(Sender: TObject;
  DataSet: TCustomClientDataSet; E: EUpdateError; UpdateKind: TUpdateKind;
  var Response: TResolverResponse);
begin
  MessageBox(Handle,'Error Actualizando','Usuarios',MB_ICONERROR or MB_OK);
  ShowMessage('Error Actualizando:'+E.Message);
end;

procedure TfrmUsuarios.edtIDPropertiesChange(Sender: TObject);
begin
  CompletaNoAsignados;
  CompletaAsignados;
  if Length(edtID.Text)=0 then  begin
    pnlPrivilegios.Visible:=False;
  end
  else
  begin
    pnlPrivilegios.Visible:=True;
  end;
end;

procedure TfrmUsuarios.FormClose(Sender: TObject; var Action: TCloseAction);
begin
if var_Posteado=False then
  begin
    if MessageBox(Handle,'Los Datos no han sido guardados, se perderan, est� Seguro de Salir?','Usuarios',MB_ICONQUESTION or MB_YESNO)=ID_YES then
    begin
      dsUsuarios.DataSet.Cancel;
      Action:=caFree;
      frmUsuarios:=nil;
    end
    else
    begin
      exit;
    end;
  end;
  Action:=caFree;
  frmUsuarios:=nil;
end;

procedure TfrmUsuarios.FormShow(Sender: TObject);
begin
  var_Posteado:=True;
  dsUsuarios.DataSet.Close;
  dsUsuarios.DataSet.Open;
  dsUsuariosGrupos.Open;
  dsPrivilegios.Open;
  dsGrupos_Privilegios.Open;
end;


procedure TfrmUsuarios.mniEliminar1Click(Sender: TObject);
var
  var_Usuario,var_UsuarioNombre : string;
begin
  var_usuario:=dsUsuarios.DataSet.FieldByName('Usuario').AsString;
  var_UsuarioNombre:=dsUsuarios.DataSet.FieldByName('Nombre').AsString;
  if MessageBox(Handle,'Desear Eliminar el usuario','Usuarios',MB_ICONQUESTION or MB_YESNO)=ID_YES then
  begin
    DatMain.qryWork1.SQL.Text:='UPDATE usuarios SET activo=0 WHERE Usuario='+QuotedStr(var_Usuario);
    DatMain.qryWork1.ExecSQL;
    dsUsuarios.DataSet.Refresh;
    fn_RegistrodeCambio(CUsuario.Usuario,'Usuarios Privilegios','Usuario Eliminado: '+var_UsuarioNombre);
  end;
end;

procedure TfrmUsuarios.lstAsignadosDblClick(Sender: TObject);
var
  var_item : Integer;
  var_item_descripcion : String;
  var_item_ID : string;

begin
  var_item:=(lstAsignados.ItemIndex);
  var_item_descripcion:=lstAsignados.Items[var_item];
  lstDisponibles.Items.Add(var_item_descripcion);

  with spActualizaGrupoPrivilegio do begin
    var_item_ID:=Copy(var_item_descripcion,1,8);
    Parameters.ParamByName('@Grupo_ID').Value:=edtID.Text;
    Parameters.ParamByName('@Privilegio_ID').Value:=var_item_ID;
    Parameters.ParamByName('@Action').Value:='D';
  end;

  spActualizaGrupoPrivilegio.ExecProc;
  lstAsignados.DeleteSelected;
end;


procedure TfrmUsuarios.lstDisponiblesDblClick(Sender: TObject);
var
  var_item : Integer;
  var_item_descripcion : String;
  var_item_ID : string;
begin
  var_item:=(lstDisponibles.ItemIndex);
  var_item_descripcion:=lstDisponibles.Items[var_item];
  lstAsignados.Items.Add(var_item_descripcion);

  with spActualizaGrupoPrivilegio do begin
    //ShowMessage(Parameters.ParamByName('@Usuario_ID').Value);
    var_item_ID:=Copy(var_item_descripcion,1,8);
    Parameters.ParamByName('@Grupo_ID').Value:=edtID.Text;
    Parameters.ParamByName('@Privilegio_ID').Value:=var_item_ID;
    Parameters.ParamByName('@Action').Value:='A';

  end;
    spActualizaGrupoPrivilegio.ExecProc;
    lstDisponibles.DeleteSelected;
end;

procedure TfrmUsuarios.CompletaAsignados;
begin
  lstAsignados.Clear;
  with qryWork1 do begin
    SQL.Text:='SELECT A.pIDRegistro,A.Codigo' +
              ' FROM Privilegios as A INNER JOIN '+
              ' [Grupos.Privilegios] AS B ON A.pIDRegistro = B.[Privilegio.ID]'+
              'WHERE [Grupo.ID]='+#39+edtID.Text+#39;
    Close;
    Open;
    First;
    while not Eof do begin
      lstAsignados.items.add(Format('%-8s',[FieldByName('pIDRegistro').AsString])+
      ' - ' + FieldByName('Codigo').AsString);
      Next;
    end;
  end;
//lst1.AddItem('test',Self);

end;


procedure TfrmUsuarios.CompletaNoAsignados;
var
  var_Privilegio_ID : String;
  var_Privilegio_Descripcion: String;

  begin
  lstDisponibles.Clear;
  with qryWork1 do begin
    SQL.Text:='SELECT A.pIDRegistro, A.Codigo FROM privilegios AS A WHERE A.pIDRegistro NOT IN '+
    '(SELECT [Privilegio.ID] FROM [grupos.privilegios] WHERE [Grupo.ID]='+#39+
    edtID.Text+#39+')';
    Close;
    Open;
    First;
    while not Eof do begin
      var_Privilegio_ID:=FieldByName('pIDRegistro').AsString;
      var_Privilegio_Descripcion:=FieldByName('Codigo').AsString;
      lstDisponibles.items.add(Format('%-8s',[FieldByName('pIDRegistro').AsString])+
      ' - ' + FieldByName('Codigo').AsString);
      Next;
    end;
  Close;
  end;
end;

procedure TfrmUsuarios.cxgrdbtblvw2CellDblClick(Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
    cxpgcntrl3.ActivePageIndex:=1;
end;

end.
