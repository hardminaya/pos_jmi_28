object DatMain: TDatMain
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Height = 522
  Width = 787
  object dxLayoutLookAndFeelList1: TdxLayoutLookAndFeelList
    Left = 488
    Top = 88
    object dxLayoutCxLookAndFeel1: TdxLayoutCxLookAndFeel
    end
    object dxLayoutWebLookAndFeel1: TdxLayoutWebLookAndFeel
    end
    object dxLayoutOfficeLookAndFeel1: TdxLayoutOfficeLookAndFeel
    end
  end
  object sp_CajaAutorizada: TADOStoredProc
    Connection = con1
    ProcedureName = 'sp_CajaAutorizada;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@NombrePC'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 90
        Value = 'asdf'
      end>
    Prepared = True
    Left = 48
    Top = 176
  end
  object ADOQuery1: TADOQuery
    Connection = con1
    Parameters = <>
    Left = 232
    Top = 264
  end
  object dsSucursal: TDataSource
    AutoEdit = False
    DataSet = qrySucursal
    Left = 304
    Top = 312
  end
  object qrySucursal: TADOQuery
    Connection = con1
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select top(1) * from sucursal')
    Left = 232
    Top = 312
    object intgrfldSucursalpkIdRegistro: TIntegerField
      FieldName = 'pkIdRegistro'
    end
    object wdstrngfldSucursalDescripcion: TWideStringField
      FieldName = 'Descripcion'
      Size = 50
    end
    object wdstrngfldSucursalDireccion: TWideStringField
      FieldName = 'Direccion'
      Size = 50
    end
    object wdstrngfldSucursalTelefono: TWideStringField
      FieldName = 'Telefono'
      Size = 15
    end
    object wdstrngfldSucursalFax: TWideStringField
      FieldName = 'Fax'
      Size = 15
    end
    object wdstrngfldSucursalRepresentante: TWideStringField
      FieldName = 'Representante'
    end
    object wdstrngfldSucursalRNC: TWideStringField
      FieldName = 'RNC'
    end
    object wdstrngfldSucursalUsuario: TWideStringField
      FieldName = 'Usuario'
    end
    object dtmfldSucursalFecha: TDateTimeField
      FieldName = 'Fecha'
    end
    object blnfldSucursalPrincipal: TBooleanField
      FieldName = 'Principal'
    end
    object wdstrngfldSucursalTienda_Principal: TWideStringField
      FieldName = 'Tienda_Principal'
      Size = 30
    end
    object wdstrngfldSucursalCodigo_Almacen: TWideStringField
      FieldName = 'Codigo_Almacen'
      Size = 8
    end
    object blnfldSucursalComprobante: TBooleanField
      FieldName = 'Comprobante'
    end
    object wdstrngfld1: TWideStringField
      FieldName = 'PrefijoTarjetaDescuento'
      Size = 50
    end
  end
  object sp_Caja: TADOStoredProc
    Connection = con1
    ProcedureName = 'sp_CAJA;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@UsuarioID'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@NombrePC'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 90
        Value = Null
      end
      item
        Name = '@Opcion'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 64
        Value = Null
      end
      item
        Name = '@Monto'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 6
        Precision = 18
        Value = Null
      end
      item
        Name = '@FechaApertura'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 10
        Value = Null
      end
      item
        Name = '@FechaCierre'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 10
        Value = Null
      end>
    Prepared = True
    Left = 48
    Top = 240
  end
  object qryTicket: TADOQuery
    Connection = con1
    CursorType = ctStatic
    Parameters = <
      item
        Name = '@pidregistro'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 15413
      end>
    Prepared = True
    SQL.Strings = (
      
        'select * from v_ImpresionTicket_new where pidregistro=:@pidregis' +
        'tro')
    Left = 488
    Top = 304
  end
  object dsTicket: TDataSource
    DataSet = qryTicket
    Left = 544
    Top = 304
  end
  object pdbplnTicket: TppDBPipeline
    DataSource = dsTicket
    AutoCreateFields = False
    UserName = 'pdbplnTicket'
    Left = 488
    Top = 208
    object pfldTicketppField1: TppField
      Alignment = taRightJustify
      FieldAlias = 'pIdRegistro'
      FieldName = 'pIdRegistro'
      FieldLength = 0
      DataType = dtInteger
      DisplayWidth = 10
      Position = 0
    end
    object pfldTicketppField2: TppField
      FieldAlias = 'ClienteId'
      FieldName = 'ClienteId'
      FieldLength = 20
      DisplayWidth = 20
      Position = 1
    end
    object pfldTicketppField3: TppField
      FieldAlias = 'ClienteNombre'
      FieldName = 'ClienteNombre'
      FieldLength = 100
      DisplayWidth = 100
      Position = 2
    end
    object pfldTicketppField4: TppField
      FieldAlias = 'ClienteRnc'
      FieldName = 'ClienteRnc'
      FieldLength = 20
      DisplayWidth = 20
      Position = 3
    end
    object pfldTicketppField5: TppField
      FieldAlias = 'DocumentoFecha'
      FieldName = 'DocumentoFecha'
      FieldLength = 0
      DataType = dtDateTime
      DisplayWidth = 18
      Position = 4
    end
    object pfldTicketppField6: TppField
      FieldAlias = 'Descripcion'
      FieldName = 'Descripcion'
      FieldLength = 50
      DisplayWidth = 50
      Position = 5
    end
    object pfldTicketppField7: TppField
      FieldAlias = 'Direccion'
      FieldName = 'Direccion'
      FieldLength = 50
      DisplayWidth = 50
      Position = 6
    end
    object pfldTicketppField8: TppField
      FieldAlias = 'Telefono'
      FieldName = 'Telefono'
      FieldLength = 15
      DisplayWidth = 15
      Position = 7
    end
    object pfldTicketppField9: TppField
      FieldAlias = 'RNC'
      FieldName = 'RNC'
      FieldLength = 20
      DisplayWidth = 20
      Position = 8
    end
    object pfldTicketppField10: TppField
      FieldAlias = 'ArticuloId'
      FieldName = 'ArticuloId'
      FieldLength = 20
      DisplayWidth = 20
      Position = 9
    end
    object pfldTicketppField11: TppField
      FieldAlias = 'ArticuloDescripcion'
      FieldName = 'ArticuloDescripcion'
      FieldLength = 100
      DisplayWidth = 100
      Position = 10
    end
    object pfldTicketppField12: TppField
      Alignment = taRightJustify
      FieldAlias = 'ArticuloPrecio'
      FieldName = 'ArticuloPrecio'
      FieldLength = 6
      DataType = dtDouble
      DisplayWidth = 20
      Position = 11
    end
    object pfldTicketppField13: TppField
      Alignment = taRightJustify
      FieldAlias = 'ArticuloCantidad'
      FieldName = 'ArticuloCantidad'
      FieldLength = 0
      DataType = dtDouble
      DisplayWidth = 19
      Position = 12
    end
    object pfldTicketppField14: TppField
      Alignment = taRightJustify
      FieldAlias = 'PrecioRealArticulo'
      FieldName = 'PrecioRealArticulo'
      FieldLength = 2
      DataType = dtDouble
      DisplayWidth = 20
      Position = 13
    end
    object pfldTicketppField15: TppField
      Alignment = taRightJustify
      FieldAlias = 'ArticuloPorcientoImpuesto'
      FieldName = 'ArticuloPorcientoImpuesto'
      FieldLength = 2
      DataType = dtDouble
      DisplayWidth = 19
      Position = 14
    end
    object pfldTicketppField16: TppField
      Alignment = taRightJustify
      FieldAlias = 'ArticuloMonto'
      FieldName = 'ArticuloMonto'
      FieldLength = 6
      DataType = dtDouble
      DisplayWidth = 20
      Position = 15
    end
    object pfldTicketppField17: TppField
      Alignment = taRightJustify
      FieldAlias = 'ArticuloPorcientoDesc'
      FieldName = 'ArticuloPorcientoDesc'
      FieldLength = 2
      DataType = dtDouble
      DisplayWidth = 19
      Position = 16
    end
    object pfldTicketppField18: TppField
      Alignment = taRightJustify
      FieldAlias = 'ArticuloImporte'
      FieldName = 'ArticuloImporte'
      FieldLength = 6
      DataType = dtDouble
      DisplayWidth = 20
      Position = 17
    end
    object pfldTicketppField19: TppField
      FieldAlias = 'NumeroNCF'
      FieldName = 'NumeroNCF'
      FieldLength = 20
      DisplayWidth = 20
      Position = 18
    end
    object pfldTicketppField20: TppField
      FieldAlias = 'NCFDescripcion'
      FieldName = 'NCFDescripcion'
      FieldLength = 50
      DisplayWidth = 50
      Position = 19
    end
    object pfldTicketppField21: TppField
      Alignment = taRightJustify
      FieldAlias = 'MontoEfectivo'
      FieldName = 'MontoEfectivo'
      FieldLength = 4
      DataType = dtDouble
      DisplayWidth = 20
      Position = 20
    end
    object pfldTicketppField22: TppField
      Alignment = taRightJustify
      FieldAlias = 'MontoDevuelta'
      FieldName = 'MontoDevuelta'
      FieldLength = 4
      DataType = dtDouble
      DisplayWidth = 20
      Position = 21
    end
    object pfldTicketppField23: TppField
      FieldAlias = 'Vendedor'
      FieldName = 'Vendedor'
      FieldLength = 101
      DisplayWidth = 101
      Position = 22
    end
    object pfldTicketppField24: TppField
      Alignment = taRightJustify
      FieldAlias = 'MontoGeneral'
      FieldName = 'MontoGeneral'
      FieldLength = 6
      DataType = dtDouble
      DisplayWidth = 20
      Position = 23
    end
    object pfldTicketppField25: TppField
      Alignment = taRightJustify
      FieldAlias = 'fkTipoNCF'
      FieldName = 'fkTipoNCF'
      FieldLength = 0
      DataType = dtInteger
      DisplayWidth = 10
      Position = 24
    end
    object pfldTicketppField26: TppField
      Alignment = taRightJustify
      FieldAlias = 'SubTotal'
      FieldName = 'SubTotal'
      FieldLength = 6
      DataType = dtDouble
      DisplayWidth = 20
      Position = 25
    end
    object pfldTicketppField27: TppField
      Alignment = taRightJustify
      FieldAlias = 'TotalDesc'
      FieldName = 'TotalDesc'
      FieldLength = 6
      DataType = dtDouble
      DisplayWidth = 20
      Position = 26
    end
    object pfldTicketppField28: TppField
      Alignment = taRightJustify
      FieldAlias = 'TotalItbis'
      FieldName = 'TotalItbis'
      FieldLength = 6
      DataType = dtDouble
      DisplayWidth = 20
      Position = 27
    end
    object pfldTicketppField29: TppField
      Alignment = taRightJustify
      FieldAlias = 'Efectivo'
      FieldName = 'Efectivo'
      FieldLength = 6
      DataType = dtDouble
      DisplayWidth = 20
      Position = 28
    end
    object pfldTicketppField30: TppField
      Alignment = taRightJustify
      FieldAlias = 'Cheque'
      FieldName = 'Cheque'
      FieldLength = 6
      DataType = dtDouble
      DisplayWidth = 20
      Position = 29
    end
    object pfldTicketppField31: TppField
      Alignment = taRightJustify
      FieldAlias = 'Tarjeta'
      FieldName = 'Tarjeta'
      FieldLength = 6
      DataType = dtDouble
      DisplayWidth = 20
      Position = 30
    end
    object pfldTicketppField32: TppField
      Alignment = taRightJustify
      FieldAlias = 'Ncredito'
      FieldName = 'Ncredito'
      FieldLength = 6
      DataType = dtDouble
      DisplayWidth = 20
      Position = 31
    end
    object pfldTicketppField33: TppField
      FieldAlias = 'NombrePC'
      FieldName = 'NombrePC'
      FieldLength = 90
      DisplayWidth = 90
      Position = 32
    end
    object pfldTicketppField34: TppField
      FieldAlias = 'Nombre'
      FieldName = 'Nombre'
      FieldLength = 50
      DisplayWidth = 50
      Position = 33
    end
    object pfldTicketppField35: TppField
      FieldAlias = 'Expr1'
      FieldName = 'Expr1'
      FieldLength = 100
      DisplayWidth = 100
      Position = 34
    end
    object pfldTicketppField36: TppField
      Alignment = taRightJustify
      FieldAlias = 'ArticuloTotalDescuento'
      FieldName = 'ArticuloTotalDescuento'
      FieldLength = 6
      DataType = dtDouble
      DisplayWidth = 20
      Position = 35
    end
    object pfldTicketppField37: TppField
      Alignment = taRightJustify
      FieldAlias = 'ArticuloPrecioImpuesto'
      FieldName = 'ArticuloPrecioImpuesto'
      FieldLength = 6
      DataType = dtDouble
      DisplayWidth = 20
      Position = 36
    end
    object pfldTicketppField38: TppField
      Alignment = taRightJustify
      FieldAlias = 'ArticuloTotalImpuesto'
      FieldName = 'ArticuloTotalImpuesto'
      FieldLength = 6
      DataType = dtDouble
      DisplayWidth = 20
      Position = 37
    end
    object pfldTicketppField39: TppField
      FieldAlias = 'Codigo_Almacen'
      FieldName = 'Codigo_Almacen'
      FieldLength = 8
      DisplayWidth = 8
      Position = 38
    end
    object pfldTicketppField40: TppField
      Alignment = taRightJustify
      FieldAlias = 'ArticuloPrecioDesc'
      FieldName = 'ArticuloPrecioDesc'
      FieldLength = 6
      DataType = dtDouble
      DisplayWidth = 20
      Position = 39
    end
    object pfldTicketppField41: TppField
      Alignment = taRightJustify
      FieldAlias = 'ArticuloImporteNeto'
      FieldName = 'ArticuloImporteNeto'
      FieldLength = 6
      DataType = dtDouble
      DisplayWidth = 20
      Position = 40
    end
    object pfldTicketppField42: TppField
      FieldAlias = 'Codigo_Vendedor'
      FieldName = 'Codigo_Vendedor'
      FieldLength = 10
      DisplayWidth = 10
      Position = 41
    end
    object pfldTicketppField43: TppField
      FieldAlias = 'Serie'
      FieldName = 'Serie'
      FieldLength = 32
      DisplayWidth = 32
      Position = 42
    end
    object pfldTicketppField44: TppField
      FieldAlias = 'Usuario'
      FieldName = 'Usuario'
      FieldLength = 10
      DisplayWidth = 10
      Position = 43
    end
    object pfldTicketppField45: TppField
      FieldAlias = 'ManejaSeriales'
      FieldName = 'ManejaSeriales'
      FieldLength = 1
      DisplayWidth = 1
      Position = 44
    end
    object pfldTicketppField46: TppField
      Alignment = taRightJustify
      FieldAlias = 'NCFControl'
      FieldName = 'NCFControl'
      FieldLength = 0
      DataType = dtInteger
      DisplayWidth = 10
      Position = 45
    end
    object pfldTicketppField47: TppField
      Alignment = taRightJustify
      FieldAlias = 'No_Linea'
      FieldName = 'No_Linea'
      FieldLength = 0
      DataType = dtInteger
      DisplayWidth = 10
      Position = 46
    end
    object pfldTicketppField48: TppField
      Alignment = taRightJustify
      FieldAlias = 'Secuencia'
      FieldName = 'Secuencia'
      FieldLength = 0
      DataType = dtInteger
      DisplayWidth = 10
      Position = 47
    end
    object pfldTicketppField49: TppField
      Alignment = taRightJustify
      FieldAlias = 'RegistroDetalle'
      FieldName = 'RegistroDetalle'
      FieldLength = 0
      DataType = dtInteger
      DisplayWidth = 10
      Position = 48
    end
    object pfldTicketppField50: TppField
      FieldAlias = 'Comprobante'
      FieldName = 'Comprobante'
      FieldLength = 0
      DataType = dtBoolean
      DisplayWidth = 5
      Position = 49
    end
    object pfldTicketppField51: TppField
      FieldAlias = 'Precio'
      FieldName = 'Precio'
      FieldLength = 10
      DisplayWidth = 10
      Position = 50
    end
  end
  object prprtTicket: TppReport
    AutoStop = False
    DataPipeline = pdbplnTicket
    PrinterSetup.BinName = 'Default'
    PrinterSetup.DocumentName = 'Report'
    PrinterSetup.Duplex = dpVertical
    PrinterSetup.PaperName = 'Custom'
    PrinterSetup.PrinterName = 'Default'
    PrinterSetup.SaveDeviceSettings = False
    PrinterSetup.mmMarginBottom = 0
    PrinterSetup.mmMarginLeft = 0
    PrinterSetup.mmMarginRight = 0
    PrinterSetup.mmMarginTop = 0
    PrinterSetup.mmPaperHeight = 279401
    PrinterSetup.mmPaperWidth = 62992
    PrinterSetup.PaperSize = 130
    Template.DatabaseSettings.DataPipeline = pdbplnReportes
    Template.DatabaseSettings.NameField = 'Name'
    Template.DatabaseSettings.TemplateField = 'Template'
    Template.FileName = 'C:\Users\132369\Desktop\CAMBIOS\Ticket.rtm'
    Template.SaveTo = stDatabase
    Template.Format = ftASCII
    ArchiveFileName = '($MyDocuments)\ReportArchive.raf'
    BeforePrint = prprtTicketBeforePrint
    DeviceType = 'Screen'
    DefaultFileDeviceType = 'PDF'
    EmailSettings.ReportFormat = 'PDF'
    LanguageID = 'Default'
    OpenFile = False
    OutlineSettings.CreateNode = True
    OutlineSettings.CreatePageNodes = True
    OutlineSettings.Enabled = True
    OutlineSettings.Visible = True
    ThumbnailSettings.Enabled = True
    ThumbnailSettings.Visible = True
    ThumbnailSettings.DeadSpace = 30
    PDFSettings.EmbedFontOptions = [efUseSubset]
    PDFSettings.EncryptSettings.AllowCopy = True
    PDFSettings.EncryptSettings.AllowInteract = True
    PDFSettings.EncryptSettings.AllowModify = True
    PDFSettings.EncryptSettings.AllowPrint = True
    PDFSettings.EncryptSettings.Enabled = False
    PDFSettings.EncryptSettings.KeyLength = kl40Bit
    PDFSettings.FontEncoding = feAnsi
    PDFSettings.ImageCompressionLevel = 25
    PreviewFormSettings.PageDisplay = pdContinuous
    PreviewFormSettings.WindowState = wsMaximized
    PreviewFormSettings.ZoomSetting = zs100Percent
    RTFSettings.DefaultFont.Charset = DEFAULT_CHARSET
    RTFSettings.DefaultFont.Color = clWindowText
    RTFSettings.DefaultFont.Height = -13
    RTFSettings.DefaultFont.Name = 'Arial'
    RTFSettings.DefaultFont.Style = []
    TextFileName = '($MyDocuments)\Report.pdf'
    TextSearchSettings.DefaultString = '<FindText>'
    TextSearchSettings.Enabled = True
    XLSSettings.AppName = 'ReportBuilder'
    XLSSettings.Author = 'ReportBuilder'
    XLSSettings.Subject = 'Report'
    XLSSettings.Title = 'Report'
    Left = 488
    Top = 256
    Version = '15.03'
    mmColumnWidth = 63500
    DataPipelineName = 'pdbplnTicket'
    object phdrbnd1: TppHeaderBand
      Background.Brush.Style = bsClear
      mmBottomOffset = 0
      mmHeight = 13229
      mmPrintPosition = 0
    end
    object pdtlbnd1: TppDetailBand
      Background1.Brush.Style = bsClear
      Background2.Brush.Style = bsClear
      mmBottomOffset = 0
      mmHeight = 13229
      mmPrintPosition = 0
    end
    object pftrbnd1: TppFooterBand
      Background.Brush.Style = bsClear
      mmBottomOffset = 0
      mmHeight = 13229
      mmPrintPosition = 0
    end
    object pdsgnlyrs1: TppDesignLayers
      object pdsgnlyr1: TppDesignLayer
        UserName = 'Foreground'
        LayerType = ltBanded
        Index = 0
      end
    end
    object prmtrlst1: TppParameterList
    end
  end
  object qryValidar_Privilegio: TADOQuery
    Connection = con1
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'Select * from vw_Usuarios_Privilegios where usuario='#39'cajero'#39)
    Left = 136
    Top = 224
  end
  object spRegistrodeCambios: TADOStoredProc
    Connection = con1
    ProcedureName = 'sp_RegistrodeCambios;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@Usuario'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 64
        Value = Null
      end
      item
        Name = '@Modulo'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 64
        Value = Null
      end
      item
        Name = '@Accion'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 255
        Value = Null
      end>
    Left = 48
    Top = 384
  end
  object conAppSecurity: TADOConnection
    ConnectionString = 
      'Provider=SQLOLEDB.1;Password=Josefina1;Persist Security Info=Tru' +
      'e;User ID=pos.jmi;Data Source=localhost\dev;Use Procedure for Pr' +
      'epare=1;Auto Translate=True;Packet Size=4096;Workstation ID=DOHA' +
      'PBT0N3G;Use Encryption for Data=False;Tag with column collation ' +
      'when possible=False'
    KeepConnection = False
    LoginPrompt = False
    Provider = 'SQLOLEDB.1'
    BeforeConnect = conAppSecurityBeforeConnect
    Left = 112
    Top = 40
  end
  object pdsgnr1: TppDesigner
    Caption = 'ReportBuilder'
    DataSettings.SessionType = 'BDESession'
    DataSettings.AllowEditSQL = False
    DataSettings.GuidCollationType = gcString
    DataSettings.IsCaseSensitive = True
    DataSettings.SQLType = sqBDELocal
    Icon.Data = {
      0000010001001010000001002000680400001600000028000000100000002000
      0000010020000000000000040000120B0000120B00000000000000000000FFFF
      FF000034000000320000002F0000002F0000002F000000240000000000FF014F
      68AB015F773D00350D00002F0000002F0000003200000034000000340000FFFF
      FF000034000000330098003200CB003200CB003200CB003200CB015351E5D9F4
      FFFF016D75F2004529D9003200CB003200CB003300980034000000340000FFFF
      FF000C0C030035350F76FCFCF2FFF8F8EEFFF8F8EEFFF8F8EEFFD3E5E2FF49B4
      C2FF79E6F7FF409DABFFB8D7D5FFFCFCF2FF013E01C9013E0100013E0100FFFF
      FF0000000066DDDDDDFFBBBBBBFF999999FFF1F1E8FFEED5CDFFEED5CDFFB5C8
      C8FF47AAB8FF79E6F7FF3E94A3FFB8D7D6FF024D02C5024D0200024D0200FFFF
      FF00000000006969416FF9F9F1FFF2F2EAFFF2F2EAFFF2F2EAFFF2F2EAFFF2F2
      EAFFB8DEDEFF48B2C1FF79E6F7FF409DACFF025A25D30931090001294F00FFFF
      FF0000000066DDDDDDFFBBBBBBFF999999FFF4F4ECFFF0D8D0FFF0D8D0FFF0D8
      D0FFF0D8D0FFB6CBCAFF47ABB9FF79E6F7FF153A15E11212122E00009C00FFFF
      FF00000000007979516DFAFAF3FFF5F5EEFFF5F5EEFFF5F5EEFFF5F5EEFFF5F5
      EEFFF5F5EEFFF5F5EEFFBAE0E1FFB0B0ACFFF7F7F7FF1A1A468E00009F46FFFF
      FF0000000066DDDDDDFFBBBBBBFF999999FFF7F7F0FFF2DCD4FFF2DCD4FFF2DC
      D4FFF2DCD4FFF2DCD4FFF2DCD4FFE3E3DEFF1D4646E08080FFFF0101B1C1FFFF
      FF00000000008888606BFCFCF7FFF8F8F3FFF8F8F3FFF8F8F3FFF8F8F3FFF8F8
      F3FFF8F8F3FFF8F8F3FFF8F8F3FFFCFCF7FF044D37CF0101DEAF0101C640FFFF
      FF0000000066DDDDDDFFBBBBBBFF999999FFF9F9F6FFF3DFD7FFF3DFD7FFF3DF
      D7FFF3DFD7FFF3DFD7FFF3DFD7FFFDFDF9FF026602C0021BA9000101C900FFFF
      FF000000000096966D69FDFDFAFFFBFBF8FFFBFBF8FFFBFBF8FFFBFBF8FFFBFB
      F8FFFBFBF8FFFBFBF8FFFBFBF8FFFDFDFAFF036903C00369030002366600FFFF
      FF0000000066DDDDDDFFBBBBBBFF999999FFFCFCFAFFF5E3DBFFF5E3DBFFF5E3
      DBFFF5E3DBFFF5E3DBFFF5E3DBFFFEFEFCFF036D03BF036D0300036D0300FFFF
      FF0000000000A1A17767FEFEFDFFFDFDFCFFFDFDFCFFFDFDFCFFFDFDFCFFFDFD
      FCFFFDFDFCFFFDFDFCFFFDFDFCFFFEFEFDFF036F03BF036F0300036F0300FFFF
      FF0000000066DDDDDDFFBBBBBBFF999999FFFEFEFEFFF6E5DDFFF6E5DDFFF6E5
      DDFFF6E5DDFFF6E5DDFFF6E5DDFFFFFFFEFF037203BE0372030003720300FFFF
      FF002B2B2000A9A97F66FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF037403BE0374030003740300FFFF
      FF00037503000376038E037603BD037603BD037603BD037603BD037603BD0376
      03BD037603BD037603BD037603BD037603BD0376038E0375030003750300FE3F
      0000C0030000C003000080030000C003000080010000C000000080000000C000
      000080030000C003000080030000C003000080030000C0030000C0030000}
    Position = poScreenCenter
    Report = prprtTicket
    IniStorageType = 'IniFile'
    IniStorageName = '($LocalAppData)\RBuilder\RBuilder.ini'
    WindowHeight = 400
    WindowLeft = 100
    WindowTop = 50
    WindowWidth = 600
    Left = 488
    Top = 152
  end
  object tblReportes: TADOTable
    Connection = con1
    CursorType = ctStatic
    TableName = 'Reportes'
    Left = 328
    Top = 416
  end
  object dsReportes: TJvDataSource
    DataSet = tblReportes
    Left = 400
    Top = 416
  end
  object pdbplnReportes: TppDBPipeline
    DataSource = dsReportes
    UserName = 'pdbplnReportes'
    Left = 472
    Top = 416
  end
  object qryRpt_Inventario: TADOQuery
    Connection = con1
    CursorType = ctStatic
    Parameters = <>
    Prepared = True
    SQL.Strings = (
      'select * from vw_Rpt_Inventario')
    Left = 624
    Top = 64
  end
  object pdbplnRpt_Inventario: TppDBPipeline
    DataSource = dsRpt_Inventario
    CloseDataSource = True
    UserName = 'Inventario'
    Left = 624
    Top = 184
  end
  object dsRpt_Inventario: TDataSource
    DataSet = qryRpt_Inventario
    Left = 624
    Top = 120
  end
  object prprtInventario: TppReport
    AutoStop = False
    PrinterSetup.BinName = 'Default'
    PrinterSetup.DocumentName = 'Report'
    PrinterSetup.Duplex = dpVertical
    PrinterSetup.PaperName = 'Letter'
    PrinterSetup.PrinterName = 'Default'
    PrinterSetup.SaveDeviceSettings = False
    PrinterSetup.mmMarginBottom = 6350
    PrinterSetup.mmMarginLeft = 6350
    PrinterSetup.mmMarginRight = 6350
    PrinterSetup.mmMarginTop = 6350
    PrinterSetup.mmPaperHeight = 279400
    PrinterSetup.mmPaperWidth = 215900
    PrinterSetup.PaperSize = 1
    Template.DatabaseSettings.DataPipeline = pdbplnReportes
    Template.DatabaseSettings.Name = 'Cuadre de Caja'
    Template.DatabaseSettings.NameField = 'Name'
    Template.DatabaseSettings.TemplateField = 'Template'
    Template.FileName = 'C:\Users\132369\Desktop\CAMBIOS\Cuadre de Caja Version_2.rtm'
    Template.SaveTo = stDatabase
    Template.Format = ftASCII
    ArchiveFileName = '($MyDocuments)\ReportArchive.raf'
    CachePages = True
    DeviceType = 'Screen'
    DefaultFileDeviceType = 'PDF'
    EmailSettings.ReportFormat = 'PDF'
    LanguageID = 'Default'
    OpenFile = False
    OutlineSettings.CreateNode = True
    OutlineSettings.CreatePageNodes = True
    OutlineSettings.Enabled = True
    OutlineSettings.Visible = True
    ThumbnailSettings.Enabled = True
    ThumbnailSettings.Visible = True
    ThumbnailSettings.DeadSpace = 30
    PDFSettings.EmbedFontOptions = [efUseSubset]
    PDFSettings.EncryptSettings.AllowCopy = True
    PDFSettings.EncryptSettings.AllowInteract = True
    PDFSettings.EncryptSettings.AllowModify = True
    PDFSettings.EncryptSettings.AllowPrint = True
    PDFSettings.EncryptSettings.Enabled = False
    PDFSettings.EncryptSettings.KeyLength = kl40Bit
    PDFSettings.FontEncoding = feAnsi
    PDFSettings.ImageCompressionLevel = 25
    PreviewFormSettings.WindowState = wsMaximized
    RTFSettings.DefaultFont.Charset = DEFAULT_CHARSET
    RTFSettings.DefaultFont.Color = clWindowText
    RTFSettings.DefaultFont.Height = -13
    RTFSettings.DefaultFont.Name = 'Arial'
    RTFSettings.DefaultFont.Style = []
    TextFileName = '($MyDocuments)\Report.pdf'
    TextSearchSettings.DefaultString = '<FindText>'
    TextSearchSettings.Enabled = True
    XLSSettings.AppName = 'ReportBuilder'
    XLSSettings.Author = 'ReportBuilder'
    XLSSettings.Subject = 'Report'
    XLSSettings.Title = 'Report'
    Left = 624
    Top = 248
    Version = '15.03'
    mmColumnWidth = 0
    object ptlbnd2: TppTitleBand
      Background.Brush.Style = bsClear
      mmBottomOffset = 0
      mmHeight = 23548
      mmPrintPosition = 0
      object plbl1: TppLabel
        UserName = 'plbl1'
        Caption = 'CUADRE DIARIO DE CAJA'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 14
        Font.Style = []
        TextAlignment = taCentered
        Transparent = True
        mmHeight = 5821
        mmLeft = 68225
        mmTop = 5556
        mmWidth = 71438
        BandType = 1
        LayerName = BandLayer4
      end
      object plbl11: TppLabel
        UserName = 'plblfecha'
        Caption = 'Fecha desde Hasta'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        Transparent = True
        mmHeight = 4233
        mmLeft = 6350
        mmTop = 13758
        mmWidth = 29898
        BandType = 1
        LayerName = BandLayer4
      end
      object pln3: TppLine
        UserName = 'pln1'
        Style = lsDouble
        Weight = 0.750000000000000000
        mmHeight = 3969
        mmLeft = 6350
        mmTop = 18785
        mmWidth = 190765
        BandType = 1
        LayerName = BandLayer4
      end
    end
    object phdrbnd2: TppHeaderBand
      Background.Brush.Style = bsClear
      mmBottomOffset = 0
      mmHeight = 14817
      mmPrintPosition = 0
      object plbl12: TppLabel
        UserName = 'plbl3'
        Caption = 'Informe Cuadre Caja.:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlue
        Font.Name = 'Arial'
        Font.Size = 12
        Font.Style = [fsBold, fsUnderline]
        Transparent = True
        mmHeight = 5027
        mmLeft = 6350
        mmTop = 1588
        mmWidth = 43921
        BandType = 0
        LayerName = BandLayer4
      end
      object pdbtxtCAJA1: TppDBText
        UserName = 'pdbtxtCAJA'
        DataField = 'CAJA'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 12
        Font.Style = [fsBold]
        Transparent = True
        mmHeight = 4763
        mmLeft = 52917
        mmTop = 1588
        mmWidth = 42069
        BandType = 0
        LayerName = BandLayer4
      end
      object plbl16: TppLabel
        UserName = 'plbl12'
        Caption = 'Responsable de Caja.:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlue
        Font.Name = 'Arial'
        Font.Size = 12
        Font.Style = [fsBold, fsUnderline]
        Transparent = True
        mmHeight = 5027
        mmLeft = 6350
        mmTop = 8202
        mmWidth = 45508
        BandType = 0
        LayerName = BandLayer4
      end
    end
    object pdtlbnd2: TppDetailBand
      Background1.Brush.Style = bsClear
      Background2.Brush.Style = bsClear
      PrintHeight = phDynamic
      mmBottomOffset = 0
      mmHeight = 207169
      mmPrintPosition = 0
      object plbl17: TppLabel
        UserName = 'plbl7'
        AutoSize = False
        Caption = 'Ventas Efectivo'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 12
        Font.Style = []
        Transparent = True
        mmHeight = 4763
        mmLeft = 47096
        mmTop = 11642
        mmWidth = 52388
        BandType = 4
        LayerName = BandLayer4
      end
      object pdbtxtEFECTIVO2: TppDBText
        UserName = 'pdbtxtEFECTIVO'
        DataField = 'EFECTIVO'
        DisplayFormat = '#,0.00;(#,0.00)'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 12
        Font.Style = []
        TextAlignment = taRightJustified
        Transparent = True
        mmHeight = 4763
        mmLeft = 159809
        mmTop = 11642
        mmWidth = 34396
        BandType = 4
        LayerName = BandLayer4
      end
      object plbl41: TppLabel
        UserName = 'plbl8'
        AutoSize = False
        Caption = 'Apertura de Caja'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 12
        Font.Style = []
        Transparent = True
        mmHeight = 4763
        mmLeft = 46831
        mmTop = 5292
        mmWidth = 52652
        BandType = 4
        LayerName = BandLayer4
      end
      object pdbtxtAPERTURA1: TppDBText
        UserName = 'pdbtxtAPERTURA'
        DataField = 'APERTURA'
        DisplayFormat = '#,0.00;(#,0.00)'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 12
        Font.Style = []
        TextAlignment = taRightJustified
        Transparent = True
        mmHeight = 4763
        mmLeft = 159809
        mmTop = 5292
        mmWidth = 34396
        BandType = 4
        LayerName = BandLayer4
      end
      object plbl42: TppLabel
        UserName = 'plbl9'
        AutoSize = False
        Caption = 'Ventas Cheque'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 12
        Font.Style = []
        Transparent = True
        mmHeight = 4763
        mmLeft = 46831
        mmTop = 17198
        mmWidth = 52388
        BandType = 4
        LayerName = BandLayer4
      end
      object pdbtxtTARJETA2: TppDBText
        UserName = 'pdbtxtTARJETA'
        DataField = 'TARJETA'
        DisplayFormat = '#,0.00;(#,0.00)'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 12
        Font.Style = []
        TextAlignment = taRightJustified
        Transparent = True
        mmHeight = 4763
        mmLeft = 159809
        mmTop = 23019
        mmWidth = 35454
        BandType = 4
        LayerName = BandLayer4
      end
      object plbl43: TppLabel
        UserName = 'plbl10'
        AutoSize = False
        Caption = 'Ventas Tarjeta'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 12
        Font.Style = []
        Transparent = True
        mmHeight = 4763
        mmLeft = 46831
        mmTop = 23019
        mmWidth = 52652
        BandType = 4
        LayerName = BandLayer4
      end
      object pdbtxtNCREDITO2: TppDBText
        UserName = 'pdbtxtNCREDITO'
        DataField = 'NCREDITO'
        DisplayFormat = '(#,0.00);(#,0.00)'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Name = 'Arial'
        Font.Size = 12
        Font.Style = []
        TextAlignment = taRightJustified
        Transparent = True
        mmHeight = 4763
        mmLeft = 159809
        mmTop = 30427
        mmWidth = 34131
        BandType = 4
        LayerName = BandLayer4
      end
      object plbl44: TppLabel
        UserName = 'plbl11'
        AutoSize = False
        Caption = 'Notas de Credito'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 12
        Font.Style = []
        Transparent = True
        mmHeight = 4763
        mmLeft = 46831
        mmTop = 30427
        mmWidth = 52652
        BandType = 4
        LayerName = BandLayer4
      end
      object pdbtxtTCAJA1: TppDBText
        UserName = 'pdbtxtTCAJA'
        Border.Color = clAqua
        Color = clRed
        DataField = 'TCAJA'
        DisplayFormat = '#,0.00;(#,0.00)'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 12
        Font.Style = [fsBold]
        TextAlignment = taRightJustified
        Transparent = True
        mmHeight = 4763
        mmLeft = 159809
        mmTop = 40217
        mmWidth = 34396
        BandType = 4
        LayerName = BandLayer4
      end
      object plbl45: TppLabel
        UserName = 'plbl4'
        AutoSize = False
        Caption = 'Total caja RD$ ---------->'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 12
        Font.Style = [fsBold]
        Transparent = True
        mmHeight = 4763
        mmLeft = 100806
        mmTop = 40217
        mmWidth = 51594
        BandType = 4
        LayerName = BandLayer4
      end
      object pdbtxtCHEQUE2: TppDBText
        UserName = 'pdbtxtCHEQUE'
        DataField = 'CHEQUE'
        DisplayFormat = '#,0.00;(#,0.00)'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 12
        Font.Style = []
        TextAlignment = taRightJustified
        Transparent = True
        mmHeight = 4763
        mmLeft = 159809
        mmTop = 17198
        mmWidth = 34396
        BandType = 4
        LayerName = BandLayer4
      end
      object plbl46: TppLabel
        UserName = 'plbl5'
        AutoSize = False
        Caption = 'Detalle Recibo de Caja'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 12
        Font.Style = []
        Transparent = True
        mmHeight = 4763
        mmLeft = 6350
        mmTop = 43921
        mmWidth = 51594
        BandType = 4
        LayerName = BandLayer4
      end
      object pln5: TppLine
        UserName = 'pln2'
        Style = lsDouble
        Weight = 0.750000000000000000
        mmHeight = 3969
        mmLeft = 5292
        mmTop = 49477
        mmWidth = 190765
        BandType = 4
        LayerName = BandLayer4
      end
      object prchtxt3: TppRichText
        UserName = 'prchtxt1'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 12
        Font.Style = []
        Caption = 'prchtxt1'
        RichText = 
          '{\rtf1\ansi\ansicpg1252\deff0\deflang1033{\fonttbl{\f0\fnil\fcha' +
          'rset0 Century Gothic;}{\f1\fnil\fcharset0 Arial;}{\f2\fnil Arial' +
          ';}}'#13#10'{\colortbl ;\red0\green0\blue0;}'#13#10'\viewkind4\uc1\pard\fi-56' +
          '0\li560\qj\cf1\f0\fs34 _________\tab de 2,000 \tab\tab RD$ = ___' +
          '_____\par'#13#10'\pard\qj _________ \tab de 1,000\tab\tab\tab RD$= ___' +
          '______\par'#13#10'_________\tab de    500\tab\tab\tab RD$= _________\p' +
          'ar'#13#10'_________\tab de    200\tab\tab\tab RD$= _________\par'#13#10'____' +
          '_____\tab de    100\tab\tab\tab RD$= _________\par'#13#10'_________\ta' +
          'b de      50 \tab\tab RD$= _________\par'#13#10'_________ \tab de     ' +
          ' 20 \tab\tab RD$= _________\par'#13#10'\tab\tab\tab Monedas\tab\tab RD' +
          '$= _________\par'#13#10'\tab\tab\par'#13#10'\tab\tab Total Efectivo \tab\tab' +
          ' RD$= _________\par'#13#10'\tab\tab Total Tarjetas\tab\tab\tab RD$= __' +
          '_______\par'#13#10'\tab\tab Total Cheques\tab\tab RD$= _________\par'#13#10 +
          '\tab\tab\tab\par'#13#10'\tab\tab Total Seg\'#39'fan Caja\tab\tab RD$= ____' +
          '_____\par'#13#10'\tab\tab Gastos\tab\tab\tab\tab RD$= _________\par'#13#10'\' +
          'tab\tab Transferencia Banco\tab RD$= _________\par'#13#10'\tab\tab Dif' +
          'erencia (+ \'#39'f3 -)\tab\tab RD$= _________\par'#13#10'\par'#13#10'\par'#13#10'\par'#13 +
          #10'\par'#13#10'\f1\par'#13#10'\par'#13#10'\par'#13#10'\pard\f2\par'#13#10'}'#13#10#0
        RemoveEmptyLines = False
        Stretch = True
        Transparent = True
        mmHeight = 124354
        mmLeft = 6350
        mmTop = 58208
        mmWidth = 188913
        BandType = 4
        LayerName = BandLayer4
        mmBottomOffset = 0
        mmOverFlowOffset = 0
        mmStopPosition = 0
        mmMinHeight = 0
      end
      object prchtxt4: TppRichText
        UserName = 'prchtxt2'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 12
        Font.Style = []
        Caption = 'prchtxt2'
        RichText = 
          '{\rtf1\ansi\ansicpg1252\deff0\deflang1033{\fonttbl{\f0\fnil\fcha' +
          'rset0 Century Gothic;}{\f1\fnil\fcharset0 Arial;}{\f2\fnil Arial' +
          ';}}'#13#10'{\colortbl ;\red0\green0\blue0;}'#13#10'\viewkind4\uc1\pard\qj\cf' +
          '1\f0\fs28 ____________________\tab\tab ____________________\tab\' +
          'tab ____________________\par'#13#10'CAJERO\tab (A)\tab\tab\tab\tab REV' +
          'ISADO POR\tab\tab\tab\tab RECIBIDO POR\par'#13#10'\f1\par'#13#10'\pard\f2\fs' +
          '24\par'#13#10'}'#13#10#0
        RemoveEmptyLines = False
        Transparent = True
        mmHeight = 16169
        mmLeft = 6085
        mmTop = 189237
        mmWidth = 186796
        BandType = 4
        LayerName = BandLayer4
        mmBottomOffset = 0
        mmOverFlowOffset = 0
        mmStopPosition = 0
        mmMinHeight = 0
      end
    end
    object pftrbnd2: TppFooterBand
      Background.Brush.Style = bsClear
      mmBottomOffset = 0
      mmHeight = 13229
      mmPrintPosition = 0
      object psystmvrbl4: TppSystemVariable
        UserName = 'psystmvrbl1'
        VarType = vtDateTime
        DisplayFormat = 'yyyy/MM/dd h:nn:ss AM/PM'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        Transparent = True
        mmHeight = 3704
        mmLeft = 5292
        mmTop = 2646
        mmWidth = 30691
        BandType = 8
        LayerName = BandLayer4
      end
      object psystmvrbl5: TppSystemVariable
        UserName = 'psystmvrbl2'
        VarType = vtDocumentName
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        Transparent = True
        mmHeight = 3704
        mmLeft = 5292
        mmTop = 8467
        mmWidth = 73819
        BandType = 8
        LayerName = BandLayer4
      end
    end
    object pdsgnlyrs2: TppDesignLayers
      object pdsgnlyr2: TppDesignLayer
        UserName = 'BandLayer4'
        LayerType = ltBanded
        Index = 0
      end
    end
    object prmtrlst2: TppParameterList
    end
  end
  object qryNC: TADOQuery
    Connection = con1
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select * from vw_RPT_NC')
    Left = 704
    Top = 56
    object intgrfldNCNumeroReq: TIntegerField
      FieldName = 'NumeroReq'
    end
    object intgrfldNCNumeroNC: TIntegerField
      FieldName = 'NumeroNC'
    end
    object bcdfldNCMonto: TBCDField
      FieldName = 'Monto'
      DisplayFormat = '99,999.99'
      Precision = 19
    end
    object bcdfldNCBalance: TBCDField
      FieldName = 'Balance'
      Precision = 19
    end
    object intgrfldNCTicket: TIntegerField
      FieldName = 'Ticket'
    end
    object wdstrngfldNCRazon: TWideStringField
      FieldName = 'Razon'
      Size = 250
    end
    object wdstrngfldNCArticuloID: TWideStringField
      FieldName = 'ArticuloID'
    end
    object intgrfldNCTicketUtilizado: TIntegerField
      FieldName = 'TicketUtilizado'
    end
    object wdstrngfldNCDescripcionArt: TWideStringField
      FieldName = 'DescripcionArt'
      Size = 100
    end
  end
  object pdbplnNC: TppDBPipeline
    DataSource = dsNC
    RefreshAfterPost = True
    UserName = 'pdbplnNC'
    Left = 704
    Top = 176
  end
  object dsNC: TDataSource
    DataSet = qryNC
    Left = 704
    Top = 112
  end
  object prprtReport: TppReport
    AutoStop = False
    DataPipeline = pdbplnTicket
    PrinterSetup.BinName = 'Default'
    PrinterSetup.DocumentName = 'Report'
    PrinterSetup.Duplex = dpVertical
    PrinterSetup.PaperName = 'Custom'
    PrinterSetup.PrinterName = 'Default'
    PrinterSetup.SaveDeviceSettings = False
    PrinterSetup.mmMarginBottom = 0
    PrinterSetup.mmMarginLeft = 0
    PrinterSetup.mmMarginRight = 0
    PrinterSetup.mmMarginTop = 0
    PrinterSetup.mmPaperHeight = 50800
    PrinterSetup.mmPaperWidth = 73660
    PrinterSetup.PaperSize = 130
    Template.DatabaseSettings.DataPipeline = pdbplnReportes
    Template.DatabaseSettings.Name = 'Cierre de Caja'
    Template.DatabaseSettings.NameField = 'Name'
    Template.DatabaseSettings.TemplateField = 'Template'
    Template.FileName = 
      'D:\vesslaBG\0.-SOURCE CODES\JMI\POS-nuevo\DATABASE Changes\repor' +
      'te NC.rtm'
    Template.SaveTo = stDatabase
    Template.Format = ftASCII
    ArchiveFileName = '($MyDocuments)\ReportArchive.raf'
    BeforePrint = prprtReportBeforePrint
    DeviceType = 'Screen'
    DefaultFileDeviceType = 'PDF'
    EmailSettings.ReportFormat = 'PDF'
    LanguageID = 'Default'
    OpenFile = False
    OutlineSettings.CreateNode = True
    OutlineSettings.CreatePageNodes = True
    OutlineSettings.Enabled = True
    OutlineSettings.Visible = True
    ThumbnailSettings.Enabled = True
    ThumbnailSettings.Visible = True
    ThumbnailSettings.DeadSpace = 30
    PDFSettings.EmbedFontOptions = [efUseSubset]
    PDFSettings.EncryptSettings.AllowCopy = True
    PDFSettings.EncryptSettings.AllowInteract = True
    PDFSettings.EncryptSettings.AllowModify = True
    PDFSettings.EncryptSettings.AllowPrint = True
    PDFSettings.EncryptSettings.Enabled = False
    PDFSettings.EncryptSettings.KeyLength = kl40Bit
    PDFSettings.FontEncoding = feAnsi
    PDFSettings.ImageCompressionLevel = 25
    PreviewFormSettings.PageDisplay = pdContinuous
    PreviewFormSettings.WindowState = wsMaximized
    PreviewFormSettings.ZoomSetting = zs100Percent
    RTFSettings.DefaultFont.Charset = DEFAULT_CHARSET
    RTFSettings.DefaultFont.Color = clWindowText
    RTFSettings.DefaultFont.Height = -13
    RTFSettings.DefaultFont.Name = 'Arial'
    RTFSettings.DefaultFont.Style = []
    TextFileName = '($MyDocuments)\Report.pdf'
    TextSearchSettings.DefaultString = '<FindText>'
    TextSearchSettings.Enabled = True
    XLSSettings.AppName = 'ReportBuilder'
    XLSSettings.Author = 'ReportBuilder'
    XLSSettings.Subject = 'Report'
    XLSSettings.Title = 'Report'
    Left = 704
    Top = 248
    Version = '15.03'
    mmColumnWidth = 73660
    DataPipelineName = 'pdbplnTicket'
    object phdrbnd3: TppHeaderBand
      Background.Brush.Style = bsClear
      PrintHeight = phDynamic
      mmBottomOffset = 0
      mmHeight = 34131
      mmPrintPosition = 0
      object plblTitulo1: TppLabel
        UserName = 'plblTitulo'
        Caption = 'CIERRE DE CAJA'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 12
        Font.Style = [fsBold]
        TextAlignment = taCentered
        Transparent = True
        mmHeight = 5027
        mmLeft = 17463
        mmTop = 6350
        mmWidth = 35983
        BandType = 0
        LayerName = Foreground1
      end
      object plbl4: TppLabel
        UserName = 'plbl2'
        Caption = '================================================='
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 6
        Font.Style = []
        Transparent = True
        mmHeight = 2646
        mmLeft = 8731
        mmTop = 19315
        mmWidth = 56621
        BandType = 0
        LayerName = Foreground1
      end
      object psystmvrbl2: TppSystemVariable
        UserName = 'psystmvrbl1'
        VarType = vtDateTime
        DisplayFormat = 'yyyy/mm/dd h:nn:ss AM/PM'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 16
        Font.Style = []
        Transparent = True
        mmHeight = 6350
        mmLeft = 6085
        mmTop = 23548
        mmWidth = 62177
        BandType = 0
        LayerName = Foreground1
      end
      object plbl5: TppLabel
        UserName = 'plbl10'
        Caption = '================================================='
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 6
        Font.Style = []
        Transparent = True
        mmHeight = 2646
        mmLeft = 8202
        mmTop = 30163
        mmWidth = 56092
        BandType = 0
        LayerName = Foreground1
      end
      object plblCaja: TppLabel
        UserName = 'plblCaja'
        Caption = 'Nombre CAJA'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 12
        Font.Style = [fsBold]
        TextAlignment = taCentered
        Transparent = True
        mmHeight = 5027
        mmLeft = 20902
        mmTop = 13229
        mmWidth = 28575
        BandType = 0
        LayerName = Foreground1
      end
    end
    object pdtlbnd3: TppDetailBand
      Visible = False
      Background1.Brush.Style = bsClear
      Background2.Brush.Style = bsClear
      PrintHeight = phDynamic
      mmBottomOffset = 0
      mmHeight = 2381
      mmPrintPosition = 0
    end
    object pftrbnd3: TppFooterBand
      Visible = False
      Background.Brush.Style = bsClear
      mmBottomOffset = 0
      mmHeight = 2381
      mmPrintPosition = 0
    end
    object pdsgnlyrs3: TppDesignLayers
      object pdsgnlyr3: TppDesignLayer
        UserName = 'Foreground1'
        LayerType = ltBanded
        Index = 0
      end
    end
    object prmtrlst4: TppParameterList
    end
  end
  object spValidarUsuario: TADOStoredProc
    Connection = con1
    CursorType = ctStatic
    ProcedureName = 'sp_VALIDARUSUARIO;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@Usuario'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 10
        Value = Null
      end>
    Left = 136
    Top = 312
  end
  object conComun: TADOConnection
    KeepConnection = False
    LoginPrompt = False
    Provider = 'SQLOLEDB.1'
    BeforeConnect = conComunBeforeConnect
    Left = 184
    Top = 40
  end
  object qryPOS: TADOQuery
    Connection = con1
    Parameters = <>
    Left = 112
    Top = 104
  end
  object qryCOMUN: TADOQuery
    Connection = conComun
    Parameters = <>
    Left = 184
    Top = 104
  end
  object sp1: TADOStoredProc
    Connection = con1
    ProcedureName = 'sp_VALIDARUSUARIO;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@Usuario'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 10
        Value = Null
      end>
    Left = 256
    Top = 424
  end
  object spCajaEstado: TADOStoredProc
    Connection = con1
    ProcedureName = 'sp_CAJA_ESTADO;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 2
      end
      item
        Name = '@NombrePC'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 90
        Value = 'test'
      end>
    Left = 40
    Top = 312
  end
  object qryWork1: TADOQuery
    Connection = con1
    Parameters = <>
    Left = 240
    Top = 104
  end
  object dsAlmacenes: TDataSource
    DataSet = qryAlmacenes
    Left = 304
    Top = 368
  end
  object qryAlmacenes: TADOQuery
    Connection = con1
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select * from almacenes')
    Left = 232
    Top = 368
  end
  object qryNCFTipos: TADOQuery
    Connection = con1
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'SELECT * FROM [ncf.tipos]')
    Left = 296
    Top = 104
  end
  object qryVendedores: TADOQuery
    Connection = con1
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'SELECT * FROM [Vendedores]')
    Left = 352
    Top = 104
  end
  object QryClientes: TADOTable
    Connection = con1
    CursorType = ctStatic
    TableName = 'clientes'
    Left = 56
    Top = 104
  end
  object con1: TADOConnection
    Connected = True
    ConnectionString = 
      'Provider=SQLOLEDB.1;Password=Josefina1;Persist Security Info=Tru' +
      'e;User ID=sa;Initial Catalog=DB_PUNTO_VENTA;Data Source=localhos' +
      't\sql2008;Use Procedure for Prepare=1;Auto Translate=True;Packet' +
      ' Size=4096;Workstation ID=DOHAPBT0N3G;Use Encryption for Data=Fa' +
      'lse;Tag with column collation when possible=False'
    KeepConnection = False
    LoginPrompt = False
    Provider = 'SQLOLEDB.1'
    BeforeConnect = con1BeforeConnect
    Left = 40
    Top = 40
  end
end
