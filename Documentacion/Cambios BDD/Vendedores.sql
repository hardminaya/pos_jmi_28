/*
   Wednesday, February 17, 20168:19:35 PM
   User: 
   Server: localhost\sqlexpress
   Database: DB_PUNTO_VENTA
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Vendedores
	DROP CONSTRAINT DF_Vendedores_Activo
GO
CREATE TABLE dbo.Tmp_Vendedores
	(
	Id int NOT NULL IDENTITY (1, 1),
	CodVendedor nchar(10) NOT NULL,
	Nombre varchar(50) NOT NULL,
	Apellido varchar(50) NOT NULL,
	Activo bit NOT NULL,
	IDSupervisor int NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_Vendedores SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.Tmp_Vendedores ADD CONSTRAINT
	DF_Vendedores_Activo DEFAULT ((1)) FOR Activo
GO
SET IDENTITY_INSERT dbo.Tmp_Vendedores ON
GO
IF EXISTS(SELECT * FROM dbo.Vendedores)
	 EXEC('INSERT INTO dbo.Tmp_Vendedores (Id, CodVendedor, Nombre, Apellido, Activo)
		SELECT Id, CodVendedor, Nombre, Apellido, Activo FROM dbo.Vendedores WITH (HOLDLOCK TABLOCKX)')
GO
COMMIT
