/*
   Sunday, January 10, 20165:09:05 PM
   User: 
   Server: localhost\sqlexpress
   Database: DB_PUNTO_VENTA
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_ListaPrecios
	(
	PIDRegistro int NOT NULL IDENTITY (1, 1),
	ID int NOT NULL,
	Descripción nvarchar(128) NOT NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_ListaPrecios SET (LOCK_ESCALATION = TABLE)
GO
SET IDENTITY_INSERT dbo.Tmp_ListaPrecios ON
GO
IF EXISTS(SELECT * FROM dbo.ListaPrecios)
	 EXEC('INSERT INTO dbo.Tmp_ListaPrecios (PIDRegistro, ID, Descripción)
		SELECT PIDRegistro, ID, Descripción FROM dbo.ListaPrecios WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_ListaPrecios OFF
GO
DROP TABLE dbo.ListaPrecios
GO
EXECUTE sp_rename N'dbo.Tmp_ListaPrecios', N'ListaPrecios', 'OBJECT' 
GO
COMMIT
select Has_Perms_By_Name(N'dbo.ListaPrecios', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.ListaPrecios', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.ListaPrecios', 'Object', 'CONTROL') as Contr_Per 