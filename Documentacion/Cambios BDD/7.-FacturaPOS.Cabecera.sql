/*
   Wednesday, February 17, 20168:25:10 PM
   User: 
   Server: localhost\sqlexpress
   Database: DB_PUNTO_VENTA
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.[FacturaPOS.Cabecera] ADD
	IDSupervisor int NULL
GO
ALTER TABLE dbo.[FacturaPOS.Cabecera] SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.[FacturaPOS.Cabecera]', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.[FacturaPOS.Cabecera]', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.[FacturaPOS.Cabecera]', 'Object', 'CONTROL') as Contr_Per 