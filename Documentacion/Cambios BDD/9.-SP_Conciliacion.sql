GO
/****** Object:  StoredProcedure [dbo].[sp_CONCILIARNC]    Script Date: 01/25/2016 10:46:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Amaury Minaya
-- Create date: 2014-08-28
-- Modified date: 2016-01-17
-- Description:	Concilia SNC, insertando NC SAP
-- =============================================
ALTER PROCEDURE [dbo].[sp_CONCILIARNC]
			(@NumeroReq int
           ,@NumeroNC int
           ,@Conciliacion_UsuarioID int)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
UPDATE 
	NC 
SET 
	NumeroNC=@NumeroNC, 
	Estado='C',
	Conciliacion_UsuarioID = @Conciliacion_UsuarioID,
	Fecha_Conciliacion= GETDATE()WHERE NumeroReq=@NumeroReq
END