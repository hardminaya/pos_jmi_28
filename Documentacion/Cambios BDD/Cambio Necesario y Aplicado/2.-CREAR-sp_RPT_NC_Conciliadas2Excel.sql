GO

/****** Object:  StoredProcedure [dbo].[sp_RPT_NC_Conciliadas2Excel]    Script Date: 03/13/2016 22:51:19 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Amaury Minaya
-- Create date: 2016-01-28
-- Description:	Consulta de Notas de Credito conciliadas por Fecha
-- =============================================
CREATE PROCEDURE [dbo].[sp_RPT_NC_Conciliadas2Excel]
	-- Add the parameters for the stored procedure here
	@Fecha_Conciliacion_Desde datetime,
	@Fecha_Conciliacion_Hasta datetime,
	@Conciliacion_UsuarioID int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

SELECT     
	--dbo.NC.Fecha
	NC.NumeroReq
	,NC.NumeroNC, 
	NC.ArticuloID, 
	Articulos.DescripcionArt,
	NC.Serie, 
	NC.Razon, 
    Usuarios.Nombre, 
    NC.Fecha_Conciliacion
FROM         
    Usuarios INNER JOIN
    NC ON dbo.Usuarios.pIdRegistro = NC.Conciliacion_UsuarioID INNER JOIN
    dbo.Articulos ON NC.ArticuloID = dbo.Articulos.CodigoArt
WHERE
	Fecha_Conciliacion 
		between 
		@fecha_Conciliacion_Desde+' 00:00:00.000' AND 
		@fecha_Conciliacion_Hasta+' 23:59:59.000' AND
		conciliacion_usuarioid =@Conciliacion_UsuarioID
ORDER BY
	Fecha_Conciliacion desc
END

GO


