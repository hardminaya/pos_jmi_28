USE [JMI_DB_PUNTO_VENTA_PROD]
GO

/****** Object:  StoredProcedure [dbo].[sp_SeleccionaNCF_NC]    Script Date: 08/30/2017 14:13:46 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SeleccionaNCF_NC]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SeleccionaNCF_NC]
GO

USE [JMI_DB_PUNTO_VENTA_PROD]
GO

/****** Object:  StoredProcedure [dbo].[sp_SeleccionaNCF_NC]    Script Date: 08/30/2017 14:13:46 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Amaury Minaya
-- Create date: 2015-09-30
-- Description:	Inserta NCF Nota de Créditos para factura
-- =============================================
CREATE PROCEDURE [dbo].[sp_SeleccionaNCF_NC]
AS
BEGIN
	DECLARE @Secuencia VARCHAR(20)
	
	SELECT TOP 1  @Secuencia =   Secuencia  FROM [NCF.Secuencia]
	WHERE Estatus = 'D' AND fkTipoNCF = '5'

	UPDATE [NCF.Secuencia] SET Estatus = 'U'
	WHERE	Secuencia = @Secuencia
	
	SELECT @Secuencia as secuencia
END


GO

