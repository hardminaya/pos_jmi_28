
GO
/****** Object:  StoredProcedure [dbo].[sp_RPT_NC_Conciliadas]    Script Date: 01/25/2016 18:12:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_RPT_NC_Conciliadas]
	-- Add the parameters for the stored procedure here
	@Fecha_Conciliacion_Desde datetime,
	@Fecha_Conciliacion_Hasta datetime,
	@Conciliacion_UsuarioID int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

SELECT     
	dbo.NC.Fecha, 
	NC.NumeroReq, 
	NC.NumeroNC, 
	NC.ArticuloID, [FacturaPOS.Detalle].ArticuloDescripcion, 
	NC.Serie, 
	NC.Razon, 
    Usuarios.Nombre, 
    NC.Fecha_Conciliacion
FROM         
	NC INNER JOIN
    Usuarios ON NC.Conciliacion_UsuarioID = Usuarios.pIdRegistro INNER JOIN
    [FacturaPOS.Detalle] ON NC.RegistroDetalle = [FacturaPOS.Detalle].pIdRegistro
WHERE
	Fecha_Conciliacion 
		between 
		@fecha_Conciliacion_Desde+' 00:00:00.000' AND 
		@fecha_Conciliacion_Hasta+' 23:59:59.000' AND
		conciliacion_usuarioid =@Conciliacion_UsuarioID
ORDER BY
	Fecha_Conciliacion desc
END
