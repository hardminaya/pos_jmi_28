USE [JMI_DB_PUNTO_VENTA_PROD]
GO
/****** Object:  Trigger [dbo].[InsertarNCF]    Script Date: 09/06/2017 00:03:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER Trigger [dbo].[InsertarNCF]
  On [dbo].[FacturaPOS.Cabecera]
  For Insert 
  As
BEGIN
   Declare @cantidad int -- Contador Tabla Control --
   Declare @FacturaNumero int
   Declare @Secuencia varchar(20)
   Declare @TipoNCF int
	-- Numero Control ---
	Select @cantidad = ControlNCF From [NCF.Control]

	--Cabecera Factura: Numero Factura
	Select @FacturaNumero = fc.pIdRegistro, @TipoNCF = fc.fkTipoNCF From [FacturaPOS.Cabecera] fc  Join inserted On inserted.pIdRegistro = fc.pIdRegistro
	Where fc.pIdRegistro = inserted.pIdRegistro


	If (@TipoNCF = 4) -- Facturas Regimen Especial ---
	Begin
		Select Top 1  @Secuencia =   Secuencia  From [NCF.Secuencia]
		Where	Estatus = 'D' And fkTipoNCF = '4'

	   Insert Into [FacturaPOS.FacturaNCF] (fkTipoNCF, fkFacturaNo, NumeroNCF)
				   Values  (3, @FacturaNumero, @Secuencia)

		Update [NCF.Secuencia] set Estatus = 'U'
		Where	Secuencia = @Secuencia
	End
	
	If (@TipoNCF = 3) -- Facturas Gubernamentales ---
	Begin
		Select Top 1  @Secuencia =   Secuencia  From [NCF.Secuencia]
		Where	Estatus = 'D' And fkTipoNCF = '3'

	   Insert Into [FacturaPOS.FacturaNCF] (fkTipoNCF, fkFacturaNo, NumeroNCF)
				   Values  (3, @FacturaNumero, @Secuencia)

		Update [NCF.Secuencia] set Estatus = 'U'
		Where	Secuencia = @Secuencia
	End

	If (@TipoNCF = 2) --Credito Fiscal ---
	Begin
		Select Top 1  @Secuencia =   Secuencia  From [NCF.Secuencia]
		Where	Estatus = 'D' And fkTipoNCF = '2'

	   Insert Into [FacturaPOS.FacturaNCF] (fkTipoNCF, fkFacturaNo, NumeroNCF)
				   Values  (2, @FacturaNumero, @Secuencia)

		Update [NCF.Secuencia] set Estatus = 'U'
		Where	Secuencia = @Secuencia
	End
	else if (@TipoNCF = 1) -- Consumidor Final --
	Begin
	If ((@cantidad = 1)) 
	Begin
	-- Secuencia Comprobante Fiscal --
		Select Top 1  @Secuencia =   Secuencia  From [NCF.Secuencia]
		Where	Estatus = 'D' And fkTipoNCF = '1'

	   Insert Into [FacturaPOS.FacturaNCF] (fkTipoNCF, fkFacturaNo, NumeroNCF, NCFControl)
				   Values  (1, @FacturaNumero, @Secuencia, @cantidad)

		Update [NCF.Secuencia] set Estatus = 'U'
		Where	Secuencia = @Secuencia

		Update [NCF.Control] Set ControlNCF = (@cantidad + 1), NCF = @Secuencia
	End
	Else
	Begin
		
		Select @Secuencia = NCF From [NCF.Control]
		  Insert Into [FacturaPOS.FacturaNCF] (fkTipoNCF, fkFacturaNo, NumeroNCF, NCFControl)
				   Values  (1, @FacturaNumero, @Secuencia, @cantidad)

		Update [NCF.Control] Set ControlNCF = @cantidad + 1
	End

	if (@cantidad = 1 )
	begin
		Update [NCF.Control] Set ControlNCF = 1
	end
	End



END
