USE [DB_PUNTO_VENTA]
GO
/****** Object:  StoredProcedure [dbo].[sp_ActualizaBalanceNC]    Script Date: 01/17/2016 23:05:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Amaury Minaya
-- Create date: 2014-05-05
-- Description:	Actualiza Balance de Nota de Credito
-- =============================================
ALTER PROCEDURE [dbo].[sp_ActualizaBalanceNC]
	@NumeroReq Numeric(19,6),
	@Monto Numeric(19,6)
AS
BEGIN
	DECLARE @Balance Money
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Update NC SET Balance=0, Estado='U' WHERE numeroReq=@numeroReq
	--RETURN (@balance)
END
