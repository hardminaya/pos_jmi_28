﻿USE [DB_PUNTO_VENTA]
GO
/****** Object:  Table [dbo].[FormaPago]    Script Date: 08/13/2014 16:06:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FormaPago](
	[idFormaPago] [int] IDENTITY(1,1) NOT NULL,
	[Codigo] [smallint] NOT NULL,
	[idUsuario] [int] NOT NULL,
	[FormaPago] [nvarchar](50) NOT NULL,
	[Fecha] [datetime] NOT NULL,
	[UltimoUsuarioActualizo] [int] NOT NULL,
	[UltimaFechaActualizacion] [datetime] NOT NULL,
 CONSTRAINT [PK_FormaPago] PRIMARY KEY CLUSTERED 
(
	[idFormaPago] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Impuestos]    Script Date: 08/13/2014 16:06:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Impuestos](
	[pIdRegistro] [int] NOT NULL,
	[Descripcion] [nvarchar](50) NULL,
	[Factor] [numeric](2, 0) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Impresoras]    Script Date: 08/13/2014 16:06:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Impresoras](
	[pIdRegistro] [int] IDENTITY(1,1) NOT NULL,
	[PrinterName] [nvarchar](100) NOT NULL,
	[Pantalla] [char](1) NULL,
 CONSTRAINT [PK_Impresoras] PRIMARY KEY CLUSTERED 
(
	[pIdRegistro] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Grupos]    Script Date: 08/13/2014 16:06:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Grupos](
	[pIdRegistro] [int] NOT NULL,
	[Descripcion] [nvarchar](30) NULL,
	[pkUsuario] [int] NULL,
 CONSTRAINT [PK_Grupos] PRIMARY KEY CLUSTERED 
(
	[pIdRegistro] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Clientes]    Script Date: 08/13/2014 16:06:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Clientes](
	[pIdRegistro] [int] IDENTITY(1,1) NOT NULL,
	[ClienteID] [nvarchar](50) NOT NULL,
	[Nombre] [nvarchar](50) NOT NULL,
	[TipoCliente] [char](2) NULL,
	[Direccion] [nvarchar](100) NULL,
	[RNC] [nvarchar](20) NULL,
	[Generico] [char](1) NULL,
 CONSTRAINT [PK_Clientes] PRIMARY KEY CLUSTERED 
(
	[pIdRegistro] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Valor ''Y'', este es el cliente que el sistema POS pondra por defecto si no se especifica uno' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Clientes', @level2type=N'COLUMN',@level2name=N'Generico'
GO
/****** Object:  Table [dbo].[Caja.MovimientosDetalle]    Script Date: 08/13/2014 16:06:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Caja.MovimientosDetalle](
	[pIdRegistro] [int] NOT NULL,
	[fkCabecera] [int] NULL,
	[FacturaNo] [int] NULL,
	[Monto] [numeric](18, 2) NULL,
 CONSTRAINT [PK_Caja.MovimientosDetalle] PRIMARY KEY CLUSTERED 
(
	[pIdRegistro] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Caja.MovimientosCabecera]    Script Date: 08/13/2014 16:06:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Caja.MovimientosCabecera](
	[pIdRegistro] [int] NOT NULL,
	[fkUsuario] [int] NULL,
	[FechaCierre] [datetime] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Caja.AbrirCerrar]    Script Date: 08/13/2014 16:06:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Caja.AbrirCerrar](
	[pIdRegistro] [int] NOT NULL,
	[fkUsuario] [int] NULL,
	[FechaApertura] [datetime] NULL,
	[FechaCierre] [datetime] NULL,
	[MontoApertura] [numeric](18, 2) NULL,
	[MontoCierre] [numeric](18, 2) NULL,
	[EstadoCaja] [char](1) NULL,
	[NombrePC] [nvarchar](90) NULL,
 CONSTRAINT [PK_Caja.AbrirCerrar] PRIMARY KEY CLUSTERED 
(
	[pIdRegistro] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'''A'' Cuando la caja este abierta y ''C'' cuando este cerrada.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Caja.AbrirCerrar', @level2type=N'COLUMN',@level2name=N'EstadoCaja'
GO
/****** Object:  Table [dbo].[Caja]    Script Date: 08/13/2014 16:06:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Caja](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[NombrePC] [nvarchar](90) NULL,
	[Supervisor] [nvarchar](50) NULL,
 CONSTRAINT [PK_Caja] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Usuarios_1]    Script Date: 08/13/2014 16:06:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Usuarios_1](
	[pIdRegistro] [int] IDENTITY(1,1) NOT NULL,
	[fkUsuario] [int] NULL,
	[VENTAS] [bit] NULL,
	[ADMINISTRAR] [bit] NULL,
	[INVENTARIO] [bit] NULL,
	[REPORTES] [bit] NULL,
	[CONSULTAS] [bit] NULL,
	[SINCRONIZAR] [bit] NULL,
	[DESCUENTO] [bit] NULL,
	[CambioAlmacen] [bit] NULL,
 CONSTRAINT [PK_Usuarios_1] PRIMARY KEY CLUSTERED 
(
	[pIdRegistro] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Usuarios]    Script Date: 08/13/2014 16:06:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Usuarios](
	[pIdRegistro] [int] IDENTITY(1,1) NOT NULL,
	[fkSucursal] [int] NOT NULL,
	[Nombre] [nvarchar](20) NOT NULL,
	[Apellido1] [nvarchar](20) NULL,
	[Apellido2] [nvarchar](20) NULL,
	[Usuario] [nvarchar](10) NULL,
	[Clave] [nvarchar](10) NULL,
	[Fecha] [datetime] NULL,
	[Bloqueo] [bit] NULL,
	[DescripcionCaja] [nvarchar](20) NULL,
	[IdAlmacen] [nvarchar](50) NULL,
	[DescripcionAlmacen] [nvarchar](50) NULL,
	[Conectado] [varchar](1) NULL,
	[NombrePC] [nvarchar](90) NULL,
	[VENTAS] [bit] NULL,
	[ADMINISTRAR] [bit] NULL,
	[INVENTARIO] [bit] NULL,
	[REPORTES] [bit] NULL,
	[CONSULTAS] [bit] NULL,
	[SINCRONIZAR] [bit] NULL,
	[SINCRONIZAR_AUT] [bit] NULL,
	[DESCUENTO] [bit] NULL,
	[CambioAlmacen] [bit] NULL,
	[PRECIO] [bit] NULL,
	[REIMPRIMIR] [bit] NULL,
	[ACCESO] [bit] NULL,
	[ABRIRCAJA] [bit] NULL,
	[CERRARCAJA] [bit] NULL,
 CONSTRAINT [PK_Usuarios] PRIMARY KEY CLUSTERED 
(
	[pIdRegistro] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Usuario.Privilegios]    Script Date: 08/13/2014 16:06:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Usuario.Privilegios](
	[Usuario.PidRegistro] [int] NULL,
	[Privilegio] [nvarchar](64) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TIMER]    Script Date: 08/13/2014 16:06:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TIMER](
	[MILISEGUNDO] [int] NOT NULL,
	[EJECUTAR] [bit] NULL,
 CONSTRAINT [PK_TIMER] PRIMARY KEY CLUSTERED 
(
	[MILISEGUNDO] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TempSerial]    Script Date: 08/13/2014 16:06:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TempSerial](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CodigoArt] [nvarchar](20) NULL,
	[Serie] [nvarchar](32) NULL,
	[Caja] [nvarchar](90) NULL,
 CONSTRAINT [PK_TempSerial] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TempArticulo]    Script Date: 08/13/2014 16:06:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TempArticulo](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CodigoArt] [nvarchar](20) NULL,
	[Cantidad] [numeric](18, 2) NULL,
	[Almacen] [nvarchar](8) NULL,
	[Caja] [nvarchar](90) NULL,
 CONSTRAINT [PK_TempArticulo] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TarjetaDescuento]    Script Date: 08/13/2014 16:06:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TarjetaDescuento](
	[TarjetaNo] [numeric](18, 0) NULL,
	[ClientID] [nvarchar](50) NULL,
	[Descuento] [numeric](18, 0) NULL,
	[Fecha_Expiracion] [date] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[System.Privilegios]    Script Date: 08/13/2014 16:06:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[System.Privilegios](
	[pidRegistro] [int] IDENTITY(1,1) NOT NULL,
	[Orden] [int] NULL,
	[Privilegio] [nvarchar](64) NULL,
	[Descripcion] [nvarchar](256) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Sucursal]    Script Date: 08/13/2014 16:06:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Sucursal](
	[pkIdRegistro] [int] NOT NULL,
	[Descripcion] [nvarchar](50) NULL,
	[Direccion] [nvarchar](50) NULL,
	[Telefono] [nvarchar](15) NULL,
	[Fax] [nvarchar](15) NULL,
	[Representante] [nvarchar](20) NULL,
	[RNC] [nvarchar](20) NULL,
	[Usuario] [nvarchar](20) NULL,
	[Fecha] [datetime] NULL,
	[Principal] [bit] NULL,
	[Tienda_Principal] [nvarchar](30) NULL,
	[Codigo_Almacen] [nvarchar](8) NULL,
	[Comprobante] [bit] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ventas]    Script Date: 08/13/2014 16:06:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ventas](
	[sucursalnumero] [nvarchar](20) NULL,
	[sucursalnombre] [nvarchar](20) NULL,
	[monto] [nvarchar](20) NULL,
	[fecha] [date] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Vendedores]    Script Date: 08/13/2014 16:06:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Vendedores](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CodVendedor] [int] NOT NULL,
	[Nombre] [varchar](50) NULL,
	[Apellido] [varchar](50) NULL,
 CONSTRAINT [PK_Vendedores_1] PRIMARY KEY CLUSTERED 
(
	[CodVendedor] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[FacturaPOS.Sim]    Script Date: 08/13/2014 16:06:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FacturaPOS.Sim](
	[pIdRegistro] [int] NOT NULL,
	[ArticuloCodigo] [nvarchar](20) NULL,
	[ArticuloDescripcion] [nvarchar](20) NULL,
	[ArticuloSerie] [nvarchar](20) NULL,
	[fkDocCabecera] [int] NULL,
 CONSTRAINT [PK_FacturaPOS.Sim] PRIMARY KEY CLUSTERED 
(
	[pIdRegistro] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[FacturaPOS.SerialesTemp]    Script Date: 08/13/2014 16:06:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FacturaPOS.SerialesTemp](
	[pIdRegistro] [int] IDENTITY(1,1) NOT NULL,
	[fkDocCabecera] [int] NULL,
	[No_Linea] [int] NULL,
	[fkArticulo] [nvarchar](20) NULL,
	[Serie] [nvarchar](20) NULL,
	[Codigo_Almacen] [nvarchar](50) NULL,
	[fkUsuario] [int] NULL,
 CONSTRAINT [PK_FacturaPOS.SerialesTemp] PRIMARY KEY CLUSTERED 
(
	[pIdRegistro] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[FacturaPOS.FacturaNCF]    Script Date: 08/13/2014 16:06:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FacturaPOS.FacturaNCF](
	[pIdRegistro] [int] IDENTITY(1,1) NOT NULL,
	[fkTipoNCF] [int] NULL,
	[fkFacturaNo] [int] NULL,
	[NumeroNCF] [nvarchar](20) NULL,
	[NCFControl] [smallint] NULL,
 CONSTRAINT [PK_FacturaPOS.FacturaNCF] PRIMARY KEY CLUSTERED 
(
	[pIdRegistro] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[FacturaPOS.FacturaComision]    Script Date: 08/13/2014 16:06:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FacturaPOS.FacturaComision](
	[pIdRegistro] [int] IDENTITY(1,1) NOT NULL,
	[fkFacturaCodigo] [int] NULL,
	[MontoComision] [numeric](18, 0) NULL,
	[fkCliente] [nvarchar](10) NULL,
	[Codigo_Almacen] [nvarchar](50) NULL
) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Este es el codigo del cliente a generar la CXC de la comision por la venta, CLARO.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FacturaPOS.FacturaComision', @level2type=N'COLUMN',@level2name=N'fkCliente'
GO
/****** Object:  Table [dbo].[FacturaPOS.Equipo]    Script Date: 08/13/2014 16:06:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FacturaPOS.Equipo](
	[pIdRegistro] [int] NOT NULL,
	[ArticuloCodigo] [nvarchar](20) NULL,
	[ArticuloDescripcion] [nvarchar](50) NULL,
	[ArticuloSerie] [nvarchar](20) NULL,
	[fkDocCabecera] [int] NULL,
 CONSTRAINT [PK_FacturaPOS.Equipo] PRIMARY KEY CLUSTERED 
(
	[pIdRegistro] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[FacturaPOS.DetalleSerial]    Script Date: 08/13/2014 16:06:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FacturaPOS.DetalleSerial](
	[pIdRegistro] [int] IDENTITY(1,1) NOT NULL,
	[Secuencia] [int] NULL,
	[fkDocCabecera] [int] NULL,
	[No_Linea] [int] NULL,
	[fkArticulo] [nvarchar](20) NULL,
	[Serie] [nvarchar](32) NULL,
	[Codigo_Almacen] [nvarchar](8) NULL,
	[fkUsuario] [int] NULL,
	[Estatus] [bit] NULL,
	[fkSucursal] [nvarchar](8) NULL,
 CONSTRAINT [PK_FacturaPOS.DetalleSerial] PRIMARY KEY CLUSTERED 
(
	[pIdRegistro] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[FacturaPOS.Detalle]    Script Date: 08/13/2014 16:06:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FacturaPOS.Detalle](
	[pIdRegistro] [int] IDENTITY(1,1) NOT NULL,
	[fkDocCabecera] [int] NULL,
	[SecSerial] [int] NULL,
	[No_Linea] [int] NULL,
	[ArticuloId] [nvarchar](20) NULL,
	[ArticuloDescripcion] [nvarchar](100) NULL,
	[ArticuloCosto] [numeric](19, 6) NULL,
	[ArticuloPrecio] [numeric](19, 6) NULL,
	[ArticuloPrecioDesc] [numeric](19, 6) NULL,
	[ArticuloCantidad] [numeric](18, 0) NULL,
	[ArticuloMonto] [numeric](19, 6) NULL,
	[ArticuloPorcientoDesc] [numeric](18, 2) NULL,
	[ArticuloTotalDescuento] [numeric](19, 6) NULL,
	[ArticuloPorcientoImpuesto] [numeric](18, 2) NULL,
	[ArticuloPrecioImpuesto] [numeric](19, 6) NULL,
	[ArticuloTotalImpuesto] [numeric](19, 6) NULL,
	[ArticuloImporte] [numeric](19, 6) NULL,
	[ArticuloImporteNeto] [numeric](19, 6) NULL,
	[fkUsuario] [int] NULL,
	[Codigo_Almacen] [nvarchar](8) NULL,
	[PlanID] [int] NULL,
	[ManejaSeriales] [char](1) NULL,
	[fkSucursal] [nvarchar](8) NULL,
 CONSTRAINT [PK_FacturaPOS.Detalle] PRIMARY KEY CLUSTERED 
(
	[pIdRegistro] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Cantidad x Precio' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FacturaPOS.Detalle', @level2type=N'COLUMN',@level2name=N'ArticuloMonto'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Representa el Codigo de la sucrusal en SAP' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FacturaPOS.Detalle', @level2type=N'COLUMN',@level2name=N'Codigo_Almacen'
GO
/****** Object:  Table [dbo].[FacturaPOS.Contrato]    Script Date: 08/13/2014 16:06:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FacturaPOS.Contrato](
	[pIdRegistro] [int] NOT NULL,
	[ContratoNumero] [int] NULL,
	[CelularNumero] [nvarchar](15) NULL,
	[ClienteNombre] [nvarchar](50) NULL,
	[ClienteCedula] [nvarchar](15) NULL,
	[fkDocCabecera] [int] NULL,
	[Codigo_Almacen] [nvarchar](50) NULL
) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Representa la sucrusal en SAP' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FacturaPOS.Contrato', @level2type=N'COLUMN',@level2name=N'Codigo_Almacen'
GO
/****** Object:  Table [dbo].[BCK02032013NCFSecuencia]    Script Date: 08/13/2014 16:06:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BCK02032013NCFSecuencia](
	[pIdRegistro] [int] NOT NULL,
	[fkTipoNCF] [int] NOT NULL,
	[Numerico] [numeric](18, 0) NULL,
	[Secuencia] [nvarchar](20) NULL,
	[Estatus] [char](1) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Articulos.SerialesTemp]    Script Date: 08/13/2014 16:06:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Articulos.SerialesTemp](
	[pIdRegistro] [int] NOT NULL,
	[SerialNo] [nvarchar](50) NULL,
	[fkIdArticulo] [nvarchar](20) NULL,
	[fkEntrada] [int] NULL,
	[Estatus] [char](1) NULL,
	[Codigo_Almacen] [nvarchar](50) NULL,
 CONSTRAINT [PK_Articulos.SerialesTemp] PRIMARY KEY CLUSTERED 
(
	[pIdRegistro] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Articulos.Seriales]    Script Date: 08/13/2014 16:06:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Articulos.Seriales](
	[pIdRegistro] [int] IDENTITY(1,1) NOT NULL,
	[No_Linea] [int] NOT NULL,
	[SerialNo] [nvarchar](32) NOT NULL,
	[fkIdArticulo] [nvarchar](20) NULL,
	[fkEntrada] [int] NULL,
	[Estatus] [char](1) NULL,
	[fkFacturaVenta] [int] NULL,
	[Codigo_Almacen] [nvarchar](8) NULL,
	[Cantidad] [int] NULL,
 CONSTRAINT [PK_Articulos.Seriales] PRIMARY KEY CLUSTERED 
(
	[pIdRegistro] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Valor ''D'' cuando esta disponible.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Articulos.Seriales', @level2type=N'COLUMN',@level2name=N'Estatus'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Cuando se vende un articulo que maneja seriales se graba el numero de documento en el que se vendio' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Articulos.Seriales', @level2type=N'COLUMN',@level2name=N'fkFacturaVenta'
GO
/****** Object:  Table [dbo].[Articulos.SalidaArticuloTranferencia]    Script Date: 08/13/2014 16:06:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Articulos.SalidaArticuloTranferencia](
	[pIdRegistro] [int] IDENTITY(1,1) NOT NULL,
	[fkEntrada] [int] NULL,
	[ArticuloCodigo] [nvarchar](20) NULL,
	[ArticuloDescripcion] [nvarchar](150) NULL,
	[ArticuloCantidad] [numeric](18, 2) NULL,
	[Codigo_Almacen] [nvarchar](50) NULL,
	[Codigo_Origen] [nvarchar](50) NULL,
	[StatusTranfers] [bit] NULL,
 CONSTRAINT [PK_Articulos.SalidaArticuloTranferencia] PRIMARY KEY CLUSTERED 
(
	[pIdRegistro] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Articulos.Precios]    Script Date: 08/13/2014 16:06:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Articulos.Precios](
	[pIdRegistro] [int] IDENTITY(1,1) NOT NULL,
	[PlanID] [int] NOT NULL,
	[fkArticulo] [nvarchar](50) NULL,
	[DescripcionPlan] [nvarchar](50) NOT NULL,
	[Costo] [numeric](19, 6) NULL,
	[Precio] [numeric](19, 6) NULL,
	[fkUsuario] [int] NULL,
	[FechaCreacion] [datetime] NULL,
	[Doc_Actualizacion] [int] NULL,
 CONSTRAINT [PK_Articulos.Precios] PRIMARY KEY CLUSTERED 
(
	[pIdRegistro] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Articulos]    Script Date: 08/13/2014 16:06:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Articulos](
	[pIdRegistro] [int] IDENTITY(1,1) NOT NULL,
	[CodigoArt] [nvarchar](20) NOT NULL,
	[DescripcionArt] [nvarchar](100) NULL,
	[GrupoArt] [int] NULL,
	[CodigoBarras] [nvarchar](50) NULL,
	[EnMano] [numeric](18, 2) NULL,
	[CompraArt] [char](1) NULL,
	[VentaArt] [char](1) NULL,
	[InventarioArt] [char](1) NULL,
	[ITBIS] [char](1) NULL,
	[ManejaSeriales] [nchar](1) NULL,
	[DescripcionEtiqueta] [nvarchar](20) NULL,
	[Documento_Actualizacion] [int] NULL,
	[Precio] [numeric](19, 6) NULL,
	[Doc_actualizacion] [int] NULL,
	[Codigo_Almacen] [nvarchar](50) NULL,
	[FechaActualizacion] [datetime] NULL,
	[FechaModificacion] [datetime] NULL,
	[Usuario] [nvarchar](50) NULL,
 CONSTRAINT [PK_Articulos] PRIMARY KEY CLUSTERED 
(
	[pIdRegistro] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [IX_Articulos] UNIQUE NONCLUSTERED 
(
	[CodigoArt] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'''Y'' cuando es verdadero, para comprar.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Articulos', @level2type=N'COLUMN',@level2name=N'CompraArt'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'''Y'' cuando es verdadero, para Vender.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Articulos', @level2type=N'COLUMN',@level2name=N'VentaArt'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'''Y'' cuando es verdadero, para llevar inventario.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Articulos', @level2type=N'COLUMN',@level2name=N'InventarioArt'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Contendra el valor ''Y'' cuando el articulo maneje numeros de series.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Articulos', @level2type=N'COLUMN',@level2name=N'ManejaSeriales'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Contiene el codigo del registro de donde se tomo la ultima actualizacion para este articulo, desde el servidor' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Articulos', @level2type=N'COLUMN',@level2name=N'Documento_Actualizacion'
GO
/****** Object:  Table [dbo].[Almacenes]    Script Date: 08/13/2014 16:06:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Almacenes](
	[IdRegistro] [int] IDENTITY(1,1) NOT NULL,
	[Descripcion] [nvarchar](50) NULL,
	[Codigo_Almacen] [nvarchar](50) NULL,
	[IdSucursal] [int] NOT NULL,
	[Status] [bit] NULL,
 CONSTRAINT [PK_Almacenes_1] PRIMARY KEY CLUSTERED 
(
	[IdRegistro] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Servidor]    Script Date: 08/13/2014 16:06:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Servidor](
	[pIdRegistro] [int] NOT NULL,
	[NOMBRE] [nvarchar](20) NOT NULL,
	[DIRECCION_IP] [nvarchar](30) NOT NULL,
	[BASE_DATOS] [nvarchar](50) NOT NULL,
	[USUARIO] [nvarchar](20) NULL,
	[CLAVE] [nvarchar](20) NULL,
	[TIPO_SERVIDOR] [nvarchar](15) NULL,
	[SAP] [bit] NULL,
	[FECHA] [datetime] NULL,
	[fkUsuario] [int] NULL,
	[Timeout] [nchar](10) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Reportes]    Script Date: 08/13/2014 16:06:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Reportes](
	[Name] [nvarchar](40) NULL,
	[Template] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[RegistrodeCambios]    Script Date: 08/13/2014 16:06:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RegistrodeCambios](
	[Fecha] [datetime] NOT NULL,
	[Usuario] [nvarchar](64) NOT NULL,
	[Modulo] [nvarchar](64) NOT NULL,
	[Accion] [nvarchar](255) NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Planes]    Script Date: 08/13/2014 16:06:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Planes](
	[PlanID] [int] NOT NULL,
	[Descripcion] [nvarchar](50) NULL,
	[fkGrupo] [int] NULL,
	[Comision] [numeric](18, 2) NULL,
	[Requiere_Contrato] [char](1) NULL,
	[Requiere_Equipo] [char](1) NULL,
	[Requiere_Card] [char](1) NULL,
	[Requiere_Sim] [char](1) NULL,
	[Requiere_Pago_A] [char](1) NULL,
	[Item_Card] [nvarchar](15) NULL,
	[Item_Sim] [nvarchar](15) NULL,
	[Item_Pago_A] [nvarchar](15) NULL,
	[Factura_Comision] [char](1) NULL,
 CONSTRAINT [PK_Planes.Configuracion] PRIMARY KEY CLUSTERED 
(
	[PlanID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Y = Aplica N = No aplica' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Planes', @level2type=N'COLUMN',@level2name=N'Factura_Comision'
GO
/****** Object:  Table [dbo].[PagoDetalle]    Script Date: 08/13/2014 16:06:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PagoDetalle](
	[idPagoDetalle] [int] IDENTITY(1,1) NOT NULL,
	[idUsuario] [int] NOT NULL,
	[idCodigoReferencia] [int] NOT NULL,
	[idFormaPago] [int] NOT NULL,
	[FormaPago] [char](1) NOT NULL,
	[Monto] [money] NOT NULL,
	[Fecha] [datetime] NOT NULL,
	[UltimoUsuarioActualizo] [int] NOT NULL,
	[UltimaFechaActualizacion] [datetime] NOT NULL,
 CONSTRAINT [PK_PagoDetalle] PRIMARY KEY CLUSTERED 
(
	[idPagoDetalle] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[NCFnousar]    Script Date: 08/13/2014 16:06:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NCFnousar](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[TIPO] [int] NULL,
	[NUMERICO] [numeric](18, 0) NULL,
	[NCF] [nvarchar](19) NULL,
 CONSTRAINT [PK_NCF] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NCF.Tipos]    Script Date: 08/13/2014 16:06:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[NCF.Tipos](
	[pIdRegistro] [int] NOT NULL,
	[Descripcion] [nvarchar](50) NULL,
	[Estructura] [nvarchar](30) NULL,
	[ManejaEstructura] [char](1) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[NCF.Secuencia]    Script Date: 08/13/2014 16:06:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[NCF.Secuencia](
	[pIdRegistro] [int] NOT NULL,
	[fkTipoNCF] [int] NOT NULL,
	[Numerico] [numeric](18, 0) NULL,
	[Secuencia] [nvarchar](20) NULL,
	[Estatus] [char](1) NULL,
 CONSTRAINT [PK_NCF.Secuencia] PRIMARY KEY CLUSTERED 
(
	[pIdRegistro] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[NCF.Control]    Script Date: 08/13/2014 16:06:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[NCF.Control](
	[ControlNCF] [smallint] NOT NULL,
	[NCF] [varchar](20) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[NC]    Script Date: 08/13/2014 16:06:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[NC](
	[NumeroReq] [int] NULL,
	[NumeroNC] [int] NULL,
	[Monto] [money] NULL,
	[Balance] [money] NULL,
	[Ticket] [int] NULL,
	[RegistroDetalle] [int] NULL,
	[Fecha] [date] NULL,
	[Estado] [char](1) NULL,
	[Razon] [nvarchar](250) NULL,
	[Serie] [nvarchar](32) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MercanciaEntrada.Detalle]    Script Date: 08/13/2014 16:06:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MercanciaEntrada.Detalle](
	[pIdRegistro] [int] IDENTITY(1,1) NOT NULL,
	[fkEntrada] [int] NOT NULL,
	[ArticuloCodigo] [nvarchar](20) NULL,
	[ArticuloDescripcion] [nvarchar](150) NULL,
	[ArticuloCantidad] [numeric](18, 2) NULL,
	[CODIGO_SUCURSAL] [nvarchar](50) NULL,
 CONSTRAINT [PK_MercanciaEntrada.Detalle] PRIMARY KEY CLUSTERED 
(
	[pIdRegistro] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MercanciaEntrada.Cabecera]    Script Date: 08/13/2014 16:06:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MercanciaEntrada.Cabecera](
	[pIdRegistro] [int] NULL,
	[Comentario] [nvarchar](200) NULL,
	[Fecha] [datetime] NULL,
	[fkUsuario] [int] NULL,
	[DocumentoSAP] [int] NULL,
	[CODIGO_SUCURSAL] [nvarchar](50) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Kardex]    Script Date: 08/13/2014 16:06:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Kardex](
	[pIDRegistro] [int] IDENTITY(1,1) NOT NULL,
	[Fecha] [datetime] NULL,
	[CodigoArt] [nvarchar](20) NULL,
	[Descripcion] [nvarchar](128) NULL,
	[DocumentoNo] [int] NULL,
	[ArticuloCantidad] [numeric](18, 0) NULL
) ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[InsertTablaTemporalSeriel]    Script Date: 08/13/2014 16:06:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[InsertTablaTemporalSeriel]
(
	@CodigoArt nvarchar(20),
	@Seriel nvarchar(32),
    @Caja nvarchar(90)
)
AS
BEGIN

--INSERTAR TABLA TEMPORAL ARTICULOS SERIALES
Insert Into dbo.[TempSerial](CodigoArt, Serie, Caja)
Values(@CodigoArt, @Seriel, @Caja)

END
GO
/****** Object:  StoredProcedure [dbo].[InsertTablaTemporal]    Script Date: 08/13/2014 16:06:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[InsertTablaTemporal]
(
	@CodigoArt nvarchar(20),
	@Cantidad numeric(18,2),
    @Almacen nvarchar(8),
    @Caja nvarchar(90)
)
AS
BEGIN

--INSERTAR TABLA TEMPORAL ARTICULOS
Insert Into TempArticulo(CodigoArt, Cantidad, Almacen, Caja)
Values(@CodigoArt, @Cantidad, @Almacen, @Caja)

END
GO
/****** Object:  StoredProcedure [dbo].[INSERTPRECIOSBACK]    Script Date: 08/13/2014 16:06:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[INSERTPRECIOSBACK]
(
	@PlanID INT,
	@CodigoArticulo nvarchar(50),
	@DescripcionPlan nvarchar(50),
	@Costo INT,
	@PRECIOSAP numeric(18,2),
	@fkUsuario INT,
	@Doc_Actualizacion INT	
	
)
AS
BEGIN

DECLARE @PRECIO numeric(18,2)

SET @PRECIO = (SELECT TOP 1 PRECIO FROM dbo.[Articulos.Precios] WHERE fkArticulo = @CodigoArticulo AND PlanID = @PlanID)
	
	IF @PRECIO <> @PRECIOSAP
		BEGIN
			UPDATE 	dbo.[Articulos.Precios] SET PRECIO = @PRECIOSAP WHERE fkArticulo = 	@CodigoArticulo AND PLANID = @PlanID
		END	
     ELSE
		BEGIN
	 INSERT INTO dbo.[Articulos.Precios]
			( PlanID, fkArticulo, DescripcionPlan,Costo,Precio, fkUsuario, FechaCreacion,Doc_Actualizacion)
		    VALUES
			(@PlanID, @CodigoArticulo,@DescripcionPlan,@Costo,@PRECIOSAP, @fkUsuario, GETDATE(),@Doc_Actualizacion)
			
		END
		


END
GO
/****** Object:  StoredProcedure [dbo].[INSERTPRECIOS]    Script Date: 08/13/2014 16:06:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--SELECT  pIdRegistro, Comentario,Fecha,DocumentoSAP
--From dbo.[MercanciaEntrada.Cabecera] 
--WHERE CODIGO_SUCURSAL = 03 AND DocumentoSAP NOT IN(SELECT DocEntry FROM SBO_JMI_PRUEBA.dbo.OWTR)
--

--DECLARE @fkEntrada int
--
--SELECT  DocEntry 'DocumentoSAP' ,'Entrada de Mercancia' 'Comentario',GetDate() 'Fecha'
--FROM SBO_JMI_PRUEBA.dbo.OWTR  WHERE U_CodSucursal = 03 AND DocEntry 
--NOT IN (SELECT DocumentoSap FROM dbo.[MercanciaEntrada.Cabecera])

CREATE PROCEDURE [dbo].[INSERTPRECIOS]
(
	@CodigoArticulo nvarchar(50),
	@PRECIOSAP numeric(18,2),
	@PlanID INT,
	@DescripcionPlan nvarchar(50),
	@fkUsuario int
)
AS
BEGIN

DECLARE @PRECIO numeric(18,2)


SET @PRECIO = (SELECT TOP 1 PRECIO FROM dbo.[Articulos.Precios] WHERE fkArticulo = @CodigoArticulo AND PlanID = @PlanID)
	
	IF (@PRECIO <= @PRECIOSAP OR @PRECIO >=@PRECIOSAP)
		BEGIN
			UPDATE 	dbo.[Articulos.Precios] SET PRECIO = @PRECIOSAP WHERE fkArticulo = 	@CodigoArticulo AND PLANID = @PlanID
		END

--		ELSE
--		BEGIN
--			INSERT dbo.[Articulos.Precios] (PlanID, DescripcionPlan,fkArticulo, Precio, fkUsuario, FechaCreacion)
--		Select @PlanID, @DescripcionPlan,@CodigoArticulo, @PRECIOSAP, @fkUsuario, GetDate() 
--		Where @CodigoArticulo NOT IN(select * from dbo.[Articulos.Precios])
--		END
	
		If Not Exists(select fkArticulo from dbo.[Articulos.Precios] where fkArticulo=@CodigoArticulo and PlanID=@PlanID)
		Begin
		INSERT dbo.[Articulos.Precios] (PlanID, DescripcionPlan,fkArticulo, Precio, fkUsuario, FechaCreacion) 
		VALUES (@PlanID, @DescripcionPlan,@CodigoArticulo, @PRECIOSAP, @fkUsuario, GetDate())
		End		



END
GO
/****** Object:  StoredProcedure [dbo].[InsertarSerialEntradaFromDbComun]    Script Date: 08/13/2014 16:06:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[InsertarSerialEntradaFromDbComun]
(
	@SerialNo nvarchar(32),
	@fkIdArticulo nvarchar(20),
	@fkEntrada int,
    @Estatus nvarchar(1),
    @Codigo_Almacen nvarchar(8)
)
AS
BEGIN

--INSERTAR SERIALES ENTRADA
INSERT INTO [dbo].[Articulos.Seriales] (SerialNo, fkIdArticulo,fkEntrada,Estatus,Codigo_Almacen) 
                                values(@SerialNo, @fkIdArticulo, @fkEntrada, @Estatus, @Codigo_Almacen)
END
GO
/****** Object:  StoredProcedure [dbo].[InsertarPrecioFromDbComun]    Script Date: 08/13/2014 16:06:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[InsertarPrecioFromDbComun]
(
	@PlanID int,
	@fkArticulo nvarchar(50),
	@DescripcionPlan nvarchar(50),
    @Precio numeric(18,2),
    @fkUsuario int,
    @FechaCreacion datetime,
    @Doc_Actualizacion int
)
AS
BEGIN

--INSERTAR LISTA DE PRECIOS
INSERT INTO [dbo].[Articulos.Precios] (PlanID, fkArticulo,DescripcionPlan,Precio, fkUsuario,FechaCreacion, Doc_Actualizacion) 
                                values(@PlanID, @fkArticulo, @DescripcionPlan, @Precio, @fkUsuario,@FechaCreacion, @Doc_Actualizacion)
END
GO
/****** Object:  StoredProcedure [dbo].[InsertarDetEntradaFromDbComun]    Script Date: 08/13/2014 16:06:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[InsertarDetEntradaFromDbComun]
(
	@fkEntrada int,
	@ArticuloCodigo nvarchar(20),
	@ArticuloDescripcion nvarchar(100),
    @ArticuloCantidad numeric(18,2),
    @CODIGO_SUCURSAL nvarchar(20)
)
AS
BEGIN

--INSERTAR DETALLE ENTRADA
INSERT INTO [dbo].[MercanciaEntrada.Detalle] (fkEntrada, ArticuloCodigo,ArticuloDescripcion,ArticuloCantidad,CODIGO_SUCURSAL) 
                                values(@fkEntrada, @ArticuloCodigo, @ArticuloDescripcion, @ArticuloCantidad, @CODIGO_SUCURSAL)
END
GO
/****** Object:  StoredProcedure [dbo].[InsertarCabEntradaFromDbComun]    Script Date: 08/13/2014 16:06:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[InsertarCabEntradaFromDbComun]
(
	@pIdRegistro int,
	@Comentario nvarchar(200),
	@Fecha datetime,
    @fkUsuario int,
    @DocumentoSAP int,
    @CODIGO_SUCURSAL nvarchar(20)
)
AS
BEGIN

--INSERTAR CABECERA ENTRADA
INSERT INTO [dbo].[MercanciaEntrada.Cabecera] (pIdRegistro, Comentario,Fecha,fkUsuario, DocumentoSAP,CODIGO_SUCURSAL) 
                                values(@pIdRegistro, @Comentario, @Fecha, @fkUsuario, @DocumentoSAP,@CODIGO_SUCURSAL)
END
GO
/****** Object:  Table [dbo].[FacturaPOS.Cabecera]    Script Date: 08/13/2014 16:06:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FacturaPOS.Cabecera](
	[pIdRegistro] [int] NOT NULL,
	[ClienteId] [nvarchar](20) NULL,
	[ClienteNombre] [nvarchar](100) NULL,
	[ClienteRnc] [nvarchar](20) NULL,
	[DocumentoFecha] [datetime] NULL,
	[DocumentoServidor] [char](1) NULL,
	[fkSucursal] [int] NULL,
	[fkUsuario] [int] NULL,
	[fkCaja] [int] NULL,
	[fkRegistroCaja] [int] NULL,
	[FormaPago] [char](1) NULL,
	[SubTotal] [numeric](19, 6) NULL,
	[TotalDesc] [numeric](19, 6) NULL,
	[TotalItbis] [numeric](19, 6) NULL,
	[Efectivo] [numeric](19, 6) NULL,
	[Cheque] [numeric](19, 6) NULL,
	[NoCheque] [numeric](18, 0) NULL,
	[Tarjeta] [numeric](19, 6) NULL,
	[NoTarjeta] [numeric](18, 0) NULL,
	[Ncredito] [numeric](19, 6) NULL,
	[Codigo_Almacen] [nvarchar](8) NULL,
	[MontoGeneral] [numeric](19, 6) NULL,
	[Codigo_Vendedor] [nchar](10) NULL,
	[NombrePC] [nvarchar](90) NULL,
	[fkTipoNCF] [int] NULL,
	[MOntoEfectivo] [money] NULL,
	[MontoDevuelta] [money] NULL,
	[PagoEfectivo] [money] NULL,
	[PagoCheque] [money] NULL,
	[PagoTarjetaCredito] [money] NULL,
	[PagoTarjetaDebito] [money] NULL,
	[PagoNotaCredito] [money] NULL,
 CONSTRAINT [PK_FacturaPOS.Cabecera] PRIMARY KEY CLUSTERED 
(
	[pIdRegistro] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Este campo tendra ''Y'' cuando el documento se alla enviado a SAP.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FacturaPOS.Cabecera', @level2type=N'COLUMN',@level2name=N'DocumentoServidor'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Este es el codigo de la caja que se genera cada vez que se abre una caja nueva, Util para el cierre de la caja y el total de ventas.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FacturaPOS.Cabecera', @level2type=N'COLUMN',@level2name=N'fkRegistroCaja'
GO
/****** Object:  View [dbo].[vw_prueba]    Script Date: 08/13/2014 16:06:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_prueba]
AS
SELECT     TOP (100) PERCENT dbo.Usuarios.pIdRegistro AS [Usuario.pidregistro], dbo.Usuarios.fkSucursal, dbo.Usuarios.Nombre, dbo.Usuarios.Apellido1, 
                      dbo.Usuarios.Apellido2, dbo.Usuarios.Usuario, dbo.Usuarios.Clave, dbo.Usuarios.Fecha, dbo.Usuarios.Bloqueo, dbo.Usuarios.DescripcionCaja, 
                      dbo.Usuarios.NombrePC, dbo.Usuarios.Conectado, dbo.Usuarios.DescripcionAlmacen, dbo.Usuarios.IdAlmacen, dbo.[System.Privilegios].Privilegio
FROM         dbo.Usuarios CROSS JOIN
                      dbo.[System.Privilegios]
ORDER BY [Usuario.pidregistro]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[41] 4[20] 2[11] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Usuarios"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 222
               Right = 223
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "System.Privilegios"
            Begin Extent = 
               Top = 6
               Left = 261
               Bottom = 125
               Right = 421
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 18
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vw_prueba'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vw_prueba'
GO
/****** Object:  View [dbo].[vw_UsuariosPrivilegios]    Script Date: 08/13/2014 16:06:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_UsuariosPrivilegios]
AS
SELECT     CASE WHEN [Usuario.Privilegios].Privilegio IS NULL THEN '0' ELSE '1' END AS Asignado, derivada.[Usuario.pidregistro], derivada.fkSucursal, derivada.Nombre, 
                      derivada.Apellido1, derivada.Apellido2, derivada.Usuario, derivada.Clave, derivada.Fecha, derivada.Bloqueo, derivada.DescripcionCaja, derivada.NombrePC, 
                      derivada.Conectado, derivada.DescripcionAlmacen, derivada.IdAlmacen, derivada.Privilegio, derivada.Descripcion, derivada.Orden
FROM         dbo.[Usuario.Privilegios] RIGHT OUTER JOIN
                          (SELECT     dbo.Usuarios.pIdRegistro AS [Usuario.pidregistro], dbo.Usuarios.fkSucursal, dbo.Usuarios.Nombre, dbo.Usuarios.Apellido1, dbo.Usuarios.Apellido2, 
                                                   dbo.Usuarios.Usuario, dbo.Usuarios.Clave, dbo.Usuarios.Fecha, dbo.Usuarios.Bloqueo, dbo.Usuarios.DescripcionCaja, dbo.Usuarios.NombrePC, 
                                                   dbo.Usuarios.Conectado, dbo.Usuarios.DescripcionAlmacen, dbo.Usuarios.IdAlmacen, dbo.[System.Privilegios].Privilegio, 
                                                   dbo.[System.Privilegios].Descripcion, dbo.[System.Privilegios].Orden
                            FROM          dbo.Usuarios CROSS JOIN
                                                   dbo.[System.Privilegios]) AS derivada ON dbo.[Usuario.Privilegios].[Usuario.PidRegistro] = derivada.[Usuario.pidregistro] AND 
                      dbo.[Usuario.Privilegios].Privilegio = derivada.Privilegio
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[25] 4[16] 2[25] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Usuario.Privilegios"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 95
               Right = 221
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "derivada"
            Begin Extent = 
               Top = 6
               Left = 259
               Bottom = 125
               Right = 442
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 19
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vw_UsuariosPrivilegios'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vw_UsuariosPrivilegios'
GO
/****** Object:  View [dbo].[vw_Usuarios_Privilegios]    Script Date: 08/13/2014 16:06:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_Usuarios_Privilegios]
AS
SELECT     dbo.Usuarios.pIdRegistro, dbo.Usuarios.fkSucursal, dbo.Usuarios.Nombre, dbo.Usuarios.Apellido1, dbo.Usuarios.Apellido2, dbo.Usuarios.Usuario, 
                      dbo.Usuarios.Clave, dbo.Usuarios.Fecha, dbo.Usuarios.Bloqueo, dbo.Usuarios.DescripcionCaja, dbo.Usuarios.IdAlmacen, dbo.Usuarios.DescripcionAlmacen, 
                      dbo.Usuarios.Conectado, dbo.Usuarios.NombrePC, dbo.[Usuario.Privilegios].Privilegio
FROM         dbo.[Usuario.Privilegios] RIGHT OUTER JOIN
                      dbo.Usuarios ON dbo.[Usuario.Privilegios].[Usuario.PidRegistro] = dbo.Usuarios.pIdRegistro
GO
/****** Object:  View [dbo].[v_Articulos_Seriales]    Script Date: 08/13/2014 16:06:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[v_Articulos_Seriales]
AS
SELECT     SerialNo, fkIdArticulo, Codigo_Almacen, Estatus
FROM         dbo.[Articulos.Seriales]
WHERE     (Estatus = 'D')
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[41] 4[20] 2[8] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Articulos.Seriales"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 114
               Right = 197
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'v_Articulos_Seriales'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'v_Articulos_Seriales'
GO
/****** Object:  View [dbo].[v_Articulos]    Script Date: 08/13/2014 16:06:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[v_Articulos]
AS
SELECT     dbo.Articulos.CodigoArt, dbo.Articulos.DescripcionArt, dbo.Articulos.GrupoArt, dbo.Articulos.CodigoBarras, dbo.Articulos.EnMano, dbo.Articulos.CompraArt, 
                      dbo.Articulos.VentaArt, dbo.Articulos.InventarioArt, dbo.Articulos.ITBIS, dbo.Articulos.ManejaSeriales, dbo.Articulos.Codigo_Almacen, dbo.[Articulos.Precios].Precio, 
                      dbo.[Articulos.Precios].PlanID, dbo.Planes.Comision, dbo.Planes.Requiere_Contrato, dbo.Planes.Requiere_Sim, dbo.Planes.Requiere_Pago_A, dbo.Planes.Item_Sim, 
                      dbo.Planes.Item_Pago_A, dbo.Planes.Factura_Comision, dbo.Impuestos.Factor, dbo.[Articulos.Precios].Costo
FROM         dbo.Planes INNER JOIN
                      dbo.[Articulos.Precios] ON dbo.Planes.PlanID = dbo.[Articulos.Precios].PlanID RIGHT OUTER JOIN
                      dbo.Articulos ON dbo.[Articulos.Precios].fkArticulo = dbo.Articulos.CodigoArt CROSS JOIN
                      dbo.Impuestos
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Articulos"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 192
               Right = 240
            End
            DisplayFlags = 280
            TopColumn = 8
         End
         Begin Table = "Articulos.Precios"
            Begin Extent = 
               Top = 6
               Left = 278
               Bottom = 125
               Right = 441
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Planes"
            Begin Extent = 
               Top = 0
               Left = 534
               Bottom = 259
               Right = 715
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Impuestos"
            Begin Extent = 
               Top = 6
               Left = 753
               Bottom = 137
               Right = 913
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
   ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'v_Articulos'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'   End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'v_Articulos'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'v_Articulos'
GO
/****** Object:  StoredProcedure [dbo].[UpdateAumStockFromDbComun]    Script Date: 08/13/2014 16:06:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UpdateAumStockFromDbComun]
(
	@CodigoArticulo nvarchar(20),
	@EnMano numeric(18,2),
	@Codigo_Almacen nvarchar(20)
    
)
AS
BEGIN

Update [dbo].[Articulos]  
	Set 
		EnMano = Isnull(EnMano,0) + (@EnMano)
 
Where 
	CodigoArt = @CodigoArticulo and
    Codigo_Almacen= @Codigo_Almacen 
END
GO
/****** Object:  StoredProcedure [dbo].[UpdateArticulosFromDbComun]    Script Date: 08/13/2014 16:06:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UpdateArticulosFromDbComun]
(
	@CodigoArticulo nvarchar(20),
	@descripcionArt nvarchar(100),
	@grupoArt int,
    @codigoBarras nvarchar(50),
    @compraArt nvarchar(1),
    @ventaArt nvarchar(1),
    @InventarioArt nvarchar(1),
    @ITBIS nvarchar(1),
    @ManejaSeriales nvarchar(1),
    @DescripcionEtiqueta nvarchar(50),
    @FechaModificacion datetime,
    @Doc_actualizacion int
)
AS
BEGIN

Update [dbo].[Articulos]  
	Set 
		DescripcionArt = @descripcionArt,
		GrupoArt = @grupoArt,
        CodigoBarras=@codigoBarras,
        CompraArt= @compraArt,
        VentaArt=@ventaArt,
        InventarioArt=@InventarioArt,
        ITBIS=@ITBIS,
        ManejaSeriales=@ManejaSeriales,
        DescripcionEtiqueta=@DescripcionEtiqueta,
        FechaModificacion=@FechaModificacion,
        Doc_actualizacion=@Doc_actualizacion
Where 
	CodigoArt = @CodigoArticulo 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_getAlmacenes]    Script Date: 08/13/2014 16:06:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_getAlmacenes]
(
	@IdSurcusal int
)
AS
BEGIN

SELECT IdRegistro AS ID,c.codigo_Almacen as Codigo,c.Descripcion,c.status, a.codigo_Almacen as Almacen_Sucursal from sucursal a 
inner join  almacenes c
on a.pkidregistro = c.IdSucursal
WHERE C.IdSucursal = @IdSurcusal and status = 'True'

END
GO
/****** Object:  StoredProcedure [dbo].[sp_GetAlmacenByUsuario]    Script Date: 08/13/2014 16:06:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_GetAlmacenByUsuario]
(
	@Usuario nvarchar(50),
	@Clave nvarchar(50)	
)
AS
BEGIN

SELECT  AL.Codigo_Almacen,AL.Descripcion, s.codigo_almacen Codigo_Sucursal --T0.pIdRegistro, T0.Nombre + ' ' + T0.Apellido1 AS Nombre, T0.Usuario, T1.VENTAS, T0.DescripcionCaja, T1.ADMINISTRAR, T1.INVENTARIO, T1.REPORTES, 
        --T1.CONSULTAS, T1.SINCRONIZAR, 
FROM  dbo.Usuarios AS T0 INNER JOIN
dbo.Usuarios_1 AS T1 ON T0.pIdRegistro = T1.fkUsuario AND T0.Bloqueo = 'false' INNER JOIN
dbo.Almacenes AL ON T0.IdAlmacen = AL.Codigo_Almacen inner join sucursal  s on (al.idsucursal =  s.pkidRegistro)
Where T0.Usuario =@Usuario And T0.Clave = @Clave
     --AND T0.DescripcionAlmacen = 'JMI'
END
GO
/****** Object:  StoredProcedure [dbo].[sp_getAlmacenById]    Script Date: 08/13/2014 16:06:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_getAlmacenById]
(
	@IdRegistro int
)
AS
BEGIN

SELECT IdRegistro as ID,Descripcion,Codigo_Almacen,Status from dbo.almacenes where IdRegistro = @IdRegistro

END
GO
/****** Object:  View [dbo].[vw_Kardex]    Script Date: 08/13/2014 16:06:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_Kardex]
AS
SELECT     dbo.Kardex.Fecha, dbo.Kardex.CodigoArt, dbo.Articulos.DescripcionArt, dbo.Kardex.DocumentoNo, dbo.Kardex.ArticuloCantidad, dbo.Articulos.ManejaSeriales, 
                      dbo.Articulos.Codigo_Almacen
FROM         dbo.Kardex INNER JOIN
                      dbo.Articulos ON dbo.Kardex.CodigoArt = dbo.Articulos.CodigoArt
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Kardex"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 125
               Right = 206
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Articulos"
            Begin Extent = 
               Top = 43
               Left = 587
               Bottom = 162
               Right = 798
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vw_Kardex'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vw_Kardex'
GO
/****** Object:  View [dbo].[vw_Clientes]    Script Date: 08/13/2014 16:06:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_Clientes]
AS
SELECT     dbo.Clientes.ClienteID, dbo.Clientes.Nombre, dbo.Clientes.TipoCliente, dbo.Clientes.Direccion, dbo.Clientes.RNC, dbo.Clientes.Generico, 
                      dbo.TarjetaDescuento.TarjetaNo, ISNULL(dbo.TarjetaDescuento.Descuento, 0) AS Descuento, dbo.TarjetaDescuento.Fecha_Expiracion
FROM         dbo.Clientes LEFT OUTER JOIN
                      dbo.TarjetaDescuento ON dbo.Clientes.ClienteID = dbo.TarjetaDescuento.ClientID
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[41] 4[20] 2[11] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Clientes"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 125
               Right = 198
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "TarjetaDescuento"
            Begin Extent = 
               Top = 6
               Left = 335
               Bottom = 125
               Right = 508
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 11
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vw_Clientes'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vw_Clientes'
GO
/****** Object:  View [dbo].[vw_CajaEstado]    Script Date: 08/13/2014 16:06:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_CajaEstado]
AS
SELECT     FechaApertura, FechaCierre, MontoApertura, MontoCierre, EstadoCaja, NombrePC
FROM         dbo.[Caja.AbrirCerrar]
WHERE     (CONVERT(date, FechaApertura) = CONVERT(date, GETDATE()))
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Caja.AbrirCerrar"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 125
               Right = 200
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vw_CajaEstado'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vw_CajaEstado'
GO
/****** Object:  View [dbo].[vw_ArticulosSeriales]    Script Date: 08/13/2014 16:06:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_ArticulosSeriales]
AS
SELECT     dbo.[Articulos.Seriales].No_Linea, dbo.[Articulos.Seriales].Codigo_Almacen, dbo.[Articulos.Seriales].fkFacturaVenta, dbo.[Articulos.Seriales].Estatus, 
                      dbo.[Articulos.Seriales].fkIdArticulo, dbo.[Articulos.Seriales].SerialNo, dbo.[Articulos.Seriales].pIdRegistro, dbo.Articulos.DescripcionArt, dbo.[Articulos.Precios].Precio, 
                      dbo.[Articulos.Precios].PlanID
FROM         dbo.[Articulos.Precios] INNER JOIN
                      dbo.Articulos ON dbo.[Articulos.Precios].fkArticulo = dbo.Articulos.CodigoArt RIGHT OUTER JOIN
                      dbo.[Articulos.Seriales] ON dbo.Articulos.Codigo_Almacen = dbo.[Articulos.Seriales].Codigo_Almacen AND dbo.Articulos.CodigoArt = dbo.[Articulos.Seriales].fkIdArticulo
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Articulos.Seriales"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 199
               Right = 206
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Articulos"
            Begin Extent = 
               Top = 0
               Left = 345
               Bottom = 199
               Right = 556
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Articulos.Precios"
            Begin Extent = 
               Top = 6
               Left = 594
               Bottom = 199
               Right = 769
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 11
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vw_ArticulosSeriales'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vw_ArticulosSeriales'
GO
/****** Object:  View [dbo].[vw_RPT_Inventario]    Script Date: 08/13/2014 16:06:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_RPT_Inventario]
AS
SELECT     TOP (100) PERCENT dbo.Articulos.CodigoArt, dbo.Articulos.DescripcionArt, dbo.Articulos.EnMano, ArticulosSerialesDisponibles.SerialNo
FROM         (SELECT     pIdRegistro, No_Linea, SerialNo, fkIdArticulo, fkEntrada, Estatus, fkFacturaVenta, Codigo_Almacen, Cantidad
                       FROM          dbo.[Articulos.Seriales]
                       WHERE      (Estatus = 'D')) AS ArticulosSerialesDisponibles RIGHT OUTER JOIN
                      dbo.Articulos ON ArticulosSerialesDisponibles.fkIdArticulo = dbo.Articulos.CodigoArt
WHERE     (dbo.Articulos.EnMano > 0)
ORDER BY dbo.Articulos.CodigoArt
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[41] 4[20] 2[22] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = -1190
      End
      Begin Tables = 
         Begin Table = "ArticulosSerialesDisponibles"
            Begin Extent = 
               Top = 58
               Left = 1578
               Bottom = 177
               Right = 1746
            End
            DisplayFlags = 280
            TopColumn = 5
         End
         Begin Table = "Articulos"
            Begin Extent = 
               Top = 6
               Left = 1228
               Bottom = 185
               Right = 1439
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 10
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vw_RPT_Inventario'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vw_RPT_Inventario'
GO
/****** Object:  View [dbo].[vw_RegistrodeCambios]    Script Date: 08/13/2014 16:06:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_RegistrodeCambios]
AS
SELECT     dbo.RegistrodeCambios.Fecha, dbo.RegistrodeCambios.Usuario, dbo.RegistrodeCambios.Modulo, dbo.RegistrodeCambios.Accion, dbo.Usuarios.Nombre
FROM         dbo.RegistrodeCambios LEFT OUTER JOIN
                      dbo.Usuarios ON dbo.RegistrodeCambios.Usuario = dbo.Usuarios.Usuario
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "RegistrodeCambios"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 125
               Right = 198
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Usuarios"
            Begin Extent = 
               Top = 6
               Left = 236
               Bottom = 254
               Right = 421
            End
            DisplayFlags = 280
            TopColumn = 2
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vw_RegistrodeCambios'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vw_RegistrodeCambios'
GO
/****** Object:  StoredProcedure [dbo].[ValidationDesc]    Script Date: 08/13/2014 16:06:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ValidationDesc]
(
	@Usuario varchar(30),
	@Clave varchar(30)
)
AS
BEGIN
SELECT us1.descuento from Usuarios as us
INNER JOIN usuarios_1 as us1 on us.Pidregistro = us1.fkUsuario
WHERE us.Usuario = @Usuario AND us.Clave = @Clave
END
GO
/****** Object:  StoredProcedure [dbo].[ValidationCambioAlmacen]    Script Date: 08/13/2014 16:06:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ValidationCambioAlmacen]
(
	@Usuario varchar(30),
	@Clave varchar(30)
)
AS
BEGIN
SELECT us1.CambioAlmacen from Usuarios as us
INNER JOIN usuarios_1 as us1 on us.Pidregistro = us1.fkUsuario
WHERE us.Usuario = @Usuario AND us.Clave = @Clave
END
GO
/****** Object:  StoredProcedure [dbo].[ValidarStockTablaTemporal]    Script Date: 08/13/2014 16:06:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ValidarStockTablaTemporal]
(
	@Caja nvarchar(90)
)
AS
BEGIN
--SELECCIONAR CAMPOS
Select CodigoArt, Sum(Cantidad) Cantidad, Almacen
From dbo.[TempArticulo]
Where 
Caja=@Caja
Group by CodigoArt, Almacen

END
GO
/****** Object:  StoredProcedure [dbo].[ValidarSerialTablaTemporal]    Script Date: 08/13/2014 16:06:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ValidarSerialTablaTemporal]
(
 @Caja nvarchar(90)
)
AS
BEGIN
--SELECCIONAR CAMPOS
Select Serie,  Count(Serie) Cantidad
From dbo.[TempSerial]
Where 
Caja=@Caja
Group by Serie

END
GO
/****** Object:  View [dbo].[v_Lista_Usuarios]    Script Date: 08/13/2014 16:06:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[v_Lista_Usuarios]
AS
SELECT     T0.pIdRegistro, T0.Nombre, T0.Usuario, T1.VENTAS, T1.ADMINISTRAR, T1.INVENTARIO, T1.REPORTES, T1.CONSULTAS, T0.DescripcionCaja, T1.DESCUENTO, 
                      T1.CambioAlmacen, T1.SINCRONIZAR, T0.IdAlmacen
FROM         dbo.Usuarios AS T0 INNER JOIN
                      dbo.Usuarios_1 AS T1 ON T0.pIdRegistro = T1.fkUsuario
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "T0"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 114
               Right = 194
            End
            DisplayFlags = 280
            TopColumn = 8
         End
         Begin Table = "T1"
            Begin Extent = 
               Top = 6
               Left = 232
               Bottom = 114
               Right = 387
            End
            DisplayFlags = 280
            TopColumn = 6
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'v_Lista_Usuarios'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'v_Lista_Usuarios'
GO
/****** Object:  StoredProcedure [dbo].[spInsertArticulosNuevos]    Script Date: 08/13/2014 16:06:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spInsertArticulosNuevos]
(
	   @CodigoArt nvarchar(20)
      ,@DescripcionArt nvarchar(100)
      ,@GrupoArt int
      ,@CodigoBarras nvarchar(50)
      ,@EnMano numeric(18, 2)
      ,@CompraArt char(1)
	  ,@VentaArt char(1)
      ,@InventarioArt char(1)
      ,@ITBIS char(1)
      ,@ManejaSeriales nchar(1)
      ,@DescripcionEtiqueta nvarchar(20)
      ,@Codigo_Almacen nvarchar(50)
      ,@FechaActualizacion datetime
      ,@Doc_Actualizacion int
)
AS
BEGIN
INSERT INTO dbo.[Articulos](
       [CodigoArt]
      ,[DescripcionArt]
      ,[GrupoArt]
      ,[CodigoBarras]
      ,[EnMano]
      ,[CompraArt]
	  ,[VentaArt]
      ,[InventarioArt]
      ,[ITBIS]
      ,[ManejaSeriales]
      ,[DescripcionEtiqueta]
      ,[Codigo_Almacen]
      ,[FechaActualizacion]
      ,[Documento_Actualizacion])

VALUES(
       @CodigoArt
      ,@DescripcionArt
      ,@GrupoArt
      ,@CodigoBarras
      ,@EnMano
      ,@CompraArt
	  ,@VentaArt
      ,@InventarioArt
      ,@ITBIS
      ,@ManejaSeriales
      ,@DescripcionEtiqueta
      ,@Codigo_Almacen
      ,@FechaActualizacion
      ,@Doc_Actualizacion)      
 
END
GO
/****** Object:  StoredProcedure [dbo].[UpdateDisStockFromDbComun]    Script Date: 08/13/2014 16:06:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UpdateDisStockFromDbComun]
(
	@CodigoArticulo nvarchar(20),
	@EnMano numeric(18,2),
	@Codigo_Almacen nvarchar(20)
    
)
AS
BEGIN

Update [dbo].[Articulos]  
	Set 
		EnMano = Isnull(EnMano,0) - (@EnMano)
 
Where 
	CodigoArt = @CodigoArticulo and
    Codigo_Almacen= @Codigo_Almacen 
END
GO
/****** Object:  StoredProcedure [dbo].[BorrarArticuloPrecioPV]    Script Date: 08/13/2014 16:06:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[BorrarArticuloPrecioPV]
(
	@PlanID int,
	@fkArticulo nvarchar(50)	
)
AS
BEGIN

--BORRAR ARTICULO DE LISTA DE PRECIOS
DELETE FROM dbo.[Articulos.Precios] WHERE PlanID=@PlanID and fkArticulo = @fkArticulo 
END
GO
/****** Object:  StoredProcedure [dbo].[GetRegistroCaja]    Script Date: 08/13/2014 16:06:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetRegistroCaja]
(
	@Fecha datetime,
    @Caja nvarchar(90)	
) 
AS
BEGIN

SELECT fkUsuario, FechaApertura, FechaCierre, MontoApertura, MontoCierre, EstadoCaja, NombrePC
FROM
dbo.[Caja.AbrirCerrar]
WHERE
FechaApertura=@Fecha and NombrePC=@Caja

END
GO
/****** Object:  StoredProcedure [dbo].[sp_ActualizaBalanceNC]    Script Date: 08/13/2014 16:06:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Amaury Minaya
-- Create date: 2014-05-05
-- Description:	Actualiza Balance de Nota de Credito
-- =============================================
CREATE PROCEDURE [dbo].[sp_ActualizaBalanceNC]
	@NumeroReq Numeric(19,6),
	@Monto Numeric(19,6)
AS
BEGIN
	DECLARE @Balance Money
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Update NC SET Balance=0 WHERE numeroReq=@numeroReq
	--RETURN (@balance)
END
GO
/****** Object:  StoredProcedure [dbo].[sp_ConsultaNC]    Script Date: 08/13/2014 16:06:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Amaury Minaya
-- Create date: 2014-05-05
-- Description:	Retorna Balance de Nota de Credito
-- =============================================
CREATE PROCEDURE [dbo].[sp_ConsultaNC]
	@NumeroReq Numeric(19,6)
AS
BEGIN
	DECLARE @Balance Money
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT @Balance = balance FROM NC WHERE numeroReq=@numeroReq
	RETURN (@balance)
END
GO
/****** Object:  StoredProcedure [dbo].[sp_CajaAutorizada]    Script Date: 08/13/2014 16:06:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Amaury Minaya
-- Create date: 2013-11-21
-- Description:	Verifica si la caja esta autoriza en la tabla CAJA
-- =============================================
CREATE PROCEDURE [dbo].[sp_CajaAutorizada]
	@NombrePC nvarchar(90)
AS
BEGIN
	DECLARE @resultado int; 
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- Verifica si la caja esta en la tabla de CAJAS
	SELECT @resultado= count(*) FROM Caja WHERE nombrePC=@NombrePC
	
	RETURN (@resultado)

END
GO
/****** Object:  StoredProcedure [dbo].[sp_Caja]    Script Date: 08/13/2014 16:06:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Amaury Minaya
-- Create date: 2014-03-17
-- Description:	Cierra o Abre Caja de Venta
-- =============================================
CREATE PROCEDURE [dbo].[sp_Caja]
	@NombrePC nvarchar(90),
	@Opcion nvarchar(64),
	@Monto numeric(18,6)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- Verifica si la caja esta en la tabla de CAJAS
	--SELECT @resultado= count(*) FROM Caja WHERE nombrePC=@NombrePC
	IF @opcion='ABRIR'  
	BEGIN
		insert into [caja.abrircerrar] values (  (select max(pidregistro)+1 from [caja.abrircerrar]),3,getdate(),NULL,@Monto,0,'A',@NombrePC)
	END
	IF @Opcion='CERRAR'
	BEGIN
		Update [caja.abrircerrar] set fechacierre=getdate(),MontoCierre=@Monto, EstadoCaja='C' WHERE (NombrePC=@NombrePC and EstadoCaja='A')
	END
END
GO
/****** Object:  StoredProcedure [dbo].[sp_ArticulosNuevos]    Script Date: 08/13/2014 16:06:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_ArticulosNuevos]
(
	
	@CodigoArt nvarchar(10),
	@DescripcionArt nvarchar(50),
	@GrupoArt int, 
	@CodigoBarras  nvarchar(50),
	@CompraArt char(1),
	@VentaArt  char(1),
	@InventarioArt char(1),
	@ITBIS  char(1),
	@ManejaSeriales nchar(1),
	@DescripcionEtiqueta nvarchar(20),
	@CODIGO_SUCURSAL nvarchar(20)
)
AS

BEGIN

--IF NOT EXISTS(SELECT CodigoArt FROM dbo.[Articulos] WHERE CodigoArt=@CodigoArt and @CODIGO_SUCURSAL NOT IN(SELECT Codigo_Almacen From DB_PUNTO_VENTA.dbo.Almacenes))
--	BEGIN
		INSERT INTO dbo.[Articulos]
        (CodigoArt, DescripcionArt, GrupoArt, CodigoBarras,CompraArt, VentaArt, InventarioArt, ITBIS, ManejaSeriales, DescripcionEtiqueta,Codigo_Almacen)
         SELECT 
			
			@CodigoArt, 
			@DescripcionArt, 
			@GrupoArt, 
			@CodigoBarras,
			@CompraArt, 
			@VentaArt, 
			@InventarioArt, 
			@ITBIS, 
			@ManejaSeriales, 
			@DescripcionEtiqueta,
			@CODIGO_SUCURSAL
			WHERE @CODIGO_SUCURSAL IN(SELECT Codigo_Almacen From DB_PUNTO_VENTA.dbo.Almacenes)

	--END
END


--INSERT INTO dbo.[Articulos]
--        (CodigoArt, DescripcionArt, GrupoArt, CodigoBarras,CompraArt, VentaArt, InventarioArt, ITBIS, ManejaSeriales, DescripcionEtiqueta,Codigo_Almacen)
--		SELECT
--			
--			@CodigoArt, 
--			@DescripcionArt, 
--			@GrupoArt, 
--			@CodigoBarras,
--			@CompraArt, 
--			@VentaArt, 
--			@InventarioArt, 
--			@ITBIS, 
--			@ManejaSeriales, 
--			@DescripcionEtiqueta,
--			@CODIGO_SUCURSAL
--			WHERE @CODIGO_SUCURSAL IN(SELECT Codigo_Almacen From DB_PUNTO_VENTA.dbo.Almacenes)
GO
/****** Object:  StoredProcedure [dbo].[sp_ActualizarSeriales]    Script Date: 08/13/2014 16:06:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_ActualizarSeriales]
(
	@CodigoArticulo nvarchar(50),
	@SerialNo nvarchar(50),
    @Almacen nvarchar(50)
)
AS
BEGIN

Update [DB_PUNTO_VENTA].[dbo].[Articulos.Seriales]  Set Estatus='U' Where fkIdArticulo= @CodigoArticulo 
And SerialNo= @SerialNo And Codigo_Almacen=@Almacen

END
GO
/****** Object:  StoredProcedure [dbo].[sp_ActualizaKardex]    Script Date: 08/13/2014 16:06:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Amaury Minaya
-- Create date: 2014-07-13
-- Description:	Actualiza Kardex de Inventario
-- =============================================
CREATE PROCEDURE [dbo].[sp_ActualizaKardex]
	(
	@CodigoArt nvarchar(20),
    @Descripcion nvarchar(128),
    @DocumentoNo int,
    @ArticuloCantidad numeric(18,0)
	)
AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRAN 
		INSERT INTO [Kardex]
		([Fecha]
		,[CodigoArt]
        ,[Descripcion]
        ,[DocumentoNo]
        ,[ArticuloCantidad])
		VALUES
        (GetDate()
        ,@CodigoArt
        ,@Descripcion
        ,@DocumentoNo
        ,@ArticuloCantidad)	
END

IF (@@error <>0)
BEGIN
	ROLLBACK TRAN
	RETURN
END
COMMIT TRAN
GO
/****** Object:  StoredProcedure [dbo].[sp_UpdatePreciosFromDbComun]    Script Date: 08/13/2014 16:06:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_UpdatePreciosFromDbComun]
(
	@PlanID INT,
	@CodigoArticulo nvarchar(50),
	@DescripcionPlan nvarchar(50),
	@Costo INT,
	@PRECIOSAP numeric(18,2),
	@fkUsuario int,
	@Doc_Actualizacion INT
)
AS
BEGIN

DECLARE @PRECIO numeric(18,2)


SET @PRECIO = (SELECT TOP 1 PRECIO FROM dbo.[Articulos.Precios] WHERE fkArticulo = @CodigoArticulo AND PlanID = @PlanID)
	
	IF (@PRECIO <= @PRECIOSAP OR @PRECIO >=@PRECIOSAP)
		BEGIN
			UPDATE 	dbo.[Articulos.Precios] SET PRECIO = @PRECIOSAP, DOC_Actualizacion=@Doc_Actualizacion
			WHERE fkArticulo = 	@CodigoArticulo AND PLANID = @PlanID
		END

		If Not Exists(select fkArticulo from dbo.[Articulos.Precios] where fkArticulo=@CodigoArticulo and PlanID=@PlanID)
		Begin
		INSERT dbo.[Articulos.Precios] (PlanID, fkArticulo,DescripcionPlan,Costo, Precio, fkUsuario, FechaCreacion,Doc_Actualizacion) 
		VALUES (@PlanID, @CodigoArticulo, @DescripcionPlan,@Costo, @PRECIOSAP, @fkUsuario, GetDate(),@Doc_Actualizacion)
		End		



END
GO
/****** Object:  StoredProcedure [dbo].[sp_UpdateAlmacen]    Script Date: 08/13/2014 16:06:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_UpdateAlmacen]
(
	@IdRegistro int,
	@Codigo_Almacen nvarchar(50),
	@Descripcion nvarchar(50)

)
AS
BEGIN

UPDATE dbo.Almacenes SET Codigo_Almacen = @Codigo_Almacen,  Descripcion = @Descripcion
WHERE IdRegistro = @IdRegistro

END
GO
/****** Object:  StoredProcedure [dbo].[sp_RegistrodeCambios]    Script Date: 08/13/2014 16:06:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Amaury Minaya
-- Create date: 2014/04/23
-- Description:	Inserta historial de cambios en la base de datos
-- =============================================
CREATE PROCEDURE [dbo].[sp_RegistrodeCambios]
	-- Add the parameters for the stored procedure here
	@Usuario nvarchar(64),
	@Modulo  nvarchar(64),
	@Accion nvarchar(255)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO registrodecambios VALUES(getdate(),@usuario,@Modulo,@Accion)
END
GO
/****** Object:  StoredProcedure [dbo].[sp_MovimientoSeriales]    Script Date: 08/13/2014 16:06:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_MovimientoSeriales]
(
	@idFactura int
)
AS
BEGIN

-- Insertar Datos Detalle Serial ---
Insert Into [FacturaPOS.DetalleSerial] (fkDocCabecera, No_Linea, fkArticulo, Serie, Codigo_Almacen, fkUsuario)
Select 
	fd.fkDocCabecera, fd.No_Linea, fd.ArticuloId, st.Serie, fd.Codigo_Almacen, fd.fkUsuario 
From [FacturaPos.Detalle] fd Inner Join [FacturaPOS.SerialesTemp] st On (fd.ArticuloId = st.fkArticulo)
Where 
	fd.fkDocCabecera = @idFactura
	

-- Actualizar Estatus Articulos Seriales ---

Update [Articulos.Seriales] 
		Set 
			No_Linea = fd.No_Linea,
			fkFacturaVenta = fd.fkDocCabecera,  
			Estatus = 'U'
From [FacturaPos.Detalle] fd Inner Join [FacturaPOS.SerialesTemp] st On (fd.ArticuloId = st.fkArticulo)
		Inner Join [Articulos.Seriales] ar On (st.fkArticulo = ar.fkIdArticulo And st.Serie = ar.SerialNo And fd.Codigo_Almacen = ar.Codigo_Almacen)
Where 
	fd.pidRegistro = @idFactura

END
GO
/****** Object:  StoredProcedure [dbo].[sp_InsertSalidaArticulos]    Script Date: 08/13/2014 16:06:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_InsertSalidaArticulos]
(
	@fkEntrada int,
	@ArticuloCodigo nvarchar(30),
	@ArticuloDescripcion nvarchar(150),
	@ArticuloCantidad numeric(18,2),
	@Codigo_Almacen nvarchar(50),
	@Codigo_Origen nvarchar(50)
)
AS
BEGIN

INSERT INTO DB_PUNTO_VENTA.dbo.[Articulos.SalidaArticuloTranferencia]
(fkEntrada, ArticuloCodigo, ArticuloDescripcion, ArticuloCantidad,Codigo_Almacen,Codigo_Origen)
VALUES
(@fkEntrada,@ArticuloCodigo,@ArticuloDescripcion,@ArticuloCantidad,@Codigo_Almacen,@Codigo_Origen )

END
GO
/****** Object:  StoredProcedure [dbo].[sp_InsertaNC]    Script Date: 08/13/2014 16:06:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Amaury Minaya
-- Create date: 2014-07-03
-- Description:	Inserta Solicitud de Nota de Creditos
-- =============================================
CREATE PROCEDURE [dbo].[sp_InsertaNC]
			(@NumeroReq int
           ,@NumeroNC int
           ,@Monto Money
           ,@Balance Money
           ,@Ticket int
           ,@RegistroDetalle int
           ,@Fecha date
           ,@Estado char(1)
           ,@Razon nvarchar(250)
           ,@Serie nvarchar(32))
AS
BEGIN
	DECLARE @NumeroReqProx int;
	
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

-- Establece número para esta Nota de Credito
SELECT @NumeroReqProx = max(NumeroReq)+1 from [NC]

-- Estable la fecha de la factura, la fecha del servidor de Base de Datos
SET @Fecha=GetDate()

INSERT INTO [dbo].[NC]
           ([NumeroReq]
           ,[NumeroNC]
           ,[Monto]
           ,[Balance]
           ,[Ticket]
           ,[RegistroDetalle]
           ,[Fecha]
           ,[Estado]
           ,[Razon]
           ,[Serie])
     VALUES
           (@NumeroReqProx
           ,@NumeroNC
           ,@Monto
           ,@Balance
           ,@Ticket
           ,@RegistroDetalle
           ,@Fecha
           ,@Estado
           ,@Razon
           ,@Serie)
	Return (@NumeroReqProx)
END
GO
/****** Object:  StoredProcedure [dbo].[sp_insertAlmacenes]    Script Date: 08/13/2014 16:06:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_insertAlmacenes]
(
	@Descripcion nvarchar(50),
	@Codigo_Almacen nvarchar(50),
	@Status  bit,
	@IdSucursal int
)
AS

BEGIN

	INSERT INTO dbo.Almacenes
	(
	Descripcion,
	Codigo_Almacen,
	Status,
	IdSucursal
	)
	VALUES
	(
	@Descripcion,
	@Codigo_Almacen,
	@Status,
	@IdSucursal
	)
END
GO
/****** Object:  StoredProcedure [dbo].[sp_InsertaFacturaSeriales]    Script Date: 08/13/2014 16:06:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Amaury Minaya
-- Create date: 2014-4-13
-- Description:	Inserta Seriales de Facturas
-- =============================================
CREATE PROCEDURE [dbo].[sp_InsertaFacturaSeriales]
			(@Secuencia int
           ,@fkDocCabecera int
           ,@No_Linea int
           ,@fkArticulo nvarchar(20)
           ,@Serie nvarchar(32)
           ,@Codigo_Almacen nvarchar(8)
           ,@fkUsuario int
           ,@Estatus bit
           ,@fkSucursal nvarchar(8))
AS
BEGIN
	--DECLARE @NoFactura int;
	
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
BEGIN TRAN 
INSERT INTO [db_punto_venta].[dbo].[FacturaPOS.DetalleSerial]
           ([Secuencia]
           ,[fkDocCabecera]
           ,[No_Linea]
           ,[fkArticulo]
           ,[Serie]
           ,[Codigo_Almacen]
           ,[fkUsuario]
           ,[Estatus]
           ,[fkSucursal])
     VALUES
           (@Secuencia
           ,@fkDocCabecera
           ,@No_Linea
           ,@fkArticulo
           ,@Serie
           ,@Codigo_Almacen
           ,@fkUsuario
           ,@Estatus
           ,@fkSucursal)	
END

-- Actualiza Disponibilidad de Seriales
UPDATE 
	[Articulos.Seriales]
SET 
	No_linea=@No_Linea,
	Estatus='U',
	fkFacturaVenta=@fkDocCabecera
WHERE 
	SerialNo=@Serie

IF (@@error <>0)
BEGIN
	ROLLBACK TRAN
	RETURN
END
COMMIT TRAN
GO
/****** Object:  StoredProcedure [dbo].[sp_InsertaFacturaDetalle]    Script Date: 08/13/2014 16:06:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Amaury Minaya
-- Create date: 2013-11-30
-- Description:	Inserta detalle de Factura
-- =============================================
CREATE PROCEDURE [dbo].[sp_InsertaFacturaDetalle]
			(@fkDocCabecera int
           ,@SecSerial int
           ,@No_Linea int
           ,@ArticuloID nvarchar(20)
           ,@ArticuloDescripcion nvarchar(100)
           ,@ArticuloCosto numeric (18,6)
           ,@ArticuloPrecio numeric(18,6)
           ,@ArticuloPrecioDesc numeric(18,2)
           ,@ArticuloCantidad numeric(18,0)
           ,@ArticuloMonto numeric(18,6)
           ,@ArticuloPorcientoDesc numeric(18,2)
           ,@ArticuloTotalDescuento numeric(18,6)
           ,@ArticuloPorcientoImpuesto numeric(18,6)
           ,@ArticuloPrecioImpuesto numeric(18,6)
           ,@ArticuloTotalImpuesto numeric(18,6)
           ,@ArticuloImporte numeric(18,6)
           ,@ArticuloImporteNeto numeric(18,6)
           ,@fkUsuario int
           ,@Codigo_Almacen nvarchar(8)
           ,@PlanID int
           ,@ManejaSeriales char(1)
           ,@fkSucursal nvarchar(8))
AS
BEGIN
	DECLARE @cantidad numeric(18,0);
	
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
BEGIN TRAN
INSERT INTO [db_punto_venta].[dbo].[FacturaPOS.Detalle]
           ([fkDocCabecera]
           ,[SecSerial]
           ,[No_Linea]
           ,[ArticuloId]
           ,[ArticuloDescripcion]
           ,[ArticuloCosto]
           ,[ArticuloPrecio]
           ,[ArticuloPrecioDesc]
           ,[ArticuloCantidad]
           ,[ArticuloMonto]
           ,[ArticuloPorcientoDesc]
           ,[ArticuloTotalDescuento]
           ,[ArticuloPorcientoImpuesto]
           ,[ArticuloPrecioImpuesto]
           ,[ArticuloTotalImpuesto]
           ,[ArticuloImporte]
           ,[ArticuloImporteNeto]
           ,[fkUsuario]
           ,[Codigo_Almacen]
           ,[PlanID]
           ,[ManejaSeriales]
           ,[fkSucursal])
     VALUES
           (@fkDocCabecera
           ,@SecSerial
           ,@No_Linea
           ,@ArticuloId
			,@ArticuloDescripcion
			,@ArticuloCosto
			,@ArticuloPrecio
			,@ArticuloPrecioDesc
			,@ArticuloCantidad
			,@ArticuloMonto
			,@ArticuloPorcientoDesc
			,@ArticuloTotalDescuento
			,@ArticuloPorcientoImpuesto
			,@ArticuloPrecioImpuesto
			,@ArticuloTotalImpuesto
			,@ArticuloImporte
			,@ArticuloImporteNeto
			,@fkUsuario
			,@Codigo_Almacen
			,@PlanID
			,@ManejaSeriales
			,@fkSucursal)	
END

-- Actualiza Inventario del Articulo
UPDATE 
	articulos 
SET 
	enmano=enmano-@ArticuloCantidad 
WHERE 
	codigoart=@ArticuloId and 
	codigo_almacen=@Codigo_Almacen

-- Actualiza Kardex
set @cantidad=@articuloCantidad*-1

EXEC	[dbo].[sp_ActualizaKardex]
		@CodigoArt = @ArticuloId,
		@Descripcion = N'Factura',
		@DocumentoNo = @fkDocCabecera,
		@ArticuloCantidad = @Cantidad
		
IF (@@error <>0)
BEGIN
	ROLLBACK TRAN
	RETURN
END
COMMIT TRAN
GO
/****** Object:  StoredProcedure [dbo].[sp_InsertaFactura]    Script Date: 08/13/2014 16:06:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Amaury Minaya
-- Create date: 2013-11-30
-- Description:	Inserta Maestro de Factura
-- =============================================
CREATE PROCEDURE [dbo].[sp_InsertaFactura]
			(@pIdRegistro int
           ,@ClienteId nvarchar (20)
           ,@ClienteNombre nvarchar(100)
           ,@ClienteRnc nvarchar(20)
           ,@DocumentoFecha date
           ,@DocumentoServidor char(1)
           ,@fkSucursal int
           ,@fkUsuario int
           ,@fkCaja int
           ,@fkRegistroCaja int
           ,@FormaPago char(1)
           ,@SubTotal numeric(18,6)
           ,@TotalDesc numeric(18,6)
           ,@TotalItbis numeric(18,6)
           ,@Efectivo numeric(18,6)
           ,@Cheque numeric(18,2)
           ,@NoCheque numeric(18,6)
           ,@Tarjeta numeric(18,6)
           ,@NoTarjeta numeric(18,0)
           ,@Ncredito numeric(18,6)
           ,@Codigo_Almacen nvarchar(8)
           ,@MontoGeneral numeric(18,6)
           ,@Codigo_Vendedor nchar(10)
           ,@NombrePC nvarchar(90)
           ,@fkTipoNCF int
           ,@MOntoEfectivo money
           ,@MontoDevuelta money
           ,@PagoEfectivo money
           ,@PagoCheque money
           ,@PagoTarjetaCredito money
           ,@PagoTarjetaDebito money
           ,@PagoNotaCredito money)


AS
BEGIN
	DECLARE @NoFactura int;
	
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

SELECT @NoFactura = max(pidregistro)+1 from [Facturapos.Cabecera]

-- Estable la fecha de la factura, la fecha del servidor de Base de Datos
SET @DocumentoFecha=GetDate()

INSERT INTO [db_punto_venta].[dbo].[FacturaPOS.Cabecera]
           ([pIdRegistro]
           ,[ClienteId]
           ,[ClienteNombre]
           ,[ClienteRnc]
           ,[DocumentoFecha]
           ,[DocumentoServidor]
           ,[fkSucursal]
           ,[fkUsuario]
           ,[fkCaja]
           ,[fkRegistroCaja]
           ,[FormaPago]
           ,[SubTotal]
           ,[TotalDesc]
           ,[TotalItbis]
           ,[Efectivo]
           ,[Cheque]
           ,[NoCheque]
           ,[Tarjeta]
           ,[NoTarjeta]
           ,[Ncredito]
           ,[Codigo_Almacen]
           ,[MontoGeneral]
           ,[Codigo_Vendedor]
           ,[NombrePC]
           ,[fkTipoNCF]
           ,[MOntoEfectivo]
           ,[MontoDevuelta]
           ,[PagoEfectivo]
           ,[PagoCheque]
           ,[PagoTarjetaCredito]
           ,[PagoTarjetaDebito]
           ,[PagoNotaCredito])
     VALUES
           (@NoFactura
           ,@ClienteId
           ,@ClienteNombre
           ,@ClienteRnc
           ,@DocumentoFecha
           ,@DocumentoServidor
           ,@fkSucursal
           ,@fkUsuario
           ,@fkCaja
           ,@fkRegistroCaja
           ,@FormaPago
           ,@SubTotal
           ,@TotalDesc
           ,@TotalItbis
           ,@Efectivo
           ,@Cheque
           ,@NoCheque
           ,@Tarjeta
           ,@NoTarjeta
           ,@Ncredito
           ,@Codigo_Almacen
           ,@MontoGeneral
           ,@Codigo_Vendedor
           ,@NombrePC
           ,@fkTipoNCF
           ,@MOntoEfectivo
           ,@MontoDevuelta
           ,@PagoEfectivo
           ,@PagoCheque
           ,@PagoTarjetaCredito
           ,@PagoTarjetaDebito
           ,@PagoNotaCredito)
	Return @NoFactura
	
END
GO
/****** Object:  StoredProcedure [dbo].[sp_ActualizaEstadoFactura]    Script Date: 08/13/2014 16:06:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Amaury Minaya
-- Create date: 2014-07-03
-- Description:	Actualiza Estado de Factura
-- =============================================
CREATE PROCEDURE [dbo].[sp_ActualizaEstadoFactura]
			(@FacturaNo int
            ,@Estado char(1))
           
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

UPDATE [FacturaPOS.Cabecera] SET DocumentoServidor=@Estado WHERE pidregistro=@FacturaNo

END
GO
/****** Object:  StoredProcedure [dbo].[CuadredeCaja]    Script Date: 08/13/2014 16:06:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CuadredeCaja]
(
	@i_date datetime,
	@e_date datetime,
	@terminal varchar(50)
)
AS
BEGIN

SELECT 
       @terminal AS CAJA,
       --APERTURA DE CAJA
       ISNULL((SELECT SUM(MontoApertura) FROM dbo.[Caja.AbrirCerrar] WHERE  FechaApertura BETWEEN @i_date AND @e_date AND NombrePC=@terminal),0) as 'APERTURA',
       ISNULL((SELECT SUM(pd.Monto) FROM dbo.[FacturaPOS.Cabecera] fc
		 Inner Join PagoDetalle pd On (fc.pIdRegistro = pd.idCodigoReferencia) WHERE  DocumentoFecha BETWEEN @i_date AND @e_date AND NombrePC=@terminal AND pd.Formapago='E'),0) as 'EFECTIVO',
       ISNULL((SELECT SUM(pd.Monto) FROM dbo.[FacturaPOS.Cabecera] fc
		 Inner Join PagoDetalle pd On (fc.pIdRegistro = pd.idCodigoReferencia) WHERE  DocumentoFecha BETWEEN @i_date AND @e_date AND NombrePC=@terminal AND pd.Formapago='T'),0) as 'TARJETA',
       ISNULL((SELECT SUM(pd.Monto) FROM dbo.[FacturaPOS.Cabecera] fc
		 Inner Join PagoDetalle pd On (fc.pIdRegistro = pd.idCodigoReferencia) WHERE  DocumentoFecha BETWEEN @i_date AND @e_date AND NombrePC=@terminal AND pd.Formapago='K'),0) as 'CHEQUE',
       ISNULL((SELECT SUM(pd.Monto) FROM dbo.[FacturaPOS.Cabecera] fc
		 Inner Join PagoDetalle pd On (fc.pIdRegistro = pd.idCodigoReferencia) WHERE  DocumentoFecha BETWEEN @i_date AND @e_date AND NombrePC=@terminal AND pd.Formapago='N'),0) as 'NCREDITO',
-- TOTAL CAJA
       (ISNULL((SELECT SUM(MontoApertura) FROM dbo.[Caja.AbrirCerrar] WHERE  FechaApertura BETWEEN @i_date AND @e_date AND NombrePC=@terminal),0) +
       ISNULL((SELECT SUM(pd.Monto) FROM dbo.[FacturaPOS.Cabecera] fc
		 Inner Join PagoDetalle pd On (fc.pIdRegistro = pd.idCodigoReferencia) WHERE  DocumentoFecha BETWEEN @i_date AND @e_date AND NombrePC=@terminal AND pd.Formapago='E'),0)+
       ISNULL((SELECT SUM(pd.Monto) FROM dbo.[FacturaPOS.Cabecera] fc
		 Inner Join PagoDetalle pd On (fc.pIdRegistro = pd.idCodigoReferencia) WHERE  DocumentoFecha BETWEEN @i_date AND @e_date AND NombrePC=@terminal AND pd.Formapago='T'),0)+
       ISNULL((SELECT SUM(pd.Monto) FROM dbo.[FacturaPOS.Cabecera] fc
		 Inner Join PagoDetalle pd On (fc.pIdRegistro = pd.idCodigoReferencia) WHERE  DocumentoFecha BETWEEN @i_date AND @e_date AND NombrePC=@terminal AND pd.Formapago='K'),0)+
       ISNULL((SELECT SUM(pd.Monto) FROM dbo.[FacturaPOS.Cabecera] fc
		 Inner Join PagoDetalle pd On (fc.pIdRegistro = pd.idCodigoReferencia) WHERE  DocumentoFecha BETWEEN @i_date AND @e_date AND NombrePC=@terminal AND pd.Formapago='N'),0)) as 'TCAJA',
-- TOTAL CIERRE
       ISNULL((SELECT SUM(MontoCierre) FROM dbo.[Caja.AbrirCerrar] WHERE  FechaCierre BETWEEN @i_date AND @e_date AND NombrePC=@terminal),0) as 'CIERRE',
-- DIFERENCIA
       (ISNULL((SELECT SUM(MontoApertura) FROM dbo.[Caja.AbrirCerrar] WHERE  FechaApertura BETWEEN @i_date AND @e_date AND NombrePC=@terminal),0) +
       ISNULL((SELECT SUM(pd.Monto) FROM dbo.[FacturaPOS.Cabecera] fc
		 Inner Join PagoDetalle pd On (fc.pIdRegistro = pd.idCodigoReferencia) WHERE  DocumentoFecha BETWEEN @i_date AND @e_date AND NombrePC=@terminal AND pd.Formapago='E'),0)+
       ISNULL((SELECT SUM(pd.Monto) FROM dbo.[FacturaPOS.Cabecera] fc
		 Inner Join PagoDetalle pd On (fc.pIdRegistro = pd.idCodigoReferencia) WHERE  DocumentoFecha BETWEEN @i_date AND @e_date AND NombrePC=@terminal AND pd.Formapago='T'),0)+
       ISNULL((SELECT SUM(pd.Monto) FROM dbo.[FacturaPOS.Cabecera] fc
		 Inner Join PagoDetalle pd On (fc.pIdRegistro = pd.idCodigoReferencia) WHERE  DocumentoFecha BETWEEN @i_date AND @e_date AND NombrePC=@terminal AND pd.Formapago='K'),0)+
       ISNULL((SELECT SUM(pd.Monto) FROM dbo.[FacturaPOS.Cabecera] fc
		 Inner Join PagoDetalle pd On (fc.pIdRegistro = pd.idCodigoReferencia) WHERE  DocumentoFecha BETWEEN @i_date AND @e_date AND NombrePC=@terminal AND pd.Formapago='N'),0))-
       ISNULL((SELECT SUM(MontoCierre) FROM dbo.[Caja.AbrirCerrar] WHERE  FechaCierre BETWEEN @i_date AND @e_date AND NombrePC=@terminal),0) AS 'DIFERENCIA'

END
GO
/****** Object:  StoredProcedure [dbo].[UpdateCabFacturaPOS]    Script Date: 08/13/2014 16:06:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UpdateCabFacturaPOS]
(
	@FacturaNo int
	
)
AS
BEGIN

Update [dbo].[FacturaPOS.Cabecera]  
	Set 
		DocumentoServidor = 'Y'		
Where 
	pIdRegistro = @FacturaNo
END
GO
/****** Object:  StoredProcedure [dbo].[SpGetInvoiceSend]    Script Date: 08/13/2014 16:06:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SpGetInvoiceSend]
AS

BEGIN

SELECT     T0.pIdRegistro, T0.ClienteId, T0.ClienteNombre,T0.Codigo_Almacen, T0.ClienteRnc, T0.DocumentoFecha, T1.Descripcion, T1.Direccion, T1.Telefono, T1.RNC, T2.ArticuloId, 
                      T2.ArticuloDescripcion, T2.ArticuloPrecio, T2.ArticuloCantidad, T2.ArticuloMonto, T2.ArticuloPorcientoDesc, T2.ArticuloPorcientoImpuesto, 
                      T2.ArticuloImporte, T3.NumeroNCF, T4.Descripcion AS NCFDescripcion
FROM         dbo.[FacturaPOS.Cabecera] AS T0 INNER JOIN
                      dbo.[FacturaPOS.Detalle] AS T2 ON T2.fkDocCabecera = T0.pIdRegistro INNER JOIN
                      dbo.Sucursal AS T1 ON T1.pkIdRegistro = T0.fkSucursal INNER JOIN
                      dbo.[FacturaPOS.FacturaNCF] AS T3 ON T3.fkFacturaNo = T0.pIdRegistro INNER JOIN
                      dbo.[NCF.Tipos] AS T4 ON T4.pIdRegistro = T3.fkTipoNCF WHERE T0.DocumentoServidor = 'N'


END
GO
/****** Object:  View [dbo].[v_ImpresionTicket_New]    Script Date: 08/13/2014 16:06:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[v_ImpresionTicket_New]
AS
SELECT     TOP (100) PERCENT T0.pIdRegistro, T0.ClienteId, T0.ClienteNombre, T0.ClienteRnc, T0.DocumentoFecha, T1.Descripcion, T1.Direccion, T1.Telefono, T1.RNC, 
                      T2.ArticuloId, T2.ArticuloDescripcion, T2.ArticuloPrecio, T2.ArticuloCantidad, CONVERT(NUMERIC(19, 2), ROUND(T2.ArticuloMonto / T2.ArticuloCantidad, 2)) 
                      AS PrecioRealArticulo, T2.ArticuloPorcientoImpuesto, T2.ArticuloMonto, T2.ArticuloPorcientoDesc, T2.ArticuloImporte, T3.NumeroNCF, 
                      T4.Descripcion AS NCFDescripcion, T0.MontoEfectivo, T0.MontoDevuelta, RTRIM(dbo.Vendedores.Nombre) + ' ' + RTRIM(dbo.Vendedores.Apellido) AS Vendedor, 
                      T0.MontoGeneral, T0.fkTipoNCF, T0.SubTotal, T0.TotalDesc, T0.TotalItbis, T0.Efectivo, T0.Cheque, T0.Tarjeta, T0.Ncredito, T0.NombrePC, dbo.Vendedores.Nombre, 
                      T2.ArticuloDescripcion AS Expr1, T2.ArticuloTotalDescuento, T2.ArticuloPrecioImpuesto, T2.ArticuloTotalImpuesto, T2.Codigo_Almacen, T2.ArticuloPrecioDesc, 
                      T2.ArticuloImporteNeto, T0.Codigo_Vendedor, dbo.[FacturaPOS.DetalleSerial].Serie, dbo.Usuarios.Usuario, dbo.Articulos.ManejaSeriales, T3.NCFControl, T2.No_Linea, 
                      ISNULL(dbo.[FacturaPOS.DetalleSerial].Secuencia, 0) AS Secuencia, T2.pIdRegistro AS RegistroDetalle, T1.Comprobante
FROM         dbo.[FacturaPOS.Cabecera] AS T0 INNER JOIN
                      dbo.[FacturaPOS.Detalle] AS T2 ON T2.fkDocCabecera = T0.pIdRegistro INNER JOIN
                      dbo.Sucursal AS T1 ON T1.pkIdRegistro = T0.fkSucursal INNER JOIN
                      dbo.Vendedores ON T0.Codigo_Vendedor = dbo.Vendedores.CodVendedor INNER JOIN
                      dbo.[NCF.Tipos] AS T4 ON T0.fkTipoNCF = T4.pIdRegistro INNER JOIN
                      dbo.Usuarios ON T0.fkUsuario = dbo.Usuarios.pIdRegistro LEFT OUTER JOIN
                      dbo.[FacturaPOS.DetalleSerial] ON T2.No_Linea = dbo.[FacturaPOS.DetalleSerial].No_Linea AND 
                      T2.fkDocCabecera = dbo.[FacturaPOS.DetalleSerial].fkDocCabecera AND T2.ArticuloId = dbo.[FacturaPOS.DetalleSerial].fkArticulo LEFT OUTER JOIN
                      dbo.Articulos ON T2.Codigo_Almacen = dbo.Articulos.Codigo_Almacen AND T2.ArticuloId = dbo.Articulos.CodigoArt LEFT OUTER JOIN
                      dbo.[FacturaPOS.FacturaNCF] AS T3 ON T3.fkFacturaNo = T0.pIdRegistro
ORDER BY RegistroDetalle, T2.No_Linea
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[36] 4[15] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "T0"
            Begin Extent = 
               Top = 181
               Left = 478
               Bottom = 364
               Right = 652
            End
            DisplayFlags = 280
            TopColumn = 7
         End
         Begin Table = "T2"
            Begin Extent = 
               Top = 291
               Left = 2
               Bottom = 487
               Right = 208
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "T1"
            Begin Extent = 
               Top = 0
               Left = 18
               Bottom = 237
               Right = 177
            End
            DisplayFlags = 280
            TopColumn = 1
         End
         Begin Table = "Vendedores"
            Begin Extent = 
               Top = 0
               Left = 905
               Bottom = 108
               Right = 1056
            End
            DisplayFlags = 280
            TopColumn = 1
         End
         Begin Table = "T4"
            Begin Extent = 
               Top = 109
               Left = 716
               Bottom = 217
               Right = 881
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Usuarios"
            Begin Extent = 
               Top = 327
               Left = 690
               Bottom = 446
               Right = 873
            End
            DisplayFlags = 280
            TopColumn = 3
         End
         Begin Table = "FacturaPOS.DetalleSerial"
            Begin Extent = 
               Top = 294
               Left = 246
               Bottom = 413
               Right = 414
            End
            DisplayFlags = 28' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'v_ImpresionTicket_New'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'0
            TopColumn = 0
         End
         Begin Table = "Articulos"
            Begin Extent = 
               Top = 418
               Left = 382
               Bottom = 537
               Right = 593
            End
            DisplayFlags = 280
            TopColumn = 8
         End
         Begin Table = "T3"
            Begin Extent = 
               Top = 293
               Left = 949
               Bottom = 439
               Right = 1100
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 51
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1635
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1470
         Width = 5760
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 5910
         Alias = 2370
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'v_ImpresionTicket_New'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'v_ImpresionTicket_New'
GO
/****** Object:  View [dbo].[v_ImpresionTicket]    Script Date: 08/13/2014 16:06:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[v_ImpresionTicket]
AS
SELECT     T0.pIdRegistro, T0.ClienteId, T0.ClienteNombre, T0.ClienteRnc, T0.DocumentoFecha, T1.Descripcion, T1.Direccion, T1.Telefono, T1.RNC, T2.ArticuloId, 
                      T2.ArticuloDescripcion + RTRIM(ISNULL(DetalleSerial.Serie, N'   ')) AS ArticuloDescripcion, T2.ArticuloPrecio, T2.ArticuloCantidad, CONVERT(NUMERIC(19, 2), 
                      ROUND(T2.ArticuloMonto / T2.ArticuloCantidad, 2)) AS PrecioRealArticulo, T2.ArticuloPorcientoImpuesto, T2.ArticuloMonto, T2.ArticuloPorcientoDesc, T2.ArticuloImporte, 
                      T3.NumeroNCF, T4.Descripcion AS NCFDescripcion, T0.MOntoEfectivo, T0.MontoDevuelta, RTRIM(dbo.Vendedores.Nombre) + ' ' + RTRIM(dbo.Vendedores.Apellido) 
                      AS Vendedor, DetalleSerial.Serie, T0.MontoGeneral, T0.fkTipoNCF
FROM         dbo.[FacturaPOS.Cabecera] AS T0 INNER JOIN
                      dbo.[FacturaPOS.Detalle] AS T2 ON T2.fkDocCabecera = T0.pIdRegistro INNER JOIN
                      dbo.Sucursal AS T1 ON T1.pkIdRegistro = T0.fkSucursal INNER JOIN
                      dbo.Vendedores ON T0.Codigo_Vendedor = dbo.Vendedores.CodVendedor INNER JOIN
                      dbo.[NCF.Tipos] AS T4 ON T0.fkTipoNCF = T4.pIdRegistro LEFT OUTER JOIN
                      dbo.[FacturaPOS.FacturaNCF] AS T3 ON T3.fkFacturaNo = T0.pIdRegistro LEFT OUTER JOIN
                          (SELECT DISTINCT fkDocCabecera, fkArticulo, dbo.fnListaSeriales(fkDocCabecera, fkArticulo) AS Serie
                            FROM          dbo.[FacturaPOS.DetalleSerial]) AS DetalleSerial ON T2.fkDocCabecera = DetalleSerial.fkDocCabecera AND T2.ArticuloId = DetalleSerial.fkArticulo
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[36] 4[15] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "T0"
            Begin Extent = 
               Top = 181
               Left = 478
               Bottom = 364
               Right = 652
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "T2"
            Begin Extent = 
               Top = 293
               Left = 89
               Bottom = 489
               Right = 295
            End
            DisplayFlags = 280
            TopColumn = 3
         End
         Begin Table = "T1"
            Begin Extent = 
               Top = 19
               Left = 0
               Bottom = 256
               Right = 159
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "T3"
            Begin Extent = 
               Top = 293
               Left = 949
               Bottom = 439
               Right = 1100
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "T4"
            Begin Extent = 
               Top = 109
               Left = 716
               Bottom = 217
               Right = 881
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Vendedores"
            Begin Extent = 
               Top = 6
               Left = 494
               Bottom = 114
               Right = 645
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "DetalleSerial"
            Begin Extent = 
               Top = 288
               Left = 241
               Bottom = 381
               Right = 394
            End
            DisplayFlags = 280
            T' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'v_ImpresionTicket'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'opColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 27
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1635
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'v_ImpresionTicket'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'v_ImpresionTicket'
GO
/****** Object:  View [dbo].[v_FacturasContratoSAP]    Script Date: 08/13/2014 16:06:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[v_FacturasContratoSAP]
AS
SELECT DISTINCT 
                      dbo.[FacturaPOS.Contrato].ContratoNumero, dbo.[FacturaPOS.Contrato].CelularNumero, dbo.[FacturaPOS.Contrato].ClienteNombre, 
                      dbo.[FacturaPOS.Contrato].ClienteCedula, dbo.[FacturaPOS.Contrato].fkDocCabecera, dbo.[FacturaPOS.Contrato].Codigo_Almacen, 
                      dbo.[FacturaPOS.Cabecera].Codigo_Almacen AS Sucursal
FROM         dbo.[FacturaPOS.Cabecera] INNER JOIN
                      dbo.[FacturaPOS.Contrato] ON dbo.[FacturaPOS.Cabecera].pIdRegistro = dbo.[FacturaPOS.Contrato].fkDocCabecera
WHERE     (dbo.[FacturaPOS.Cabecera].DocumentoServidor = 'N')
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "FacturaPOS.Cabecera"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 121
               Right = 213
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "FacturaPOS.Contrato"
            Begin Extent = 
               Top = 6
               Left = 251
               Bottom = 121
               Right = 412
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'v_FacturasContratoSAP'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'v_FacturasContratoSAP'
GO
/****** Object:  View [dbo].[v_Facturas.CabeceraSAP]    Script Date: 08/13/2014 16:06:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[v_Facturas.CabeceraSAP]
AS
SELECT DISTINCT 
                      dbo.[FacturaPOS.Cabecera].pIdRegistro, dbo.[FacturaPOS.Cabecera].ClienteId, dbo.[FacturaPOS.Cabecera].ClienteNombre, 
                      dbo.[FacturaPOS.Cabecera].DocumentoFecha, dbo.[FacturaPOS.Cabecera].fkSucursal, dbo.[FacturaPOS.Cabecera].FormaPago, 
                      dbo.[FacturaPOS.Cabecera].Codigo_Almacen, dbo.[FacturaPOS.Cabecera].MontoGeneral, dbo.[FacturaPOS.Cabecera].Codigo_Vendedor, 
                      dbo.[FacturaPOS.FacturaNCF].fkTipoNCF, 
                      CASE WHEN dbo.[FacturaPOS.FacturaNCF].NCFControl = 1 THEN dbo.[FacturaPOS.FacturaNCF].NumeroNCF ELSE dbo.[FacturaPOS.FacturaNCF].NumeroNCF
                       END AS NCF, dbo.[FacturaPOS.Cabecera].NombrePC, dbo.[FacturaPOS.Cabecera].SubTotal, dbo.[FacturaPOS.Cabecera].TotalDesc, 
                      dbo.[FacturaPOS.Cabecera].TotalItbis, dbo.[FacturaPOS.Cabecera].ClienteRnc, dbo.[FacturaPOS.Cabecera].NoCheque, 
                      dbo.[FacturaPOS.Cabecera].Ncredito, dbo.[FacturaPOS.Cabecera].Efectivo, dbo.[FacturaPOS.Cabecera].Cheque, dbo.[FacturaPOS.Cabecera].Tarjeta, 
                      dbo.[FacturaPOS.Cabecera].MontoDevuelta
FROM         dbo.[FacturaPOS.Cabecera] LEFT OUTER JOIN
                      dbo.[FacturaPOS.FacturaNCF] ON dbo.[FacturaPOS.Cabecera].pIdRegistro = dbo.[FacturaPOS.FacturaNCF].fkFacturaNo
WHERE     (dbo.[FacturaPOS.Cabecera].DocumentoServidor = N'N')
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[34] 4[27] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "FacturaPOS.Cabecera"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 121
               Right = 213
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "FacturaPOS.FacturaNCF"
            Begin Extent = 
               Top = 6
               Left = 251
               Bottom = 121
               Right = 403
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 23
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'v_Facturas.CabeceraSAP'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'v_Facturas.CabeceraSAP'
GO
/****** Object:  View [dbo].[v_FacturaDetalleSerialSAP]    Script Date: 08/13/2014 16:06:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[v_FacturaDetalleSerialSAP]
AS
SELECT DISTINCT 
                      dbo.[FacturaPOS.DetalleSerial].No_Linea, dbo.[FacturaPOS.DetalleSerial].Serie, dbo.[FacturaPOS.DetalleSerial].fkArticulo, 
                      dbo.[FacturaPOS.DetalleSerial].Estatus, dbo.[FacturaPOS.DetalleSerial].fkDocCabecera, dbo.[FacturaPOS.DetalleSerial].Codigo_Almacen, 
                      dbo.[FacturaPOS.Cabecera].Codigo_Almacen AS Sucursal, dbo.[FacturaPOS.DetalleSerial].fkSucursal
FROM         dbo.[FacturaPOS.Cabecera] INNER JOIN
                      dbo.[FacturaPOS.DetalleSerial] ON dbo.[FacturaPOS.Cabecera].pIdRegistro = dbo.[FacturaPOS.DetalleSerial].fkDocCabecera
WHERE     (dbo.[FacturaPOS.Cabecera].DocumentoServidor = 'N')
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "FacturaPOS.Cabecera"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 121
               Right = 213
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "FacturaPOS.DetalleSerial"
            Begin Extent = 
               Top = 6
               Left = 251
               Bottom = 121
               Right = 411
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'v_FacturaDetalleSerialSAP'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'v_FacturaDetalleSerialSAP'
GO
/****** Object:  View [dbo].[v_FacturaDetalleSAP]    Script Date: 08/13/2014 16:06:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[v_FacturaDetalleSAP]
AS
SELECT DISTINCT 
                      dbo.[FacturaPOS.Detalle].fkDocCabecera, dbo.[FacturaPOS.Detalle].No_Linea, dbo.[FacturaPOS.Detalle].ArticuloId, 
                      dbo.[FacturaPOS.Detalle].ArticuloDescripcion, dbo.[FacturaPOS.Detalle].ArticuloPrecio, dbo.[FacturaPOS.Detalle].ArticuloPrecioDesc, 
                      dbo.[FacturaPOS.Detalle].ArticuloCantidad, dbo.[FacturaPOS.Detalle].ArticuloMonto, dbo.[FacturaPOS.Detalle].ArticuloPorcientoDesc, 
                      dbo.[FacturaPOS.Detalle].ArticuloTotalDescuento, dbo.[FacturaPOS.Detalle].ArticuloPorcientoImpuesto, 
                      dbo.[FacturaPOS.Detalle].ArticuloPrecioImpuesto, dbo.[FacturaPOS.Detalle].ArticuloTotalImpuesto, dbo.[FacturaPOS.Detalle].ArticuloImporte, 
                      dbo.[FacturaPOS.Detalle].ArticuloImporteNeto, dbo.[FacturaPOS.Detalle].fkUsuario, dbo.[FacturaPOS.Detalle].Codigo_Almacen, 
                      dbo.[FacturaPOS.Cabecera].Codigo_Almacen AS Sucursal, dbo.[FacturaPOS.Detalle].fkSucursal
FROM         dbo.[FacturaPOS.Cabecera] INNER JOIN
                      dbo.[FacturaPOS.Detalle] ON dbo.[FacturaPOS.Cabecera].pIdRegistro = dbo.[FacturaPOS.Detalle].fkDocCabecera
WHERE     (dbo.[FacturaPOS.Cabecera].DocumentoServidor = 'N')
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "FacturaPOS.Cabecera"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 121
               Right = 213
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "FacturaPOS.Detalle"
            Begin Extent = 
               Top = 6
               Left = 251
               Bottom = 121
               Right = 458
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 20
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'v_FacturaDetalleSAP'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'v_FacturaDetalleSAP'
GO
/****** Object:  View [dbo].[v_FacturaComisionSAP]    Script Date: 08/13/2014 16:06:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[v_FacturaComisionSAP]
AS
SELECT DISTINCT 
                      dbo.[FacturaPOS.FacturaComision].fkFacturaCodigo, dbo.[FacturaPOS.FacturaComision].MontoComision, dbo.[FacturaPOS.FacturaComision].fkCliente, 
                      dbo.[FacturaPOS.FacturaComision].Codigo_Almacen, dbo.[FacturaPOS.Cabecera].Codigo_Almacen AS Sucursal
FROM         dbo.[FacturaPOS.Cabecera] INNER JOIN
                      dbo.[FacturaPOS.FacturaComision] ON dbo.[FacturaPOS.Cabecera].pIdRegistro = dbo.[FacturaPOS.FacturaComision].fkFacturaCodigo
WHERE     (dbo.[FacturaPOS.Cabecera].DocumentoServidor = 'N')
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "FacturaPOS.Cabecera"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 121
               Right = 213
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "FacturaPOS.FacturaComision"
            Begin Extent = 
               Top = 6
               Left = 251
               Bottom = 121
               Right = 411
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'v_FacturaComisionSAP'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'v_FacturaComisionSAP'
GO
/****** Object:  View [dbo].[V_DetalleFacturasPosv]    Script Date: 08/13/2014 16:06:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[V_DetalleFacturasPosv]
AS
SELECT     pIdRegistro, ClienteId, ClienteNombre, DocumentoFecha, 
                      CASE FormaPago WHEN 'E' THEN 'Efectivo' WHEN 'T' THEN 'Tarjeta' WHEN 'N' THEN 'Nota de Credito' WHEN 'K' THEN 'Cheque' END AS FormaPago, 
                      Codigo_Almacen, SUM(MontoGeneral) AS MontoGeneral, Codigo_Vendedor, NombrePC
FROM         dbo.[FacturaPOS.Cabecera]
WHERE     (DocumentoServidor = N'N')
GROUP BY pIdRegistro, ClienteId, ClienteNombre, DocumentoFecha, FormaPago, Codigo_Almacen, MontoGeneral, Codigo_Vendedor, NombrePC
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "FacturaPOS.Cabecera"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 114
               Right = 228
            End
            DisplayFlags = 280
            TopColumn = 11
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 12
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'V_DetalleFacturasPosv'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'V_DetalleFacturasPosv'
GO
/****** Object:  View [dbo].[vw_Qry_Flujo_Cajas]    Script Date: 08/13/2014 16:06:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_Qry_Flujo_Cajas]
AS
SELECT     TOP (100) PERCENT dbo.[FacturaPOS.Cabecera].ClienteNombre, CONVERT(DATE, dbo.[FacturaPOS.Cabecera].DocumentoFecha, 111) AS Fecha, 
                      dbo.[FacturaPOS.Cabecera].MontoGeneral, dbo.Sucursal.Descripcion, dbo.[FacturaPOS.Cabecera].pIdRegistro AS FacturaNo, 
                      dbo.[FacturaPOS.Cabecera].DocumentoFecha, dbo.[FacturaPOS.Cabecera].NombrePC
FROM         dbo.[FacturaPOS.Cabecera] INNER JOIN
                      dbo.Sucursal ON dbo.[FacturaPOS.Cabecera].fkSucursal = dbo.Sucursal.pkIdRegistro
ORDER BY dbo.[FacturaPOS.Cabecera].DocumentoFecha DESC, FacturaNo DESC
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "FacturaPOS.Cabecera"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 125
               Right = 221
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Sucursal"
            Begin Extent = 
               Top = 6
               Left = 457
               Bottom = 125
               Right = 625
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vw_Qry_Flujo_Cajas'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vw_Qry_Flujo_Cajas'
GO
/****** Object:  View [dbo].[vw_Qry_Articulos_Ventas]    Script Date: 08/13/2014 16:06:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_Qry_Articulos_Ventas]
AS
SELECT     dbo.[FacturaPOS.Detalle].fkDocCabecera, dbo.[FacturaPOS.Detalle].ArticuloId, dbo.[FacturaPOS.Detalle].ArticuloDescripcion, dbo.[FacturaPOS.Detalle].ArticuloCantidad, 
                      dbo.[FacturaPOS.Cabecera].DocumentoFecha, dbo.[FacturaPOS.Cabecera].fkCaja
FROM         dbo.[FacturaPOS.Detalle] INNER JOIN
                      dbo.[FacturaPOS.Cabecera] ON dbo.[FacturaPOS.Detalle].fkDocCabecera = dbo.[FacturaPOS.Cabecera].pIdRegistro INNER JOIN
                      dbo.Articulos ON dbo.[FacturaPOS.Detalle].ArticuloId = dbo.Articulos.CodigoArt
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Articulos"
            Begin Extent = 
               Top = 176
               Left = 377
               Bottom = 295
               Right = 588
            End
            DisplayFlags = 280
            TopColumn = 15
         End
         Begin Table = "FacturaPOS.Cabecera"
            Begin Extent = 
               Top = 30
               Left = 336
               Bottom = 149
               Right = 519
            End
            DisplayFlags = 280
            TopColumn = 9
         End
         Begin Table = "FacturaPOS.Detalle"
            Begin Extent = 
               Top = 25
               Left = 18
               Bottom = 144
               Right = 233
            End
            DisplayFlags = 280
            TopColumn = 18
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vw_Qry_Articulos_Ventas'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vw_Qry_Articulos_Ventas'
GO
/****** Object:  View [dbo].[vw_Ticket_NC]    Script Date: 08/13/2014 16:06:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_Ticket_NC]
AS
SELECT     TOP (100) PERCENT T0.pIdRegistro, T0.ClienteId, T0.ClienteNombre, T0.ClienteRnc, T0.DocumentoFecha, T1.Descripcion, T1.Direccion, T1.Telefono, T1.RNC, 
                      T2.ArticuloId, T2.ArticuloDescripcion, T2.ArticuloPrecio, T2.ArticuloCantidad, CONVERT(NUMERIC(19, 2), ROUND(T2.ArticuloMonto / T2.ArticuloCantidad, 2)) 
                      AS PrecioRealArticulo, T2.ArticuloPorcientoImpuesto, T2.ArticuloMonto, T2.ArticuloPorcientoDesc, T2.ArticuloImporte, T3.NumeroNCF, 
                      T4.Descripcion AS NCFDescripcion, T0.MOntoEfectivo, T0.MontoDevuelta, RTRIM(dbo.Vendedores.Nombre) + ' ' + RTRIM(dbo.Vendedores.Apellido) AS Vendedor, 
                      T0.MontoGeneral, T0.fkTipoNCF, T0.SubTotal, T0.TotalDesc, T0.TotalItbis, T0.Efectivo, T0.Cheque, T0.Tarjeta, T0.Ncredito, T0.NombrePC, dbo.Vendedores.Nombre, 
                      T2.ArticuloDescripcion AS Expr1, T2.ArticuloTotalDescuento, T2.ArticuloPrecioImpuesto, T2.ArticuloTotalImpuesto, T2.Codigo_Almacen, T2.ArticuloPrecioDesc, 
                      T2.ArticuloImporteNeto, T0.Codigo_Vendedor, dbo.[FacturaPOS.DetalleSerial].Serie, dbo.Usuarios.Usuario, dbo.Articulos.ManejaSeriales, T3.NCFControl, T2.No_Linea, 
                      ISNULL(dbo.[FacturaPOS.DetalleSerial].Secuencia, 0) AS Secuencia, T2.pIdRegistro AS RegistroDetalle, T1.Comprobante, 
                      T2.ArticuloImporte / T2.ArticuloCantidad AS ImporteporArticulo, CASE dbo.Articulos.ManejaSeriales WHEN 'Y' THEN 1 ELSE T2.ArticuloCantidad END AS Cantidad
FROM         dbo.[FacturaPOS.Cabecera] AS T0 INNER JOIN
                      dbo.[FacturaPOS.Detalle] AS T2 ON T2.fkDocCabecera = T0.pIdRegistro INNER JOIN
                      dbo.Sucursal AS T1 ON T1.pkIdRegistro = T0.fkSucursal INNER JOIN
                      dbo.Vendedores ON T0.Codigo_Vendedor = dbo.Vendedores.CodVendedor INNER JOIN
                      dbo.[NCF.Tipos] AS T4 ON T0.fkTipoNCF = T4.pIdRegistro INNER JOIN
                      dbo.Usuarios ON T0.fkUsuario = dbo.Usuarios.pIdRegistro LEFT OUTER JOIN
                      dbo.[FacturaPOS.DetalleSerial] ON T2.No_Linea = dbo.[FacturaPOS.DetalleSerial].No_Linea AND 
                      T2.fkDocCabecera = dbo.[FacturaPOS.DetalleSerial].fkDocCabecera AND T2.ArticuloId = dbo.[FacturaPOS.DetalleSerial].fkArticulo LEFT OUTER JOIN
                      dbo.Articulos ON T2.Codigo_Almacen = dbo.Articulos.Codigo_Almacen AND T2.ArticuloId = dbo.Articulos.CodigoArt LEFT OUTER JOIN
                      dbo.[FacturaPOS.FacturaNCF] AS T3 ON T3.fkFacturaNo = T0.pIdRegistro
ORDER BY RegistroDetalle, T2.No_Linea
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[41] 4[9] 2[32] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "T0"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 125
               Right = 221
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "T2"
            Begin Extent = 
               Top = 6
               Left = 259
               Bottom = 125
               Right = 474
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "T1"
            Begin Extent = 
               Top = 6
               Left = 512
               Bottom = 125
               Right = 680
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Vendedores"
            Begin Extent = 
               Top = 6
               Left = 718
               Bottom = 125
               Right = 878
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "T4"
            Begin Extent = 
               Top = 6
               Left = 916
               Bottom = 125
               Right = 1090
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Usuarios"
            Begin Extent = 
               Top = 126
               Left = 38
               Bottom = 245
               Right = 223
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "FacturaPOS.DetalleSerial"
            Begin Extent = 
               Top = 126
               Left = 261
               Bottom = 245
               Right = 429
            End
            DisplayFlags = 280
   ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vw_Ticket_NC'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'         TopColumn = 0
         End
         Begin Table = "Articulos"
            Begin Extent = 
               Top = 126
               Left = 467
               Bottom = 245
               Right = 678
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "T3"
            Begin Extent = 
               Top = 126
               Left = 716
               Bottom = 245
               Right = 876
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vw_Ticket_NC'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vw_Ticket_NC'
GO
/****** Object:  View [dbo].[vw_RPT_Ventas]    Script Date: 08/13/2014 16:06:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_RPT_Ventas]
AS
SELECT     TOP (100) PERCENT dbo.[FacturaPOS.Cabecera].ClienteNombre, CONVERT(DATE, dbo.[FacturaPOS.Cabecera].DocumentoFecha, 111) AS Fecha, 
                      dbo.[FacturaPOS.Cabecera].MontoGeneral, dbo.Vendedores.Nombre, dbo.Vendedores.Apellido, dbo.Sucursal.Descripcion, 
                      dbo.[FacturaPOS.Cabecera].pIdRegistro AS FacturaNo, dbo.[FacturaPOS.Cabecera].DocumentoFecha, dbo.[FacturaPOS.Cabecera].NombrePC
FROM         dbo.[FacturaPOS.Cabecera] INNER JOIN
                      dbo.Vendedores ON dbo.[FacturaPOS.Cabecera].Codigo_Vendedor = dbo.Vendedores.CodVendedor INNER JOIN
                      dbo.Sucursal ON dbo.[FacturaPOS.Cabecera].fkSucursal = dbo.Sucursal.pkIdRegistro
ORDER BY dbo.[FacturaPOS.Cabecera].DocumentoFecha DESC, FacturaNo DESC
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[42] 4[20] 2[18] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "FacturaPOS.Cabecera"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 125
               Right = 221
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Vendedores"
            Begin Extent = 
               Top = 6
               Left = 259
               Bottom = 213
               Right = 419
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Sucursal"
            Begin Extent = 
               Top = 6
               Left = 457
               Bottom = 185
               Right = 625
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 10
         Width = 284
         Width = 1500
         Width = 1950
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vw_RPT_Ventas'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vw_RPT_Ventas'
GO
/****** Object:  StoredProcedure [dbo].[ENVIAR_FACTURACION_COMUN]    Script Date: 08/13/2014 16:06:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ENVIAR_FACTURACION_COMUN] 
AS
BEGIN TRY
       BEGIN TRANSACTION

       -- Envia Cabecera Factura a la Comun ---
		INSERT INTO DB_PV_COMUN.dbo.[FacturaPOS.Cabecera]
					(FacturaNoPOS, ClienteID,ClienteNombre, FechaDocumento, fkSucursal, FormaPago, FechaEnvio, Codigo_Almacen,MontoGeneral,Codigo_Vendedor, fkTipoNCF, NumeroNCF, NombrePC) 
		Select 
				pIdRegistro, ClienteId, ClienteNombre, DocumentoFecha, fkSucursal, FormaPago, GetDate() As FechaEnvio, Codigo_Almacen, MontoGeneral, Codigo_Vendedor, fkTipoNCF, NCF, NombrePC
			From dbo.[v_Facturas.CabeceraSAP]
	
		-- Enviar Detalle Factura --
		INSERT INTO DB_PV_COMUN.dbo.[FacturaPOS.Detalle]
				(fkDocCabecera, No_Linea, ArticuloId, ArticuloDescripcion, ArticuloPrecio, ArticuloCantidad,
				 ArticuloMonto, ArticuloPorcientoDesc, ArticuloPorcientoImpuesto, ArticuloImporte,
				 fkUsuario, Codigo_Almacen )
		Select 
			 fad.fkDocCabecera, fad.No_Linea, fad.ArticuloId, fad.ArticuloDescripcion, fad.ArticuloPrecio, fad.ArticuloCantidad,
			 fad.ArticuloMonto, fad.ArticuloPorcientoDesc, fad.ArticuloPorcientoImpuesto, fad.ArticuloImporte,
			 fad.fkUsuario, fad.Codigo_Almacen 
		From dbo.[v_Facturas.CabeceraSAP] fac Inner Join dbo.[FacturaPOS.Detalle] fad On (fac.pIdRegistro = fad.fkDocCabecera)

		-- Enviar Contrato de Facturas ---
		INSERT INTO DB_PV_COMUN.dbo.[FacturaPOS.Contrato]
				(ContratoNumero, CelularNumero, ClienteNombre, ClienteCedula, fkDocCabecera,Codigo_Almacen)
		Select 
			 fad.ContratoNumero, fad.CelularNumero, fad.ClienteNombre, fad.ClienteCedula, fad.fkDocCabecera, fad.Codigo_Almacen  
		From dbo.[v_Facturas.CabeceraSAP] fac Inner Join dbo.[FacturaPOS.Contrato] fad On (fac.pIdRegistro = fad.fkDocCabecera)


		-- Enviar Comision de Facturas ---
		INSERT INTO DB_PV_COMUN.dbo.[FacturaPOS.FacturaComision]
				(fkFacturaCodigo, MontoComision, fkCliente, Codigo_Almacen)
		Select 
			 fad.fkFacturaCodigo, fad.MontoComision, fad.fkCliente, fad.Codigo_Almacen 
		From dbo.[v_Facturas.CabeceraSAP] fac Inner Join dbo.[FacturaPOS.FacturaComision]  fad On (fac.pIdRegistro = fad.fkFacturaCodigo)	
		Where
			fad.MontoComision > 0

		-- Enviar Seriales de Factura Detalle --
		INSERT INTO DB_PV_COMUN.dbo.[FacturaPOS.Seriales]
				(SerialNo, fkIdArticulo, Estatus, fkFacturaVenta, Codigo_Almacen,No_Linea)
		Select fad.Serie, fad.fkArticulo, 'U' Estatus, fad.fkDocCabecera, fad.Codigo_Almacen, fad.No_Linea
		From dbo.[v_Facturas.CabeceraSAP] fac Inner Join dbo.[FacturaPOS.DetalleSerial]  fad On (fac.pIdRegistro = fad.fkDocCabecera)		

		-- Actualizar Estatus Cabecera Factura ---
		UPDATE dbo.[FacturaPOS.Cabecera] 
			Set DocumentoServidor ='Y' 
		From dbo.[v_Facturas.CabeceraSAP] fac Inner Join dbo.[FacturaPOS.Cabecera] fad On (fac.pIdRegistro = fad.pIdRegistro)	

       COMMIT
    END TRY
    BEGIN CATCH
      IF @@TRANCOUNT > 0
         ROLLBACK

      -- Raise an error with the details of the exception
      DECLARE @ErrMsg nvarchar(4000), @ErrSeverity int
      SELECT @ErrMsg = ERROR_MESSAGE(),
             @ErrSeverity = ERROR_SEVERITY()

      RAISERROR(@ErrMsg, @ErrSeverity, 1)
    END CATCH
GO
/****** Object:  View [dbo].[vw_NC]    Script Date: 08/13/2014 16:06:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_NC]
AS
SELECT     dbo.NC.NumeroReq, dbo.NC.NumeroNC, dbo.NC.Monto, dbo.NC.Balance, dbo.NC.Ticket, dbo.NC.RegistroDetalle, dbo.NC.Estado, dbo.NC.Razon, 
                      dbo.v_ImpresionTicket_New.pIdRegistro, dbo.v_ImpresionTicket_New.ClienteId, dbo.v_ImpresionTicket_New.ClienteNombre, dbo.v_ImpresionTicket_New.ClienteRnc, 
                      dbo.v_ImpresionTicket_New.DocumentoFecha, dbo.v_ImpresionTicket_New.Descripcion, dbo.v_ImpresionTicket_New.Direccion, 
                      dbo.v_ImpresionTicket_New.Telefono, dbo.v_ImpresionTicket_New.RNC, dbo.v_ImpresionTicket_New.ArticuloId, dbo.v_ImpresionTicket_New.ArticuloDescripcion, 
                      dbo.v_ImpresionTicket_New.ArticuloPrecio, dbo.v_ImpresionTicket_New.ArticuloCantidad, dbo.v_ImpresionTicket_New.PrecioRealArticulo, 
                      dbo.v_ImpresionTicket_New.ArticuloPorcientoImpuesto, dbo.v_ImpresionTicket_New.ArticuloMonto, dbo.v_ImpresionTicket_New.ArticuloPorcientoDesc, 
                      dbo.v_ImpresionTicket_New.ArticuloImporte, dbo.v_ImpresionTicket_New.NumeroNCF, dbo.v_ImpresionTicket_New.NCFDescripcion, 
                      dbo.v_ImpresionTicket_New.MontoEfectivo, dbo.v_ImpresionTicket_New.MontoDevuelta, dbo.v_ImpresionTicket_New.Vendedor, 
                      dbo.v_ImpresionTicket_New.MontoGeneral, dbo.v_ImpresionTicket_New.fkTipoNCF, dbo.v_ImpresionTicket_New.SubTotal, dbo.v_ImpresionTicket_New.TotalDesc, 
                      dbo.v_ImpresionTicket_New.TotalItbis, dbo.v_ImpresionTicket_New.Efectivo, dbo.v_ImpresionTicket_New.Cheque, dbo.v_ImpresionTicket_New.Tarjeta, 
                      dbo.v_ImpresionTicket_New.Ncredito, dbo.v_ImpresionTicket_New.NombrePC, dbo.v_ImpresionTicket_New.Nombre, dbo.v_ImpresionTicket_New.Expr1 AS Expr2, 
                      dbo.v_ImpresionTicket_New.ArticuloTotalDescuento, dbo.v_ImpresionTicket_New.ArticuloPrecioImpuesto, dbo.v_ImpresionTicket_New.ArticuloTotalImpuesto, 
                      dbo.v_ImpresionTicket_New.Codigo_Almacen, dbo.v_ImpresionTicket_New.ArticuloPrecioDesc, dbo.v_ImpresionTicket_New.ArticuloImporteNeto, 
                      dbo.v_ImpresionTicket_New.Codigo_Vendedor, dbo.v_ImpresionTicket_New.Serie, dbo.v_ImpresionTicket_New.Usuario, dbo.v_ImpresionTicket_New.ManejaSeriales, 
                      dbo.v_ImpresionTicket_New.NCFControl, dbo.v_ImpresionTicket_New.No_Linea, dbo.v_ImpresionTicket_New.Secuencia, dbo.NC.Fecha, 
                      dbo.v_ImpresionTicket_New.RegistroDetalle AS Expr1
FROM         dbo.NC LEFT OUTER JOIN
                      dbo.v_ImpresionTicket_New ON dbo.NC.Ticket = dbo.v_ImpresionTicket_New.pIdRegistro AND 
                      dbo.NC.RegistroDetalle = dbo.v_ImpresionTicket_New.RegistroDetalle AND dbo.NC.Serie = dbo.v_ImpresionTicket_New.Serie
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "NC"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 240
               Right = 200
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "v_ImpresionTicket_New"
            Begin Extent = 
               Top = 50
               Left = 647
               Bottom = 287
               Right = 862
            End
            DisplayFlags = 280
            TopColumn = 38
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 59
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vw_NC'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vw_NC'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vw_NC'
GO
/****** Object:  StoredProcedure [dbo].[sp_ENVIAR_FACTURACION_COMUN]    Script Date: 08/13/2014 16:06:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Amaury Minaya
-- Create date: 2014-07-06
-- Description:	Enviar Facturas a GC-Server Comun
-- =============================================

CREATE PROCEDURE [dbo].[sp_ENVIAR_FACTURACION_COMUN] 
AS
BEGIN TRY
	DECLARE @CantFacturas int;
	
	select @CantFacturas=count(*) From dbo.[v_Facturas.CabeceraSAP]
       --BEGIN TRANSACTION
	IF @CantFacturas >=1
	BEGIN

       -- Envia Cabecera Factura a la Comun ---
		INSERT INTO [10.0.0.2].DB_PV_COMUN.dbo.[FacturaPOS.Cabecera]
					(FacturaNoPOS, ClienteID,ClienteNombre, ClienteRNC,FechaDocumento, fkSucursal, FormaPago, FechaEnvio, Codigo_Almacen,MontoGeneral,
					Codigo_Vendedor, fkTipoNCF, NumeroNCF, NombrePC,PagoNotaCredito,NumeroNotaCredito,Efectivo,Cheque,Tarjeta,NCredito,MontoDevuelta) 
		Select 
			pIdRegistro, 
			ClienteId, 
			ClienteNombre, 
			ClienteRNC,
			DocumentoFecha, 
			fkSucursal, 
			FormaPago, 
			GetDate() As FechaEnvio, 
			Codigo_Almacen, 
			MontoGeneral, 
			Codigo_Vendedor, 
			fkTipoNCF, 
			NCF, 
			NombrePC,
			NCredito,
			NoCheque,
			Efectivo,
			Cheque,
			Tarjeta,
			NCredito,
			MontoDevuelta
			From dbo.[v_Facturas.CabeceraSAP]
	
		-- Enviar Detalle Factura --		
		INSERT INTO [10.0.0.2].DB_PV_COMUN.dbo.[FacturaPOS.Detalle]
				(fkDocCabecera, No_Linea, ArticuloId, ArticuloDescripcion, ArticuloPrecio, ArticuloCantidad,
				 ArticuloMonto, ArticuloPorcientoDesc, ArticuloPorcientoImpuesto, ArticuloImporte,
				 fkUsuario, Codigo_Almacen,fkSucursal )
		Select 
			 fad.fkDocCabecera, fad.No_Linea, fad.ArticuloId, fad.ArticuloDescripcion, fad.ArticuloPrecio, fad.ArticuloCantidad,
			 fad.ArticuloMonto, fad.ArticuloPorcientoDesc, fad.ArticuloPorcientoImpuesto, fad.ArticuloImporte,
			 fad.fkUsuario, fad.Codigo_Almacen,fad.Codigo_Almacen 
		From dbo.[v_Facturas.CabeceraSAP] fac Inner Join dbo.[FacturaPOS.Detalle] fad On (fac.pIdRegistro = fad.fkDocCabecera)


		-- Enviar Seriales de Factura Detalle --
		INSERT INTO [10.0.0.2].DB_PV_COMUN.dbo.[FacturaPOS.Seriales]
				(SerialNo, fkIdArticulo, Estatus, fkFacturaVenta, Codigo_Almacen,No_Linea,fkSucursal)
		Select fad.Serie, fad.fkArticulo, 'U' Estatus, fad.fkDocCabecera, fad.Codigo_Almacen, fad.No_Linea,fad.Codigo_Almacen
		From dbo.[v_Facturas.CabeceraSAP] fac Inner Join dbo.[FacturaPOS.DetalleSerial]  fad On (fac.pIdRegistro = fad.fkDocCabecera)		

		-- Actualizar Estatus Cabecera Factura ---
		UPDATE dbo.[FacturaPOS.Cabecera] 
			Set DocumentoServidor ='Y' 
		From dbo.[v_Facturas.CabeceraSAP] fac Inner Join dbo.[FacturaPOS.Cabecera] fad On (fac.pIdRegistro = fad.pIdRegistro)	

       --COMMIT
    END
    RETURN(@CantFacturas)
    END TRY
    
    
    BEGIN CATCH
      IF @@TRANCOUNT > 0
         ROLLBACK

      -- Raise an error with the details of the exception
      DECLARE @ErrMsg nvarchar(4000), @ErrSeverity int
      SELECT @ErrMsg = ERROR_MESSAGE(),
             @ErrSeverity = ERROR_SEVERITY()

      RAISERROR(@ErrMsg, @ErrSeverity, 1)
    END CATCH
GO
/****** Object:  StoredProcedure [dbo].[sp_VerificaNC]    Script Date: 08/13/2014 16:06:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Amaury Minaya
-- Create date: 2014-07-05
-- Description:	Verifica si el articulo ya tiene Nota de Credito
-- =============================================
CREATE PROCEDURE [dbo].[sp_VerificaNC]
	@Ticket Int,
	@RegistroDetalle Int,
	@Serie nvarchar(32)
AS
BEGIN
	DECLARE @NumeroReq int;
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT @NumeroReq = numeroReq from vw_NC where ticket=@ticket and registrodetalle=@registrodetalle and serie=@serie
	RETURN (@NumeroReq)
END
GO
/****** Object:  Default [DF_Almacenes_IdSucursal]    Script Date: 08/13/2014 16:06:14 ******/
ALTER TABLE [dbo].[Almacenes] ADD  CONSTRAINT [DF_Almacenes_IdSucursal]  DEFAULT ((1)) FOR [IdSucursal]
GO
/****** Object:  Default [DF_Almacenes_Status]    Script Date: 08/13/2014 16:06:14 ******/
ALTER TABLE [dbo].[Almacenes] ADD  CONSTRAINT [DF_Almacenes_Status]  DEFAULT ((0)) FOR [Status]
GO
/****** Object:  Default [DF_Articulos_EnMano]    Script Date: 08/13/2014 16:06:14 ******/
ALTER TABLE [dbo].[Articulos] ADD  CONSTRAINT [DF_Articulos_EnMano]  DEFAULT ((0)) FOR [EnMano]
GO
/****** Object:  Default [DF_Articulos_FechaModificacion]    Script Date: 08/13/2014 16:06:14 ******/
ALTER TABLE [dbo].[Articulos] ADD  CONSTRAINT [DF_Articulos_FechaModificacion]  DEFAULT (getdate()) FOR [FechaModificacion]
GO
/****** Object:  Default [DF_Articulos.Precios_Costo]    Script Date: 08/13/2014 16:06:14 ******/
ALTER TABLE [dbo].[Articulos.Precios] ADD  CONSTRAINT [DF_Articulos.Precios_Costo]  DEFAULT ((0)) FOR [Costo]
GO
/****** Object:  Default [DF_Articulos.Precios_Doc_Actualizacion]    Script Date: 08/13/2014 16:06:14 ******/
ALTER TABLE [dbo].[Articulos.Precios] ADD  CONSTRAINT [DF_Articulos.Precios_Doc_Actualizacion]  DEFAULT ((-1)) FOR [Doc_Actualizacion]
GO
/****** Object:  Default [DF_Articulos.SalidaArticuloTranferencia_StatusTranfers]    Script Date: 08/13/2014 16:06:14 ******/
ALTER TABLE [dbo].[Articulos.SalidaArticuloTranferencia] ADD  CONSTRAINT [DF_Articulos.SalidaArticuloTranferencia_StatusTranfers]  DEFAULT ((1)) FOR [StatusTranfers]
GO
/****** Object:  Default [DF_Articulos.Seriales_No_Linea]    Script Date: 08/13/2014 16:06:14 ******/
ALTER TABLE [dbo].[Articulos.Seriales] ADD  CONSTRAINT [DF_Articulos.Seriales_No_Linea]  DEFAULT ((0)) FOR [No_Linea]
GO
/****** Object:  Default [DF_Articulos.Seriales_fkFacturaVenta]    Script Date: 08/13/2014 16:06:14 ******/
ALTER TABLE [dbo].[Articulos.Seriales] ADD  CONSTRAINT [DF_Articulos.Seriales_fkFacturaVenta]  DEFAULT ((0)) FOR [fkFacturaVenta]
GO
/****** Object:  Default [DF_Articulos.Seriales_Cantidad]    Script Date: 08/13/2014 16:06:14 ******/
ALTER TABLE [dbo].[Articulos.Seriales] ADD  CONSTRAINT [DF_Articulos.Seriales_Cantidad]  DEFAULT ((1)) FOR [Cantidad]
GO
/****** Object:  Default [DF_Caja.AbrirCerrar_MontoApertura]    Script Date: 08/13/2014 16:06:14 ******/
ALTER TABLE [dbo].[Caja.AbrirCerrar] ADD  CONSTRAINT [DF_Caja.AbrirCerrar_MontoApertura]  DEFAULT ((0)) FOR [MontoApertura]
GO
/****** Object:  Default [DF_Caja.AbrirCerrar_MontoCierre]    Script Date: 08/13/2014 16:06:14 ******/
ALTER TABLE [dbo].[Caja.AbrirCerrar] ADD  CONSTRAINT [DF_Caja.AbrirCerrar_MontoCierre]  DEFAULT ((0)) FOR [MontoCierre]
GO
/****** Object:  Default [DF_FacturaPOS.Cabecera_DocumentoServidor]    Script Date: 08/13/2014 16:06:14 ******/
ALTER TABLE [dbo].[FacturaPOS.Cabecera] ADD  CONSTRAINT [DF_FacturaPOS.Cabecera_DocumentoServidor]  DEFAULT ('N') FOR [DocumentoServidor]
GO
/****** Object:  Default [DF_FacturaPOS.Cabecera_FormaPago]    Script Date: 08/13/2014 16:06:14 ******/
ALTER TABLE [dbo].[FacturaPOS.Cabecera] ADD  CONSTRAINT [DF_FacturaPOS.Cabecera_FormaPago]  DEFAULT ('E') FOR [FormaPago]
GO
/****** Object:  Default [DF_FacturaPOS.Cabecera_SubTotal]    Script Date: 08/13/2014 16:06:14 ******/
ALTER TABLE [dbo].[FacturaPOS.Cabecera] ADD  CONSTRAINT [DF_FacturaPOS.Cabecera_SubTotal]  DEFAULT ((0)) FOR [SubTotal]
GO
/****** Object:  Default [DF_FacturaPOS.Cabecera_SubTotal1]    Script Date: 08/13/2014 16:06:14 ******/
ALTER TABLE [dbo].[FacturaPOS.Cabecera] ADD  CONSTRAINT [DF_FacturaPOS.Cabecera_SubTotal1]  DEFAULT ((0)) FOR [TotalDesc]
GO
/****** Object:  Default [DF_FacturaPOS.Cabecera_SubTotal2]    Script Date: 08/13/2014 16:06:14 ******/
ALTER TABLE [dbo].[FacturaPOS.Cabecera] ADD  CONSTRAINT [DF_FacturaPOS.Cabecera_SubTotal2]  DEFAULT ((0)) FOR [TotalItbis]
GO
/****** Object:  Default [DF_FacturaPOS.Cabecera_TotalItbis1]    Script Date: 08/13/2014 16:06:14 ******/
ALTER TABLE [dbo].[FacturaPOS.Cabecera] ADD  CONSTRAINT [DF_FacturaPOS.Cabecera_TotalItbis1]  DEFAULT ((0)) FOR [Efectivo]
GO
/****** Object:  Default [DF_FacturaPOS.Cabecera_TotalItbis2]    Script Date: 08/13/2014 16:06:14 ******/
ALTER TABLE [dbo].[FacturaPOS.Cabecera] ADD  CONSTRAINT [DF_FacturaPOS.Cabecera_TotalItbis2]  DEFAULT ((0)) FOR [Cheque]
GO
/****** Object:  Default [DF_FacturaPOS.Cabecera_TotalItbis3]    Script Date: 08/13/2014 16:06:14 ******/
ALTER TABLE [dbo].[FacturaPOS.Cabecera] ADD  CONSTRAINT [DF_FacturaPOS.Cabecera_TotalItbis3]  DEFAULT ((0)) FOR [NoCheque]
GO
/****** Object:  Default [DF_FacturaPOS.Cabecera_TotalItbis4]    Script Date: 08/13/2014 16:06:14 ******/
ALTER TABLE [dbo].[FacturaPOS.Cabecera] ADD  CONSTRAINT [DF_FacturaPOS.Cabecera_TotalItbis4]  DEFAULT ((0)) FOR [Tarjeta]
GO
/****** Object:  Default [DF_FacturaPOS.Cabecera_TotalItbis5]    Script Date: 08/13/2014 16:06:14 ******/
ALTER TABLE [dbo].[FacturaPOS.Cabecera] ADD  CONSTRAINT [DF_FacturaPOS.Cabecera_TotalItbis5]  DEFAULT ((0)) FOR [NoTarjeta]
GO
/****** Object:  Default [DF_FacturaPOS.Cabecera_TotalItbis6]    Script Date: 08/13/2014 16:06:14 ******/
ALTER TABLE [dbo].[FacturaPOS.Cabecera] ADD  CONSTRAINT [DF_FacturaPOS.Cabecera_TotalItbis6]  DEFAULT ((0)) FOR [Ncredito]
GO
/****** Object:  Default [DF_FacturaPOS.Cabecera_MOntoEfectivo]    Script Date: 08/13/2014 16:06:14 ******/
ALTER TABLE [dbo].[FacturaPOS.Cabecera] ADD  CONSTRAINT [DF_FacturaPOS.Cabecera_MOntoEfectivo]  DEFAULT ((0)) FOR [MOntoEfectivo]
GO
/****** Object:  Default [DF_FacturaPOS.Cabecera_MontoDevuelta]    Script Date: 08/13/2014 16:06:14 ******/
ALTER TABLE [dbo].[FacturaPOS.Cabecera] ADD  CONSTRAINT [DF_FacturaPOS.Cabecera_MontoDevuelta]  DEFAULT ((0)) FOR [MontoDevuelta]
GO
/****** Object:  Default [DF_FacturaPOS.Cabecera_PagoEfectivo]    Script Date: 08/13/2014 16:06:14 ******/
ALTER TABLE [dbo].[FacturaPOS.Cabecera] ADD  CONSTRAINT [DF_FacturaPOS.Cabecera_PagoEfectivo]  DEFAULT ((0)) FOR [PagoEfectivo]
GO
/****** Object:  Default [DF_FacturaPOS.Cabecera_PagoCheque]    Script Date: 08/13/2014 16:06:14 ******/
ALTER TABLE [dbo].[FacturaPOS.Cabecera] ADD  CONSTRAINT [DF_FacturaPOS.Cabecera_PagoCheque]  DEFAULT ((0)) FOR [PagoCheque]
GO
/****** Object:  Default [DF_FacturaPOS.Cabecera_PagoTarjetaCredito]    Script Date: 08/13/2014 16:06:14 ******/
ALTER TABLE [dbo].[FacturaPOS.Cabecera] ADD  CONSTRAINT [DF_FacturaPOS.Cabecera_PagoTarjetaCredito]  DEFAULT ((0)) FOR [PagoTarjetaCredito]
GO
/****** Object:  Default [DF_FacturaPOS.Cabecera_PagoTarjetaDebito]    Script Date: 08/13/2014 16:06:14 ******/
ALTER TABLE [dbo].[FacturaPOS.Cabecera] ADD  CONSTRAINT [DF_FacturaPOS.Cabecera_PagoTarjetaDebito]  DEFAULT ((0)) FOR [PagoTarjetaDebito]
GO
/****** Object:  Default [DF_FacturaPOS.Cabecera_PagoNotaCredito]    Script Date: 08/13/2014 16:06:14 ******/
ALTER TABLE [dbo].[FacturaPOS.Cabecera] ADD  CONSTRAINT [DF_FacturaPOS.Cabecera_PagoNotaCredito]  DEFAULT ((0)) FOR [PagoNotaCredito]
GO
/****** Object:  Default [DF_FacturaPOS.Detalle_SecSerial]    Script Date: 08/13/2014 16:06:14 ******/
ALTER TABLE [dbo].[FacturaPOS.Detalle] ADD  CONSTRAINT [DF_FacturaPOS.Detalle_SecSerial]  DEFAULT ((-1)) FOR [SecSerial]
GO
/****** Object:  Default [DF_FacturaPOS.Detalle_No_Linea]    Script Date: 08/13/2014 16:06:14 ******/
ALTER TABLE [dbo].[FacturaPOS.Detalle] ADD  CONSTRAINT [DF_FacturaPOS.Detalle_No_Linea]  DEFAULT ((0)) FOR [No_Linea]
GO
/****** Object:  Default [DF_FacturaPOS.Detalle_ArticuloCosto]    Script Date: 08/13/2014 16:06:14 ******/
ALTER TABLE [dbo].[FacturaPOS.Detalle] ADD  CONSTRAINT [DF_FacturaPOS.Detalle_ArticuloCosto]  DEFAULT ((0)) FOR [ArticuloCosto]
GO
/****** Object:  Default [DF_FacturaPOS.Detalle_ArticuloPrecio]    Script Date: 08/13/2014 16:06:14 ******/
ALTER TABLE [dbo].[FacturaPOS.Detalle] ADD  CONSTRAINT [DF_FacturaPOS.Detalle_ArticuloPrecio]  DEFAULT ((0)) FOR [ArticuloPrecio]
GO
/****** Object:  Default [DF_FacturaPOS.Detalle_ArticuloPrecioDesc]    Script Date: 08/13/2014 16:06:14 ******/
ALTER TABLE [dbo].[FacturaPOS.Detalle] ADD  CONSTRAINT [DF_FacturaPOS.Detalle_ArticuloPrecioDesc]  DEFAULT ((0)) FOR [ArticuloPrecioDesc]
GO
/****** Object:  Default [DF_FacturaPOS.Detalle_ArticuloCantidad]    Script Date: 08/13/2014 16:06:14 ******/
ALTER TABLE [dbo].[FacturaPOS.Detalle] ADD  CONSTRAINT [DF_FacturaPOS.Detalle_ArticuloCantidad]  DEFAULT ((0)) FOR [ArticuloCantidad]
GO
/****** Object:  Default [DF_FacturaPOS.Detalle_ArticuloMonto]    Script Date: 08/13/2014 16:06:14 ******/
ALTER TABLE [dbo].[FacturaPOS.Detalle] ADD  CONSTRAINT [DF_FacturaPOS.Detalle_ArticuloMonto]  DEFAULT ((0)) FOR [ArticuloMonto]
GO
/****** Object:  Default [DF_FacturaPOS.Detalle_ArticuloPorcientoDesc]    Script Date: 08/13/2014 16:06:14 ******/
ALTER TABLE [dbo].[FacturaPOS.Detalle] ADD  CONSTRAINT [DF_FacturaPOS.Detalle_ArticuloPorcientoDesc]  DEFAULT ((0)) FOR [ArticuloPorcientoDesc]
GO
/****** Object:  Default [DF_FacturaPOS.Detalle_ArticuloTotalDescuento]    Script Date: 08/13/2014 16:06:14 ******/
ALTER TABLE [dbo].[FacturaPOS.Detalle] ADD  CONSTRAINT [DF_FacturaPOS.Detalle_ArticuloTotalDescuento]  DEFAULT ((0)) FOR [ArticuloTotalDescuento]
GO
/****** Object:  Default [DF_FacturaPOS.Detalle_ArticuloPorcientoImpuesto]    Script Date: 08/13/2014 16:06:14 ******/
ALTER TABLE [dbo].[FacturaPOS.Detalle] ADD  CONSTRAINT [DF_FacturaPOS.Detalle_ArticuloPorcientoImpuesto]  DEFAULT ((0)) FOR [ArticuloPorcientoImpuesto]
GO
/****** Object:  Default [DF_FacturaPOS.Detalle_ArticuloPrecioImpuesto]    Script Date: 08/13/2014 16:06:14 ******/
ALTER TABLE [dbo].[FacturaPOS.Detalle] ADD  CONSTRAINT [DF_FacturaPOS.Detalle_ArticuloPrecioImpuesto]  DEFAULT ((0)) FOR [ArticuloPrecioImpuesto]
GO
/****** Object:  Default [DF_FacturaPOS.Detalle_ArticuloTotalImpuesto]    Script Date: 08/13/2014 16:06:14 ******/
ALTER TABLE [dbo].[FacturaPOS.Detalle] ADD  CONSTRAINT [DF_FacturaPOS.Detalle_ArticuloTotalImpuesto]  DEFAULT ((0)) FOR [ArticuloTotalImpuesto]
GO
/****** Object:  Default [DF_FacturaPOS.Detalle_ArticuloImporte]    Script Date: 08/13/2014 16:06:14 ******/
ALTER TABLE [dbo].[FacturaPOS.Detalle] ADD  CONSTRAINT [DF_FacturaPOS.Detalle_ArticuloImporte]  DEFAULT ((0)) FOR [ArticuloImporte]
GO
/****** Object:  Default [DF_FacturaPOS.Detalle_ArticuloImporteNeto]    Script Date: 08/13/2014 16:06:14 ******/
ALTER TABLE [dbo].[FacturaPOS.Detalle] ADD  CONSTRAINT [DF_FacturaPOS.Detalle_ArticuloImporteNeto]  DEFAULT ((0)) FOR [ArticuloImporteNeto]
GO
/****** Object:  Default [DF_FacturaPOS.Detalle_PlanID]    Script Date: 08/13/2014 16:06:14 ******/
ALTER TABLE [dbo].[FacturaPOS.Detalle] ADD  CONSTRAINT [DF_FacturaPOS.Detalle_PlanID]  DEFAULT ((0)) FOR [PlanID]
GO
/****** Object:  Default [DF_FacturaPOS.Detalle_ManejaSeriales]    Script Date: 08/13/2014 16:06:14 ******/
ALTER TABLE [dbo].[FacturaPOS.Detalle] ADD  CONSTRAINT [DF_FacturaPOS.Detalle_ManejaSeriales]  DEFAULT ('N') FOR [ManejaSeriales]
GO
/****** Object:  Default [DF_FacturaPOS.DetalleSerial_Secuencia]    Script Date: 08/13/2014 16:06:14 ******/
ALTER TABLE [dbo].[FacturaPOS.DetalleSerial] ADD  CONSTRAINT [DF_FacturaPOS.DetalleSerial_Secuencia]  DEFAULT ((1)) FOR [Secuencia]
GO
/****** Object:  Default [DF_FacturaPOS.DetalleSerial_Estatus]    Script Date: 08/13/2014 16:06:14 ******/
ALTER TABLE [dbo].[FacturaPOS.DetalleSerial] ADD  CONSTRAINT [DF_FacturaPOS.DetalleSerial_Estatus]  DEFAULT ((0)) FOR [Estatus]
GO
/****** Object:  Default [DF_FacturaPOS.FacturaNCF_NCFControl]    Script Date: 08/13/2014 16:06:14 ******/
ALTER TABLE [dbo].[FacturaPOS.FacturaNCF] ADD  CONSTRAINT [DF_FacturaPOS.FacturaNCF_NCFControl]  DEFAULT ((1)) FOR [NCFControl]
GO
/****** Object:  Default [DF_FormaPago_Codigo]    Script Date: 08/13/2014 16:06:14 ******/
ALTER TABLE [dbo].[FormaPago] ADD  CONSTRAINT [DF_FormaPago_Codigo]  DEFAULT ((0)) FOR [Codigo]
GO
/****** Object:  Default [DF_FormaPago_Fecha]    Script Date: 08/13/2014 16:06:14 ******/
ALTER TABLE [dbo].[FormaPago] ADD  CONSTRAINT [DF_FormaPago_Fecha]  DEFAULT (getdate()) FOR [Fecha]
GO
/****** Object:  Default [DF_Table_1_UltimoUsuarioActualizo]    Script Date: 08/13/2014 16:06:14 ******/
ALTER TABLE [dbo].[FormaPago] ADD  CONSTRAINT [DF_Table_1_UltimoUsuarioActualizo]  DEFAULT (getdate()) FOR [UltimaFechaActualizacion]
GO
/****** Object:  Default [DF_NCF.Control_ControlNCF]    Script Date: 08/13/2014 16:06:14 ******/
ALTER TABLE [dbo].[NCF.Control] ADD  CONSTRAINT [DF_NCF.Control_ControlNCF]  DEFAULT ((0)) FOR [ControlNCF]
GO
/****** Object:  Default [DF_PagoDetalle_Monto]    Script Date: 08/13/2014 16:06:14 ******/
ALTER TABLE [dbo].[PagoDetalle] ADD  CONSTRAINT [DF_PagoDetalle_Monto]  DEFAULT ((0)) FOR [Monto]
GO
/****** Object:  Default [DF_PagoDetalle_Fecha]    Script Date: 08/13/2014 16:06:14 ******/
ALTER TABLE [dbo].[PagoDetalle] ADD  CONSTRAINT [DF_PagoDetalle_Fecha]  DEFAULT (getdate()) FOR [Fecha]
GO
/****** Object:  Default [DF_PagoDetalle_UltimaFechaActualizacion]    Script Date: 08/13/2014 16:06:14 ******/
ALTER TABLE [dbo].[PagoDetalle] ADD  CONSTRAINT [DF_PagoDetalle_UltimaFechaActualizacion]  DEFAULT (getdate()) FOR [UltimaFechaActualizacion]
GO
/****** Object:  Default [DF_Planes_Requiere_Contrato]    Script Date: 08/13/2014 16:06:14 ******/
ALTER TABLE [dbo].[Planes] ADD  CONSTRAINT [DF_Planes_Requiere_Contrato]  DEFAULT ('N') FOR [Requiere_Contrato]
GO
/****** Object:  Default [DF_Planes_Requiere_Equipo]    Script Date: 08/13/2014 16:06:14 ******/
ALTER TABLE [dbo].[Planes] ADD  CONSTRAINT [DF_Planes_Requiere_Equipo]  DEFAULT ('N') FOR [Requiere_Equipo]
GO
/****** Object:  Default [DF_Planes_Requiere_Card]    Script Date: 08/13/2014 16:06:14 ******/
ALTER TABLE [dbo].[Planes] ADD  CONSTRAINT [DF_Planes_Requiere_Card]  DEFAULT ('N') FOR [Requiere_Card]
GO
/****** Object:  Default [DF_Planes_Requiere_Sim]    Script Date: 08/13/2014 16:06:14 ******/
ALTER TABLE [dbo].[Planes] ADD  CONSTRAINT [DF_Planes_Requiere_Sim]  DEFAULT ('N') FOR [Requiere_Sim]
GO
/****** Object:  Default [DF_Planes_Requiere_Pago_A]    Script Date: 08/13/2014 16:06:14 ******/
ALTER TABLE [dbo].[Planes] ADD  CONSTRAINT [DF_Planes_Requiere_Pago_A]  DEFAULT ('N') FOR [Requiere_Pago_A]
GO
/****** Object:  Default [DF_Sucursal_pkIdRegistro]    Script Date: 08/13/2014 16:06:14 ******/
ALTER TABLE [dbo].[Sucursal] ADD  CONSTRAINT [DF_Sucursal_pkIdRegistro]  DEFAULT ((1)) FOR [pkIdRegistro]
GO
/****** Object:  Default [DF_Sucursal_Comprobante]    Script Date: 08/13/2014 16:06:14 ******/
ALTER TABLE [dbo].[Sucursal] ADD  CONSTRAINT [DF_Sucursal_Comprobante]  DEFAULT ((1)) FOR [Comprobante]
GO
/****** Object:  Default [DF_TempArticulo_Cantidad]    Script Date: 08/13/2014 16:06:14 ******/
ALTER TABLE [dbo].[TempArticulo] ADD  CONSTRAINT [DF_TempArticulo_Cantidad]  DEFAULT ((0)) FOR [Cantidad]
GO
/****** Object:  Default [DF_TIMER_MILISEGUNDO]    Script Date: 08/13/2014 16:06:14 ******/
ALTER TABLE [dbo].[TIMER] ADD  CONSTRAINT [DF_TIMER_MILISEGUNDO]  DEFAULT ((30000)) FOR [MILISEGUNDO]
GO
/****** Object:  Default [DF_TIMER_EJECUTAR_1]    Script Date: 08/13/2014 16:06:14 ******/
ALTER TABLE [dbo].[TIMER] ADD  CONSTRAINT [DF_TIMER_EJECUTAR_1]  DEFAULT ((1)) FOR [EJECUTAR]
GO
/****** Object:  Default [DF_Usuarios_fkSucursal]    Script Date: 08/13/2014 16:06:14 ******/
ALTER TABLE [dbo].[Usuarios] ADD  CONSTRAINT [DF_Usuarios_fkSucursal]  DEFAULT ((1)) FOR [fkSucursal]
GO
/****** Object:  Default [DF_Usuarios_Conectado]    Script Date: 08/13/2014 16:06:14 ******/
ALTER TABLE [dbo].[Usuarios] ADD  CONSTRAINT [DF_Usuarios_Conectado]  DEFAULT ('N') FOR [Conectado]
GO
/****** Object:  Default [DF_Usuarios_VENTAS]    Script Date: 08/13/2014 16:06:14 ******/
ALTER TABLE [dbo].[Usuarios] ADD  CONSTRAINT [DF_Usuarios_VENTAS]  DEFAULT ((0)) FOR [VENTAS]
GO
/****** Object:  Default [DF_Usuarios_VENTAS1]    Script Date: 08/13/2014 16:06:14 ******/
ALTER TABLE [dbo].[Usuarios] ADD  CONSTRAINT [DF_Usuarios_VENTAS1]  DEFAULT ((0)) FOR [ADMINISTRAR]
GO
/****** Object:  Default [DF_Usuarios_VENTAS2]    Script Date: 08/13/2014 16:06:14 ******/
ALTER TABLE [dbo].[Usuarios] ADD  CONSTRAINT [DF_Usuarios_VENTAS2]  DEFAULT ((0)) FOR [INVENTARIO]
GO
/****** Object:  Default [DF_Usuarios_VENTAS3]    Script Date: 08/13/2014 16:06:14 ******/
ALTER TABLE [dbo].[Usuarios] ADD  CONSTRAINT [DF_Usuarios_VENTAS3]  DEFAULT ((0)) FOR [REPORTES]
GO
/****** Object:  Default [DF_Usuarios_VENTAS4]    Script Date: 08/13/2014 16:06:14 ******/
ALTER TABLE [dbo].[Usuarios] ADD  CONSTRAINT [DF_Usuarios_VENTAS4]  DEFAULT ((0)) FOR [CONSULTAS]
GO
/****** Object:  Default [DF_Usuarios_VENTAS5]    Script Date: 08/13/2014 16:06:14 ******/
ALTER TABLE [dbo].[Usuarios] ADD  CONSTRAINT [DF_Usuarios_VENTAS5]  DEFAULT ((0)) FOR [SINCRONIZAR]
GO
/****** Object:  Default [DF_Usuarios_SINCRONIZAR_AUT]    Script Date: 08/13/2014 16:06:14 ******/
ALTER TABLE [dbo].[Usuarios] ADD  CONSTRAINT [DF_Usuarios_SINCRONIZAR_AUT]  DEFAULT ((0)) FOR [SINCRONIZAR_AUT]
GO
/****** Object:  Default [DF_Usuarios_VENTAS6]    Script Date: 08/13/2014 16:06:14 ******/
ALTER TABLE [dbo].[Usuarios] ADD  CONSTRAINT [DF_Usuarios_VENTAS6]  DEFAULT ((0)) FOR [DESCUENTO]
GO
/****** Object:  Default [DF_Usuarios_VENTAS7]    Script Date: 08/13/2014 16:06:14 ******/
ALTER TABLE [dbo].[Usuarios] ADD  CONSTRAINT [DF_Usuarios_VENTAS7]  DEFAULT ((0)) FOR [CambioAlmacen]
GO
/****** Object:  Default [DF_Usuarios_VENTAS8]    Script Date: 08/13/2014 16:06:14 ******/
ALTER TABLE [dbo].[Usuarios] ADD  CONSTRAINT [DF_Usuarios_VENTAS8]  DEFAULT ((0)) FOR [PRECIO]
GO
/****** Object:  Default [DF_Usuarios_PRECIO1]    Script Date: 08/13/2014 16:06:14 ******/
ALTER TABLE [dbo].[Usuarios] ADD  CONSTRAINT [DF_Usuarios_PRECIO1]  DEFAULT ((0)) FOR [REIMPRIMIR]
GO
/****** Object:  Default [DF_Usuarios_1_DESCUENTO]    Script Date: 08/13/2014 16:06:14 ******/
ALTER TABLE [dbo].[Usuarios_1] ADD  CONSTRAINT [DF_Usuarios_1_DESCUENTO]  DEFAULT ((0)) FOR [DESCUENTO]
GO
/****** Object:  Default [DF_Usuarios_1_CambioAlmacen]    Script Date: 08/13/2014 16:06:14 ******/
ALTER TABLE [dbo].[Usuarios_1] ADD  CONSTRAINT [DF_Usuarios_1_CambioAlmacen]  DEFAULT ((0)) FOR [CambioAlmacen]
GO
/****** Object:  ForeignKey [FK_FacturaPOS.Detalle_FacturaPOS.Detalle]    Script Date: 08/13/2014 16:06:14 ******/
ALTER TABLE [dbo].[FacturaPOS.Detalle]  WITH CHECK ADD  CONSTRAINT [FK_FacturaPOS.Detalle_FacturaPOS.Detalle] FOREIGN KEY([pIdRegistro])
REFERENCES [dbo].[FacturaPOS.Detalle] ([pIdRegistro])
GO
ALTER TABLE [dbo].[FacturaPOS.Detalle] CHECK CONSTRAINT [FK_FacturaPOS.Detalle_FacturaPOS.Detalle]
GO
