unit UntCerrarCaja;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, dxSkinsCore,
  dxSkinsDefaultPainters, Vcl.StdCtrls, cxMaskEdit, cxDropDownEdit, cxCalendar,
  cxTextEdit, cxCurrencyEdit, Vcl.ImgList, Vcl.ExtCtrls, Vcl.ComCtrls,
  dxGDIPlusClasses, JvExControls, JvXPCore, JvXPButtons;

type
  TfrmCerrarCaja = class(TForm)
    lbl1: TLabel;
    lbl2: TLabel;
    lbl3: TLabel;
    lbl4: TLabel;
    edtMonto: TcxCurrencyEdit;
    edtDescripcion: TcxTextEdit;
    edtResponsable: TcxTextEdit;
    dtpFecha: TDateTimePicker;
    btnCerrarCaja: TJvXPButton;
    btnSalir: TJvXPButton;
    procedure FormShow(Sender: TObject);
    procedure btnCerrarCajaClick(Sender: TObject);
    procedure btnSalirClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FechaaCerrar;

  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmCerrarCaja: TfrmCerrarCaja;

implementation

{$R *.dfm}

uses UntMain, UntData, UntDataReportes;

procedure TfrmCerrarCaja.btnCerrarCajaClick(Sender: TObject);
begin
  //cCaja.MontoCierre:=DatMain.sp_Caja.Parameters.ParamByName('@Return_value').Value;
  frmMain.actCierraCaja.Execute;
  fn_RegistrodeCambio(CUsuario.Usuario,'Cierra Caja','Caja Cerrada: '+
  ccaja.Nombre+', Monto RD$: '+ FormatFloat ('###,###,##0.00',ccaja.MontoCierre));
end;

procedure TfrmCerrarCaja.btnSalirClick(Sender: TObject);
begin
  self.Close;
end;

procedure TfrmCerrarCaja.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action:=caFree;
  frmCerrarCaja:=nil;
end;

procedure TfrmCerrarCaja.FormShow(Sender: TObject);
begin
  FechaaCerrar;
  edtDescripcion.Text:=cCaja.Nombre;
  edtResponsable.Text:=cCaja.Responsable;
  btnCerrarCaja.SetFocus;
end;

procedure TfrmCerrarCaja.FechaaCerrar;
begin
  with DatMain.qryPOS do
  begin
    SQL.Text:='SELECT TOP 1 FechaApertura as Date FROM [Caja.AbrirCerrar] '+
    'WHERE EstadoCaja='+#39+'A'+#39+' AND NombrePC='+#39+ccaja.Nombre+#39;
    Open;
    dtpFecha.DateTime:=FieldByName('Date').Value;
  end;
end;

end.
