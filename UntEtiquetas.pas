unit UntEtiquetas;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, dxSkinsCore,
  dxSkinsDefaultPainters, Vcl.Menus, cxSpinEdit, cxDBEdit, Vcl.StdCtrls,
  cxButtons, cxTextEdit, cxMaskEdit, cxButtonEdit, Vcl.Mask, JvExMask, JvSpin,
  cxPC, dxDockControl, dxDockPanel, cxLabel, Vcl.ExtCtrls, Vcl.DBCtrls,
  JvDBControls, Data.DB, JvDataSource, Data.Win.ADODB, JvADOQuery,
  dxSkinsdxDockControlPainter, cxClasses, ppDB, ppDBPipe, ppDesignLayer,
  ppBands, ppCtrls, ppPrnabl, ppClass, ppBarCod, ppCache, ppParameter, ppComm,
  ppRelatv, ppProd, ppReport, ppViewr, cxNavigator, cxDBNavigator,
  Vcl.PlatformDefaultStyleActnCtrls, Vcl.ActnList, Vcl.ActnMan,
  Vcl.Imaging.pngimage, dxSkinBlack, dxSkinBlue, dxSkinBlueprint, dxSkinCaramel,
  dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinDevExpressDarkStyle,
  dxSkinDevExpressStyle, dxSkinFoggy, dxSkinGlassOceans, dxSkinHighContrast,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black, dxSkinOffice2007Blue,
  dxSkinOffice2007Green, dxSkinOffice2007Pink, dxSkinOffice2007Silver,
  dxSkinOffice2010Black, dxSkinOffice2010Blue, dxSkinOffice2010Silver,
  dxSkinPumpkin, dxSkinSeven, dxSkinSevenClassic, dxSkinSharp, dxSkinSharpPlus,
  dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinTheAsphaltWorld, dxSkinValentine, dxSkinVS2010, dxSkinWhiteprint,
  dxSkinXmas2008Blue;

type
  TfrmEtiquetas = class(TForm)
    grp1: TGroupBox;
    cxlbl2: TcxLabel;
    lbl1: TLabel;
    lbl2: TLabel;
    lbl4: TLabel;
    edt2: TcxDBTextEdit;
    edt1: TcxDBTextEdit;
    edt4: TcxDBTextEdit;
    lbl8: TLabel;
    edt6: TcxDBTextEdit;
    grp2: TGroupBox;
    cxlbl1: TcxLabel;
    lbl6: TLabel;
    grp3: TGroupBox;
    cxlbl3: TcxLabel;
    qryEtiqueta: TJvADOQuery;
    dsEtiqueta: TJvDataSource;
    lbl5: TLabel;
    btnImprimir: TcxButton;
    edtCantidad: TcxSpinEdit;
    edtCodigoArt: TcxTextEdit;
    chkImprimePrecio: TCheckBox;
    edt3: TcxDBTextEdit;
    lbl3: TLabel;
    procedure btnImprimirClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure edtCodigoArtKeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmEtiquetas: TfrmEtiquetas;

implementation

{$R *.dfm}

uses UntData, UntPreview, UntDataReportes;


procedure TfrmEtiquetas.btnImprimirClick(Sender: TObject);
begin
  with DatReportes.RptEtiqueta do begin
    Template.DatabaseSettings.Name:= 'Etiqueta';
    Template.LoadFromDatabase;

    PrinterSetup.Copies:=StrToInt(edtCantidad.Text);
//    fn_RegistrodeCambio(CUsuario.Usuario,'Impresión','Articulos o.: '+IntToStr(cFactura.Numero)+' Impresa');
    PrinterSetup.PrinterName:='Default';
    ShowCancelDialog:=True;
    PrinterSetup.Copies:=StrToInt(edtCantidad.Text);
    ShowPrintDialog:=True;
    if chkImprimePrecio.Checked then begin
      DatReportes.pdbtxtPrecio.Visible:=true;
    end
    else begin
      DatReportes.pdbtxtPrecio.Visible:=False;
    end;

    DeviceType:='Printer';

    PrintReport;

    edtCodigoArt.SetFocus;

  end;
end;

procedure TfrmEtiquetas.edtCodigoArtKeyPress(Sender: TObject; var Key: Char);
begin
if Key=#13 then begin
  with qryEtiqueta do
  begin
    SQL.Text:='SELECT * FROM vw_Etiqueta WHERE CodigoArt='+QuotedStr(edtCodigoArt.Text);
    Close;
    Open;
  end;
end;
end;

procedure TfrmEtiquetas.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action:=caFree;
  frmEtiquetas:=nil;
end;

procedure TfrmEtiquetas.FormShow(Sender: TObject);
begin
  edtCodigoArt.SetFocus;
end;

end.

