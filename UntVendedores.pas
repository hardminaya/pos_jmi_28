unit UntVendedores;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, dxSkinsCore, dxSkinsDefaultPainters,
  dxSkinscxPCPainter, cxPCdxBarPopupMenu, Vcl.ExtCtrls, Vcl.DBCtrls, cxPC,
  Data.DB, Data.Win.ADODB, cxStyles, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxDBData, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGridLevel, cxClasses, cxGridCustomView, cxGrid,
  Vcl.StdCtrls, cxVGrid, cxDBVGrid, cxInplaceContainer, cxNavigator,
  cxDBNavigator, Vcl.Menus, cxButtons, Vcl.ComCtrls, Vcl.ToolWin,
  Datasnap.DBClient, Datasnap.Provider, JvMenus, cxContainer, cxTextEdit,
  cxDBEdit, dxStatusBar, dxRibbonStatusBar, Vcl.DBActns, Vcl.StdActns,
  Vcl.ActnList, Vcl.PlatformDefaultStyleActnCtrls, Vcl.ActnMan;

type
  TfrmVendedores = class(TForm)
    dsVendedores: TDataSource;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1Id: TcxGridDBColumn;
    cxGrid1DBTableView1CodVendedor: TcxGridDBColumn;
    cxGrid1DBTableView1Nombre: TcxGridDBColumn;
    cxGrid1DBTableView1Apellido: TcxGridDBColumn;
    cxStyleRepository1: TcxStyleRepository;
    cxStyle1: TcxStyle;
    cxtbshtRegistro: TcxTabSheet;
    tblVendedores: TADOTable;
    edt1: TcxDBTextEdit;
    edt2: TcxDBTextEdit;
    edt3: TcxDBTextEdit;
    edt4: TcxDBTextEdit;
    tlb3: TToolBar;
    btn10: TToolButton;
    btnDataSetPrior1: TToolButton;
    btn14: TToolButton;
    btnDataSetEdit1: TToolButton;
    btn13: TToolButton;
    btn12: TToolButton;
    btn15: TToolButton;
    btn11: TToolButton;
    btn9: TToolButton;
    btn16: TToolButton;
    actmgr1: TActionManager;
    dtstpr1: TDataSetPrior;
    dtstnxt1: TDataSetNext;
    act1: TDataSetEdit;
    act3: TDataSetInsert;
    wndwcls1: TWindowClose;
    act2: TDataSetCancel;
    act4: TDataSetPost;
    act5: TDataSetDelete;
    lbl1: TLabel;
    lbl2: TLabel;
    lbl3: TLabel;
    lbl4: TLabel;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure mniModificar1Click(Sender: TObject);
    procedure mniEliminar1Click(Sender: TObject);
    procedure btnSalirClick(Sender: TObject);
    procedure cxGrid1DBTableView1CellDblClick(Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure qryVendedoresPostError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure dtstprvdrVendedoresUpdateError(Sender: TObject;
      DataSet: TCustomClientDataSet; E: EUpdateError; UpdateKind: TUpdateKind;
      var Response: TResolverResponse);
    procedure FormShow(Sender: TObject);

  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmVendedores: TfrmVendedores;
  var_Posteado : Boolean;

implementation

{$R *.dfm}

uses UntMain, UntData;

procedure TfrmVendedores.btnSalirClick(Sender: TObject);
begin
  Self.Close;
end;

procedure TfrmVendedores.cxGrid1DBTableView1CellDblClick(
  Sender: TcxCustomGridTableView; ACellViewInfo: TcxGridTableDataCellViewInfo;
  AButton: TMouseButton; AShift: TShiftState; var AHandled: Boolean);
begin
  cxPageControl1.ActivePageIndex:=1;
end;

procedure TfrmVendedores.dtstprvdrVendedoresUpdateError(Sender: TObject;
  DataSet: TCustomClientDataSet; E: EUpdateError; UpdateKind: TUpdateKind;
  var Response: TResolverResponse);
begin
  ShowMessage(E.Message);
end;

procedure TfrmVendedores.FormClose(Sender: TObject; var Action: TCloseAction);
begin
//if dsVendedores.State=dsEdit then begin
//    if MessageBox(Handle,'Los Datos no han sido guardados, se perderan, est� Seguro de Salir?','Vendedores',MB_ICONQUESTION or MB_YESNO)=ID_YES then
//    begin
//      dsVendedores.DataSet.Cancel;
//      Action:=caFree;
//      frmVendedores:=nil;
//    end
//    else
//    begin
//      exit;
//    end;
//  end;
  Action:=caFree;
  frmVendedores:=nil;
end;

procedure TfrmVendedores.FormShow(Sender: TObject);
begin
  dsVendedores.DataSet.Open;
end;

procedure TfrmVendedores.mniEliminar1Click(Sender: TObject);
begin
  if MessageBox(Handle,'Desear Eliminar el registro?','Vendedores',MB_ICONQUESTION or MB_YESNO)=ID_YES then
  begin
    dsVendedores.DataSet.Delete;;
  end;
end;

procedure TfrmVendedores.mniModificar1Click(Sender: TObject);
begin
  var_Posteado:=False;
  cxPageControl1.ActivePageIndex:=1;
  dsVendedores.DataSet.Edit;
end;

procedure TfrmVendedores.qryVendedoresPostError(DataSet: TDataSet;
  E: EDatabaseError; var Action: TDataAction);
begin
  ShowMessage(E.Message);
end;

end.
