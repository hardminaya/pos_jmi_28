unit UntConsultaVentasxVendedores;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, dxSkinsCore, dxSkinsDefaultPainters,
  dxSkinscxPCPainter, cxPCdxBarPopupMenu, Vcl.Menus, cxContainer, cxEdit,
  Data.DB, Data.Win.ADODB, JvComponentBase, JvDBGridExport, cxTextEdit,
  cxMaskEdit, cxDropDownEdit, cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox,
  cxNavigator, cxDBNavigator, Vcl.ComCtrls, JvExComCtrls, JvDateTimePicker,
  Vcl.StdCtrls, cxButtons, JvExControls, JvXPCore, JvXPButtons, Vcl.Grids,
  Vcl.DBGrids, JvExDBGrids, JvDBGrid, cxPC, cxCurrencyEdit, Vcl.ToolWin,
  Vcl.DBActns, Vcl.StdActns, Vcl.ActnList, Vcl.PlatformDefaultStyleActnCtrls,
  Vcl.ActnMan;

type
  TfrmConsultaVentasXVendedores = class(TForm)
    lbl2: TLabel;
    lbl1: TLabel;
    lbl3: TLabel;
    lblVendedor: TLabel;
    cxpgcntrl1: TcxPageControl;
    cxtbsht1: TcxTabSheet;
    jvdbgrd1: TJvDBGrid;
    btn1: TJvXPToolButton;
    btnExportarExcel: TcxButton;
    jvdtmpckrHasta: TJvDateTimePicker;
    jvdtmpckrDesde: TJvDateTimePicker;
    btnRefrescar: TcxButton;
    cxdbnvgtr1: TcxDBNavigator;
    cbbVendedor: TcxLookupComboBox;
    jvdbgrdxclxprt1: TJvDBGridExcelExport;
    spVentasporVendedor: TADOStoredProc;
    dsVentasporVendedor: TDataSource;
    flsvdlg1: TFileSaveDialog;
    dsVendedores: TDataSource;
    qry1: TADOQuery;
    dtmfldVentasporVendedorDocumentoFecha: TDateTimeField;
    intgrfldVentasporVendedorFacturaNo: TIntegerField;
    wdstrngfldVentasporVendedorClienteID: TWideStringField;
    wdstrngfldVentasporVendedorClienteNombre: TWideStringField;
    QryArticuloVentasporVendedorMontoGeneral: TFMTBCDField;
    QryArticuloVentasporVendedorNCredito: TFMTBCDField;
    QryArticuloVentasporVendedorImporte: TFMTBCDField;
    lbl4: TLabel;
    lblTotal: TLabel;
    QryArticuloVentasporVendedorTotal: TFMTBCDField;
    actmgr1: TActionManager;
    dtstpr1: TDataSetPrior;
    dtstnxt1: TDataSetNext;
    act1: TDataSetEdit;
    act3: TDataSetInsert;
    wndwcls1: TWindowClose;
    act2: TDataSetCancel;
    act4: TDataSetPost;
    act5: TDataSetDelete;
    tlb3: TToolBar;
    btn16: TToolButton;
    acExportarExcel: TAction;
    procedure Refrescar;
    procedure cbbVendedorPropertiesChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnExportarExcelClick(Sender: TObject);
    procedure jvdtmpckrDesdeChange(Sender: TObject);
    procedure jvdtmpckrHastaChange(Sender: TObject);
    procedure btnRefrescarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acExportarExcelExecute(Sender: TObject);

  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmConsultaVentasXVendedores: TfrmConsultaVentasXVendedores;
  var_Vendedor : string;

implementation

uses
  UntData;

{$R *.dfm}

procedure TfrmConsultaVentasXVendedores.acExportarExcelExecute(Sender: TObject);
var
  archivo : String;
begin
  flsvdlg1.Execute;
  archivo:=flsvdlg1.FileName;
  if Length(archivo)>0 then begin
    jvdbgrdxclxprt1.FileName:=archivo;
    jvdbgrdxclxprt1.ExportGrid;
  end;
end;

procedure TfrmConsultaVentasXVendedores.btnExportarExcelClick(Sender: TObject);
var
  archivo : String;
begin
  flsvdlg1.Execute;
  archivo:=flsvdlg1.FileName;
  if Length(archivo)>0 then begin
    jvdbgrdxclxprt1.FileName:=archivo;
    jvdbgrdxclxprt1.ExportGrid;
  end;
end;

procedure TfrmConsultaVentasXVendedores.btnRefrescarClick(Sender: TObject);
begin
  Refrescar;
end;

procedure TfrmConsultaVentasXVendedores.cbbVendedorPropertiesChange(
  Sender: TObject);
begin
  with cbbVendedor.Properties.DataController do
  begin
    var_Vendedor:=Values[FocusedRecordIndex,1];
  end;
  Refrescar;
end;

procedure TfrmConsultaVentasXVendedores.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action:=caFree;
  frmConsultaVentasXVendedores:=nil;
end;

procedure TfrmConsultaVentasXVendedores.FormShow(Sender: TObject);
begin
  jvdtmpckrDesde.Date:=Now;
  jvdtmpckrHasta.Date:=Now;
end;

procedure TfrmConsultaVentasXVendedores.jvdtmpckrDesdeChange(Sender: TObject);
begin
  Refrescar;
end;

procedure TfrmConsultaVentasXVendedores.jvdtmpckrHastaChange(Sender: TObject);
begin
  Refrescar;
end;

procedure TfrmConsultaVentasXVendedores.Refrescar;
begin
  if cbbVendedor.Text='' then begin
    MessageBox(Handle,'Debe seleccionar un vendedor','ATENCION Consulta Ventas por Vendedores',MB_ICONWARNING or MB_OK);
    Exit;
  end;

  fn_RegistrodeCambio(CUsuario.Usuario,'Consulta','Ventas del '+

  FormatDateTime('YYYY-MM-DD',jvdtmpckrDesde.Date)+' al '+
  FormatDateTime('YYYY-MM-DD',jvdtmpckrHasta.Date)+ ' Vendedor: '+
  cbbVendedor.Text);

//  lblTipoN.Caption:=cbbTipoNCF.Text;

  with spVentasporVendedor do begin
    Close;
    Parameters.ParamByName('@CodVendedor').Value:=var_Vendedor;
    Parameters.ParamByName('@Fecha_Desde').Value:=FormatDateTime('YYYY-MM-DD',jvdtmpckrDesde.Date);
    Parameters.ParamByName('@Fecha_Hasta').Value:=FormatDateTime('YYYY-MM-DD',jvdtmpckrHasta.Date);
    Open;
    lblTotal.Caption:= Format('%m',[spVentasporVendedor.FieldByName('Total').AsFloat]);
    lblVendedor.Caption:=cbbVendedor.Text;
  end;
end;

end.
