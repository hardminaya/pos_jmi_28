unit UntInventarioEntradas;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Vcl.Menus, dxSkinsCore, dxSkinsDefaultPainters,
  cxControls, cxStyles, dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, Data.DB, cxDBData, cxMemo, cxCheckBox, Data.Win.ADODB,
  cxClasses, Datasnap.DBClient, Vcl.StdCtrls, cxGridLevel,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGridCustomView,
  cxGrid, cxButtons, JvDataSource, Datasnap.Provider, dxSkinBlack, dxSkinBlue,
  dxSkinBlueprint, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide,
  dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinFoggy,
  dxSkinGlassOceans, dxSkinHighContrast, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSevenClassic, dxSkinSharp, dxSkinSharpPlus, dxSkinSilver,
  dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008, dxSkinTheAsphaltWorld,
  dxSkinValentine, dxSkinVS2010, dxSkinWhiteprint, dxSkinXmas2008Blue;

type
  TfrmInventarioEntrada = class(TForm)
    dtstprvdrInventarioEntrada: TDataSetProvider;
    dsInventarioEntrada: TJvDataSource;
    btnSalir: TcxButton;
    lbl1: TLabel;
    cxgrd1: TcxGrid;
    cxgrdbtblvwGrid1DBTableView1: TcxGridDBTableView;
    GridGrid1DBTableView1fkEntrada: TcxGridDBColumn;
    GridGrid1DBTableView1Registro: TcxGridDBColumn;
    GridGrid1DBTableView1ArticuloCodigo: TcxGridDBColumn;
    GridGrid1DBTableView1ArticuloDescripcion: TcxGridDBColumn;
    GridGrid1DBTableView1ArticuloCantidad: TcxGridDBColumn;
    GridGrid1DBTableView1Codigo_Almacen: TcxGridDBColumn;
    GridGrid1DBTableView1ManejaSeriales: TcxGridDBColumn;
    GridGrid1DBTableView1DocumentoSAP: TcxGridDBColumn;
    GridGrid1DBTableView1Procesado: TcxGridDBColumn;
    cxgrdlvlGrid1Level1: TcxGridLevel;
    cbbRegistros: TComboBox;
    dsetInventarioEntrada: TClientDataSet;
    cxstylrpstry1: TcxStyleRepository;
    cxstyl1: TcxStyle;
    qryInventarioEntrada: TADOQuery;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure cbbRegistrosChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnSalirClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmInventarioEntrada: TfrmInventarioEntrada;

implementation

uses
  UntData;

{$R *.dfm}

procedure TfrmInventarioEntrada.btnSalirClick(Sender: TObject);
begin
  Self.Close;
end;

procedure TfrmInventarioEntrada.cbbRegistrosChange(Sender: TObject);
begin
  qryInventarioEntrada.SQL.Text:='SELECT TOP('+cbbRegistros.Text+') * FROM vw_InventarioEntrada ORDER BY Registro DESC';
  //ShowMessage(qryInventarioEntrada.SQL.Text);
  dsetInventarioEntrada.Close;
  dsetInventarioEntrada.Open;
end;

procedure TfrmInventarioEntrada.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action:=caFree;
  frmInventarioEntrada:=nil;
end;

procedure TfrmInventarioEntrada.FormShow(Sender: TObject);
begin
  dsetInventarioEntrada.Open;
end;

end.
