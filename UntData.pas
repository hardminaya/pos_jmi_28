unit UntData;

interface

uses
  System.SysUtils, System.Classes, Data.DB, Data.Win.ADODB, Datasnap.Provider,
  Datasnap.DBClient, dxLayoutLookAndFeels,Windows, ppBands, ppCache, ppClass,
  ppDesignLayer, ppParameter, ppEndUsr, ppDB, ppDBPipe, ppComm, ppRelatv,
  ppProd, ppReport, Vcl.Forms, Math, ppPrnabl, ppCtrls, ppVar,System.IniFiles,JvCipher,IdCoder,
  custom_driver, ado_driver, ppStrtch, ppMemo, ppModule, raCodMod, JvDataSource,
  ppRichTx,WebPosData;

type TValoresINI = record
  Key : String;
  ArchivoINI : String;
  DBClaveIsEnCrypted : Boolean;
  DBUsuario : String;
  DBClave : String;
  DBServer : String;
  ImprimirTicket : Boolean;
  TicketAuto : Boolean;
  PapelTamano : Double;
  RegistroTamano : Double;
  UDLPos : string;
  UDLSeguridad : string;
  UDLComun : string;
  Usuario : string;
  Contrasena : string;
  ImprimirWebPos : Boolean;
  RutaArchivoWebPos : string;
  IDEstacionWebPos : Integer;
  RutaOrdenPedido : string;
  ClaveOrdenPedido : string;
  ComprobantesTipo : string;
  PermiteVentaArticuloSinInventario : Boolean;

  //IDEstacionWebPos : string;
end;

type TSucursal = record
  Representante : string;
  ManejaComprobantes : Boolean;
  PrefijoTarjetaDescuento : String;
end;

type TCaja = record
  Nombre : String;
  Responsable : String;
  MontoApertura : Double;
  MontoCierre : Double;
  ImprimeCopia : Integer;
  SeleccionaSeriales : Boolean;
  EmiteCoprobantes : Boolean;
end;

type TCliente = record
  Codigo : String;
  Nombre : String;
  TipoCliente: String;
  Direccion : String;
  RNC : String;
  TarjetaID : String;
  Descuento : Integer;
  TarjetaExp : TDate;
  Generico : string;
end;

type TFactura = record
  Numero : Integer;
  SubTotal : Double;
  Descuentos : Double;
  Impuestos : Double;
  Total : Double;
  Pago : Double;
  TipoNCF : Integer;
  Acepta_Pago_TarjetaCredito : string;
end;

type TVariables = record
  ArticuloaCriteriodeBusqueda : String;
  ArticuloCantidadEncontrada : Integer;
  SucursalID : Integer;
  SucursalNombre : String;
  Impuestos : Integer;
  Cajero : String;
  VendedorID: String;
  VendedorNombre : String;
  Privilegio : String;
  PrivilegioElevado : Boolean;
  PrivilegioElevadoUsuario : String;
  DirectoryWorkspace : string;
  TarjetaDescuentoID : String;
  SQLTransactionLevel : Integer;
  LargoTicket : Double;
  NombreTicketReporte : string;
  ImprimirWebPos : Boolean;
  RutaArchivoWebPos : string;
  IDEstacionWebPos : Integer;
  ImprimirTicket : Boolean;
end;

type TFacturaD = record
  Codigo : string;
  Descripcion : String;
  Precio : Double;
  Costo : Double;
  PrecioDesc : Double;
  Cantidad : Integer;
  Monto : Double;
  PorCientoDescuento: Double;
  TotalDescuento : Double;
  PorCientoImpuestos: Double;
  PrecioImpuestos : Double;
  TotalImpuesto : Double;
  Importe : Double;
  CodigoAlmacen: String;
  PlanID : Integer;
  ManejaSeriales : String;
  InventarioArt : string;
  ArticuloItbis : string;
end;

type TNotaCreditoC = record

end;

type TNotaCreditoD = record
  CabeceraId : Integer;
  No_Linea : Integer;
  Articulo_Id : Integer;
  Articulo_Descripcion : string;
  Articulo_Cantidad: integer;
  Artiulo_Importe: Double;
  Almacen_Id : string;
  Sucursal_Id: string;
  Estatus : string;
end;

type TArticulo = record
  CodigoArt : String;
  DescripcionArt : String;
  Costo : Double;
  Precio : Double;
  Monto : Double;
  EnMano : Integer;
  Cantidad : Integer;
  ManejaSeriales : String;
  InventarioArt : string;
  Factura: Integer;
  Serial: String;
  Acepta_Pago_TarjetaCredito : string;
  Itbis: string;
end;

Type TPago = record
  Efectivo : Double;
  TarjetaCredito : Double;
  TarjetaDebito: Double;
  Cheque : Double;
  NotaCreditoNumero : Double;
  NotaCreditoMonto : Currency;
  Total : Double;
  Devuelta : Double;
  Pendiente : Double;
end;

type TUsuario = record
  UsuarioID : Integer;
  Nombre : String;
  Usuario : string;
  Clave : string;
  ID_Almacen   : String;
  Almacen_Descripcion : String;
end;

type TVendedor = record
  CodVendedor : Integer;
  Nombre : string;
end;
    // Record para almacenar la info de la version del programa
type TVersionInfo = record
    Major:   string;
    Minor:   string;
    Release: string;
    Build:   string;
end;
// procedimiento para llamar la funcion Generatetxt del Webposinterface
type tGenerateWebPosTXT = function (aFile:tdatafile):boolean  ;

type
  TDatMain = class(TDataModule)
    dxLayoutLookAndFeelList1: TdxLayoutLookAndFeelList;
    dxLayoutCxLookAndFeel1: TdxLayoutCxLookAndFeel;
    dxLayoutWebLookAndFeel1: TdxLayoutWebLookAndFeel;
    dxLayoutOfficeLookAndFeel1: TdxLayoutOfficeLookAndFeel;
    sp_CajaAutorizada: TADOStoredProc;
    ADOQuery1: TADOQuery;
    dsSucursal: TDataSource;
    qrySucursal: TADOQuery;
    sp_Caja: TADOStoredProc;
    qryTicket: TADOQuery;
    dsTicket: TDataSource;
    pdbplnTicket: TppDBPipeline;
    prprtTicket: TppReport;
    qryValidar_Privilegio: TADOQuery;
    intgrfldSucursalpkIdRegistro: TIntegerField;
    wdstrngfldSucursalDescripcion: TWideStringField;
    wdstrngfldSucursalDireccion: TWideStringField;
    wdstrngfldSucursalTelefono: TWideStringField;
    wdstrngfldSucursalFax: TWideStringField;
    wdstrngfldSucursalRepresentante: TWideStringField;
    wdstrngfldSucursalRNC: TWideStringField;
    wdstrngfldSucursalUsuario: TWideStringField;
    dtmfldSucursalFecha: TDateTimeField;
    blnfldSucursalPrincipal: TBooleanField;
    wdstrngfldSucursalTienda_Principal: TWideStringField;
    wdstrngfldSucursalCodigo_Almacen: TWideStringField;
    blnfldSucursalComprobante: TBooleanField;
    spRegistrodeCambios: TADOStoredProc;
    conAppSecurity: TADOConnection;
    pdsgnr1: TppDesigner;
    prmtrlst1: TppParameterList;
    tblReportes: TADOTable;
    dsReportes: TJvDataSource;
    pdbplnReportes: TppDBPipeline;
    qryRpt_Inventario: TADOQuery;
    pdbplnRpt_Inventario: TppDBPipeline;
    dsRpt_Inventario: TDataSource;
    prprtInventario: TppReport;
    prmtrlst2: TppParameterList;
    qryNC: TADOQuery;
    pdbplnNC: TppDBPipeline;
    dsNC: TDataSource;
    intgrfldNCNumeroReq: TIntegerField;
    intgrfldNCNumeroNC: TIntegerField;
    bcdfldNCMonto: TBCDField;
    bcdfldNCBalance: TBCDField;
    intgrfldNCTicket: TIntegerField;
    wdstrngfldNCRazon: TWideStringField;
    prprtReport: TppReport;
    prmtrlst4: TppParameterList;
    spValidarUsuario: TADOStoredProc;
    conComun: TADOConnection;
    qryPOS: TADOQuery;
    qryCOMUN: TADOQuery;
    wdstrngfldNCArticuloID: TWideStringField;
    intgrfldNCTicketUtilizado: TIntegerField;
    wdstrngfldNCDescripcionArt: TWideStringField;
    ptlbnd2: TppTitleBand;
    plbl1: TppLabel;
    plbl11: TppLabel;
    pln3: TppLine;
    phdrbnd2: TppHeaderBand;
    plbl12: TppLabel;
    pdbtxtCAJA1: TppDBText;
    plbl16: TppLabel;
    pdtlbnd2: TppDetailBand;
    plbl17: TppLabel;
    pdbtxtEFECTIVO2: TppDBText;
    plbl41: TppLabel;
    pdbtxtAPERTURA1: TppDBText;
    plbl42: TppLabel;
    pdbtxtTARJETA2: TppDBText;
    plbl43: TppLabel;
    pdbtxtNCREDITO2: TppDBText;
    plbl44: TppLabel;
    pdbtxtTCAJA1: TppDBText;
    plbl45: TppLabel;
    pdbtxtCHEQUE2: TppDBText;
    plbl46: TppLabel;
    pln5: TppLine;
    prchtxt3: TppRichText;
    prchtxt4: TppRichText;
    pftrbnd2: TppFooterBand;
    psystmvrbl4: TppSystemVariable;
    psystmvrbl5: TppSystemVariable;
    pdsgnlyrs2: TppDesignLayers;
    pdsgnlyr2: TppDesignLayer;
    phdrbnd3: TppHeaderBand;
    plblTitulo1: TppLabel;
    plbl4: TppLabel;
    pdtlbnd3: TppDetailBand;
    psystmvrbl2: TppSystemVariable;
    plbl5: TppLabel;
    pftrbnd3: TppFooterBand;
    pdsgnlyrs3: TppDesignLayers;
    pdsgnlyr3: TppDesignLayer;
    plblCaja: TppLabel;
    sp1: TADOStoredProc;
    spCajaEstado: TADOStoredProc;
    qryWork1: TADOQuery;
    wdstrngfld1: TWideStringField;
    pdsgnlyrs1: TppDesignLayers;
    pdsgnlyr1: TppDesignLayer;
    phdrbnd1: TppHeaderBand;
    pdtlbnd1: TppDetailBand;
    pftrbnd1: TppFooterBand;
    dsAlmacenes: TDataSource;
    qryAlmacenes: TADOQuery;
    qryNCFTipos: TADOQuery;
    qryVendedores: TADOQuery;
    QryClientes: TADOTable;
    con1: TADOConnection;
    pfldTicketppField51: TppField;
    procedure con1BeforeConnect(Sender: TObject);
    procedure conAppSecurityBeforeConnect(Sender: TObject);
    procedure conComunBeforeConnect(Sender: TObject);
    procedure LeerArchivoINI();
    procedure LeerParametros();
    procedure DataModuleCreate(Sender: TObject);
    procedure prprtReportBeforePrint(Sender: TObject);
    procedure prprtTicketBeforePrint(Sender: TObject);

  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  DatMain: TDatMain;
  cArticulo : TArticulo;
  cVariables : TVariables;
  cCliente : TCliente;
  cFactura: TFactura;
  cFacturaD: TFacturaD;
  cPago : TPago;
  cCaja : TCaja;
  cSucursal : TSucursal;
  cUsuario : TUsuario;
  cVendedor : TVendedor;
  cValoresIni : TValoresINI;
  cIniFile : TIniFile;
  VersionInfo:TVersionInfo;

  //  g_Impuestos : Integer;
  function ComputerName():String;
  function fn_CajaAutorizada(param_caja:string) : Integer;
  function fn_CajaEstado(param_caja:String) : Integer;
  function fn_ValidarPrivilegio(param_Usuario,param_Clave,param_Privilegio:string) : Integer;
  function fn_RegistrodeCambio(param_usuario,Param_Modulo,Param_Accion:string) : string;
  function fn_FacturasPendientes() : Integer;
  function fn_encripta(const Key: String; const Value: String): String;
  function fn_desencripta(const Key: String; const Value: String): String;
  function GetFileVersion(exeName : string): string;
  function AddLeadingZeroes(const aNumber, Length : integer) : string;
  function fnSeleccionaNCF_NC() : String;
      procedure EscribirOrdenPedido(Nombre : string);

implementation

{%CLASSGROUP 'System.Classes.TPersistent'}

{$R *.dfm}

function fn_Encripta(const Key: String; const Value: String): String;
 var
  VC: TJvVigenereCipher;
begin
 VC:=TJvVigenereCipher.Create(nil);
 Result:=VC.EncodeString(Key, Value);
 VC.Free;
end;

function fn_Desencripta(const Key: String; const Value: String): String;
 var
  VC: TJvVigenereCipher;
begin
 VC:=TJvVigenereCipher.Create(nil);
 Result:=VC.DecodeString(Key,Value);
 VC.Free;
end;

function ComputerName():String;
var
  ComputerName: Array [0 .. 256] of char;
  Size: DWORD;
begin
  Size := 256;
  GetComputerName(ComputerName, Size);
  Result := ComputerName;
end;

procedure TDatMain.con1BeforeConnect(Sender: TObject);
var
  var_stringConexionPOS : string;
begin
  var_stringConexionPOS:=StringReplace(cValoresIni.UDLPos,'[usuario]',cValoresIni.DBUsuario,[rfReplaceAll,rfIgnoreCase]);
  var_stringConexionPOS:=StringReplace(var_stringConexionPOS,'[password]',cValoresIni.DBClave,[rfReplaceAll,rfIgnoreCase]);
  con1.ConnectionString:=var_stringConexionPOS;
end;

procedure TDatMain.conAppSecurityBeforeConnect(Sender: TObject);
var
  var_stringConexionPOS : string;
begin

  var_stringConexionPOS:=StringReplace(cValoresIni.UDLSeguridad,'[usuario]',cValoresIni.DBUsuario,[rfReplaceAll,rfIgnoreCase]);
  var_stringConexionPOS:=StringReplace(var_stringConexionPOS,'[password]',cValoresIni.DBClave,[rfReplaceAll,rfIgnoreCase]);
  conAppSecurity.ConnectionString:=var_stringConexionPOS;
 end;

procedure TDatMain.conComunBeforeConnect(Sender: TObject);
var
  var_stringConexionPOS : string;
begin
  var_stringConexionPOS:=StringReplace(cValoresIni.UDLComun,'[usuario]',cValoresIni.DBUsuario,[rfReplaceAll,rfIgnoreCase]);
  var_stringConexionPOS:=StringReplace(var_stringConexionPOS,'[password]',cValoresIni.DBClave,[rfReplaceAll,rfIgnoreCase]);
  conComun.ConnectionString:=var_stringConexionPOS;
end;

procedure TDatMain.DataModuleCreate(Sender: TObject);
begin
  //PauseSplash;
end;

procedure TDatMain.LeerArchivoINI;
begin
  cValoresIni.Key:='-khHr9fuG7LQlVjas2Cxud2O-Glejx1iaV2-LSvz4UQsNVTLQS6Aw52NUm3U0uAA';

  with cValoresIni do
  begin
    ArchivoINI:=ChangeFileExt('pos.exe', '.ini');

    cIniFile:=TIniFile.Create(cVariables.DirectoryWorkspace+cValoresIni.ArchivoINI);

    Usuario:=cIniFile.ReadString('Programa','Usuario','');
    Contrasena:=cIniFile.ReadString('Programa','Contrasena','');

    DBUsuario:=cIniFile.ReadString('Base de Datos','Usuario','Usuario');
    DBClave:=cIniFile.ReadString('Base de Datos','Clave','Clave');
    DBServer:=cIniFile.ReadString('Base de Datos','Servidor','Servidor');
    DBClaveIsEnCrypted:=cIniFile.ReadBool('Base de Datos','ClaveIsEnCrypted',FALSE);

    Imprimirticket:=cIniFile.ReadBool('Impresion','ImprimirTicket',FALSE);
    TicketAuto:=cIniFile.ReadBool('Impresion','TicketAuto',FALSE);
    PapelTamano:=cIniFile.ReadFloat('Impresion','PapelTamano',4.000);
    RegistroTamano:=cIniFile.ReadFloat('Impresion','RegistroTamano',0.5000);
    RutaOrdenPedido:=cIniFile.ReadString('Impresion','RutaOrdenPedido','');
    ClaveOrdenPedido:=cIniFile.ReadString('Impresion','ClaveOrdenPedido','0000');
    ComprobantesTipo:=cIniFile.ReadString('Impresion','ComprobantesTipo','0');
    PermiteVentaArticuloSinInventario:=cIniFile.ReadBool('Impresion','PermiteVentaArticuloSinInventario',FALSE);

    UDLPos:=cIniFile.ReadString('Base de Datos','UDLPos','pos.udl');
    UDLSeguridad:=cIniFile.ReadString('Base de Datos','UDLSeguridad','seguridad.udl');
    UDLComun:=cIniFile.ReadString('Base de Datos','UdlComun','Comun.udl');

    ImprimirWebPos:=cIniFile.ReadBool('WebPos','ImprimirWebPos',FALSE);
    RutaArchivoWebPos:=cIniFile.ReadString('WebPos','RutaArchivoWebPos','C:\VESSLABG\RELEASE\POSJMI\2.8\Release\');
    IDEstacionWebPos:=cIniFile.ReadInteger('WebPos','IDEstacionWebPos',-99);

    cVariables.ImprimirWebPos:=ImprimirWebPos;
    cVariables.RutaArchivoWebPos:=RutaArchivoWebPos;
    cVariables.IDEstacionWebPos:=IDEstacionWebPos;
    cVariables.ImprimirTicket:=ImprimirTicket;

    if DBClaveisEnCrypted=FALSE then
    begin
      cIniFile.WriteString('Base de Datos','Clave',fn_encripta(cValoresIni.Key,DBClave));
      cIniFile.WriteBool('Base de Datos','ClaveIsEncrypted',TRUE);
    end
    else
    begin
      cValoresIni.DBClave:=fn_desencripta(cValoresIni.Key,cValoresIni.DBClave);
    end;
  end;
end;

function fn_CajaAutorizada(param_caja:string) : Integer;
begin
  with datmain.sp_CajaAutorizada do
  begin
    Parameters.ParamByName('@NombrePC').Value:=param_caja;
    ExecProc;
    Result:=Parameters.ParamByName('@Return_Value').Value;
  end
end;

function fn_CajaEstado(param_caja:string) : Integer;
begin
  with DatMain.spCajaEstado do
  begin
    Close;
    Parameters.ParamByName('@NombrePC').Value:=param_caja;
    ExecProc;
    Result:=Parameters.ParamByName('@RETURN_VALUE').Value;
//    Result:=FieldByName('@RETURN_VALUE').AsInteger;
    // 1= Caja Abierta para Ventas
    // 2= Caja Abierta otro d�a No Ventas
    // 3= Caja Cerrada para Ventas (HOY)
    // 0= No hay Registro de Caja
  end
end;

function fn_ValidarPrivilegio(param_Usuario,param_Clave,param_Privilegio : string) : Integer;
var
  var_SQLText : String;
begin
  with datmain.qryValidar_Privilegio do
  begin
    Close;
    var_SQLText:='Select * from vw_Usuarios_Privilegios where usuario='+
    QuotedStr(param_Usuario)+' and Privilegio='+
    QuotedStr(param_Privilegio);
    SQL.Text:=var_SQLText;
    Open;
    Result:=RecordCount;
  end;
end;

function fn_RegistrodeCambio(param_usuario,Param_Modulo,Param_Accion:string) : string;
begin
  with datmain.spRegistrodeCambios do
  begin
    Parameters.ParamByName('@Usuario').Value:=param_usuario;
    Parameters.ParamByName('@Modulo').Value:=Param_Modulo;
    Parameters.ParamByName('@Accion').Value:=Param_Accion;
    ExecProc;
  end
end;

function fn_FacturasPendientes() : Integer;
begin
  with datmain.qryWork1 do
  begin
    Close;
    SQL.Text:='select count(*) as Cantidad from [facturapos.cabecera] where documentoservidor<>' + #39+'Y'+#39;
    Open;
    Result:=FieldByName('Cantidad').AsInteger;
  end;
end;

function GetFileVersion(exeName : string): string;
const
  c_StringInfo = 'StringFileInfo\040904E4\FileVersion';
var
  n, Len : cardinal;
  Buf, Value : PChar;
begin
  Result := '';
  n := GetFileVersionInfoSize(PChar(exeName),n);
  if n > 0 then begin
    Buf := AllocMem(n);
    try
      GetFileVersionInfo(PChar(exeName),0,n,Buf);
      if VerQueryValue(Buf,PChar(c_StringInfo),Pointer(Value),Len) then begin
        Result := Trim(Value);
      end;
    finally
      FreeMem(Buf,n);
    end;
  end;
end;

procedure TDatMain.LeerParametros;
var
  var_SQLText : string;
begin
  with datmain do
  begin
    var_SQLText:='Select * from caja where nombrepc='+#39+ComputerName+#39;
    qryWork1.SQL.Text:=var_SQLText;
    qryWork1.Open;
    cCaja.ImprimeCopia:=qryWork1.FieldByName('ImprimieCopia').AsInteger;
    cCaja.EmiteCoprobantes:=qryWork1.FieldByName('EmiteComprobantes').AsBoolean;
    cCaja.SeleccionaSeriales:=qryWork1.FieldByName('SeleccionaSeriales').AsBoolean;
    qryWork1.Close;
  end;

  with DatMain.QryPOS do
  begin
    var_SQLText:='Select * from sucursal';
    SQL.Text:=var_SQLText;
    Open;
    cSucursal.Representante:=qryPOS.FieldByName('Representante').AsString;
    cSucursal.PrefijoTarjetaDescuento:=qryPOS.FieldByName('PrefijoTarjetaDescuento').AsString;
    Close;
  end;
end;

procedure TDatMain.prprtReportBeforePrint(Sender: TObject);
begin
//  prprtReport.PrinterDevice.PageRequest.PageSetting:=psPageList;
//  prprtReport.PrinterDevice.PageRequest.PageList.Add('2');
end;

procedure TDatMain.prprtTicketBeforePrint(Sender: TObject);
begin
//  prprtTicket.PrinterDevice.PageRequest.PageSetting:=psPageList;
//  prprtTicket.PrinterDevice.PageRequest.PageList.Add('2');
end;

function AddLeadingZeroes(const aNumber, Length : integer) : string;
begin
   result := Format('%.*d', [Length, aNumber]) ;
end;

function fnSeleccionaNCF_NC():String;
{ -------------------------------------------------------------------------
 Selecciona NCF tipo NC para asignarlo a las NC de Factuas Creditos Fiscal
 -------------------------------------------------------------------------}
var
  varquery : TADOQuery;
begin
  varquery:= TADOQuery.Create(nil);
  varquery.Connection:=DatMain.con1;
  varquery.SQL.Text:='Exec [sp_SeleccionaNCF_NC]';
  varquery.Open;
  Result:=varquery.FieldByName('Secuencia').AsString;
end;

procedure EscribirOrdenPedido(Nombre : String);
var
  archivo : TFileStream;
begin
  archivo:=TFileStream.Create(cValoresIni.RutaOrdenPedido+Nombre+'.txt',fmCreate);
  archivo.Free;
  //AssignFile(Nombre,cValoresIni.RutaOrdenPedido+'.txt');
  //Rewrite(Nombre);
end;

end.

