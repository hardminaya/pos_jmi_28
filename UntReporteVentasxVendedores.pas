unit UntReporteVentasxVendedores;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, dxSkinsCore, dxSkinsDefaultPainters,
  dxSkinscxPCPainter, cxPCdxBarPopupMenu, Vcl.Menus, cxContainer, cxEdit,
  Data.DB, Data.Win.ADODB, JvComponentBase, JvDBGridExport, cxTextEdit,
  cxMaskEdit, cxDropDownEdit, cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox,
  cxNavigator, cxDBNavigator, Vcl.ComCtrls, JvExComCtrls, JvDateTimePicker,
  Vcl.StdCtrls, cxButtons, JvExControls, JvXPCore, JvXPButtons, Vcl.Grids,
  Vcl.DBGrids, JvExDBGrids, JvDBGrid, cxPC;

type
  TfrmReporteVentasXVendedores = class(TForm)
    lbl2: TLabel;
    lbl1: TLabel;
    lbl3: TLabel;
    lblTipoNCF: TLabel;
    cxpgcntrl1: TcxPageControl;
    cxtbsht1: TcxTabSheet;
    jvdbgrd1: TJvDBGrid;
    btn1: TJvXPToolButton;
    btnExportarExcel: TcxButton;
    jvdtmpckrHasta: TJvDateTimePicker;
    jvdtmpckrDesde: TJvDateTimePicker;
    btnSalir: TcxButton;
    btnRefrescar: TcxButton;
    cxdbnvgtr1: TcxDBNavigator;
    cbbTipoNCF: TcxLookupComboBox;
    jvdbgrdxclxprt1: TJvDBGridExcelExport;
    sp1: TADOStoredProc;
    dtmfldsp1DocumentoFecha: TDateTimeField;
    wdstrngfldsp1NumeroNCF: TWideStringField;
    wdstrngfldsp1ClienteNombre: TWideStringField;
    wdstrngfldsp1ClienteRNC: TWideStringField;
    QryArticulosp1Monto: TFMTBCDField;
    QryArticulosp1ITBIS: TFMTBCDField;
    QryArticulosp1MontoGeneral: TFMTBCDField;
    ds1: TDataSource;
    flsvdlg1: TFileSaveDialog;
    dsNCFTipos: TDataSource;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmReporteVentasXVendedores: TfrmReporteVentasXVendedores;

implementation

{$R *.dfm}

end.
