unit UntStartUPLogin;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Vcl.Menus, dxSkinsCore, dxSkinsDefaultPainters,
  Vcl.StdCtrls, cxButtons, dxGDIPlusClasses, Vcl.ExtCtrls, JvMenus,
  cxControls, cxContainer, cxEdit, cxTextEdit,Winapi.ShellAPI, dxSkinBlack,
  dxSkinBlue, dxSkinBlueprint, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom,
  dxSkinDarkSide, dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinFoggy,
  dxSkinGlassOceans, dxSkinHighContrast, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSevenClassic, dxSkinSharp, dxSkinSharpPlus, dxSkinSilver,
  dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008, dxSkinTheAsphaltWorld,
  dxSkinValentine, dxSkinVS2010, dxSkinWhiteprint, dxSkinXmas2008Blue;

type
  TfrmStartUPLogin = class(TForm)
    pnl1: TPanel;
    img1: TImage;
    btnEntrar: TcxButton;
    btnSalir: TcxButton;
    lbl5: TLabel;
    lbl1: TLabel;
    edtUsuario: TcxTextEdit;
    edtPassword: TcxTextEdit;
    procedure btnEntrarClick(Sender: TObject);
    procedure btnSalirClick(Sender: TObject);
    procedure edt1KeyPress(Sender: TObject; var Key: Char);
    procedure FormShow(Sender: TObject);

  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmStartUPLogin: TfrmStartUPLogin;
  var_version: string;
  var_mensaje_caption: string;

implementation

{$R *.dfm}

uses UntData, UntMain;

procedure TfrmStartUPLogin.btnEntrarClick(Sender: TObject);
begin
  with DatMain do
  begin
    LeerArchivoINI;
    frmMain.usrs1.StoreDir:=cVariables.DirectoryWorkspace+'Users';
    con1.Close;
    conAppSecurity.Close;
    conComun.Close;
    Try
      con1.Connected:=True;
      conAppSecurity.Connected:=True;
      LeerParametros;
    except
      on E:Exception do
      begin
       MessageDlg('Error Base de Datos NO Disponible '+ e.Message,mtError,[mbOK],0);
       Exit;
      end;
    End;
    ModalResult:=mrOk;
  end;
end;

procedure TfrmStartUPLogin.btnSalirClick(Sender: TObject);
begin
  Application.Terminate;
end;

procedure TfrmStartUPLogin.edt1KeyPress(Sender: TObject; var Key: Char);
begin
  if Key=#13 then btnEntrarClick(Sender);
end;

procedure TfrmStartUPLogin.FormShow(Sender: TObject);
begin
  var_version:= 'Version: '+ GetFileVersion(ParamStr(0));
  var_mensaje_caption:='Conectado a ';
  Caption:=var_version;
  edtUsuario.Text:=cValoresIni.Usuario;
  edtPassword.Text:=cValoresIni.Contrasena;
end;


end.
