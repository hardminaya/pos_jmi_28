object frmArticulos: TfrmArticulos
  Left = 0
  Top = 0
  Width = 1092
  Height = 671
  AutoScroll = True
  BorderIcons = [biSystemMenu]
  Caption = 'Maestro de Articulos'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Visible = True
  WindowState = wsMaximized
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object cxpgcntrl1: TcxPageControl
    Left = 0
    Top = 65
    Width = 1074
    Height = 559
    Align = alClient
    TabOrder = 1
    Properties.ActivePage = cxtbsht1
    ClientRectBottom = 553
    ClientRectLeft = 3
    ClientRectRight = 1068
    ClientRectTop = 26
    object cxtbsht1: TcxTabSheet
      Caption = 'Listado'
      ImageIndex = 0
      object cxgrd1: TcxGrid
        Left = 0
        Top = 0
        Width = 1065
        Height = 527
        Align = alClient
        TabOrder = 0
        object cxgrdbtblvwGrid1DBTableView1: TcxGridDBTableView
          OnCellDblClick = cxgrdbtblvwGrid1DBTableView1CellDblClick
          DataController.DataSource = dsArticulos
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          object GridGrid1DBTableView1CodigoArt: TcxGridDBColumn
            Caption = 'C'#243'digo'
            DataBinding.FieldName = 'CodigoArt'
            HeaderAlignmentHorz = taCenter
            Options.Editing = False
            Styles.Header = cxstyl1
            Width = 106
          end
          object GridGrid1DBTableView1DescripcionArt: TcxGridDBColumn
            Caption = 'Descripcion'
            DataBinding.FieldName = 'DescripcionArt'
            HeaderAlignmentHorz = taCenter
            Options.Editing = False
            Styles.Header = cxstyl1
            Width = 448
          end
          object GridGrid1DBTableView1GrupoArt: TcxGridDBColumn
            Caption = 'Grupo'
            DataBinding.FieldName = 'GrupoArt'
            HeaderAlignmentHorz = taCenter
            Options.Editing = False
            Styles.Header = cxstyl1
            Width = 47
          end
          object GridGrid1DBTableView1CodigoBarras: TcxGridDBColumn
            Caption = 'BarCode'
            DataBinding.FieldName = 'CodigoBarras'
            HeaderAlignmentHorz = taCenter
            Options.Editing = False
            Styles.Header = cxstyl1
            Width = 156
          end
          object ColAlmacen: TcxGridDBColumn
            Caption = 'Almac'#233'n'
            DataBinding.FieldName = 'Codigo_Almacen'
            HeaderAlignmentHorz = taCenter
            Styles.Header = cxstyl1
            Width = 78
          end
          object GridGrid1DBTableView1EnMano: TcxGridDBColumn
            Caption = 'En Mano'
            DataBinding.FieldName = 'EnMano'
            HeaderAlignmentHorz = taCenter
            Options.Editing = False
            Styles.Header = cxstyl1
          end
        end
        object cxgrdlvlGrid1Level1: TcxGridLevel
          GridView = cxgrdbtblvwGrid1DBTableView1
        end
      end
    end
    object cxtbsht2: TcxTabSheet
      Caption = 'Registro'
      ImageIndex = 1
      object cxdbtxtdt1: TcxDBTextEdit
        Left = 16
        Top = 32
        DataBinding.DataField = 'CodigoArt'
        DataBinding.DataSource = dsArticulos
        StyleDisabled.BorderColor = clBtnShadow
        StyleDisabled.BorderStyle = ebsOffice11
        StyleDisabled.Color = clBtnFace
        StyleFocused.BorderColor = clRed
        StyleFocused.BorderStyle = ebsThick
        StyleFocused.Color = clGradientActiveCaption
        StyleHot.BorderColor = clRed
        TabOrder = 0
        Width = 121
      end
      object cxdbtxtdt2: TcxDBTextEdit
        Left = 143
        Top = 32
        DataBinding.DataField = 'DescripcionArt'
        DataBinding.DataSource = dsArticulos
        StyleFocused.BorderColor = clRed
        StyleFocused.BorderStyle = ebsThick
        StyleFocused.Color = clGradientActiveCaption
        StyleHot.BorderColor = clRed
        TabOrder = 1
        Width = 465
      end
      object cxdbtxtdt3: TcxDBTextEdit
        Left = 614
        Top = 32
        DataBinding.DataField = 'GrupoArt'
        DataBinding.DataSource = dsArticulos
        StyleFocused.BorderColor = clRed
        StyleFocused.BorderStyle = ebsThick
        StyleFocused.Color = clGradientActiveCaption
        StyleHot.BorderColor = clRed
        TabOrder = 2
        Width = 121
      end
      object cxdbtxtdt4: TcxDBTextEdit
        Left = 16
        Top = 82
        DataBinding.DataField = 'CodigoBarras'
        DataBinding.DataSource = dsArticulos
        StyleFocused.BorderColor = clRed
        StyleFocused.BorderStyle = ebsThick
        StyleFocused.Color = clGradientActiveCaption
        StyleHot.BorderColor = clRed
        TabOrder = 7
        Width = 121
      end
      object cxdbmskdt1: TcxDBMaskEdit
        Left = 16
        Top = 132
        DataBinding.DataField = 'Precio'
        DataBinding.DataSource = dsArticulos
        StyleFocused.BorderColor = clRed
        StyleFocused.BorderStyle = ebsThick
        StyleFocused.Color = clGradientActiveCaption
        StyleHot.BorderColor = clRed
        TabOrder = 11
        Width = 121
      end
      object cxlbl1: TcxLabel
        Left = 16
        Top = 16
        Caption = 'C'#243'digo'
      end
      object cxlbl2: TcxLabel
        Left = 143
        Top = 16
        Caption = 'Descripci'#243'n'
      end
      object cxlbl3: TcxLabel
        Left = 614
        Top = 16
        Caption = 'Grupo'
      end
      object cxlbl4: TcxLabel
        Left = 16
        Top = 67
        Caption = 'C'#243'digo de Barras'
      end
      object cxlbl5: TcxLabel
        Left = 143
        Top = 113
        Caption = 'En Mano'
      end
      object cxdbtxtdt5: TcxDBTextEdit
        Left = 143
        Top = 82
        DataBinding.DataField = 'DescripcionEtiqueta'
        DataBinding.DataSource = dsArticulos
        StyleFocused.BorderColor = clRed
        StyleFocused.BorderStyle = ebsThick
        StyleFocused.Color = clGradientActiveCaption
        StyleHot.BorderColor = clRed
        TabOrder = 8
        Width = 121
      end
      object cxdbmskdt2: TcxDBMaskEdit
        Left = 143
        Top = 132
        DataBinding.DataField = 'EnMano'
        DataBinding.DataSource = dsArticulos
        Enabled = False
        StyleFocused.BorderColor = clRed
        StyleFocused.BorderStyle = ebsThick
        StyleFocused.Color = clGradientActiveCaption
        StyleHot.BorderColor = clRed
        TabOrder = 14
        Width = 121
      end
      object cxlbl6: TcxLabel
        Left = 143
        Top = 67
        Caption = 'Descripci'#243'n de la Etiqueta'
      end
      object cxlbl7: TcxLabel
        Left = 16
        Top = 113
        Caption = 'Precio'
      end
      object grp1: TGroupBox
        Left = 297
        Top = 59
        Width = 438
        Height = 94
        Caption = 'Caracteristicas'
        TabOrder = 3
        object cxdbchckbx6: TcxDBCheckBox
          Left = 16
          Top = 27
          Caption = 'Se Compra'
          DataBinding.DataField = 'CompraArt'
          DataBinding.DataSource = dsArticulos
          Properties.ValueChecked = 'Y'
          Properties.ValueUnchecked = 'N'
          StyleFocused.BorderColor = clRed
          StyleFocused.BorderStyle = ebsThick
          StyleHot.BorderColor = clRed
          TabOrder = 0
          Width = 121
        end
        object cxdbchckbx7: TcxDBCheckBox
          Left = 16
          Top = 52
          Caption = 'Se Vende'
          DataBinding.DataField = 'VentaArt'
          DataBinding.DataSource = dsArticulos
          Properties.ValueChecked = 'Y'
          Properties.ValueUnchecked = 'N'
          StyleFocused.BorderColor = clRed
          StyleFocused.BorderStyle = ebsThick
          StyleHot.BorderColor = clRed
          TabOrder = 3
          Width = 121
        end
        object cxdbchckbx8: TcxDBCheckBox
          Left = 143
          Top = 27
          Caption = 'Afecta Inventario'
          DataBinding.DataField = 'InventarioArt'
          DataBinding.DataSource = dsArticulos
          Properties.ValueChecked = 'Y'
          Properties.ValueUnchecked = 'N'
          StyleFocused.BorderColor = clRed
          StyleFocused.BorderStyle = ebsThick
          StyleHot.BorderColor = clRed
          TabOrder = 1
          Width = 121
        end
        object cxdbchckbx9: TcxDBCheckBox
          Left = 270
          Top = 27
          Caption = 'Aplica Impuestos'
          DataBinding.DataField = 'ITBIS'
          DataBinding.DataSource = dsArticulos
          Properties.ValueChecked = 'Y'
          Properties.ValueUnchecked = 'N'
          StyleFocused.BorderColor = clRed
          StyleFocused.BorderStyle = ebsThick
          StyleHot.BorderColor = clRed
          TabOrder = 2
          Width = 121
        end
        object cxdbchckbx10: TcxDBCheckBox
          Left = 143
          Top = 52
          Caption = 'Maneja Seriales'
          DataBinding.DataField = 'ManejaSeriales'
          DataBinding.DataSource = dsArticulos
          Properties.ValueChecked = 'Y'
          Properties.ValueUnchecked = 'N'
          StyleFocused.BorderColor = clRed
          StyleFocused.BorderStyle = ebsThick
          StyleHot.BorderColor = clRed
          TabOrder = 4
          Width = 121
        end
        object cxdbchckbxAceptaPagoTarjetaCredito: TcxDBCheckBox
          Left = 270
          Top = 52
          Caption = 'Acepta Pago con TC'
          DataBinding.DataField = 'Acepta_Pago_TarjetaCredito'
          DataBinding.DataSource = dsArticulos
          Properties.ValueChecked = 'S'
          Properties.ValueUnchecked = 'N'
          StyleFocused.BorderColor = clRed
          StyleFocused.BorderStyle = ebsThick
          StyleHot.BorderColor = clRed
          TabOrder = 5
          Width = 121
        end
      end
    end
  end
  object tlb1: TToolBar
    Left = 0
    Top = 0
    Width = 1074
    Height = 65
    ButtonHeight = 60
    ButtonWidth = 57
    Caption = 'tlb1'
    Images = frmMain.ilToolBar32
    ShowCaptions = True
    TabOrder = 0
    object btnRegistro: TToolButton
      Left = 0
      Top = 0
      Caption = 'Registro'
      DropdownMenu = jvpmn1
      ImageIndex = 9
      PopupMenu = jvpmn1
      Style = tbsDropDown
    end
    object btnAnterior: TToolButton
      Left = 76
      Top = 0
      Caption = 'Anterior'
      ImageIndex = 3
      OnClick = btnAnteriorClick
    end
    object btnSiguiente: TToolButton
      Left = 133
      Top = 0
      Caption = 'Siguiente'
      ImageIndex = 0
      OnClick = btnSiguienteClick
    end
    object btnGuardar: TToolButton
      Left = 190
      Top = 0
      Caption = 'Guardar'
      ImageIndex = 2
      Visible = False
      OnClick = btnGuardarClick
    end
    object btnCancelar: TToolButton
      Left = 247
      Top = 0
      Caption = 'Cancelar'
      ImageIndex = 5
      Visible = False
      OnClick = btnCancelarClick
    end
    object btn2: TToolButton
      Left = 304
      Top = 0
      Width = 55
      Caption = 'btn5'
      ImageIndex = 5
      Style = tbsSeparator
    end
    object btnSalir: TToolButton
      Left = 359
      Top = 0
      Caption = 'Salir'
      ImageIndex = 1
      OnClick = btnSalirClick
    end
  end
  object dsetArticulos: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'dtstprvdrArticulos'
    OnPostError = dsetArticulosPostError
    Left = 592
    Top = 8
    object atncfldArticulospIdRegistro: TAutoIncField
      FieldName = 'pIdRegistro'
      ReadOnly = True
    end
    object wdstrngfldArticulosCodigoArt: TWideStringField
      FieldName = 'CodigoArt'
    end
    object wdstrngfldArticulosDescripcionArt: TWideStringField
      FieldName = 'DescripcionArt'
      Size = 100
    end
    object intgrfldArticulosGrupoArt: TIntegerField
      FieldName = 'GrupoArt'
    end
    object wdstrngfldArticulosCodigoBarras: TWideStringField
      FieldName = 'CodigoBarras'
      Size = 50
    end
    object bcdfldArticulosEnMano: TBCDField
      FieldName = 'EnMano'
      Precision = 18
      Size = 2
    end
    object strngfldArticulosCompraArt: TStringField
      FieldName = 'CompraArt'
      FixedChar = True
      Size = 1
    end
    object strngfldArticulosVentaArt: TStringField
      FieldName = 'VentaArt'
      FixedChar = True
      Size = 1
    end
    object strngfldArticulosInventarioArt: TStringField
      FieldName = 'InventarioArt'
      FixedChar = True
      Size = 1
    end
    object strngfldArticulosITBIS: TStringField
      FieldName = 'ITBIS'
      FixedChar = True
      Size = 1
    end
    object wdstrngfldArticulosManejaSeriales: TWideStringField
      FieldName = 'ManejaSeriales'
      FixedChar = True
      Size = 1
    end
    object wdstrngfldArticulosDescripcionEtiqueta: TWideStringField
      FieldName = 'DescripcionEtiqueta'
    end
    object intgrfldArticulosDocumento_Actualizacion: TIntegerField
      FieldName = 'Documento_Actualizacion'
    end
    object QryArticuloArticulosPrecio: TFMTBCDField
      FieldName = 'Precio'
      Precision = 19
      Size = 6
    end
    object intgrfldArticulosDoc_actualizacion: TIntegerField
      FieldName = 'Doc_actualizacion'
    end
    object wdstrngfldArticulosCodigo_Almacen: TWideStringField
      FieldName = 'Codigo_Almacen'
      Size = 50
    end
    object dtmfldArticulosFechaActualizacion: TDateTimeField
      FieldName = 'FechaActualizacion'
    end
    object dtmfldArticulosFechaModificacion: TDateTimeField
      FieldName = 'FechaModificacion'
    end
    object wdstrngfldArticulosUsuario: TWideStringField
      FieldName = 'Usuario'
      Size = 50
    end
    object strngfldArticulosAcepta_Pago_TarjetaCredito: TStringField
      FieldName = 'Acepta_Pago_TarjetaCredito'
      FixedChar = True
      Size = 1
    end
  end
  object dtstprvdrArticulos: TDataSetProvider
    DataSet = tblArticulos
    Options = [poAllowCommandText, poUseQuoteChar]
    UpdateMode = upWhereKeyOnly
    OnUpdateData = dtstprvdrArticulosUpdateData
    OnUpdateError = dtstprvdrArticulosUpdateError
    Left = 504
    Top = 8
  end
  object cxstylrpstry1: TcxStyleRepository
    Left = 760
    Top = 64
    PixelsPerInch = 120
    object cxstyl1: TcxStyle
      AssignedValues = [svFont]
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
    end
    object cxstyl2: TcxStyle
      AssignedValues = [svFont]
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
    end
  end
  object jvpmn1: TJvPopupMenu
    Images = frmMain.ilToolBar32
    ImageMargin.Left = 0
    ImageMargin.Top = 0
    ImageMargin.Right = 0
    ImageMargin.Bottom = 0
    ImageSize.Height = 0
    ImageSize.Width = 0
    Left = 696
    Top = 64
    object mniBuscar1: TMenuItem
      Caption = '&Buscar'
      Enabled = False
      ImageIndex = 11
      OnClick = mniBuscar1Click
    end
    object mniN1: TMenuItem
      Caption = '-'
    end
    object mniAgregar1: TMenuItem
      Caption = '&Agregar'
      ImageIndex = 10
      OnClick = mniAgregar1Click
    end
    object mniN2: TMenuItem
      Caption = '-'
    end
    object mniModificar1: TMenuItem
      Caption = '&Modificar'
      ImageIndex = 9
      OnClick = mniModificar1Click
    end
    object mniN3: TMenuItem
      Caption = '-'
    end
    object mniEliminar1: TMenuItem
      Caption = '&Eliminar'
      ImageIndex = 8
      OnClick = mniEliminar1Click
    end
  end
  object ds1: TJvDataSource
    AutoEdit = False
    DataSet = dsetArticulos
    Left = 624
    Top = 64
  end
  object dsArticulos: TDataSource
    DataSet = dsetArticulos
    Left = 656
    Top = 8
  end
  object tblArticulos: TADOTable
    Connection = DatMain.con1
    CursorType = ctStatic
    TableName = 'Articulos'
    Left = 432
    Top = 8
    object atncfldArticulospIdRegistro1: TAutoIncField
      FieldName = 'pIdRegistro'
      ReadOnly = True
    end
    object wdstrngfldArticulosCodigoArt1: TWideStringField
      FieldName = 'CodigoArt'
    end
    object wdstrngfldArticulosDescripcionArt1: TWideStringField
      FieldName = 'DescripcionArt'
      Size = 100
    end
    object intgrfldArticulosGrupoArt1: TIntegerField
      FieldName = 'GrupoArt'
    end
    object wdstrngfldArticulosCodigoBarras1: TWideStringField
      FieldName = 'CodigoBarras'
      Size = 50
    end
    object bcdfldArticulosEnMano1: TBCDField
      FieldName = 'EnMano'
      Precision = 18
      Size = 2
    end
    object strngfldArticulosCompraArt1: TStringField
      FieldName = 'CompraArt'
      FixedChar = True
      Size = 1
    end
    object strngfldArticulosVentaArt1: TStringField
      FieldName = 'VentaArt'
      FixedChar = True
      Size = 1
    end
    object strngfldArticulosInventarioArt1: TStringField
      FieldName = 'InventarioArt'
      FixedChar = True
      Size = 1
    end
    object strngfldArticulosITBIS1: TStringField
      FieldName = 'ITBIS'
      FixedChar = True
      Size = 1
    end
    object wdstrngfldArticulosManejaSeriales1: TWideStringField
      FieldName = 'ManejaSeriales'
      FixedChar = True
      Size = 1
    end
    object wdstrngfldArticulosDescripcionEtiqueta1: TWideStringField
      FieldName = 'DescripcionEtiqueta'
    end
    object intgrfldArticulosDocumento_Actualizacion1: TIntegerField
      FieldName = 'Documento_Actualizacion'
    end
    object fmtbcdfldArticulosPrecio: TFMTBCDField
      FieldName = 'Precio'
      Precision = 19
      Size = 6
    end
    object intgrfldArticulosDoc_actualizacion1: TIntegerField
      FieldName = 'Doc_actualizacion'
    end
    object wdstrngfldArticulosCodigo_Almacen1: TWideStringField
      FieldName = 'Codigo_Almacen'
      Size = 50
    end
    object dtmfldArticulosFechaActualizacion1: TDateTimeField
      FieldName = 'FechaActualizacion'
    end
    object dtmfldArticulosFechaModificacion1: TDateTimeField
      FieldName = 'FechaModificacion'
    end
    object wdstrngfldArticulosUsuario1: TWideStringField
      FieldName = 'Usuario'
      Size = 50
    end
    object tblArticulosAcepta_Pago_TarjetaCredito: TStringField
      FieldName = 'Acepta_Pago_TarjetaCredito'
      FixedChar = True
      Size = 1
    end
  end
end
