unit UntGeneraNCFaFactura;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Vcl.Menus, dxSkinsCore, dxSkinsDefaultPainters,
  cxControls, cxContainer, cxEdit, cxTextEdit, cxMaskEdit, cxDropDownEdit,
  Vcl.StdCtrls, cxButtons, Vcl.ComCtrls, Vcl.ToolWin, Vcl.Mask, Vcl.ActnList,
  Data.DB, Data.Win.ADODB, dxSkinBlack, dxSkinBlue, dxSkinBlueprint,
  dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide,
  dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinFoggy,
  dxSkinGlassOceans, dxSkinHighContrast, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSevenClassic, dxSkinSharp, dxSkinSharpPlus, dxSkinSilver,
  dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008, dxSkinTheAsphaltWorld,
  dxSkinValentine, dxSkinVS2010, dxSkinWhiteprint, dxSkinXmas2008Blue;

type
  TfrmGeneraNCFaFactura = class(TForm)
    medt1: TMaskEdit;
    tlb1: TToolBar;
    btnSalir: TToolButton;
    btnImprimir: TcxButton;
    btn1: TcxButton;
    edtClienteRNC: TMaskEdit;
    cbbTipoNCF: TcxComboBox;
    lbl1: TLabel;
    lbl2: TLabel;
    edtClienteNombre: TMaskEdit;
    lbl3: TLabel;
    medt4: TMaskEdit;
    lbl4: TLabel;
    actlst1: TActionList;
    sp1: TADOStoredProc;
    sp2: TADOStoredProc;
    acConsultaRNC: TAction;
    spConsultaRNC: TADOStoredProc;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btn1Click(Sender: TObject);
    procedure acConsultaRNCExecute(Sender: TObject);
    procedure btnImprimirClick(Sender: TObject);
    procedure btnSalirClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmGeneraNCFaFactura: TfrmGeneraNCFaFactura;

implementation

{$R *.dfm}

procedure TfrmGeneraNCFaFactura.acConsultaRNCExecute(Sender: TObject);
var
  var_RNC : String;
begin
  with spConsultaRNC do
  begin
    Close;
    var_RNC:= InputBox('Consulta RNC','Indique N�mero RNC','');
    Parameters.ParamByName('@RNC').Value:=var_RNC;
    Open;
    if  RecordCount>0 then
    begin
      edtClienteNombre.Text:=FieldByName('Nombre').Value;
      edtClienteRNC.Text:=var_RNC;
    end
    else
    begin
      ShowMessage('Registro NO Encontrado');
    end;
  end;
end;

procedure TfrmGeneraNCFaFactura.btn1Click(Sender: TObject);
begin
  Self.Close;
end;

procedure TfrmGeneraNCFaFactura.btnImprimirClick(Sender: TObject);
begin
  acConsultaRNC.Execute;
end;

procedure TfrmGeneraNCFaFactura.btnSalirClick(Sender: TObject);
begin
  Self.Close;
end;

procedure TfrmGeneraNCFaFactura.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action:=caFree;
  FrmGeneraNCFaFactura:=nil;
end;

end.
