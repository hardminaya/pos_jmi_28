unit frmNotasdeCredito;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, dxSkinsCore, dxSkinsDefaultPainters,
  dxSkinscxPCPainter, cxPCdxBarPopupMenu, cxStyles, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxEdit, Data.DB, cxDBData, cxGridLevel,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGridCustomView,
  cxGrid, cxPC, Data.Win.ADODB, Datasnap.DBClient, Datasnap.Provider, cxClasses,
  Vcl.Menus, JvMenus, Vcl.ActnList, JvDataSource, Vcl.ComCtrls, Vcl.ToolWin;

type
  TForm1 = class(TForm)
    tlb1: TToolBar;
    btnRegistro: TToolButton;
    btnAnterior: TToolButton;
    btnSiguiente: TToolButton;
    btnGuardar: TToolButton;
    btnCancelar: TToolButton;
    btn2: TToolButton;
    btnSalir: TToolButton;
    ds1: TJvDataSource;
    actlst1: TActionList;
    acSalir: TAction;
    jvpmn1: TJvPopupMenu;
    mniAgregar1: TMenuItem;
    mniModificar1: TMenuItem;
    mniEliminar1: TMenuItem;
    cxstylrpstry1: TcxStyleRepository;
    cxstyl1: TcxStyle;
    cxstyl2: TcxStyle;
    con1: TADOConnection;
    dtstprvdr1: TDataSetProvider;
    dseti: TClientDataSet;
    atncflddset1pIdRegistro: TAutoIncField;
    wdstrngflddset1CodigoArt: TWideStringField;
    wdstrngflddset1DescripcionArt: TWideStringField;
    intgrflddsetintgrflddset1GrupoArt: TIntegerField;
    wdstrngflddset1CodigoBarras: TWideStringField;
    bcdflddset1EnMano: TBCDField;
    strngflddset1CompraArt: TStringField;
    strngflddset1VentaArt: TStringField;
    strngflddset1InventarioArt: TStringField;
    strngflddset1ITBIS: TStringField;
    wdstrngflddset1ManejaSeriales: TWideStringField;
    wdstrngflddset1DescripcionEtiqueta: TWideStringField;
    intgrflddsetintgrflddset1Documento_Actualizacion: TIntegerField;
    QryArticulodset1Precio: TFMTBCDField;
    intgrflddsetintgrflddset1Doc_actualizacion: TIntegerField;
    wdstrngflddset1Codigo_Almacen: TWideStringField;
    dtmflddset1FechaActualizacion: TDateTimeField;
    dtmflddset1FechaModificacion: TDateTimeField;
    wdstrngflddset1Usuario: TWideStringField;
    qry1: TADOQuery;
    cxpgcntrl1: TcxPageControl;
    cxtbsht1: TcxTabSheet;
    cxgrd1: TcxGrid;
    cxgrdbtblvwGrid1DBTableView1: TcxGridDBTableView;
    ColGrid1DBTableView1CodigoArt: TcxGridDBColumn;
    ColGrid1DBTableView1DescripcionArt: TcxGridDBColumn;
    ColGrid1DBTableView1EnMano: TcxGridDBColumn;
    ColGrid1DBTableView1Codigo_Almacen: TcxGridDBColumn;
    cxgrdlvlGrid1Level1: TcxGridLevel;
    cxtbsht2: TcxTabSheet;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

uses
  UntMain;

{$R *.dfm}

end.
