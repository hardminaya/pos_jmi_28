unit UntConsultaArticulos;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, dxSkinsDefaultPainters,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit,
  Data.DB, cxDBData, cxGridLevel, cxClasses, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  Data.Win.ADODB, Vcl.Menus, Vcl.ActnList, Vcl.StdCtrls, cxButtons, cxContainer,
  cxTextEdit, cxListBox, cxDropDownEdit, cxBlobEdit, cxCalc, cxButtonEdit,
  cxMaskEdit, cxCalendar, Vcl.Buttons, cxLabel, cxCheckBox, dxSkinBlack,
  dxSkinBlue, dxSkinBlueprint, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom,
  dxSkinDarkSide, dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinFoggy,
  dxSkinGlassOceans, dxSkinHighContrast, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSevenClassic, dxSkinSharp, dxSkinSharpPlus, dxSkinSilver,
  dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008, dxSkinTheAsphaltWorld,
  dxSkinValentine, dxSkinVS2010, dxSkinWhiteprint, dxSkinXmas2008Blue;

type
  TfrmConsultaArticulo = class(TForm)
    MainView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    DataSource1: TDataSource;
    ColCodigoArt: TcxGridDBColumn;
    ColDescripcionArt: TcxGridDBColumn;
    ColCodigoBarras: TcxGridDBColumn;
    ColEnMano: TcxGridDBColumn;
    ColPrecio: TcxGridDBColumn;
    btnAceptar: TcxButton;
    btnCancelar: TcxButton;
    ActionList1: TActionList;
    acSeleccionArticulo: TAction;
    cxtxtdtArticulo: TcxTextEdit;
    ColAlmacen: TcxGridDBColumn;
    ColSerial: TcxGridDBColumn;
    btn1: TBitBtn;
    cxlkndflcntrlr1: TcxLookAndFeelController;
    ColCosto: TcxGridDBColumn;
    ColIventarioArt: TcxGridDBColumn;
    ColAceptaPagoTarjetaCredito: TcxGridDBColumn;
    ColItbis: TcxGridDBColumn;
    procedure MainView1DblClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure acSeleccionArticuloExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure MainView1KeyPress(Sender: TObject; var Key: Char);
    procedure FormShow(Sender: TObject);
    procedure cxtxtdtArticuloKeyPress(Sender: TObject; var Key: Char);
    procedure btn1Click(Sender: TObject);

////    procedure cxGrid1DBTableView1CellDblClick(Sender: TcxCustomGridTableView;
//      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
//      AShift: TShiftState; var AHandled: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmConsultaArticulo: TfrmConsultaArticulo;

implementation

{$R *.dfm}

uses UntData, UntMain, UntFacturacion, UntCantidad, UntSeriales,
  UntConsultaSeriales;

procedure TfrmConsultaArticulo.acSeleccionArticuloExecute(Sender: TObject);
begin
  With cArticulo do
  begin
    MainView1.Controller.SelectedRowCount;
    CodigoArt:=(MainView1.Controller.SelectedRecords[0].Values[0]);
    DescripcionArt:=(MainView1.Controller.SelectedRecords[0].Values[2]);
    Precio:=(MainView1.Controller.SelectedRecords[0].Values[5]);
    EnMano:=(MainView1.Controller.SelectedRecords[0].Values[4]);
    ManejaSeriales:=(MainView1.Controller.SelectedRecords[0].Values[6]);
    Costo:=(MainView1.Controller.SelectedRecords[0].Values[7]);
    InventarioArt:=(MainView1.Controller.SelectedRecords[0].Values[8]);
    Acepta_Pago_TarjetaCredito:=(MainView1.Controller.SelectedRecords[0].Values[9]);
    Itbis:=(MainView1.Controller.SelectedRecords[0].Values[10]);

    if (EnMano>0) or (cValoresIni.PermiteVentaArticuloSinInventario=true) then begin
      if ManejaSeriales='Y' then
      begin
        if cCaja.SeleccionaSeriales=FALSE then
        begin
          MessageDlg('La lista de seriales no est� disponible, debe indicar el serial completo',mtError,[mbOK],0);
          Exit;
        end;

        if not assigned(frmConsultaSeriales) then frmConsultaSeriales:=TfrmConsultaSeriales.Create(frmFacturacion);
        frmConsultaSeriales.ShowModal;
        self.Close;
      end
      else
      begin
        if cArticulo.Acepta_Pago_TarjetaCredito='N' then begin
          if MessageBox(Handle,'Este Articulo NO acepta pago con Tarjeta de ' +
            'Cr�dito, Desea continuar de todos modos?','ATENCION'
            ,MB_ICONQUESTION or MB_YESNO) =IDYES then begin
              cFactura.Acepta_Pago_TarjetaCredito:='N';
            end
          else
          begin
            Exit;
          end;
        end;

        if not assigned (frmCantidad) then frmCantidad:=TfrmCantidad.Create(frmFacturacion);
        frmCantidad.ShowModal;
        self.Close;
      end;
    end
    else
    begin
       MessageDlg('Articulo NO hay disponible',mtError,[mbOK],0);
      end;
  end;
end;



procedure TfrmConsultaArticulo.btn1Click(Sender: TObject);
begin
  cVariables.ArticuloaCriteriodeBusqueda:=cxtxtdtArticulo.Text;
  frmFacturacion.acBuscarArticulo.Execute;
end;

procedure TfrmConsultaArticulo.btnCancelarClick(Sender: TObject);
begin
  self.Close;
end;

procedure TfrmConsultaArticulo.cxtxtdtArticuloKeyPress(Sender: TObject;
  var Key: Char);
begin
//  cVariables.ArticuloaCriteriodeBusqueda:=cxtxtdtArticulo.Text;
//  frmFacturacion.acBuscarArticulo.Execute;
end;

procedure TfrmConsultaArticulo.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action:=caFree;
  frmConsultaArticulo:=nil;
end;

procedure TfrmConsultaArticulo.FormShow(Sender: TObject);
begin
  cxtxtdtArticulo.Text:=cVariables.ArticuloaCriteriodeBusqueda;
  btnAceptar.SetFocus;
end;

procedure TfrmConsultaArticulo.MainView1DblClick(Sender: TObject);

begin
  acSeleccionArticulo.Execute;
end;


procedure TfrmConsultaArticulo.MainView1KeyPress(Sender: TObject;
  var Key: Char);
begin
  if key=#13 then acSeleccionArticulo.Execute;
  if Key=#27 then frmConsultaArticulo.Close;
end;

end.

