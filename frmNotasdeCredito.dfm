object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Notas de Creditos'
  ClientHeight = 577
  ClientWidth = 882
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object tlb1: TToolBar
    Left = 0
    Top = 0
    Width = 882
    Height = 65
    ButtonHeight = 60
    ButtonWidth = 57
    Caption = 'tlb1'
    Images = frmMain.ilToolBar32
    ShowCaptions = True
    TabOrder = 0
    ExplicitLeft = -423
    ExplicitWidth = 950
    object btnRegistro: TToolButton
      Left = 0
      Top = 0
      Caption = 'Registro'
      DropdownMenu = jvpmn1
      ImageIndex = 9
      PopupMenu = jvpmn1
      Style = tbsDropDown
    end
    object btnAnterior: TToolButton
      Left = 72
      Top = 0
      Caption = 'Anterior'
      ImageIndex = 3
    end
    object btnSiguiente: TToolButton
      Left = 129
      Top = 0
      Caption = 'Siguiente'
      ImageIndex = 0
    end
    object btnGuardar: TToolButton
      Left = 186
      Top = 0
      Caption = 'Guardar'
      ImageIndex = 2
    end
    object btnCancelar: TToolButton
      Left = 243
      Top = 0
      Caption = 'Cancelar'
      ImageIndex = 5
    end
    object btn2: TToolButton
      Left = 300
      Top = 0
      Width = 55
      Caption = 'btn5'
      ImageIndex = 5
      Style = tbsSeparator
    end
    object btnSalir: TToolButton
      Left = 355
      Top = 0
      Caption = 'Salir'
      ImageIndex = 1
    end
  end
  object cxpgcntrl1: TcxPageControl
    Left = 0
    Top = 65
    Width = 882
    Height = 512
    Align = alClient
    TabOrder = 1
    Properties.ActivePage = cxtbsht1
    ExplicitLeft = -423
    ExplicitTop = -247
    ExplicitWidth = 950
    ExplicitHeight = 490
    ClientRectBottom = 506
    ClientRectLeft = 3
    ClientRectRight = 876
    ClientRectTop = 26
    object cxtbsht1: TcxTabSheet
      Caption = 'Listado'
      ImageIndex = 0
      ExplicitWidth = 941
      ExplicitHeight = 458
      object cxgrd1: TcxGrid
        Left = 0
        Top = 0
        Width = 873
        Height = 480
        Align = alClient
        TabOrder = 0
        ExplicitWidth = 941
        ExplicitHeight = 458
        object cxgrdbtblvwGrid1DBTableView1: TcxGridDBTableView
          DataController.DataSource = ds1
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          object ColGrid1DBTableView1CodigoArt: TcxGridDBColumn
            DataBinding.FieldName = 'CodigoArt'
            HeaderAlignmentHorz = taCenter
            Styles.Header = cxstyl2
            Width = 96
          end
          object ColGrid1DBTableView1DescripcionArt: TcxGridDBColumn
            DataBinding.FieldName = 'DescripcionArt'
            HeaderAlignmentHorz = taCenter
            Styles.Header = cxstyl2
            Width = 532
          end
          object ColGrid1DBTableView1EnMano: TcxGridDBColumn
            DataBinding.FieldName = 'EnMano'
            HeaderAlignmentHorz = taCenter
            Styles.Header = cxstyl2
            Width = 61
          end
          object ColGrid1DBTableView1Codigo_Almacen: TcxGridDBColumn
            DataBinding.FieldName = 'Codigo_Almacen'
            HeaderAlignmentHorz = taCenter
            Styles.Header = cxstyl2
            Width = 112
          end
        end
        object cxgrdlvlGrid1Level1: TcxGridLevel
          GridView = cxgrdbtblvwGrid1DBTableView1
        end
      end
    end
    object cxtbsht2: TcxTabSheet
      Caption = 'Registro'
      ImageIndex = 1
      ExplicitLeft = 0
      ExplicitTop = 24
      ExplicitWidth = 838
      ExplicitHeight = 426
    end
  end
  object ds1: TJvDataSource
    AutoEdit = False
    DataSet = dseti
    Left = 80
    Top = 504
  end
  object actlst1: TActionList
    Left = 384
    Top = 504
    object acSalir: TAction
      Caption = 'acSalir'
    end
  end
  object jvpmn1: TJvPopupMenu
    Images = frmMain.ilToolBar32
    ImageMargin.Left = 0
    ImageMargin.Top = 0
    ImageMargin.Right = 0
    ImageMargin.Bottom = 0
    ImageSize.Height = 0
    ImageSize.Width = 0
    Left = 696
    Top = 24
    object mniAgregar1: TMenuItem
      Caption = 'Agregar'
      ImageIndex = 10
    end
    object mniModificar1: TMenuItem
      Caption = 'Modificar'
      ImageIndex = 9
    end
    object mniEliminar1: TMenuItem
      Caption = 'Eliminar'
      ImageIndex = 8
    end
  end
  object cxstylrpstry1: TcxStyleRepository
    Left = 760
    Top = 24
    PixelsPerInch = 96
    object cxstyl1: TcxStyle
      AssignedValues = [svFont]
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
    end
    object cxstyl2: TcxStyle
      AssignedValues = [svFont]
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
    end
  end
  object con1: TADOConnection
    Connected = True
    ConnectionString = 
      'Provider=SQLOLEDB.1;Integrated Security=SSPI;Persist Security In' +
      'fo=False;Initial Catalog=DB_PUNTO_VENTA;Data Source=DOHAPBT0N3G\' +
      'SQLEXPRESS'
    LoginPrompt = False
    Provider = 'SQLOLEDB.1'
    Left = 336
    Top = 504
  end
  object dtstprvdr1: TDataSetProvider
    DataSet = qry1
    Left = 256
    Top = 504
  end
  object dseti: TClientDataSet
    Active = True
    Aggregates = <>
    Params = <>
    ProviderName = 'dtstprvdr1'
    Left = 200
    Top = 504
    object atncflddset1pIdRegistro: TAutoIncField
      FieldName = 'pIdRegistro'
      ReadOnly = True
    end
    object wdstrngflddset1CodigoArt: TWideStringField
      FieldName = 'CodigoArt'
    end
    object wdstrngflddset1DescripcionArt: TWideStringField
      FieldName = 'DescripcionArt'
      Size = 100
    end
    object intgrflddsetintgrflddset1GrupoArt: TIntegerField
      FieldName = 'GrupoArt'
    end
    object wdstrngflddset1CodigoBarras: TWideStringField
      FieldName = 'CodigoBarras'
      Size = 50
    end
    object bcdflddset1EnMano: TBCDField
      FieldName = 'EnMano'
      Precision = 18
      Size = 2
    end
    object strngflddset1CompraArt: TStringField
      FieldName = 'CompraArt'
      FixedChar = True
      Size = 1
    end
    object strngflddset1VentaArt: TStringField
      FieldName = 'VentaArt'
      FixedChar = True
      Size = 1
    end
    object strngflddset1InventarioArt: TStringField
      FieldName = 'InventarioArt'
      FixedChar = True
      Size = 1
    end
    object strngflddset1ITBIS: TStringField
      FieldName = 'ITBIS'
      FixedChar = True
      Size = 1
    end
    object wdstrngflddset1ManejaSeriales: TWideStringField
      FieldName = 'ManejaSeriales'
      FixedChar = True
      Size = 1
    end
    object wdstrngflddset1DescripcionEtiqueta: TWideStringField
      FieldName = 'DescripcionEtiqueta'
    end
    object intgrflddsetintgrflddset1Documento_Actualizacion: TIntegerField
      FieldName = 'Documento_Actualizacion'
    end
    object QryArticulodset1Precio: TFMTBCDField
      FieldName = 'Precio'
      Precision = 19
      Size = 6
    end
    object intgrflddsetintgrflddset1Doc_actualizacion: TIntegerField
      FieldName = 'Doc_actualizacion'
    end
    object wdstrngflddset1Codigo_Almacen: TWideStringField
      FieldName = 'Codigo_Almacen'
      Size = 50
    end
    object dtmflddset1FechaActualizacion: TDateTimeField
      FieldName = 'FechaActualizacion'
    end
    object dtmflddset1FechaModificacion: TDateTimeField
      FieldName = 'FechaModificacion'
    end
    object wdstrngflddset1Usuario: TWideStringField
      FieldName = 'Usuario'
      Size = 50
    end
  end
  object qry1: TADOQuery
    Active = True
    Connection = con1
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'SELECT * FROM [Articulos]')
    Left = 24
    Top = 504
  end
end
