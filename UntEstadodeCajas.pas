unit UntEstadodeCajas;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, dxSkinsCore, dxSkinsDefaultPainters,
  dxSkinscxPCPainter, cxPCdxBarPopupMenu, Vcl.Menus, Vcl.ComCtrls, Vcl.ToolWin,
  Datasnap.DBClient, Vcl.ActnList, Data.DB, Data.Win.ADODB, JvComponentBase,
  JvDBGridExport, Vcl.StdCtrls, JvExStdCtrls, JvCombobox, JvExComCtrls,
  JvDateTimePicker, cxButtons, JvExControls, JvXPCore, JvXPButtons, Vcl.Grids,
  Vcl.DBGrids, JvExDBGrids, JvDBGrid, cxPC, Datasnap.Provider,
  Vcl.Samples.Calendar, cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxEdit, cxDBData, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxGridLevel, cxClasses, cxGridCustomView, cxGrid, users;

type
  TfrmEstadodeCajas = class(TForm)
    tlb1: TToolBar;
    btnSalir: TToolButton;
    ds2: TDataSource;
    dtstprvdr1: TDataSetProvider;
    dset1: TClientDataSet;
    btnRefrescar: TToolButton;
    btn5: TToolButton;
    qry1: TADOQuery;
    cxgrdbtblvwGrid1DBTableView1: TcxGridDBTableView;
    cxgrdlvlGrid1Level1: TcxGridLevel;
    cxgrd1: TcxGrid;
    GridGrid1DBTableView1fkUsuario: TcxGridDBColumn;
    GridGrid1DBTableView1FechaApertura: TcxGridDBColumn;
    GridGrid1DBTableView1FechaCierre: TcxGridDBColumn;
    GridGrid1DBTableView1MontoApertura: TcxGridDBColumn;
    GridGrid1DBTableView1MontoCierre: TcxGridDBColumn;
    GridGrid1DBTableView1EstadoCaja: TcxGridDBColumn;
    GridGrid1DBTableView1NombrePC: TcxGridDBColumn;
    atncflddset1pIdRegistro: TAutoIncField;
    intgrflddset1fkUsuario: TIntegerField;
    dtmflddset1FechaApertura: TDateTimeField;
    dtmflddset1FechaCierre: TDateTimeField;
    bcdflddset1MontoApertura: TBCDField;
    bcdflddset1MontoCierre: TBCDField;
    strngflddset1EstadoCaja: TStringField;
    wdstrngflddset1NombrePC: TWideStringField;
    cxstylrpstry1: TcxStyleRepository;
    cxstyl1: TcxStyle;
    btnModificar: TToolButton;
    usrsrg1: TUsersReg;
    btn2: TToolButton;
    procedure btnSalirClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnRefrescarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Refrescar;
    procedure btnModificarClick(Sender: TObject);

  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmEstadodeCajas: TfrmEstadodeCajas;

implementation

uses
  UntData, UntMain;

{$R *.dfm}


procedure TfrmEstadodeCajas.btnSalirClick(Sender: TObject);
begin
  Self.Close;
end;

procedure TfrmEstadodeCajas.btnModificarClick(Sender: TObject);
begin
  GridGrid1DBTableView1EstadoCaja.Options.Editing:=True;
  dset1.Edit;
  dset1.ApplyUpdates(0)
end;

procedure TfrmEstadodeCajas.btnRefrescarClick(Sender: TObject);
begin
  Refrescar;
end;

procedure TfrmEstadodeCajas.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action:=caFree;
  frmEstadodeCajas:=nil;
end;

procedure TfrmEstadodeCajas.FormShow(Sender: TObject);
begin
  dset1.Open;
end;

procedure TfrmEstadodeCajas.Refrescar;
begin
  dset1.Close;
  dset1.Open;
end;

end.
