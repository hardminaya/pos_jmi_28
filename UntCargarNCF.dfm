object frmCargarNCF: TfrmCargarNCF
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  Caption = 'Cargar Secuencia de NCF'
  ClientHeight = 287
  ClientWidth = 376
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object lbl1: TLabel
    Left = 16
    Top = 64
    Width = 35
    Height = 18
    Caption = 'Desde'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lbl2: TLabel
    Left = 18
    Top = 107
    Width = 33
    Height = 13
    Caption = 'Hasta'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lbl3: TLabel
    Left = 16
    Top = 22
    Width = 121
    Height = 13
    Caption = 'Tipo de Comprobante'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lbldesde: TLabel
    Left = 170
    Top = 83
    Width = 175
    Height = 13
    AutoSize = False
  end
  object lblhasta: TLabel
    Left = 173
    Top = 126
    Width = 179
    Height = 13
    AutoSize = False
  end
  object lbl6: TLabel
    Left = 170
    Top = 22
    Width = 59
    Height = 13
    Caption = 'Estructura'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lblActualNCF: TLabel
    Left = 18
    Top = 150
    Width = 339
    Height = 13
    AutoSize = False
  end
  object btnGenerar: TButton
    Left = 18
    Top = 188
    Width = 339
    Height = 45
    Cursor = crHandPoint
    Caption = '&Generar'
    TabOrder = 5
    OnClick = btnGenerarClick
  end
  object pb1: TProgressBar
    Left = 18
    Top = 165
    Width = 339
    Height = 17
    TabOrder = 4
  end
  object cxdblblEstructura: TcxDBLabel
    Left = 170
    Top = 38
    DataBinding.DataField = 'Estructura'
    DataBinding.DataSource = dsTiposComprobantes
    Height = 21
    Width = 153
  end
  object cbb2: TDBLookupComboBox
    Left = 18
    Top = 38
    Width = 145
    Height = 21
    KeyField = 'pIdRegistro'
    ListField = 'Descripcion'
    ListFieldIndex = 1
    ListSource = dsTiposComprobantes
    TabOrder = 0
  end
  object edtDesde: TcxSpinEdit
    Left = 18
    Top = 80
    Properties.AssignedValues.EditFormat = True
    Properties.BeepOnError = True
    Properties.ImmediatePost = True
    Properties.MinValue = 1.000000000000000000
    Properties.Nullstring = '0'
    Properties.UseNullString = True
    Properties.OnChange = edtDesdePropertiesChange
    TabOrder = 2
    Value = 1
    Width = 145
  end
  object edtHasta: TcxSpinEdit
    Left = 18
    Top = 123
    Properties.AssignedValues.EditFormat = True
    Properties.BeepOnError = True
    Properties.ImmediatePost = True
    Properties.MinValue = 1.000000000000000000
    Properties.Nullstring = '0'
    Properties.UseNullString = True
    Properties.OnChange = edtHastaPropertiesChange
    TabOrder = 3
    Value = 1
    Width = 145
  end
  object qryTiposComprobantes: TADOQuery
    Connection = DatMain.con1
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select * from [NCF.Tipos]')
    Left = 72
    Top = 224
    object intgrfldTiposComprobantespIdRegistro: TIntegerField
      FieldName = 'pIdRegistro'
    end
    object wdstrngfldTiposComprobantesDescripcion: TWideStringField
      FieldName = 'Descripcion'
      Size = 50
    end
    object wdstrngfldTiposComprobantesEstructura: TWideStringField
      FieldName = 'Estructura'
      Size = 30
    end
    object strngfldTiposComprobantesManejaEstructura: TStringField
      FieldName = 'ManejaEstructura'
      FixedChar = True
      Size = 1
    end
  end
  object dsTiposComprobantes: TDataSource
    DataSet = qryTiposComprobantes
    Left = 152
    Top = 224
  end
  object qryWork1: TADOQuery
    Connection = DatMain.con1
    Parameters = <>
    Left = 240
    Top = 224
  end
end
