unit UntPago;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, cxLookAndFeelPainters, StdCtrls, cxButtons, cxControls,
  cxContainer, cxEdit, cxTextEdit, cxCurrencyEdit, ExtCtrls, Grids, ValEdit,
  cxMaskEdit, DB, StrUtils, ADODB, ActnList, cxGraphics,
  cxLookAndFeels, dxSkinsCore, dxSkinsDefaultPainters, cxStyles,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage, cxDBData,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGridLevel,
  cxClasses, cxGridCustomView, cxGrid, Datasnap.Provider, Datasnap.DBClient,math,
  cxLabel, cxDropDownEdit, dxGDIPlusClasses, cxImage, Vcl.ImgList, cxPC,
  JvExExtCtrls, JvExtComponent, JvPanel, dxBar, dxDockControl, dxDockPanel,
  dxSkinsdxBarPainter, dxSkinsdxDockControlPainter, dxSkinBlack, dxSkinBlue,
  dxSkinBlueprint, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide,
  dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinFoggy,
  dxSkinGlassOceans, dxSkinHighContrast, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSevenClassic, dxSkinSharp, dxSkinSharpPlus, dxSkinSilver,
  dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008, dxSkinTheAsphaltWorld,
  dxSkinValentine, dxSkinVS2010, dxSkinWhiteprint, dxSkinXmas2008Blue,WebPosData;

type
  TfrmPago = class(TForm)
    btnAceptar: TcxButton;
    btnCancelar: TcxButton;
    qry1: TADOQuery;
    actlst1: TActionList;
    Label5: TLabel;
    cxLabel1: TcxLabel;
    cxLabel2: TcxLabel;
    cxLabel3: TcxLabel;
    cxLabel4: TcxLabel;
    cxLabel5: TcxLabel;
    actRecalcularPago: TAction;
    lbl1: TLabel;
    lbl2: TLabel;
    actLimpiarPago1: TAction;
    actProcesarPago: TAction;
    edtTotalCobrar: TcxCurrencyEdit;
    edtRecibo: TcxCurrencyEdit;
    edtDevuelta: TcxCurrencyEdit;
    img1: TcxImage;
    img2: TcxImage;
    img3: TcxImage;
    img4: TcxImage;
    sp_ConsultaNC: TADOStoredProc;
    spActualizaBalanceNC: TADOStoredProc;
    pnl1: TPanel;
    edtEfectivo: TcxCurrencyEdit;
    edtTarjetaCredito: TcxCurrencyEdit;
    edtTarjetaDebito: TcxCurrencyEdit;
    edtCheque: TcxCurrencyEdit;
    edtNumeroNC: TcxCurrencyEdit;
    edtNotaCredito: TcxCurrencyEdit;
    spTarjetaDescuento_Utiliza: TADOStoredProc;
    lblTipoNCF: TLabel;
    procedure actRecalcularPagoExecute(Sender: TObject);
    procedure edtEfectivoKeyPress(Sender: TObject; var Key: Char);
    procedure FormShow(Sender: TObject);
    procedure btnAceptarClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure cxcrncydtedtEfectivoKeyPress(Sender: TObject; var Key: Char);
    procedure actLimpiarPago1Execute(Sender: TObject);
    procedure actProcesarPagoExecute(Sender: TObject);
    procedure btn1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure edtNumeroNCPropertiesEditValueChanged(Sender: TObject);

  private
    { Private declarations }
    FNumFactura: string;
    FIDFactura: integer;

  public
    { Public declarations }
    property NumFactura: string read FNumFactura write FNumFactura;
    property IDFactura: integer read FIDFactura write FIDFactura;
  end;

var
  frmPago: TfrmPago;

implementation

uses untData, UntFacturacion, UntMain, UntPreview;

{$R *.dfm}

var
  sRef, sExp, sApproval: string;
  GenerarWebpostxt : tGenerateWebPosTXT;

procedure TfrmPago.actLimpiarPago1Execute(Sender: TObject);
begin
// Limpia Clase pago blanqueando todos lo valores
  with cPago do
  begin
    Efectivo:=0;
    TarjetaCredito:=0;
    TarjetaDebito:=0;
    Cheque:=0;
    NotaCreditoNumero:=0;
    NotaCreditoMonto:=0;
    Pendiente:=0;
    Devuelta:=0;
  end;

 // Limpia Todos los Campos del formulario pago
  edtEfectivo.Text:='0';
  edtTarjetaCredito.Text:='0';
  edtTarjetaDebito.Text:='0';
  edtCheque.Text:='0';
  edtNumeroNC.Text:='0';
  edtNotaCredito.Text:='0';
  edtRecibo.Text:='0';

  //Limpiar valores por defectos para comprobantes
  With cVariables do begin
    ImprimirWebPos:=cValoresIni.ImprimirWebPos;
    RutaArchivoWebPos:=cValoresIni.RutaArchivoWebPos;
    IDEstacionWebPos:=cValoresIni.IDEstacionWebPos;
    ImprimirTicket:=cValoresIni.ImprimirTicket;
  end;

end;

procedure TfrmPago.actProcesarPagoExecute(Sender: TObject);
var
  var_pregunta,RowIndex : Integer;
  Dll:Cardinal;
  documento:TDataFile;
begin
 try
  Screen.Cursor:=crSQLWait;
  // Inicio de Transaccion
  DatMain.con1.Open;
  cVariables.SQLTransactionLevel:=DatMain.con1.BeginTrans;

  with frmFacturacion do begin
    try begin
      //if not (cVariables.ImprimirWebPos) then cFactura.TipoNCF:=6;
      if (cValoresIni.ComprobantesTipo='7') then cFactura.TipoNCF:=7;

      InsertaFactura(Self);
      InsertaFacturaDetalle(Self);
      InsertaFacturaDetalleSerial(Self);

      if StrToInt(edtNumeroNC.Text)>0 then begin
        spActualizaBalanceNC.Parameters.ParamByName('@NumeroReq').Value:=edtNumeroNC.Text;
        spActualizaBalanceNC.ExecProc;
       end;

      // Actualiza Tarjeta de descuento aplicada
      if Length(cCliente.TarjetaID) > 0 then begin
        spTarjetaDescuento_Utiliza.Parameters.ParamByName('@TarjetaID').value:=ccliente.TarjetaID;
        spTarjetaDescuento_Utiliza.ExecProc;
        fn_RegistrodeCambio(CUsuario.Usuario,'Pago','Tarjeta de Descuento No.:'+ccliente.TarjetaID+
        ' utilizada');
      end;

      DatMain.con1.CommitTrans;
    end;
  except
    on E:Exception do begin
      DatMain.con1.RollbackTrans;
      MessageBox(Handle,'ERROR al Generar Factura. Operacion NO efectuada','Generar Factura',MB_ICONERROR or MB_OK);
      ShowMessage(E.Message);
      Exit;
    end;
  end;

    dxstsbr1.Panels[0].Text:='Ultimo Ticket No.: '+inttostr(cFactura.Numero)+ ' en Fecha: '+ FormatDateTime('yyyy/mm/dd hh:mm:ss am/pm',Now);

    if (cFactura.TipoNCF=6) then begin
       EscribirOrdenPedido(IntToStr(cFactura.Numero));
       MessageBox(Handle,'Buscar Orden de Pedido','Impresión de Ticket',MB_ICONINFORMATION or MB_OK);
    end;
    acCancelar.Execute;

    datmain.qryTicket.Parameters.ParamByName('@PIDRegistro').Value:= cFactura.Numero;
    datmain.qryTicket.Close;
    datmain.qryTicket.Open;

    if (cVariables.ImprimirWebPos) then begin

    // Generar archivo WEBPOS
    //llamar la dll de webposinterface y asignar el procedimiento a la variable.
    // hay que llenar la variable Documento
    //ShowMessage('Imprimiendo WebPos Cabecera');
    documento.FilePath:=cvariables.RutaArchivoWebPos;
    documento.IDStation:=cVariables.IDEstacionWebPos;//ID de la estacion que esta imprimiendo
    case datmain.qryTicket.FieldByName('fkTipoNCF').AsInteger of
     1: documento.DocumentType:='FC';
     2: documento.DocumentType:='FF';
     3: documento.DocumentType:='FG';
     4: documento.DocumentType:='FE';
    end;
    documento.NCFNumber:=DatMain.qryTicket.FieldByName('NumeroNCF').Value;// numero de comprobante o numero consecutivo de no tener
    documento.Client:=cCliente.Nombre;// nombre del cliente o nombre del webpos para contado
    documento.ClientRNC:=cCliente.RNC; // rnc del cliente
    if (documento.DocumentType='FE') then begin
      documento.ITBISExonerated:='Si';
    end
    else begin
      documento.ITBISExonerated:='No'
    end;
    documento.ClientAddress:=cCliente.Direccion; // direccion de cliente
    SetLength(documento.Detail,datmain.qryTicket.RecordCount);
     datmain.qryTicket.First;
    for RowIndex:=0 to  datmain.qryTicket.RecordCount -1 do
    begin
      //ShowMessage('Imprimiendo Detalle del WebPOS');
      documento.Detail[RowIndex].Code:= datmain.qryTicket.FieldByName('ArticuloID').Value;
      documento.Detail[RowIndex].Price:= datmain.qryTicket.FieldByName('ArticuloPrecio').AsFloat;
      if LowerCase(datmain.qryTicket.FieldByName('ManejaSeriales').AsString) = LowerCase('Y') then
      begin
         documento.Detail[RowIndex].Quantity:= 1;//datmain.qryTicket.FieldByName('ArticuloCantidad').AsFloat;
         documento.Detail[RowIndex].Comment:= 'Serial: '+ datmain.qryTicket.FieldByName('Serie').Value;
      end else
      begin
         documento.Detail[RowIndex].Quantity:= datmain.qryTicket.FieldByName('ArticuloCantidad').AsFloat;
         documento.Detail[RowIndex].Comment:= '' ;
      end;
      documento.Detail[RowIndex].Discount:= datmain.qryTicket.FieldByName('ArticuloPorcientoDesc').Value;
      documento.Detail[RowIndex].DiscountType:= '%'; // colocar el valor para descuento por monto

      if LowerCase(datmain.qryTicket.FieldByName('ITBIS').AsString) = LowerCase('Y') then
      begin
        documento.Detail[RowIndex].ITBISType:= 't2';
      end else
      begin
        documento.Detail[RowIndex].ITBISType:= 't0';
      end;

      documento.Detail[RowIndex].Description:= datmain.qryTicket.FieldByName('ArticuloDescripcion').Value;

      datmain.qryTicket.Next;
    end;
    RowIndex:=0;// reiniciar para pagos
    if datmain.qryTicket.FieldByName('Cheque').Value > 0 then
    begin
    //en este punto rowindex consigue el tamanio de payments
     RowIndex := length(documento.Payments);
     SetLength(documento.Payments,RowIndex+1);
     documento.Payments[RowIndex].Amount:= datmain.qryTicket.FieldByName('Cheque').Value;
     documento.Payments[RowIndex].PayType:= '02';
     documento.Payments[RowIndex].Description:='Cheque';
    end;

    if datmain.qryTicket.FieldByName('Tarjeta').Value > 0 then
    begin
     //en este punto rowindex consigue el tamanio de payments
     RowIndex := length(documento.Payments);
     SetLength(documento.Payments,RowIndex+1);
     documento.Payments[RowIndex].Amount:= datmain.qryTicket.FieldByName('Tarjeta').Value;
     documento.Payments[RowIndex].PayType:= '03';
     documento.Payments[RowIndex].Description:='Tarjeta';
     end;
    {if cPago.TarjetaDebito > 0 then
    begin
    //en este punto rowindex consigue el tamanio de payments
      RowIndex := length(documento.Payments);
      SetLength(documento.Payments,RowIndex+1);
      documento.Payments[RowIndex].Amount:= cPago.TarjetaDebito;
      documento.Payments[RowIndex].PayType:= '04';
      documento.Payments[RowIndex].Description:='Tarjeta Debito';
    end;  }

    if datmain.qryTicket.FieldByName('NCredito').Value > 0 then
    begin
    //en este punto rowindex consigue el tamanio de payments
     RowIndex := length(documento.Payments);
     SetLength(documento.Payments,RowIndex+1);
     documento.Payments[RowIndex].Amount:= datmain.qryTicket.FieldByName('ncredito').AsFloat;
     documento.Payments[RowIndex].PayType:= '05';
     documento.Payments[RowIndex].Description:='Nota de Credito';
    end;

    if datmain.qryTicket.FieldByName('Efectivo').Value > 0 then
    begin
     //en este punto rowindex consigue el tamanio de payments
      RowIndex := length(documento.Payments);
      SetLength(documento.Payments,RowIndex+1);
      documento.Payments[RowIndex].Amount:= datmain.qryTicket.FieldByName('Efectivo').Value;
      documento.Payments[RowIndex].PayType:= '01';
      documento.Payments[RowIndex].Description:='Efectivo';
    end;
    //documento.Fooder asignar los valores a imprimir al final de la factura
    //documento.AdditionalInfo asignar los valores a informacion adicional
    documento.DiscountOverSubTotal:=0; // cuanto fue descontado sobre el total

    //Agrega nombre de Caja al final del documento
    SetLength(documento.Fooder,2);
    documento.Fooder[0]:='Caja:' + cCaja.Nombre;
    documento.Fooder[1]:='TicketNo.:' + IntToStr(cFactura.Numero);

    // Retorna los valores de la orden
    // asignar los valores de pagos

    Dll := LoadLibrary(PWideChar(ExtractFilePath(Application.ExeName)+'Dll\WebPosInterface.dll'));

    if Dll <> 0 then
     begin
       @GenerarWebpostxt := GetProcAddress(dll,'GenerateTXTFile');
       if Assigned(GenerarWebpostxt) then
          begin
           if not GenerarWebpostxt(documento) then
             begin

             end;
          end;
     end;
    end;

    frmPreview1.prprtTicket.Template.DatabaseSettings.Name:=
      cVariables.NombreTicketReporte;
    frmPreview1.prprtTicket.Template.LoadFromDatabase;
    frmPreview1.plblTitulo.Text:='FACTURA';

    cVariables.LargoTicket:=cValoresIni.PapelTamano;

    frmPreview1.prprtTicket.PrinterSetup.PaperHeight:=cVariables.LargoTicket;

    fn_RegistrodeCambio(CUsuario.Usuario,'Pago','Pago efectuado a Factura No.:'+IntToStr(cFactura.Numero));

    if cVariables.ImprimirTicket=true then begin
      if cValoresIni.TicketAuto=TRUE then begin
        with frmPreview1.prprtTicket do begin
            PrinterSetup.Copies:=1;
            DeviceType:='Printer';
            fn_RegistrodeCambio(CUsuario.Usuario,'Pago','Factura No.: '+IntToStr(cFactura.Numero)+' Impresa');
            PrinterSetup.PrinterName:='Default';
            ShowCancelDialog:=False;
            ShowPrintDialog:=False;
            PrintReport;
        end;
      end;
          if not Assigned(frmPreview1) then frmPreview1:=TfrmPreview1.Create(frmFacturacion);
          frmPreview1.prprtTicket.PrinterSetup.Copies:=1;

          if cCaja.ImprimeCopia<=0 then frmPreview1.btnImprimir.Visible:=False;
          //frmPreview1.ShowModal;
          Screen.Cursor:=crDefault;

          frmFacturacion.acCancelar.Execute;
    end;
  end;
 finally

 end;
end;

procedure TfrmPago.btn1Click(Sender: TObject);
begin
  edtEfectivo.Text:='0';
  edtTarjetaCredito.Text:='0';
  edtTarjetaDebito.Text:='0';
  edtCheque.Text:='0';
  actRecalcularPago.Execute;
end;

procedure TfrmPago.btnAceptarClick(Sender: TObject);
var
  var_pregunta : Integer;
begin
  if (fn_CajaEstado(cCaja.Nombre)<>1) or (fn_CajaAutorizada(cCaja.Nombre)=0)then begin
    MessageDlg('Caja NO Autorizada o CERRADA',mtError,[mbYes],0);
    ModalResult:=mrNone;
    Exit;
  end;
  var_pregunta:=MessageBox(Handle,'General Factura y Efectuar Pago?','Procesar Pago',MB_ICONQUESTION or MB_YESNO);
  if var_pregunta=ID_YES then begin
    actProcesarPago.Execute;
    actLimpiarPago1.Execute;
    ModalResult:=mrOk;
    self.CloseModal;
  end;
end;

procedure TfrmPago.btnCancelarClick(Sender: TObject);
begin
  actLimpiarPago1.Execute;
  self.CloseModal;
end;

procedure TfrmPago.cxcrncydtedtEfectivoKeyPress(Sender: TObject; var Key: Char);
begin
  if key=#13 then edtTarjetaCredito.SetFocus;
end;

procedure TfrmPago.edtEfectivoKeyPress(Sender: TObject; var Key: Char);
begin
  if key=#13 then edtTarjetaCredito.SetFocus;

end;

procedure TfrmPago.edtNumeroNCPropertiesEditValueChanged(Sender: TObject);
var
  var_NC_Balance:Currency;
begin
  if Length(edtNumeroNC.Text)> 0 then begin
    sp_ConsultaNC.Parameters.ParamByName('@NumeroReq').Value:=edtNumeroNC.Text;
    sp_ConsultaNC.ExecProc;
    var_NC_Balance:=sp_ConsultaNC.Parameters.ParamByName('@RETURN_VALUE').Value;
    edtNotaCredito.Value:=var_NC_Balance;
    actRecalcularPago.Execute;
  end;
end;

procedure TfrmPago.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action:=caFree;
  frmPago:=nil;
end;

procedure TfrmPago.FormShow(Sender: TObject);
begin
  if cFactura.Acepta_Pago_TarjetaCredito='N' then edtTarjetaCredito.Enabled:=
    false;

  edtTotalCobrar.Text:=format('%m',[cFactura.Total]);
  lblTipoNCF.Caption:=frmFacturacion.cbbTipoNCF.Text;
  actRecalcularPago.Execute;
//  edtEfectivo.Text:='0.00';
  edtEfectivo.SetFocus;
end;

procedure TfrmPago.actRecalcularPagoExecute(Sender: TObject);
begin
  With cPago do
  begin
    Efectivo:=0;
    TarjetaCredito:=0;
    TarjetaDebito:=0;
    Cheque:=0;
    NotaCreditoNumero:=0;
    NotaCreditoMonto:=0;

    Total:=0;
    Pendiente:=0;
    Devuelta:=0;

    Efectivo:=StrToFloat(edtEfectivo.Text);
    TarjetaCredito:=StrToFloat(edtTarjetaCredito.text);
    TarjetaDebito:=StrToFloat(edtTarjetaDebito.text);
    Cheque:=StrToFloat(edtCheque.text);
    NotaCreditoNumero:=StrToInt(edtNumeroNC.Text);
    NotaCreditoMonto:=StrToFloat(edtNotaCredito.text);

    //ShowMessage(FloatToStr(cfactura.total));
    Total:=cpago.Efectivo+cPago.TarjetaCredito+TarjetaDebito+Cheque+NotaCreditoMonto;
    Pendiente:=cFactura.Total-Total;
    Devuelta:=(Total-cFactura.Total);

// Depliega valores en pantalla
    edtRecibo.Text:=format('%m',[cPago.Total]);

//    if Devuelta>0 then
//    begin
      edtDevuelta.text:=Format('%m',[Devuelta]);
//    end
//    else
//    begin
//      edtDevuelta.text:='';
//    end;

    if (cPago.Total>=cFactura.Total) and SysUtils.DirectoryExists(cVariables.RutaArchivoWebPos) and (cVariables.ImprimirWebPos)
    then
    begin
      btnAceptar.Enabled:=true;
      pnl1.Enabled:=False;
    end
    else
    begin
      btnAceptar.Enabled:=false;
      pnl1.Enabled:=True;
    end;
  end;
end;

end.
