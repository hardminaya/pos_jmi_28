//--------------------------------------------------------
// Accelerated Ideas - Software solutions for todays world
//              www.accelerated-ideas.com
//
//       Delphi tutorials - Splash Screen (Nov 2006)
//                  'Share your ideas'
//--------------------------------------------------------

unit UntSplashScreen;

interface

uses
  
Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, jpeg, ComCtrls;

type
  TFSplashForm = class(TForm)
    Panel1: TPanel;
    Image1: TImage;
    Label1: TLabel;
    lLoading: TLabel;
  private
    FUpdateIteration: Integer;
    { Private declarations }
  public
    { Public declarations }
    constructor Create(AOwner: TComponent);
    procedure Update(const ADescr: String = 'Loading');
  end;

var
  FSplashForm: TFSplashForm;

implementation

{$R *.DFM}

constructor TFSplashForm.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
end;

procedure TFSplashForm.Update(const ADescr: String = 'Loading');
var
  i: Integer;
  dots: String;
begin
  inc(FUpdateIteration);
  dots := '';
  for i := 0 to FUpdateIteration do
  begin
    dots := dots + '.';
  end;

  if ADescr <> 'Loading' then
    lLoading.Caption := ADescr + dots
  else
    lLoading.Caption := ADescr + ' ' + Application.Title + dots;

  lLoading.Update;
end;

end.
