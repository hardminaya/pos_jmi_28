unit UntSucursal;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, dxSkinsDefaultPainters,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit,
  Data.DB, cxDBData, cxGridLevel, cxClasses, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid, cxCheckBox,
  Data.Win.ADODB, Vcl.ExtCtrls, Vcl.DBCtrls, cxVGrid, cxDBVGrid,
  cxInplaceContainer;

type
  TfrmSucursal = class(TForm)
    dsSucursales: TDataSource;
    cxstylrpstry1: TcxStyleRepository;
    cxstyl1: TcxStyle;
    dbnvgr1: TDBNavigator;
    qrySucursal: TADOQuery;
    intgrfldSucursalpkIdRegistro: TIntegerField;
    wdstrngfldSucursalDescripcion: TWideStringField;
    wdstrngfldSucursalDireccion: TWideStringField;
    wdstrngfldSucursalTelefono: TWideStringField;
    wdstrngfldSucursalFax: TWideStringField;
    wdstrngfldSucursalRepresentante: TWideStringField;
    wdstrngfldSucursalRNC: TWideStringField;
    wdstrngfldSucursalUsuario: TWideStringField;
    dtmfldSucursalFecha: TDateTimeField;
    blnfldSucursalPrincipal: TBooleanField;
    wdstrngfldSucursalTienda_Principal: TWideStringField;
    wdstrngfldSucursalCodigo_Almacen: TWideStringField;
    blnfldSucursalComprobante: TBooleanField;
    wdstrngfld1: TWideStringField;
    cxdbvrtclgrd1: TcxDBVerticalGrid;
    cxdbdtrwcxdbvrtclgrd1pkIdRegistro: TcxDBEditorRow;
    cxdbdtrwcxdbvrtclgrd1Descripcion: TcxDBEditorRow;
    cxdbdtrwcxdbvrtclgrd1Direccion: TcxDBEditorRow;
    cxdbdtrwcxdbvrtclgrd1Telefono: TcxDBEditorRow;
    cxdbdtrwcxdbvrtclgrd1Fax: TcxDBEditorRow;
    cxdbdtrwcxdbvrtclgrd1Representante: TcxDBEditorRow;
    cxdbdtrwcxdbvrtclgrd1RNC: TcxDBEditorRow;
    cxdbdtrwcxdbvrtclgrd1Usuario: TcxDBEditorRow;
    cxdbdtrwcxdbvrtclgrd1Fecha: TcxDBEditorRow;
    cxdbdtrwcxdbvrtclgrd1Principal: TcxDBEditorRow;
    cxdbdtrwcxdbvrtclgrd1Tienda_Principal: TcxDBEditorRow;
    cxdbdtrwcxdbvrtclgrd1Codigo_Almacen: TcxDBEditorRow;
    cxdbdtrwcxdbvrtclgrd1Comprobante: TcxDBEditorRow;
    cxdbdtrwcxdbvrtclgrd1PrefijoTarjetaDescuento: TcxDBEditorRow;
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmSucursal: TfrmSucursal;

implementation

uses
  UntData;

{$R *.dfm}

procedure TfrmSucursal.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action:=caFree;
  frmSucursal:=nil;
end;

procedure TfrmSucursal.FormShow(Sender: TObject);
begin
  dsSucursales.DataSet.Open;
end;

end.
