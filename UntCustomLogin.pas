unit UntCustomLogin;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Vcl.Menus, dxSkinsCore, dxSkinsDefaultPainters,
  Vcl.StdCtrls, cxButtons, users, cxControls, cxContainer, cxEdit, cxTextEdit;

type
  TfrmCustomLogin = class(TForm)
    btnProceder: TcxButton;
    btnAbortar: TcxButton;
    Label1: TLabel;
    Label2: TLabel;
    txtUsuario: TcxTextEdit;
    txtPassword: TcxTextEdit;
    procedure btnProcederClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure txtPasswordKeyPress(Sender: TObject; var Key: Char);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);

  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmCustomLogin: TfrmCustomLogin;

implementation

{$R *.dfm}

uses UntData, UntMain;

procedure TfrmCustomLogin.btnProcederClick(Sender: TObject);
var
  var_Valido : TLoginStatus;
begin
  var_Valido:=frmMain.usrs1.verifyUser(txtUsuario.Text,txtPassword.Text);
  if var_valido=TLoginStatus.lsValidUser then begin
    if fn_ValidarPrivilegio(txtUsuario.Text,txtPassword.Text,cVariables.Privilegio)=1 then
    begin
      cVariables.PrivilegioElevado:=True;
      cVariables.PrivilegioElevadoUsuario:=txtUsuario.Text;
    end
    else
    begin
      cVariables.PrivilegioElevado:=False;
      MessageDlg('Usuario '+txtUsuario.Text+' No puede Realizar: '+cVariables.Privilegio,mtError,[mbYes],0);
    end;
  end;
end;

procedure TfrmCustomLogin.FormClose(Sender: TObject; var Action: TCloseAction);
begin
//  Action:=caFree;
//  Self:=nil;
end;

procedure TfrmCustomLogin.FormShow(Sender: TObject);
begin
  txtUsuario.SetFocus;
  txtUsuario.Clear;
  txtPassword.Clear;
end;

procedure TfrmCustomLogin.txtPasswordKeyPress(Sender: TObject; var Key: Char);
begin
   if Ord(Key) = VK_RETURN then btnProceder.Click;
end;

end.
