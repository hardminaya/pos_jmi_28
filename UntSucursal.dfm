object frmSucursal: TfrmSucursal
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu]
  Caption = 'Sucursal'
  ClientHeight = 412
  ClientWidth = 815
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Visible = True
  WindowState = wsMaximized
  OnClose = FormClose
  OnShow = FormShow
  DesignSize = (
    815
    412)
  PixelsPerInch = 96
  TextHeight = 13
  object dbnvgr1: TDBNavigator
    Left = 8
    Top = 312
    Width = 792
    Height = 65
    DataSource = dsSucursales
    VisibleButtons = [nbInsert, nbEdit, nbPost, nbCancel]
    Anchors = [akBottom]
    Kind = dbnHorizontal
    TabOrder = 1
  end
  object cxdbvrtclgrd1: TcxDBVerticalGrid
    Left = 8
    Top = 8
    Width = 793
    Height = 298
    OptionsView.RowHeaderWidth = 198
    TabOrder = 0
    DataController.DataSource = dsSucursales
    Version = 1
    object cxdbdtrwcxdbvrtclgrd1pkIdRegistro: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'pkIdRegistro'
      Styles.Header = cxstyl1
      ID = 1
      ParentID = -1
      Index = 0
      Version = 1
    end
    object cxdbdtrwcxdbvrtclgrd1Descripcion: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Descripcion'
      Styles.Header = cxstyl1
      ID = 2
      ParentID = -1
      Index = 1
      Version = 1
    end
    object cxdbdtrwcxdbvrtclgrd1Direccion: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Direccion'
      Styles.Header = cxstyl1
      ID = 3
      ParentID = -1
      Index = 2
      Version = 1
    end
    object cxdbdtrwcxdbvrtclgrd1Telefono: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Telefono'
      Styles.Header = cxstyl1
      ID = 4
      ParentID = -1
      Index = 3
      Version = 1
    end
    object cxdbdtrwcxdbvrtclgrd1Fax: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Fax'
      Styles.Header = cxstyl1
      ID = 5
      ParentID = -1
      Index = 4
      Version = 1
    end
    object cxdbdtrwcxdbvrtclgrd1Representante: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Representante'
      Styles.Header = cxstyl1
      ID = 6
      ParentID = -1
      Index = 5
      Version = 1
    end
    object cxdbdtrwcxdbvrtclgrd1RNC: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'RNC'
      Styles.Header = cxstyl1
      ID = 7
      ParentID = -1
      Index = 6
      Version = 1
    end
    object cxdbdtrwcxdbvrtclgrd1Usuario: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Usuario'
      Styles.Header = cxstyl1
      ID = 8
      ParentID = -1
      Index = 7
      Version = 1
    end
    object cxdbdtrwcxdbvrtclgrd1Fecha: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Fecha'
      Styles.Header = cxstyl1
      ID = 9
      ParentID = -1
      Index = 8
      Version = 1
    end
    object cxdbdtrwcxdbvrtclgrd1Principal: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Principal'
      Styles.Header = cxstyl1
      ID = 10
      ParentID = -1
      Index = 9
      Version = 1
    end
    object cxdbdtrwcxdbvrtclgrd1Tienda_Principal: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Tienda_Principal'
      Styles.Header = cxstyl1
      ID = 11
      ParentID = -1
      Index = 10
      Version = 1
    end
    object cxdbdtrwcxdbvrtclgrd1Codigo_Almacen: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Codigo_Almacen'
      Styles.Header = cxstyl1
      ID = 12
      ParentID = -1
      Index = 11
      Version = 1
    end
    object cxdbdtrwcxdbvrtclgrd1Comprobante: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Comprobante'
      Styles.Header = cxstyl1
      ID = 13
      ParentID = -1
      Index = 12
      Version = 1
    end
    object cxdbdtrwcxdbvrtclgrd1PrefijoTarjetaDescuento: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'PrefijoTarjetaDescuento'
      Styles.Header = cxstyl1
      ID = 14
      ParentID = -1
      Index = 13
      Version = 1
    end
  end
  object dsSucursales: TDataSource
    DataSet = qrySucursal
    Left = 312
    Top = 232
  end
  object cxstylrpstry1: TcxStyleRepository
    Left = 72
    Top = 224
    PixelsPerInch = 96
    object cxstyl1: TcxStyle
      AssignedValues = [svFont]
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clSkyBlue
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
    end
  end
  object qrySucursal: TADOQuery
    Connection = DatMain.con1
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select * from sucursal')
    Left = 176
    Top = 240
    object intgrfldSucursalpkIdRegistro: TIntegerField
      FieldName = 'pkIdRegistro'
    end
    object wdstrngfldSucursalDescripcion: TWideStringField
      FieldName = 'Descripcion'
      Size = 50
    end
    object wdstrngfldSucursalDireccion: TWideStringField
      FieldName = 'Direccion'
      Size = 50
    end
    object wdstrngfldSucursalTelefono: TWideStringField
      FieldName = 'Telefono'
      Size = 15
    end
    object wdstrngfldSucursalFax: TWideStringField
      FieldName = 'Fax'
      Size = 15
    end
    object wdstrngfldSucursalRepresentante: TWideStringField
      FieldName = 'Representante'
    end
    object wdstrngfldSucursalRNC: TWideStringField
      FieldName = 'RNC'
    end
    object wdstrngfldSucursalUsuario: TWideStringField
      FieldName = 'Usuario'
    end
    object dtmfldSucursalFecha: TDateTimeField
      FieldName = 'Fecha'
    end
    object blnfldSucursalPrincipal: TBooleanField
      FieldName = 'Principal'
    end
    object wdstrngfldSucursalTienda_Principal: TWideStringField
      FieldName = 'Tienda_Principal'
      Size = 30
    end
    object wdstrngfldSucursalCodigo_Almacen: TWideStringField
      FieldName = 'Codigo_Almacen'
      Size = 8
    end
    object blnfldSucursalComprobante: TBooleanField
      FieldName = 'Comprobante'
    end
    object wdstrngfld1: TWideStringField
      FieldName = 'PrefijoTarjetaDescuento'
      Size = 50
    end
  end
end
