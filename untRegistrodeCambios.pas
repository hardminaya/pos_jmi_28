unit untRegistrodeCambios;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, dxSkinsDefaultPainters,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit,
  Data.DB, cxDBData, Data.Win.ADODB, cxGridLevel, cxClasses, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid, Vcl.Buttons,
  Vcl.Menus, Datasnap.Provider, Datasnap.DBClient, Vcl.StdCtrls, Vcl.ExtCtrls,
  Vcl.Samples.Spin, Vcl.ComCtrls, Vcl.ToolWin, Vcl.Touch.GestureMgr;

type
  TfrmRegistrodeCambios = class(TForm)
    cxgrdbtblvwGrid1DBTableView1: TcxGridDBTableView;
    cxgrdlvlGrid1Level1: TcxGridLevel;
    cxgrd1: TcxGrid;
    qryRegistrodeCambios: TADOQuery;
    dsRegistrodeCambios: TDataSource;
    dtmfldqry1Fecha: TDateTimeField;
    wdstrngfldqry1Usuario: TWideStringField;
    wdstrngfldqry1Modulo: TWideStringField;
    wdstrngfldqry1Accion: TWideStringField;
    wdstrngfldqry1Nombre: TWideStringField;
    GridFecha: TcxGridDBColumn;
    GridUsuario: TcxGridDBColumn;
    GridModulo: TcxGridDBColumn;
    GridAccion: TcxGridDBColumn;
    GridNombre: TcxGridDBColumn;
    cxstylrpstry1: TcxStyleRepository;
    cxstyl1: TcxStyle;
    dsetRegistrodeCambios: TClientDataSet;
    dtstprvdr1: TDataSetProvider;
    mm1: TMainMenu;
    cbbDias: TComboBox;
    edtintervalo: TSpinEdit;
    lbl1: TLabel;
    tmr1: TTimer;
    lbl2: TLabel;
    tlb1: TToolBar;
    btnRegistro: TToolButton;
    btnAnterior: TToolButton;
    btnSiguiente: TToolButton;
    btnGuardar: TToolButton;
    btnCancelar: TToolButton;
    btn2: TToolButton;
    btnSalir: TToolButton;
    btnRefrescar: TToolButton;
    procedure FormShow(Sender: TObject);
    procedure tmr1Timer(Sender: TObject);
    procedure edtintervaloChange(Sender: TObject);
    procedure cbbDiasChange(Sender: TObject);
    procedure btnSalirClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnRefrescarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmRegistrodeCambios: TfrmRegistrodeCambios;

implementation

{$R *.dfm}

uses UntData, UntMain, UntNotasdeCredito;


procedure TfrmRegistrodeCambios.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action:=caFree;
  frmRegistrodeCambios:=nil;
end;

procedure TfrmRegistrodeCambios.FormShow(Sender: TObject);
begin
  dsetRegistrodeCambios.Open;
end;

procedure TfrmRegistrodeCambios.btnRefrescarClick(Sender: TObject);
begin
  dsetRegistrodeCambios.Refresh;
end;

procedure TfrmRegistrodeCambios.btnSalirClick(Sender: TObject);
begin
  Self.Close;
end;

procedure TfrmRegistrodeCambios.cbbDiasChange(Sender: TObject);
begin
  with qryRegistrodeCambios do begin
    if cbbDias.Text='Todos' then begin
    qryRegistrodeCambios.SQL.Text:='select top 1000 * from vw_registrodecambios'+
    ' order by Fecha desc,pidregistro desc';
    end;

    if cbbDias.Text='Ultimos 30 D�as' then begin
    qryRegistrodeCambios.SQL.Text:='select * from vw_registrodecambios' +
    ' where fecha >= getdate()-5 order by Fecha desc,pidregistro desc';
    end;
    Close;
    Open;
  end;
end;

procedure TfrmRegistrodeCambios.edtintervaloChange(Sender: TObject);
begin
  tmr1.Interval:=(edtintervalo.Value*1000);
end;

procedure TfrmRegistrodeCambios.tmr1Timer(Sender: TObject);
begin
  dsetRegistrodeCambios.Refresh;
end;

end.
