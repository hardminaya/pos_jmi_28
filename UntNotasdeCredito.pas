unit UntNotasdeCredito;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, dxSkinsCore, dxSkinsDefaultPainters,
  dxSkinscxPCPainter, cxPCdxBarPopupMenu, cxStyles, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxEdit, Data.DB, cxDBData, cxGridLevel,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGridCustomView,
  cxGrid, cxPC, Data.Win.ADODB, Datasnap.DBClient, Datasnap.Provider, cxClasses,
  Vcl.Menus, JvMenus, Vcl.ActnList, JvDataSource, Vcl.ComCtrls, Vcl.ToolWin,
  cxContainer, cxTextEdit, cxMaskEdit, cxButtonEdit, cxDBEdit, cxLabel,
  Vcl.StdCtrls, cxSpinEdit, Vcl.ExtCtrls, Vcl.Mask, JvExMask, JvToolEdit,
  JvMaskEdit, cxBlobEdit, cxDropDownEdit, cxImageComboBox, cxHyperLinkEdit,
  Vcl.ImgList, cxImage, cxMRUEdit, JvExExtCtrls, JvRadioGroup, cxGroupBox,
  cxRadioGroup, cxCheckBox, cxButtons, users, cxCurrencyEdit, cxLookupEdit,
  cxDBLookupEdit, cxDBLookupComboBox, Vcl.DBCtrls, Vcl.DBLookup, cxCalendar,
  dxSkinBlack, dxSkinBlue, dxSkinBlueprint, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkRoom, dxSkinDarkSide, dxSkinDevExpressDarkStyle,
  dxSkinDevExpressStyle, dxSkinFoggy, dxSkinGlassOceans, dxSkinHighContrast,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black, dxSkinOffice2007Blue,
  dxSkinOffice2007Green, dxSkinOffice2007Pink, dxSkinOffice2007Silver,
  dxSkinOffice2010Black, dxSkinOffice2010Blue, dxSkinOffice2010Silver,
  dxSkinPumpkin, dxSkinSeven, dxSkinSevenClassic, dxSkinSharp, dxSkinSharpPlus,
  dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinTheAsphaltWorld, dxSkinValentine, dxSkinVS2010, dxSkinWhiteprint,
  dxSkinXmas2008Blue, UntData,WebPosData;

type
  TfrmNotasdeCredito = class(TForm)
    tlb1: TToolBar;
    btnRegistro: TToolButton;
    btnAnterior: TToolButton;
    btnSiguiente: TToolButton;
    btnGuardar: TToolButton;
    btnCancelar: TToolButton;
    btn2: TToolButton;
    btnSalir: TToolButton;
    ds1: TJvDataSource;
    actlst1: TActionList;
    acSalir: TAction;
    jvpmn1: TJvPopupMenu;
    mniAgregar1: TMenuItem;
    mniModificar1: TMenuItem;
    mniEliminar1: TMenuItem;
    cxstylrpstry1: TcxStyleRepository;
    cxstyl1: TcxStyle;
    cxstyl2: TcxStyle;
    qry1: TADOQuery;
    cxpgcntrl1: TcxPageControl;
    cxtbsht1: TcxTabSheet;
    cxgrd1: TcxGrid;
    cxgrdbtblvwGrid1DBTableView1: TcxGridDBTableView;
    cxgrdlvlGrid1Level1: TcxGridLevel;
    ColGrid1DBTableView1NumeroReq: TcxGridDBColumn;
    ColGrid1DBTableView1NumeroNC: TcxGridDBColumn;
    ColGrid1DBTableView1Monto: TcxGridDBColumn;
    ColGrid1DBTableView1Balance: TcxGridDBColumn;
    ColGrid1DBTableView1Ticket: TcxGridDBColumn;
    ColGrid1DBTableView1Fecha: TcxGridDBColumn;
    ColEstado: TcxGridDBColumn;
    cxtbsht3: TcxTabSheet;
    qryTickets: TADOQuery;
    dsTicket: TJvDataSource;
    grpInformacionCliente: TGroupBox;
    cxlbl1: TcxLabel;
    edtFecha: TcxDBTextEdit;
    edtClientNombre: TcxDBTextEdit;
    edtFacturaTotal: TcxDBTextEdit;
    cxlbl2: TcxLabel;
    cxlbl4: TcxLabel;
    edtTipoComprobante: TcxDBTextEdit;
    cxlbl3: TcxLabel;
    edtVendedor: TcxDBTextEdit;
    cxlbl5: TcxLabel;
    acSolicitarNC: TAction;
    spInsertaNC: TADOStoredProc;
    cxspndtTicket: TcxSpinEdit;
    lbl1: TLabel;
    grp1: TGroupBox;
    cxstyl3: TcxStyle;
    intgrfldTicketspIdRegistro: TIntegerField;
    wdstrngfldTicketsClienteId: TWideStringField;
    wdstrngfldTicketsClienteNombre: TWideStringField;
    wdstrngfldTicketsClienteRnc: TWideStringField;
    dtmfldTicketsDocumentoFecha: TDateTimeField;
    wdstrngfldTicketsDescripcion: TWideStringField;
    wdstrngfldTicketsDireccion: TWideStringField;
    wdstrngfldfactra: TWideStringField;
    wdstrngfldTicketsRNC: TWideStringField;
    wdstrngfldTicketsArticuloId: TWideStringField;
    wdstrngfldTicketsArticuloDescripcion: TWideStringField;
    QryArticuloTicketsArticuloPrecio: TFMTBCDField;
    bcdfldTicketsArticuloCantidad: TBCDField;
    bcdfldTicketsPrecioRealArticulo: TBCDField;
    bcdfldTicketsArticuloPorcientoImpuesto: TBCDField;
    QryArticuloTicketsArticuloMonto: TFMTBCDField;
    bcdfldTicketsArticuloPorcientoDesc: TBCDField;
    QryArticuloTicketsArticuloImporte: TFMTBCDField;
    wdstrngfldTicketsNumeroNCF: TWideStringField;
    wdstrngfldTicketsNCFDescripcion: TWideStringField;
    bcdfldTicketsMontoEfectivo: TBCDField;
    bcdfldTicketsMontoDevuelta: TBCDField;
    strngfldTicketsVendedor: TStringField;
    QryArticuloTicketsMontoGeneral: TFMTBCDField;
    intgrfldTicketsfkTipoNCF: TIntegerField;
    QryArticuloTicketsSubTotal: TFMTBCDField;
    QryArticuloTicketsTotalDesc: TFMTBCDField;
    QryArticuloTicketsTotalItbis: TFMTBCDField;
    QryArticuloTicketsEfectivo: TFMTBCDField;
    QryArticuloTicketsCheque: TFMTBCDField;
    QryArticuloTicketsTarjeta: TFMTBCDField;
    QryArticuloTicketsNcredito: TFMTBCDField;
    wdstrngfldTicketsNombrePC: TWideStringField;
    strngfldTicketsNombre: TStringField;
    wdstrngfldTicketsExpr1: TWideStringField;
    QryArticuloTicketsArticuloTotalDescuento: TFMTBCDField;
    QryArticuloTicketsArticuloPrecioImpuesto: TFMTBCDField;
    QryArticuloTicketsArticuloTotalImpuesto: TFMTBCDField;
    wdstrngfldTicketsCodigo_Almacen: TWideStringField;
    QryArticuloTicketsArticuloPrecioDesc: TFMTBCDField;
    QryArticuloTicketsArticuloImporteNeto: TFMTBCDField;
    wdstrngfldTicketsCodigo_Vendedor: TWideStringField;
    wdstrngfldTicketsSerie: TWideStringField;
    wdstrngfldTicketsUsuario: TWideStringField;
    wdstrngfldTicketsManejaSeriales: TWideStringField;
    smlntfldTicketsNCFControl: TSmallintField;
    intgrfldTicketsNo_Linea: TIntegerField;
    intgrfldTicketsSecuencia: TIntegerField;
    intgrfldTicketsRegistroDetalle: TIntegerField;
    blnfldTicketsComprobante: TBooleanField;
    cxlbl6: TcxLabel;
    spVerificaNC: TADOStoredProc;
    ColSerie: TcxGridDBColumn;
    cxstyl4: TcxStyle;
    ColArticuloDescripcion: TcxGridDBColumn;
    ColFacturaUtilizado: TcxGridDBColumn;
    QryArticuloTicketsImporteporArticulo: TFMTBCDField;
    bcdfldTicketsCantidad: TBCDField;
    GridColAccion: TcxGridDBColumn;
    acConciliarNC: TAction;
    spConciliarNC: TADOStoredProc;
    il1: TImageList;
    cxstyl5: TcxStyle;
    dset1: TClientDataSet;
    dtstprvdr1: TDataSetProvider;
    intgrflddset1NumeroReq: TIntegerField;
    intgrflddset1NumeroNC: TIntegerField;
    bcdflddset1Monto: TBCDField;
    bcdflddset1Balance: TBCDField;
    intgrflddset1Ticket: TIntegerField;
    strngflddset1Estado: TStringField;
    wdstrngflddset1Serie: TWideStringField;
    intgrfldqry1NumeroReq: TIntegerField;
    intgrfldqry1NumeroNC: TIntegerField;
    bcdfldqry1Monto: TBCDField;
    bcdfldqry1Balance: TBCDField;
    intgrfldqry1Ticket: TIntegerField;
    intgrfldqry1RegistroDetalle: TIntegerField;
    strngfldqry1Estado: TStringField;
    wdstrngfldqry1Razon: TWideStringField;
    wdstrngfldqry1ArticuloID: TWideStringField;
    wdstrngfldqry1Serie: TWideStringField;
    wdstrngfldqry1DescripcionArt: TWideStringField;
    wdstrngflddset1DescripcionArt: TWideStringField;
    wdstrngfldqry1AlmacenID: TWideStringField;
    intgrfldqry1TicketUtilizado: TIntegerField;
    intgrflddset1TicketUtilizado: TIntegerField;
    rg1: TRadioGroup;
    acEliminarNC: TAction;
    usrsrg1: TUsersReg;
    dsCllientes: TDataSource;
    tblClientes: TADOTable;
    dsAlmacenes: TDataSource;
    qryAlmacenes: TADOQuery;
    wdstrngfldqry1NCF_NC: TWideStringField;
    wdstrngflddset1NCF_NC: TWideStringField;
    cxgrdbclmnGrid1DBTableView1Column1: TcxGridDBColumn;
    dtmfldqry1Fecha: TDateTimeField;
    dtmflddset1Fecha: TDateTimeField;
    cxgrdbclmnseleccion: TcxGridDBColumn;
    edtNumeroNCF: TcxDBTextEdit;
    lbl4: TcxLabel;
    sp2: TADOStoredProc;
    cdsNotaCredito: TClientDataSet;
    dsNotaCreditoD: TDataSource;
    cxGrid1: TcxGrid;
    cxGridDBTableView1: TcxGridDBTableView;
    cxGridLevel1: TcxGridLevel;
    cxGridDBArticuloId: TcxGridDBColumn;
    cxGridDBArticuloCantidad: TcxGridDBColumn;
    DataSetProvider1: TDataSetProvider;
    cdsNotaCreditopIdRegistro: TIntegerField;
    cdsNotaCreditoClienteId: TWideStringField;
    cdsNotaCreditoClienteNombre: TWideStringField;
    cdsNotaCreditoClienteRnc: TWideStringField;
    cdsNotaCreditoDocumentoFecha: TDateTimeField;
    cdsNotaCreditoDescripcion: TWideStringField;
    cdsNotaCreditoDireccion: TWideStringField;
    cdsNotaCreditoTelefono: TWideStringField;
    cdsNotaCreditoRNC: TWideStringField;
    cdsNotaCreditoArticuloId: TWideStringField;
    cdsNotaCreditoArticuloDescripcion: TWideStringField;
    cdsNotaCreditoArticuloPrecio: TFMTBCDField;
    cdsNotaCreditoArticuloCantidad: TBCDField;
    cdsNotaCreditoPrecioRealArticulo: TBCDField;
    cdsNotaCreditoArticuloPorcientoImpuesto: TBCDField;
    cdsNotaCreditoArticuloMonto: TFMTBCDField;
    cdsNotaCreditoArticuloPorcientoDesc: TBCDField;
    cdsNotaCreditoArticuloImporte: TFMTBCDField;
    cdsNotaCreditoNumeroNCF: TWideStringField;
    cdsNotaCreditoNCFDescripcion: TWideStringField;
    cdsNotaCreditoMontoEfectivo: TBCDField;
    cdsNotaCreditoMontoDevuelta: TBCDField;
    cdsNotaCreditoVendedor: TStringField;
    cdsNotaCreditoMontoGeneral: TFMTBCDField;
    cdsNotaCreditofkTipoNCF: TIntegerField;
    cdsNotaCreditoSubTotal: TFMTBCDField;
    cdsNotaCreditoTotalDesc: TFMTBCDField;
    cdsNotaCreditoTotalItbis: TFMTBCDField;
    cdsNotaCreditoEfectivo: TFMTBCDField;
    cdsNotaCreditoCheque: TFMTBCDField;
    cdsNotaCreditoTarjeta: TFMTBCDField;
    cdsNotaCreditoNcredito: TFMTBCDField;
    cdsNotaCreditoNombrePC: TWideStringField;
    cdsNotaCreditoNombre: TStringField;
    cdsNotaCreditoExpr1: TWideStringField;
    cdsNotaCreditoArticuloTotalDescuento: TFMTBCDField;
    cdsNotaCreditoArticuloPrecioImpuesto: TFMTBCDField;
    cdsNotaCreditoArticuloTotalImpuesto: TFMTBCDField;
    cdsNotaCreditoCodigo_Almacen: TWideStringField;
    cdsNotaCreditoArticuloPrecioDesc: TFMTBCDField;
    cdsNotaCreditoArticuloImporteNeto: TFMTBCDField;
    cdsNotaCreditoCodigo_Vendedor: TWideStringField;
    cdsNotaCreditoSerie: TWideStringField;
    cdsNotaCreditoUsuario: TWideStringField;
    cdsNotaCreditoManejaSeriales: TWideStringField;
    cdsNotaCreditoNCFControl: TSmallintField;
    cdsNotaCreditoNo_Linea: TIntegerField;
    cdsNotaCreditoSecuencia: TIntegerField;
    cdsNotaCreditoRegistroDetalle: TIntegerField;
    cdsNotaCreditoComprobante: TBooleanField;
    cdsNotaCreditoImporteporArticulo: TFMTBCDField;
    cdsNotaCreditoCantidad: TBCDField;
    cxGridDBArticuloDescripcion1: TcxGridDBColumn;
    cxGridDBArticuloImporte: TcxGridDBColumn;
    btnSolicitar: TcxButton;
    btnLimpiar: TcxButton;
    spInsertaNCCabecera: TADOStoredProc;
    cxLabel1: TcxLabel;
    edtClienteRNC: TcxDBTextEdit;
    cxGridDBImportePorArticulo: TcxGridDBColumn;
    acRecalcularNC: TAction;
    acRecalcularTotal: TAction;
    cxGridDBTableView1Column1: TcxGridDBColumn;
    edtTotal: TEdit;
    cxLabel3: TcxLabel;
    procedure btnSalirClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ColAccionPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure acSolicitarNCExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure cxspndtTicketPropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
    procedure cxtbsht3Show(Sender: TObject);
    procedure acConciliarNCExecute(Sender: TObject);
    procedure cxspndtTicketPropertiesEditValueChanged(Sender: TObject);
    procedure cxgrdbtblvwGrid1DBTableView1DblClick(Sender: TObject);
    procedure rg1Click(Sender: TObject);
    procedure acEliminarNCExecute(Sender: TObject);
    procedure ConciliarNC;
    procedure EliminarNC;
    procedure btnLimpiarClick(Sender: TObject);
    procedure cdsNotaCreditoArticuloCantidadValidate(Sender: TField);
    procedure acRecalcularNCExecute(Sender: TObject);
    procedure cdsNotaCreditoArticuloCantidadChange(Sender: TField);
    procedure acRecalcularTotalExecute(Sender: TObject);
    procedure cdsNotaCreditoAfterDelete(DataSet: TDataSet);
    procedure dsNotaCreditoDDataChange(Sender: TObject; Field: TField);
    procedure cxGridDBTableView1Column1PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);

  private
    { Private declarations }
    var_NumeroReq : Integer;
    var_Estado : string;
    var_SqlText,var_NumeroNC : string;
    var_NCF_NC : string;
    FNumFactura: string;
    FIDFactura: integer;
    var_TotalNC : Currency;

  public
    { Public declarations }
    property NumFactura: string read FNumFactura write FNumFactura;
    property IDFactura: integer read FIDFactura write FIDFactura;
  end;

var
  frmNotasdeCredito: TfrmNotasdeCredito;
  var_AlmacenCodigo : Integer;
  sRef, sExp, sApproval: string;
  GenerarWebpostxt : tGenerateWebPosTXT;

  cNotaCreditoT : TNotaCreditoC;
  cNotaCreditoD : TNotaCreditoD;

  Total : Currency;

implementation

uses
  UntMain, UntArticulos, UntCustomLogin;

{$R *.dfm}

procedure TfrmNotasdeCredito.acConciliarNCExecute(Sender: TObject);
begin
  cVariables.Privilegio:='CONCILIARNC';
  if fn_ValidarPrivilegio(CUsuario.Usuario,CUsuario.Clave,cVariables.Privilegio)=1 then
  begin
      ConciliarNC;
  end
  else
  begin
   if not Assigned (frmCustomLogin) then frmCustomLogin:=TfrmCustomLogin.Create(nil);
    frmCustomLogin.ShowModal;
    if cVariables.PrivilegioElevado=TRUE then
    begin
        ConciliarNC;
    end;
  end;
end;

procedure TfrmNotasdeCredito.acEliminarNCExecute(Sender: TObject);
begin
  if dset1.FieldByName('Balance').AsFloat=dset1.FieldByName('Monto').AsFloat
  then begin
    EliminarNC;
  end
  else begin
    MessageBox(Handle,'Solicitud NO puede ser eliminada fu� utilizada en una factura (MSG:99999)','Solicitud Notas de Creditos',MB_ICONERROR or MB_OK)
  end;
end;

procedure TfrmNotasdeCredito.acRecalcularNCExecute(Sender: TObject);
begin
   with cdsNotaCredito  do
   begin
    Edit;
    cdsNotaCreditoArticuloImporte.Value:= cdsNotaCreditoArticuloCantidad.Value*cdsNotaCreditoImporteporArticulo.Value;
   end;
end;

procedure TfrmNotasdeCredito.acRecalcularTotalExecute(Sender: TObject);

begin
 FormatSettings.CurrencyDecimals:=2;
var_TotalNC:=0;
dsNotaCreditoD.DataSet.First;
while not dsNotaCreditoD.DataSet.Eof do
  begin
  //ShowMessage(dsNotaCreditoD.DataSet .FieldByName('ArticuloImporte').Value);
  var_TotalNC:= var_TotalNC + dsNotaCreditoD.DataSet .FieldByName('ArticuloImporte').Value;
  dsNotaCreditoD.DataSet.Next;
  end;
  edtTotal.Text:=Format('%m',[var_TotalNC]);
end;

procedure TfrmNotasdeCredito.acSolicitarNCExecute(Sender: TObject);
var
  RowIndex : Integer;
  var_razon : String;
  var_Cantidad : String;
  var_NumeroReq : Integer;
  var_pregunta : integer;
  Dll:Cardinal;
  documento:TDataFile;
  var_fkTipoNCF : Integer;

begin
//  with spVerificaNC do begin
//    Parameters.ParamByName('@Ticket').Value:=qryTickets.FieldByName('pidRegistro').AsInteger;
//    Parameters.ParamByName('@RegistroDetalle').Value:=qryTickets.FieldByName('RegistroDetalle').AsInteger;
//    Parameters.ParamByName('@Serie').Value:=qryTickets.FieldByName('Serie').AsString;
//
//    ExecProc;
//  end;
//
//  if spVerificaNC.Parameters.ParamByName('@RETURN_VALUE').Value>0 then
//  begin
//    MessageDlg('Articulo ya tiene solicitud de NC, Solicitud No.: '
//    +IntToStr(spVerificaNC.Parameters.ParamByName('@RETURN_VALUE').Value),mtWarning,[mbOK],0);
//    Exit;
//  end;

  var_pregunta:=MessageBox(Handle,'Crear Nota de Cr�dito?','Nota de Cr�dito',MB_ICONQUESTION or MB_YESNO);
  if var_pregunta=ID_NO then Exit;

  var_razon:=InputBox('Nota de Credito','Indique Raz�n de la Nota','');

   if var_razon='' then
   begin
      MessageBox(Handle,'Debe Especificar la raz�n','Raz�n Nota de Cr�dito',MB_ICONERROR);
      Exit;
   end;
   // Selecciona NCF Siguiente para Nota de Credito de Facturas no Tipo 6
   var_fkTipoNCF:=qryTickets.FieldByName('fkTipoNCF').AsInteger;
   if (var_fkTipoNCF<>6) then
   begin
      var_NCF_NC:=fnSeleccionaNCF_NC;
   end
   else begin
      var_NCF_NC:='0000000000000000000';
   end;

   with spInsertaNC do begin
      Parameters.ParamByName('@NumeroReq').Value:=0;
      Parameters.ParamByName('@NumeroNC').Value:=0;
      Parameters.ParamByName('@Monto').Value:=var_TotalNC;
      Parameters.ParamByName('@Balance').Value:=var_TotalNC;
      Parameters.ParamByName('@Ticket').Value:=qryTickets.FieldByName('pidRegistro').AsInteger;
      Parameters.ParamByName('@fecha').Value:='2014-01-01';
      Parameters.ParamByName('@Estado').Value:='S';
      Parameters.ParamByName('@AlmacenID').Value:=qryTickets.FieldByName('Codigo_Almacen').AsString;;
      Parameters.ParamByName('@Razon').Value:= var_razon;
      Parameters.ParamByName('@NCF_NC').Value:= var_NCF_NC;
   end;

   spInsertaNC.ExecProc;
   var_fkTipoNCF:=dsNotaCreditoD.DataSet.FieldByName('fkTipoNCF').AsInteger;

    //---------------------------
    // Generar archivo WEBPOS
    //llamar la dll de webposinterface y asignar el procedimiento a la variable.
    // hay que llenar la variable Documento
    //ShowMessage('Imprimiendo WebPos Cabecera');
    documento.FilePath:=cvariables.RutaArchivoWebPos;
    documento.IDStation:=cVariables.IDEstacionWebPos;//ID de la estacion que esta imprimiendo
    //ShowMessage(IntToStr(var_fkTipoNCF));

    case var_fkTipoNCF of
     1: documento.DocumentType:='CC';
     2,3: begin
              documento.DocumentType:='CF';
              documento.ITBISExonerated:='No';
            end;
     4: begin
              documento.DocumentType:='CF';
              documento.ITBISExonerated:='Si';
     end;
    end;
    //ShowMessage(documento.DocumentType);
    //En caso de que el comprobante se de Tipo 6 'Orden de Pedido entonce no se genera NCF

    if (var_fkTipoNCF<>6) then begin

    documento.NCFNumber:=var_NCF_NC;
    documento.NCFAfecterdNumber:=edtNumeroNCF.Text;
    documento.Client:=edtClientNombre.Text;// nombre del cliente o nombre del webpos para contado
    documento.ClientRNC:=edtClienteRNC.Text; // rnc del cliente
    //documento.ITBISExonerated:='No'; // si es exonerado de itbis
    documento.ClientAddress:='Sin Direccion';// direccion de cliente
    SetLength(documento.Detail,dsNotaCreditoD.DataSet.RecordCount);

    dsNotaCreditoD.DataSet.First;

    //ShowMessage(inttostr(dsNotaCreditoD.DataSet.RecordCount));
    for RowIndex:=0 to  dsNotaCreditoD.DataSet.RecordCount -1 do
    begin

      //RowIndex:=0;
      documento.Detail[RowIndex].Code:=  dsNotaCreditoD.DataSet.FieldByName('ArticuloID').Value;
      documento.Detail[RowIndex].Price:= dsNotaCreditoD.DataSet.FieldByName('ArticuloPrecio').AsFloat;
        if LowerCase(dsNotaCreditoD.DataSet.FieldByName('ManejaSeriales').AsString) = LowerCase('Y') then
        begin
          ShowMessage('tiene serial');
          documento.Detail[RowIndex].Quantity:= dsNotaCreditoD.DataSet.FieldByName('ArticuloCantidad').AsFloat;
          documento.Detail[RowIndex].Comment:= 'Serial: '+ dsNotaCreditoD.DataSet.FieldByName('Serie').Value;
        end else
        begin
         documento.Detail[RowIndex].Quantity:= dsNotaCreditoD.DataSet.FieldByName('ArticuloCantidad').AsFloat;
         documento.Detail[RowIndex].Comment:= '' ;
        end;

      //ShowMessage('Record Count: ' + IntToStr(dsNotaCreditoD.DataSet.RecordCount) +' RowIndex: '+ inttostr(RowIndex));
      documento.Detail[RowIndex].Discount:= dsNotaCreditoD.DataSet.FieldByName('ArticuloPorcientoDesc').Value;
      documento.Detail[RowIndex].DiscountType:= '%'; // colocar el valor para descuento por monto
      documento.Detail[RowIndex].ITBISType:= 't2';
      documento.Detail[RowIndex].Description:= dsNotaCreditoD.DataSet.FieldByName('ArticuloDescripcion').Value;

      dsNotaCreditoD.DataSet.Next;
     end;
    documento.DiscountOverSubTotal:=0; // cuanto fue descontado sobre el total

    //Agrega nombre de Caja al final del documento
    SetLength(documento.Fooder,1);
    documento.Fooder[0]:='Caja:' + cCaja.Nombre;

    Dll := LoadLibrary(PWideChar(ExtractFilePath(Application.ExeName)+'Dll\WebPosInterface.dll'));

    if Dll <> 0 then
     begin
       @GenerarWebpostxt := GetProcAddress(dll,'GenerateTXTFile');
       if Assigned(GenerarWebpostxt) then
          begin
           if not GenerarWebpostxt(documento) then
             begin

             end;
          end;
     end;
    end;

   //-----------------

   var_NumeroReq:=(spInsertaNC.Parameters.ParamByName('@RETURN_VALUE').Value);
   fn_RegistrodeCambio(CUsuario.Usuario,'Nota de Credito','Nota de Credito Solicitada No.: '+IntToStr(var_NumeroReq));
   MessageDlg('Nota de Credito Creada No.: '+IntToStr(var_NumeroReq),mtInformation,[mbOK],0);

   dset1.Close;
   dset1.Open;
end;

procedure TfrmNotasdeCredito.btnLimpiarClick(Sender: TObject);
var
  var_pregunta : Integer;

begin
var_pregunta:=MessageBox(Handle,'Limpiar todos los datos?','Limpiar Sistema',MB_ICONQUESTION or MB_YESNO);
  if var_pregunta=ID_YES then
  begin
    //LimpiarCampos;
  end;
end;

procedure TfrmNotasdeCredito.btnSalirClick(Sender: TObject);
begin
  Self.Close;
end;

procedure TfrmNotasdeCredito.cdsNotaCreditoAfterDelete(DataSet: TDataSet);
begin
  acRecalcularTotal.Execute;
end;

procedure TfrmNotasdeCredito.cdsNotaCreditoArticuloCantidadChange(
  Sender: TField);
begin
  acRecalcularTotal.Execute;
end;

procedure TfrmNotasdeCredito.cdsNotaCreditoArticuloCantidadValidate(
  Sender: TField);
begin
 acRecalcularNC.Execute;

end;

procedure TfrmNotasdeCredito.ColAccionPropertiesButtonClick(Sender: TObject;
  AButtonIndex: Integer);
begin
  acSolicitarNC.Execute;
end;

procedure TfrmNotasdeCredito.cxgrdbtblvwGrid1DBTableView1DblClick(
  Sender: TObject);
begin
  cxpgcntrl1.ActivePageIndex:=1;
  cxspndtTicket.Text:=dset1.FieldByName('Ticket').Value;
end;

procedure TfrmNotasdeCredito.cxGridDBTableView1Column1PropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  ShowMessage('Eliminar esta linea ?');
end;

procedure TfrmNotasdeCredito.cxspndtTicketPropertiesEditValueChanged(
  Sender: TObject);
begin
  With qryTickets do
  Begin
    Close;
    Parameters.ParamByName('@Ticket').Value:=cxspndtTicket.Text;
    fn_RegistrodeCambio(CUsuario.Usuario,'Nota de Credito','Consultando Factura No.:'+cxspndtTicket.Text);
    Open;
// Notas de Creditos
    cdsNotaCredito.Close;
    cdsNotaCredito.Open;
    acRecalcularTotal.Execute;

  End;
end;

procedure TfrmNotasdeCredito.cxspndtTicketPropertiesValidate(Sender: TObject;
  var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
begin
  if cxspndtTicket.Text='' then cxspndtTicket.Text:='0';
end;

procedure TfrmNotasdeCredito.cxtbsht3Show(Sender: TObject);
begin
  cxspndtTicket.SetFocus;
end;

procedure TfrmNotasdeCredito.dsNotaCreditoDDataChange(Sender: TObject;
  Field: TField);
begin
  acRecalcularTotal.Execute;
end;

procedure TfrmNotasdeCredito.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action:=caFree;
  frmNotasdeCredito:=nil;
end;

procedure TfrmNotasdeCredito.FormShow(Sender: TObject);
begin
  var_AlmacenCodigo:=0;
//  cxspndtTicket.SetFocus;
  rg1.Buttons[0].Checked:=True;
  dset1.Open;
  tblClientes.Open;

  qryAlmacenes.Open;
end;

procedure TfrmNotasdeCredito.rg1Click(Sender: TObject);
var
  var_sql : String;
begin
  with rg1 do
  begin
    if Items.Strings[ItemIndex]='Todas' then
    begin
      var_sql :='SELECT * FROM [vw_NC] ORDER BY numeroReq DESC';
    end
    else
    begin
      var_sql :='SELECT * FROM [vw_NC] WHERE NumeroNC = 0 ORDER BY numeroReq DESC';
    end;
  end;
  qry1.SQL.Text:=var_sql;
  dset1.Close;
  dset1.Open;
end;

procedure TfrmNotasdeCredito.ConciliarNC;
begin
  var_NumeroReq:=dset1.FieldByName('NumeroReq').AsInteger;
  var_Estado:=dset1.FieldByName('Estado').AsString;

  if var_Estado='C' then begin
    MessageBox(Handle,'Nota de Cr�dito ya fue conciliada (MSG:99999)','N�mero Nota Cr�dito SAP',MB_ICONERROR);
    Exit;
  end;

  var_NumeroNC:=InputBox('Conciliar Nota de Credito','Indique N�mero Nota Cr�dito SAP','');

  if var_NumeroNC='' then
  begin
   //MessageDlg('Nota de Cr�dito',mtError ,[mbOK],0);
   Exit;
  end;

  with spConciliarNC do
  begin
   Parameters.ParamByName('@NumeroReq').Value:=var_NumeroReq;
   Parameters.ParamByName('@NumeroNC').Value:=StrToInt(var_NumeroNC);
   Parameters.ParamByName('@Conciliacion_UsuarioID').Value:=cUsuario.UsuarioID;
  end;
  spConciliarNC.ExecProc;
  fn_RegistrodeCambio(CUsuario.Usuario,'Nota de Credito','Nota de Credito No.: '+
  IntToStr(var_NumeroReq)+' Conciliada con NC SAP No.: '+var_NumeroNC);

  MessageDlg('Nota de Credito Conciliada',mtInformation,[mbOK],0);

  dset1.Close;
  dset1.Open;
end;

procedure TfrmNotasdeCredito.EliminarNC;
begin
  if MessageBox(Handle,'Desear Eliminar la solicitud de Nota de Cr�dito?','Solcitud Notas de Cr�dito',MB_ICONQUESTION or MB_YESNO)=ID_YES then begin
    var_NumeroReq:=dset1.FieldByName('NumeroReq').AsInteger;

    var_sqltext:='DELETE FROM [NC] WHERE NumeroReq='+IntToStr(var_NumeroReq);
    DatMain.con1.Execute(var_SqlText);
    dset1.Delete;
    dset1.ApplyUpdates(0);

    fn_RegistrodeCambio(CUsuario.Usuario,'Nota de Credito','Nota de Credito No.: '+
    IntToStr(var_NumeroReq)+ ' ELIMINADA del sistema');
  end;
end;

end.
