object frmRegistrodeCambios: TfrmRegistrodeCambios
  Left = 0
  Top = 0
  Caption = 'Registro de Cambios'
  ClientHeight = 545
  ClientWidth = 929
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  Menu = mm1
  OldCreateOrder = False
  WindowState = wsMaximized
  OnClose = FormClose
  OnShow = FormShow
  DesignSize = (
    929
    545)
  PixelsPerInch = 96
  TextHeight = 13
  object lbl1: TLabel
    Left = 208
    Top = 71
    Width = 133
    Height = 13
    Caption = 'Refrescar cada  (segundos)'
  end
  object lbl2: TLabel
    Left = 8
    Top = 70
    Width = 37
    Height = 13
    Caption = 'Mostrar'
  end
  object tlb1: TToolBar
    Left = 0
    Top = 0
    Width = 929
    Height = 65
    ButtonHeight = 60
    ButtonWidth = 60
    Caption = 'tlb1'
    Images = frmMain.ilToolBar32
    ShowCaptions = True
    TabOrder = 0
    object btnRegistro: TToolButton
      Left = 0
      Top = 0
      Caption = 'Registro'
      ImageIndex = 9
      Style = tbsDropDown
      Visible = False
    end
    object btnAnterior: TToolButton
      Left = 75
      Top = 0
      Caption = 'Anterior'
      ImageIndex = 3
      Visible = False
    end
    object btnSiguiente: TToolButton
      Left = 135
      Top = 0
      Caption = 'Siguiente'
      ImageIndex = 0
      Visible = False
    end
    object btnGuardar: TToolButton
      Left = 195
      Top = 0
      Caption = 'Guardar'
      ImageIndex = 2
      Visible = False
    end
    object btnCancelar: TToolButton
      Left = 255
      Top = 0
      Caption = 'Cancelar'
      ImageIndex = 5
      Visible = False
    end
    object btn2: TToolButton
      Left = 315
      Top = 0
      Width = 55
      Caption = 'btn5'
      ImageIndex = 5
      Style = tbsSeparator
      Visible = False
    end
    object btnSalir: TToolButton
      Left = 370
      Top = 0
      Caption = 'Salir'
      ImageIndex = 1
      OnClick = btnSalirClick
    end
    object btnRefrescar: TToolButton
      Left = 430
      Top = 0
      Caption = 'Refrescar'
      ImageIndex = 13
      OnClick = btnRefrescarClick
    end
  end
  object cxgrd1: TcxGrid
    AlignWithMargins = True
    Left = 8
    Top = 114
    Width = 913
    Height = 410
    Anchors = [akLeft, akTop, akRight, akBottom]
    TabOrder = 3
    LookAndFeel.Kind = lfOffice11
    LookAndFeel.NativeStyle = False
    object cxgrdbtblvwGrid1DBTableView1: TcxGridDBTableView
      DataController.DataSource = dsRegistrodeCambios
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      object GridFecha: TcxGridDBColumn
        DataBinding.FieldName = 'Fecha'
        HeaderAlignmentHorz = taCenter
        Options.Editing = False
        Styles.Header = cxstyl1
        Width = 146
      end
      object GridUsuario: TcxGridDBColumn
        DataBinding.FieldName = 'Usuario'
        HeaderAlignmentHorz = taCenter
        Options.Editing = False
        Styles.Header = cxstyl1
        Width = 73
      end
      object GridNombre: TcxGridDBColumn
        DataBinding.FieldName = 'Nombre'
        HeaderAlignmentHorz = taCenter
        Options.Editing = False
        Styles.Header = cxstyl1
      end
      object GridModulo: TcxGridDBColumn
        DataBinding.FieldName = 'Modulo'
        HeaderAlignmentHorz = taCenter
        Options.Editing = False
        Styles.Header = cxstyl1
        Width = 112
      end
      object GridAccion: TcxGridDBColumn
        DataBinding.FieldName = 'Accion'
        HeaderAlignmentHorz = taCenter
        Options.Editing = False
        Styles.Header = cxstyl1
        Width = 489
      end
    end
    object cxgrdlvlGrid1Level1: TcxGridLevel
      GridView = cxgrdbtblvwGrid1DBTableView1
    end
  end
  object cbbDias: TComboBox
    Left = 8
    Top = 86
    Width = 177
    Height = 22
    Align = alCustom
    Style = csOwnerDrawFixed
    ItemIndex = 0
    TabOrder = 1
    Text = 'Ultimos 30 D'#237'as'
    OnChange = cbbDiasChange
    Items.Strings = (
      'Ultimos 30 D'#237'as'
      'Todos')
  end
  object edtintervalo: TSpinEdit
    Left = 208
    Top = 86
    Width = 133
    Height = 22
    MaxValue = 300
    MinValue = 5
    TabOrder = 2
    Value = 60
    OnChange = edtintervaloChange
  end
  object qryRegistrodeCambios: TADOQuery
    Connection = DatMain.con1
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      
        'select top 1000 * from vw_registrodecambios order by Fecha desc,' +
        'pidregistro desc')
    Left = 200
    Top = 184
    object dtmfldqry1Fecha: TDateTimeField
      FieldName = 'Fecha'
    end
    object wdstrngfldqry1Usuario: TWideStringField
      FieldName = 'Usuario'
      Size = 64
    end
    object wdstrngfldqry1Modulo: TWideStringField
      FieldName = 'Modulo'
      Size = 64
    end
    object wdstrngfldqry1Accion: TWideStringField
      FieldName = 'Accion'
      Size = 255
    end
    object wdstrngfldqry1Nombre: TWideStringField
      FieldName = 'Nombre'
    end
  end
  object dsRegistrodeCambios: TDataSource
    DataSet = dsetRegistrodeCambios
    Left = 472
    Top = 184
  end
  object cxstylrpstry1: TcxStyleRepository
    Left = 200
    Top = 248
    PixelsPerInch = 96
    object cxstyl1: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBlack
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clHighlight
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
    end
  end
  object dsetRegistrodeCambios: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'dtstprvdr1'
    Left = 368
    Top = 184
  end
  object dtstprvdr1: TDataSetProvider
    DataSet = qryRegistrodeCambios
    Left = 288
    Top = 184
  end
  object mm1: TMainMenu
    Images = frmMain.cxImageList1
    Left = 544
    Top = 184
  end
  object tmr1: TTimer
    Interval = 60000
    OnTimer = tmr1Timer
    Left = 288
    Top = 256
  end
end
