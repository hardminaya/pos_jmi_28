object frmClientes: TfrmClientes
  Left = 0
  Top = 0
  Caption = 'Clientes'
  ClientHeight = 494
  ClientWidth = 938
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Visible = True
  WindowState = wsMaximized
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object lbl2: TLabel
    Left = 514
    Top = 127
    Width = 44
    Height = 13
    Caption = 'Nombre'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object cxPageControl1: TcxPageControl
    Left = 0
    Top = 65
    Width = 938
    Height = 429
    Align = alClient
    TabOrder = 1
    Properties.ActivePage = cxTabSheet1
    ClientRectBottom = 423
    ClientRectLeft = 3
    ClientRectRight = 932
    ClientRectTop = 26
    object cxTabSheet1: TcxTabSheet
      Caption = 'Listado'
      ImageIndex = 0
      object cxGrid1: TcxGrid
        Left = 0
        Top = 0
        Width = 929
        Height = 397
        Align = alClient
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        LookAndFeel.Kind = lfOffice11
        LookAndFeel.NativeStyle = False
        object cxGrid1DBTableView1: TcxGridDBTableView
          OnCellDblClick = cxGrid1DBTableView1CellDblClick
          DataController.DataSource = dsClientes
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          object cxGrid1DBTableView1ClienteID: TcxGridDBColumn
            DataBinding.FieldName = 'ClienteID'
            HeaderAlignmentHorz = taCenter
            Options.Editing = False
            Styles.Content = cxstyl1
            Styles.Header = cxstyl1
            Width = 93
          end
          object cxGrid1DBTableView1Nombre: TcxGridDBColumn
            DataBinding.FieldName = 'Nombre'
            HeaderAlignmentHorz = taCenter
            Options.Editing = False
            Styles.Content = cxstyl1
            Styles.Header = cxstyl1
            Width = 398
          end
          object cxGrid1DBTableView1TipoCliente: TcxGridDBColumn
            Caption = 'Tipo'
            DataBinding.FieldName = 'TipoCliente'
            HeaderAlignmentHorz = taCenter
            Options.Editing = False
            Styles.Content = cxstyl1
            Styles.Header = cxstyl1
            Width = 66
          end
          object cxGrid1DBTableView1RNC: TcxGridDBColumn
            DataBinding.FieldName = 'RNC'
            HeaderAlignmentHorz = taCenter
            Options.Editing = False
            Styles.Content = cxstyl1
            Styles.Header = cxstyl1
            Width = 137
          end
          object cxGrid1DBTableView1TarjetaNo: TcxGridDBColumn
            DataBinding.FieldName = 'TarjetaNo'
            HeaderAlignmentHorz = taCenter
            Options.Editing = False
            Styles.Content = cxstyl1
            Styles.Header = cxstyl1
            Width = 84
          end
          object cxGrid1DBTableView1Descuento: TcxGridDBColumn
            DataBinding.FieldName = '%'
            HeaderAlignmentHorz = taCenter
            Options.Editing = False
            Styles.Content = cxstyl1
            Styles.Header = cxstyl1
            Width = 50
          end
        end
        object cxGrid1Level1: TcxGridLevel
          GridView = cxGrid1DBTableView1
        end
      end
    end
    object cxtbshtEstadisticasDetalle: TcxTabSheet
      Caption = 'Detalle'
      ImageIndex = 1
      object lblNombre: TLabel
        Left = 151
        Top = 19
        Width = 44
        Height = 13
        Caption = 'Nombre'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblDireccion: TLabel
        Left = 24
        Top = 62
        Width = 52
        Height = 13
        Caption = 'Direcci'#243'n'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblTRNC: TLabel
        Left = 24
        Top = 105
        Width = 22
        Height = 13
        Caption = 'RNC'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lbl1: TLabel
        Left = 24
        Top = 19
        Width = 60
        Height = 13
        Caption = 'CodigoPOS'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lbl3: TLabel
        Left = 479
        Top = 19
        Width = 49
        Height = 13
        Caption = 'Tel'#233'fono'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblEmail: TLabel
        Left = 479
        Top = 62
        Width = 146
        Height = 13
        Caption = 'Correo Electr'#243'nico (Email)'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object cxdbrdgrpTipoCliente: TcxDBRadioGroup
        Left = 24
        Top = 162
        Hint = 'Seleccionar el tipo de cliente para la lista de precio'
        Alignment = alTopCenter
        Caption = 'Tipo de Cliente'
        DataBinding.DataField = 'TipoCliente'
        DataBinding.DataSource = dsClientes
        ParentFont = False
        ParentShowHint = False
        Properties.Items = <
          item
            Caption = 'Al Detalle'
            Value = '21'
          end
          item
            Caption = 'Por Mayor'
            Value = '20'
          end>
        ShowHint = True
        Style.LookAndFeel.Kind = lfStandard
        Style.LookAndFeel.NativeStyle = False
        Style.Shadow = False
        Style.TextStyle = []
        Style.TransparentBorder = True
        StyleDisabled.LookAndFeel.Kind = lfStandard
        StyleDisabled.LookAndFeel.NativeStyle = False
        StyleFocused.LookAndFeel.Kind = lfStandard
        StyleFocused.LookAndFeel.NativeStyle = False
        StyleHot.LookAndFeel.Kind = lfStandard
        StyleHot.LookAndFeel.NativeStyle = False
        TabOrder = 8
        Height = 105
        Width = 137
      end
      object cxdbnvgtr1: TcxDBNavigator
        Left = 24
        Top = 273
        Width = 48
        Height = 42
        Buttons.First.Visible = False
        Buttons.PriorPage.Visible = False
        Buttons.Prior.Visible = False
        Buttons.Next.Visible = False
        Buttons.NextPage.Visible = False
        Buttons.Last.Visible = False
        Buttons.Insert.Visible = False
        Buttons.Delete.Visible = False
        Buttons.Edit.Visible = False
        Buttons.Post.Visible = False
        Buttons.Cancel.Visible = False
        Buttons.Refresh.Visible = False
        Buttons.SaveBookmark.Visible = False
        Buttons.GotoBookmark.Visible = False
        Buttons.Filter.Visible = False
        DataSource = dsClientes
        InfoPanel.DisplayMask = '[RecordIndex] de [RecordCount]'
        InfoPanel.Font.Charset = DEFAULT_CHARSET
        InfoPanel.Font.Color = clWindowText
        InfoPanel.Font.Height = -11
        InfoPanel.Font.Name = 'Tahoma'
        InfoPanel.Font.Style = [fsBold]
        InfoPanel.ParentFont = False
        InfoPanel.Visible = True
        LookAndFeel.Kind = lfOffice11
        LookAndFeel.NativeStyle = True
        TabOrder = 9
      end
      object edt2: TcxDBMaskEdit
        Left = 479
        Top = 36
        DataBinding.DataField = 'Telefono1'
        DataBinding.DataSource = dsClientes
        ParentFont = False
        Properties.BeepOnError = True
        Properties.EditMask = '!\(999\)000-0000;1;_'
        Properties.UseLeftAlignmentOnEditing = False
        StyleFocused.BorderColor = clRed
        StyleFocused.BorderStyle = ebsThick
        StyleFocused.Color = clGradientActiveCaption
        StyleHot.BorderColor = clRed
        TabOrder = 2
        Width = 121
      end
      object edt3: TcxDBTextEdit
        Left = 151
        Top = 35
        DataBinding.DataField = 'Nombre'
        DataBinding.DataSource = dsClientes
        ParentFont = False
        StyleFocused.BorderColor = clRed
        StyleFocused.BorderStyle = ebsThick
        StyleFocused.Color = clGradientActiveCaption
        StyleHot.BorderColor = clRed
        TabOrder = 1
        Width = 322
      end
      object edt4: TcxDBTextEdit
        Left = 24
        Top = 78
        DataBinding.DataField = 'Direccion'
        DataBinding.DataSource = dsClientes
        ParentFont = False
        StyleFocused.BorderColor = clRed
        StyleFocused.BorderStyle = ebsThick
        StyleFocused.Color = clGradientActiveCaption
        StyleHot.BorderColor = clRed
        TabOrder = 3
        Width = 449
      end
      object edt5: TcxDBTextEdit
        Left = 24
        Top = 119
        DataBinding.DataField = 'RNC'
        DataBinding.DataSource = dsClientes
        ParentFont = False
        StyleFocused.BorderColor = clRed
        StyleFocused.BorderStyle = ebsThick
        StyleFocused.Color = clGradientActiveCaption
        StyleHot.BorderColor = clRed
        TabOrder = 5
        Width = 185
      end
      object cxdbtxtdt1: TcxDBTextEdit
        Left = 479
        Top = 78
        DataBinding.DataField = 'Email1'
        DataBinding.DataSource = dsClientes
        ParentFont = False
        StyleFocused.BorderColor = clRed
        StyleFocused.BorderStyle = ebsThick
        StyleFocused.Color = clGradientActiveCaption
        StyleHot.BorderColor = clRed
        TabOrder = 4
        Width = 290
      end
      object edt6: TcxDBTextEdit
        Left = 24
        Top = 35
        DataBinding.DataField = 'pIdRegistro'
        DataBinding.DataSource = dsClientes
        ParentFont = False
        StyleFocused.BorderColor = clRed
        StyleFocused.BorderStyle = ebsThick
        StyleFocused.Color = clGradientActiveCaption
        StyleHot.BorderColor = clRed
        TabOrder = 0
        Width = 121
      end
      object edt7: TcxDBTextEdit
        Left = 228
        Top = 119
        DataBinding.DataField = 'Generico'
        DataBinding.DataSource = dsClientes
        ParentFont = False
        StyleFocused.BorderColor = clRed
        StyleFocused.BorderStyle = ebsThick
        StyleFocused.Color = clGradientActiveCaption
        StyleHot.BorderColor = clRed
        TabOrder = 6
        Width = 185
      end
      object edt8: TcxDBTextEdit
        Left = 440
        Top = 119
        DataBinding.DataField = 'TipoCliente'
        DataBinding.DataSource = dsClientes
        ParentFont = False
        StyleFocused.BorderColor = clRed
        StyleFocused.BorderStyle = ebsThick
        StyleFocused.Color = clGradientActiveCaption
        StyleHot.BorderColor = clRed
        TabOrder = 7
        Width = 185
      end
      object dbnvgr1: TDBNavigator
        Left = 440
        Top = 320
        Width = 240
        Height = 25
        DataSource = dsClientes
        Kind = dbnHorizontal
        TabOrder = 10
      end
    end
    object cxtbshtEstadisticas: TcxTabSheet
      Caption = 'Estadisticas'
      Enabled = False
      ImageIndex = 2
    end
    object cxtbsht1: TcxTabSheet
      Caption = 'SAP'
      ImageIndex = 3
      object lbl4: TLabel
        Left = 12
        Top = 19
        Width = 60
        Height = 13
        Caption = 'CodigoSAP'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object edt1: TcxDBTextEdit
        Left = 12
        Top = 33
        DataBinding.DataField = 'ClienteID'
        DataBinding.DataSource = dsClientes
        ParentFont = False
        StyleFocused.BorderColor = clRed
        StyleFocused.BorderStyle = ebsThick
        StyleFocused.Color = clGradientActiveCaption
        StyleHot.BorderColor = clRed
        TabOrder = 0
        Width = 121
      end
    end
  end
  object tlb3: TToolBar
    Left = 0
    Top = 0
    Width = 938
    Height = 65
    BorderWidth = 2
    ButtonHeight = 52
    ButtonWidth = 51
    Caption = 'tlb1'
    Flat = False
    Images = frmMain.ilToolBar32
    ShowCaptions = True
    TabOrder = 0
    Transparent = False
    object btn10: TToolButton
      Left = 0
      Top = 0
      Action = dtstpr1
      ImageIndex = 3
    end
    object btnDataSetPrior1: TToolButton
      Left = 51
      Top = 0
      Action = dtstnxt1
      ImageIndex = 0
    end
    object btn14: TToolButton
      Left = 102
      Top = 0
      Width = 55
      Caption = 'btn5'
      ImageIndex = 5
      Style = tbsSeparator
    end
    object btnDataSetEdit1: TToolButton
      Left = 157
      Top = 0
      Action = act1
      ImageIndex = 9
    end
    object btn13: TToolButton
      Left = 208
      Top = 0
      Action = act3
      ImageIndex = 10
    end
    object btn12: TToolButton
      Left = 259
      Top = 0
      Action = act5
      ImageIndex = 8
    end
    object btn15: TToolButton
      Left = 310
      Top = 0
      Action = act4
      ImageIndex = 2
    end
    object btn11: TToolButton
      Left = 361
      Top = 0
      Action = act2
      ImageIndex = 5
    end
    object btn9: TToolButton
      Left = 412
      Top = 0
      Width = 55
      Caption = 'btn9'
      ImageIndex = 5
      Style = tbsSeparator
    end
    object btnSalir: TToolButton
      Left = 467
      Top = 0
      Hint = 'Salir de esta pantalla'
      Action = wndwcls1
      Caption = 'Sa&lir'
      ImageIndex = 1
    end
  end
  object dsClientes: TDataSource
    AutoEdit = False
    DataSet = tblClientes
    Left = 472
    Top = 296
  end
  object cxStyleRepository1: TcxStyleRepository
    Left = 136
    Top = 392
    PixelsPerInch = 96
    object cxStyle1: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clActiveCaption
      Font.Charset = ANSI_CHARSET
      Font.Color = clBtnText
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
    end
    object cxStyle2: TcxStyle
      AssignedValues = [svFont]
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = []
    end
    object cxstyl1: TcxStyle
    end
  end
  object actmgr1: TActionManager
    Left = 208
    Top = 384
    StyleName = 'Platform Default'
    object dtstpr1: TDataSetPrior
      Category = 'dataset'
      Caption = '&Anterior'
      Hint = 'Prior'
      ImageIndex = 1
    end
    object dtstnxt1: TDataSetNext
      Category = 'dataset'
      Caption = '&Siguiente'
      Hint = 'Next'
      ImageIndex = 2
    end
    object act1: TDataSetEdit
      Category = 'dataset'
      Caption = '&Editar'
      Hint = 'Edit'
      ImageIndex = 6
      OnExecute = act1Execute
      DataSource = dsClientes
    end
    object act3: TDataSetInsert
      Category = 'dataset'
      Caption = '&Agregar'
      Hint = 'Insert'
      ImageIndex = 4
      OnExecute = act3Execute
      DataSource = dsClientes
    end
    object wndwcls1: TWindowClose
      Category = 'Window'
      Caption = 'C&lose'
      Enabled = False
      Hint = 'Close'
      OnExecute = wndwcls1Execute
    end
    object act2: TDataSetCancel
      Category = 'Dataset'
      Caption = '&Cancelar'
      Hint = 'Cancel'
      ImageIndex = 8
    end
    object act4: TDataSetPost
      Category = 'Dataset'
      Caption = 'A&ceptar'
      Hint = 'Post'
      ImageIndex = 7
      DataSource = dsClientes
    end
    object act5: TDataSetDelete
      Category = 'Dataset'
      Caption = '&Eliminar'
      Hint = 'Delete'
      ImageIndex = 5
      OnExecute = act5Execute
    end
  end
  object tblClientes: TADOTable
    Connection = DatMain.con1
    CursorType = ctStatic
    TableName = 'Clientes'
    Left = 416
    Top = 296
    object atncfldClientespIdRegistro: TAutoIncField
      FieldName = 'pIdRegistro'
      ReadOnly = True
    end
    object wdstrngfldClientesClienteID: TWideStringField
      FieldName = 'ClienteID'
      Size = 50
    end
    object wdstrngfldClientesNombre: TWideStringField
      FieldName = 'Nombre'
      Size = 50
    end
    object strngfldClientesTipoCliente: TStringField
      FieldName = 'TipoCliente'
      FixedChar = True
      Size = 2
    end
    object wdstrngfldClientesDireccion: TWideStringField
      FieldName = 'Direccion'
      Size = 100
    end
    object wdstrngfldClientesRNC: TWideStringField
      FieldName = 'RNC'
    end
    object strngfldClientesGenerico: TStringField
      FieldName = 'Generico'
      FixedChar = True
      Size = 1
    end
    object wdstrngfldClientesTelefono1: TWideStringField
      FieldName = 'Telefono1'
      Size = 10
    end
    object wdstrngfldClientesEmail1: TWideStringField
      FieldName = 'Email1'
      Size = 128
    end
  end
end
