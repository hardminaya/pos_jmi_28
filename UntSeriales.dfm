object frmSeriales: TfrmSeriales
  Left = 0
  Top = 0
  BorderIcons = []
  Caption = 'frmSeriales'
  ClientHeight = 499
  ClientWidth = 394
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object txtArticuloDescripcion: TLabel
    Left = 40
    Top = 8
    Width = 318
    Height = 65
    Alignment = taCenter
    AutoSize = False
    Caption = '-- Articulo --'
    Color = clRed
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentColor = False
    ParentFont = False
    WordWrap = True
  end
  object lbl1: TLabel
    Left = 40
    Top = 90
    Width = 143
    Height = 13
    Caption = 'Indique el No. de Serial / IMEI'
  end
  object btnAceptar: TcxButton
    Left = 251
    Top = 442
    Width = 107
    Height = 49
    Caption = '&Aceptar'
    Enabled = False
    LookAndFeel.Kind = lfStandard
    LookAndFeel.NativeStyle = True
    ModalResult = 1
    TabOrder = 3
    OnClick = btnAceptarClick
  end
  object cxButton1: TcxButton
    Left = 148
    Top = 442
    Width = 97
    Height = 49
    Caption = '&Abortar'
    LookAndFeel.Kind = lfStandard
    LookAndFeel.NativeStyle = True
    ModalResult = 2
    TabOrder = 2
    OnClick = cxButton1Click
  end
  object cxButton2: TcxButton
    Left = 40
    Top = 442
    Width = 97
    Height = 49
    Caption = '&Limpiar todos'
    LookAndFeel.Kind = lfStandard
    LookAndFeel.NativeStyle = True
    OptionsImage.Glyph.Data = {
      36040000424D3604000000000000360000002800000010000000100000000100
      2000000000000004000000000000000000000000000000000000000000000000
      00000000000000000002000000060000000B0000001000000012000000130000
      00100000000C0000000700000002000000000000000000000000000000000000
      00020000000C000000190000002500000030000A0D44002E3871001014500000
      003A00000032000000280000001C0000000F0000000300000000000000000000
      000B0000001B0000002B0000003B0000004A278799CE41C5DBFF35BCD5FE3695
      A7D7013642870001013F0000002E0000001F0000000F00000001000000000000
      0008000000180000002700000036053641803BC5DBFF24C3DAFF1AC2DBFF30C2
      D9FF4CCBDFFF188DA4D6001C22500000001B0000000C00000000000000000000
      000000000005000000100000001B268EA0C818C2DBFF1DC3DDFF36D2E8FF24C1
      DAFF0FB8D3FF3BCFE2FF38B6CEF60B3A445B0000000100000000000000000000
      0000000000000000000000090A0F2CC1D8FC1ACDE6FF25D1EBFF20C5E0FF1DC1
      DBFF1EC6DCFF28BED7FF5CCFE1FE1F7384A00002020300000000000000000000
      0000000000000000000000242C393BD2E8FF2FD9F1FF37DAF2FF24CDE8FF16C5
      DEFF1FC0D8FF52BFD1EE09333C4B000000000000000000000000000000000000
      00000000000000000001002D476F50E2F6FF4FE3F8FF44DCF3FF25D4ECFF1AC5
      DDFF3EB0C4E3011B212B00000000000000000000000000000000000000000000
      000000000000000023510000AAFF3058B8FF69CFEAFF4CE6FBFF33D7EDFF2B9F
      B4D40013171E0000000000000000000000000000000000000000000000000000
      0000000B0E1211788EB2216FB8FF272CC3FF3B3ECEFF387ABFFE1D72839C0009
      0C0F000000000000000000000000000000000000000000000000000000000000
      000000080A0D0E677B9C50D3E9FF6DD4EDFF3860B9FE090972C7000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000001010E4C73B60E76ADFF319EBBE942B5CBE800000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000B2E446D2182BEFF1A79B2FE00121E35001A1F2900000000000000000000
      000000000000000000000000000000000000000000000000000000000000000C
      13233283B6F6137EC1FF1F6088C4000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000421
      33575192BAFF3585B7FF05213254000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000005
      080E1747659911374E7900000101000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000}
    TabOrder = 1
    OnClick = cxButton2Click
  end
  object cxgrd1: TcxGrid
    Left = 40
    Top = 109
    Width = 318
    Height = 327
    TabOrder = 0
    object cxgrdbtblvwGrid1DBTableView1: TcxGridDBTableView
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsData.Appending = True
      OptionsView.NoDataToDisplayInfoText = '<No hay seriales seleccionados>'
      OptionsView.ShowEditButtons = gsebAlways
      OptionsView.GroupByBox = False
      object cxgrdbclmnSerial: TcxGridDBColumn
        Caption = 'Seriales / IMEI'
        HeaderAlignmentHorz = taCenter
        MinWidth = 40
        Styles.Header = frmFacturacion.cxStyle1
        Width = 311
      end
    end
    object cxgrdlvlGrid1Level1: TcxGridLevel
      GridView = cxgrdbtblvwGrid1DBTableView1
    end
  end
  object qryWork1: TADOQuery
    Connection = datmain.con1
    Parameters = <>
    Left = 360
    Top = 304
  end
  object sp_Validar_Serial: TADOStoredProc
    Connection = datmain.con1
    ProcedureName = 'sp_Validar_Serial;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@Serial'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 32
        Value = Null
      end
      item
        Name = '@Codigo_Almacen'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 8
        Value = Null
      end>
    Left = 360
    Top = 264
  end
  object ds1: TClientDataSet
    Aggregates = <>
    Params = <>
    Left = 8
    Top = 216
  end
end
