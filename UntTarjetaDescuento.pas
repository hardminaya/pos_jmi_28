unit UntTarjetaDescuento;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, dxSkinsDefaultPainters,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit,
  Data.DB, cxDBData, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  Datasnap.Provider, Datasnap.DBClient, Data.Win.ADODB, Vcl.ComCtrls,
  Vcl.ToolWin, cxGridLevel, cxClasses, cxGridCustomView, cxGrid, cxNavigator,
  cxDBNavigator, Vcl.Menus, JvMenus, cxCalendar;

type
  TfrmTarjetaDescuento = class(TForm)
    cxgrdbtblvwGrid1DBTableView1: TcxGridDBTableView;
    cxgrdlvlGrid1Level1: TcxGridLevel;
    cxgrd1: TcxGrid;
    tlb1: TToolBar;
    btnRegistro: TToolButton;
    btnAnterior: TToolButton;
    btnSiguiente: TToolButton;
    btnGuardar: TToolButton;
    btnCancelar: TToolButton;
    btn2: TToolButton;
    btnSalir: TToolButton;
    qryTarjetas: TADOQuery;
    dsetTarjetas: TClientDataSet;
    dtstprvdrTarjetas: TDataSetProvider;
    dsTarjetas: TDataSource;
    GridGrid1DBTableView1ID: TcxGridDBColumn;
    GridGrid1DBTableView1ClienteID: TcxGridDBColumn;
    GridGrid1DBTableView1Descuento: TcxGridDBColumn;
    GridGrid1DBTableView1Fecha_Expiracion: TcxGridDBColumn;
    GridGrid1DBTableView1Fecha_Utilizada: TcxGridDBColumn;
    jvpmn1: TJvPopupMenu;
    mniAgregar1: TMenuItem;
    mniModificar1: TMenuItem;
    mniEliminar1: TMenuItem;
    cxstylrpstry1: TcxStyleRepository;
    cxstyl1: TcxStyle;
    mniBuscar1: TMenuItem;
    atncfldTarjetaspIDRegistro: TAutoIncField;
    wdstrngfldTarjetasID: TWideStringField;
    wdstrngfldTarjetasClienteID: TWideStringField;
    intgrfldTarjetasDescuento: TIntegerField;
    wdstrngfldTarjetasFecha_Expiracion: TWideStringField;
    wdstrngfldTarjetasFecha_Utilizada: TWideStringField;
    atncfldTarjetaspIDRegistro1: TAutoIncField;
    wdstrngfldTarjetasID1: TWideStringField;
    wdstrngfldTarjetasClienteID1: TWideStringField;
    intgrfldTarjetasDescuento1: TIntegerField;
    wdstrngfldTarjetasFecha_Expiracion1: TWideStringField;
    wdstrngfldTarjetasFecha_Utilizada1: TWideStringField;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnSalirClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnGuardarClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure mniModificar1Click(Sender: TObject);
    procedure mniEliminar1Click(Sender: TObject);
    procedure mniAgregar1Click(Sender: TObject);
    procedure dsetTarjetasPostError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure btnAnteriorClick(Sender: TObject);
    procedure btnSiguienteClick(Sender: TObject);
    procedure Botones;
    procedure dtstprvdrTarjetasUpdateError(Sender: TObject;
      DataSet: TCustomClientDataSet; E: EUpdateError; UpdateKind: TUpdateKind;
      var Response: TResolverResponse);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmTarjetaDescuento: TfrmTarjetaDescuento;
  var_Posteado : Boolean;

implementation

uses
  UntData;

{$R *.dfm}

procedure TfrmTarjetaDescuento.btnAnteriorClick(Sender: TObject);
begin
  dsetTarjetas.Prior;
end;

procedure TfrmTarjetaDescuento.btnCancelarClick(Sender: TObject);
begin
  var_Posteado:=True;
  dsetTarjetas.CancelUpdates;
  Botones;
end;

procedure TfrmTarjetaDescuento.btnGuardarClick(Sender: TObject);
begin
  var_Posteado:=True;
  dsetTarjetas.ApplyUpdates(0);
  Botones;
end;

procedure TfrmTarjetaDescuento.btnSalirClick(Sender: TObject);
begin
  Self.Close;
end;

procedure TfrmTarjetaDescuento.btnSiguienteClick(Sender: TObject);
begin
  dsetTarjetas.Next;
end;


procedure TfrmTarjetaDescuento.dsetTarjetasPostError(DataSet: TDataSet;
  E: EDatabaseError; var Action: TDataAction);
begin
  ShowMessage(E.Message);
end;

procedure TfrmTarjetaDescuento.dtstprvdrTarjetasUpdateError(Sender: TObject;
  DataSet: TCustomClientDataSet; E: EUpdateError; UpdateKind: TUpdateKind;
  var Response: TResolverResponse);
begin
  ShowMessage(E.Message);
end;

procedure TfrmTarjetaDescuento.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
if var_Posteado=False then
  begin
    if MessageBox(Handle,'Los Datos no han sido guardados, se perderan, est� Seguro de Salir?','Tarjetas de Descuento',MB_ICONQUESTION or MB_YESNO)=ID_YES then
    begin
      dsetTarjetas.CancelUpdates;
      Action:=caFree;
      frmTarjetaDescuento:=nil;
    end
    else
    begin
      exit;
    end;
  end;
  Action:=caFree;
  frmTarjetaDescuento:=nil;
end;

procedure TfrmTarjetaDescuento.FormShow(Sender: TObject);
begin
  dsetTarjetas.Open;
end;

procedure TfrmTarjetaDescuento.mniAgregar1Click(Sender: TObject);
begin
  var_Posteado:=False;
  dsetTarjetas.Append;
  Botones;
end;

procedure TfrmTarjetaDescuento.mniEliminar1Click(Sender: TObject);
begin
  if  MessageBox(Handle,'Seguro que desea eliminar la tarjeta?','Tarjeta de Descuento',MB_ICONQUESTION or MB_YESNO) =IDYES then
  begin
    dsetTarjetas.Delete;
    dsetTarjetas.ApplyUpdates(0);
  end;
end;

procedure TfrmTarjetaDescuento.mniModificar1Click(Sender: TObject);
begin
  var_Posteado:=False;
  dsetTarjetas.Edit;
  Botones;
end;

procedure TfrmTarjetaDescuento.Botones;
begin
  if var_Posteado=False then
  begin
    btnRegistro.Visible:=False;
    btnAnterior.Visible:=False;
    btnSiguiente.Visible:=False;
    btnSalir.Visible:=False;
    btnCancelar.Visible:=True;
    btnGuardar.Visible:=True;
  end
  else
  begin
    btnRegistro.Visible:=True;
    btnAnterior.Visible:=True;
    btnSiguiente.Visible:=True;
    btnSalir.Visible:=True;
    btnCancelar.Visible:=False;
    btnGuardar.Visible:=False;
  end;
end;


end.
