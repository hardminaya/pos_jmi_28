unit UntCargarNCF;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.Mask, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit,
  dxSkinsCore, dxSkinsDefaultPainters, cxTextEdit, cxMaskEdit, JvDotNetControls,
  JvExMask, JvToolEdit, JvMaskEdit, Vcl.ComCtrls, cxDropDownEdit, cxLookupEdit,
  cxDBLookupEdit, cxDBLookupComboBox, Vcl.DBLookup, Data.DB, Data.Win.ADODB,
  cxLabel, cxDBLabel, Vcl.Grids, Vcl.DBGrids, Vcl.DBCtrls, Vcl.Samples.Spin,
  cxSpinEdit, dxSkinBlack, dxSkinBlue, dxSkinBlueprint, dxSkinCaramel,
  dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinDevExpressDarkStyle,
  dxSkinDevExpressStyle, dxSkinFoggy, dxSkinGlassOceans, dxSkinHighContrast,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black, dxSkinOffice2007Blue,
  dxSkinOffice2007Green, dxSkinOffice2007Pink, dxSkinOffice2007Silver,
  dxSkinOffice2010Black, dxSkinOffice2010Blue, dxSkinOffice2010Silver,
  dxSkinPumpkin, dxSkinSeven, dxSkinSevenClassic, dxSkinSharp, dxSkinSharpPlus,
  dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinTheAsphaltWorld, dxSkinValentine, dxSkinVS2010, dxSkinWhiteprint,
  dxSkinXmas2008Blue;

type
  TfrmCargarNCF = class(TForm)
    btnGenerar: TButton;
    lbl1: TLabel;
    lbl2: TLabel;
    lbl3: TLabel;
    pb1: TProgressBar;
    lbldesde: TLabel;
    lblhasta: TLabel;
    qryTiposComprobantes: TADOQuery;
    dsTiposComprobantes: TDataSource;
    intgrfldTiposComprobantespIdRegistro: TIntegerField;
    wdstrngfldTiposComprobantesDescripcion: TWideStringField;
    wdstrngfldTiposComprobantesEstructura: TWideStringField;
    strngfldTiposComprobantesManejaEstructura: TStringField;
    cxdblblEstructura: TcxDBLabel;
    lbl6: TLabel;
    cbb2: TDBLookupComboBox;
    qryWork1: TADOQuery;
    lblActualNCF: TLabel;
    edtDesde: TcxSpinEdit;
    edtHasta: TcxSpinEdit;
    procedure btnGenerarClick(Sender: TObject);
    procedure edtDesdeChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure CargarNCF;
    procedure edtHastaPropertiesChange(Sender: TObject);
    procedure edtDesdePropertiesChange(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);

  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmCargarNCF: TfrmCargarNCF;

implementation

uses
  UntData;

{$R *.dfm}

procedure TfrmCargarNCF.btnGenerarClick(Sender: TObject);
begin
  if MessageBox(Handle,'Genear los NCF?','Carga de NCF',MB_ICONQUESTION or MB_YESNO) =IDYES then
  begin
    CargarNCF;
  end;
end;

procedure TfrmCargarNCF.edtDesdeChange(Sender: TObject);
begin
  lbldesde.Caption:=cxdblblEstructura.Caption+(AddLeadingZeroes(StrToInt(edtDesde.Text),8));
  //dsTiposComprobantes.DataSet.FieldByName('Estrucutra').AsString +(AddLeadingZeroes(StrToInt(edtDesde.Text),8));
end;

procedure TfrmCargarNCF.edtDesdePropertiesChange(Sender: TObject);
begin
  lblDesde.Caption:=cxdblblEstructura.Caption+(AddLeadingZeroes(StrToInt(edtDesde.Text),8));
end;

procedure TfrmCargarNCF.edtHastaPropertiesChange(Sender: TObject);
begin
  lblhasta.Caption:=cxdblblEstructura.Caption+(AddLeadingZeroes(StrToInt(edtHasta.Text),8));
end;

procedure TfrmCargarNCF.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action:=caFree;
  frmCargarNCF:=nil;
end;

procedure TfrmCargarNCF.FormShow(Sender: TObject);
begin
  qryTiposComprobantes.Open;
  qryTiposComprobantes.First;
end;

procedure TfrmCargarNCF.CargarNCF;
var
  i,var_TipoNCF,var_Cantidad : Integer;
  var_actualNCF,var_primerNCF,var_UltimoNCF : Integer;
  SQLText : String;
  var_Respuesta: Integer;
begin
  var_TipoNCF:=dsTiposComprobantes.DataSet.FieldByName('PIDRegistro').AsInteger;
  var_Cantidad:=StrToInt(edtHasta.Text)-StrToInt(edtDesde.Text);
  pb1.Min:=0;
  pb1.Max:=var_Cantidad;
  var_primerNCF:=StrToInt(edtDesde.Text);
  var_actualNCF:=var_primerNCF;

  //edt3.Text:=(AddLeadingZeroes(StrToInt(edt1.Text),8));
  for i := 0 to var_Cantidad do
  begin
    with qryWork1 do begin
      lblActualNCF.Caption:=cxdblblEstructura.Caption+AddLeadingZeroes(var_actualNCF+i,8);
      SQL.Text:='INSERT INTO [ncf.secuencia] VALUES('+
      IntToStr(var_TipoNCF)+','+
      IntToStr(var_actualNCF+i)+','+
      QuotedStr(lblActualNCF.Caption)+','+
      QuotedStr('D')+')';
     Try
        //ShowMessage(qryWork1.SQL.Text);
        ExecSQL;
      Except
      on E: Exception do begin
        var_Respuesta:=MessageBox(Handle,'Se ha producido un error al cargar NCF',
        'Carga de NCF',MB_ICONQUESTION or MB_YESNO);
        ShowMessage(e.Message);

        case var_Respuesta of
//          IDYES: Exit;
          IDNO: Abort;
        end;

        end;
//        if var_Respuesta=IDABORT then begin
////          Exit;
////        end;
//
//        end;
     end;
      //ShowMessage(lblActualNCF.Caption);
      //ShowMessage('I: '+IntToStr(i));
      //var_actualNCF:=var_actualNCF+1;

    end;
    pb1.Position:=i;
    Application.ProcessMessages;
  end;
  MessageDlg('Total: ' + IntToStr(i)+ ' Cargados en el sistema',mtInformation,[mbOK],0);
end;
end.
