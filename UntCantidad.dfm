object frmCantidad: TfrmCantidad
  Left = 780
  Top = 228
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'Cantidad de Articulos'
  ClientHeight = 476
  ClientWidth = 333
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -16
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poMainFormCenter
  OnClose = FormClose
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 19
  object txtArticuloDescripcion: TLabel
    Left = 8
    Top = 8
    Width = 318
    Height = 89
    Alignment = taCenter
    AutoSize = False
    Caption = '-- Articulo --'
    Color = clRed
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWhite
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    WordWrap = True
  end
  object cxButton1: TcxButton
    Left = 8
    Top = 161
    Width = 75
    Height = 57
    Cursor = crHandPoint
    Caption = '7'
    LookAndFeel.NativeStyle = True
    TabOrder = 1
    OnClick = cxButton1Click
  end
  object cxButton2: TcxButton
    Left = 89
    Top = 161
    Width = 75
    Height = 57
    Cursor = crHandPoint
    Caption = '8'
    LookAndFeel.NativeStyle = True
    TabOrder = 2
    OnClick = cxButton2Click
  end
  object cxButton3: TcxButton
    Left = 170
    Top = 161
    Width = 75
    Height = 57
    Cursor = crHandPoint
    Caption = '9'
    LookAndFeel.NativeStyle = True
    TabOrder = 3
    OnClick = cxButton3Click
  end
  object cxButton4: TcxButton
    Left = 8
    Top = 224
    Width = 75
    Height = 57
    Cursor = crHandPoint
    Caption = '4'
    LookAndFeel.Kind = lfUltraFlat
    LookAndFeel.NativeStyle = True
    TabOrder = 5
    DragCursor = crHandPoint
    OnClick = cxButton4Click
  end
  object cxButton5: TcxButton
    Left = 89
    Top = 224
    Width = 75
    Height = 57
    Cursor = crHandPoint
    Caption = '5'
    LookAndFeel.NativeStyle = True
    TabOrder = 6
    OnClick = cxButton5Click
  end
  object cxButton6: TcxButton
    Left = 170
    Top = 224
    Width = 75
    Height = 57
    Cursor = crHandPoint
    Caption = '6'
    LookAndFeel.NativeStyle = True
    TabOrder = 7
    OnClick = cxButton6Click
  end
  object cxButton7: TcxButton
    Left = 8
    Top = 287
    Width = 75
    Height = 57
    Cursor = crHandPoint
    Caption = '1'
    LookAndFeel.Kind = lfUltraFlat
    LookAndFeel.NativeStyle = True
    TabOrder = 9
    DragCursor = crHandPoint
    OnClick = cxButton7Click
  end
  object cxButton8: TcxButton
    Left = 89
    Top = 287
    Width = 75
    Height = 57
    Cursor = crHandPoint
    Caption = '2'
    LookAndFeel.NativeStyle = True
    TabOrder = 10
    OnClick = cxButton8Click
  end
  object cxButton9: TcxButton
    Left = 170
    Top = 287
    Width = 75
    Height = 57
    Cursor = crHandPoint
    Caption = '3'
    LookAndFeel.NativeStyle = True
    TabOrder = 11
    OnClick = cxButton9Click
  end
  object cxButton10: TcxButton
    Left = 8
    Top = 350
    Width = 156
    Height = 57
    Cursor = crHandPoint
    Caption = '0'
    LookAndFeel.Kind = lfUltraFlat
    LookAndFeel.NativeStyle = True
    TabOrder = 13
    DragCursor = crHandPoint
    OnClick = cxButton10Click
  end
  object cxButton11: TcxButton
    Left = 170
    Top = 350
    Width = 75
    Height = 57
    Cursor = crHandPoint
    Caption = '.'
    LookAndFeel.Kind = lfUltraFlat
    LookAndFeel.NativeStyle = True
    TabOrder = 14
    DragCursor = crHandPoint
    OnClick = cxButton11Click
  end
  object cxButton12: TcxButton
    Left = 251
    Top = 224
    Width = 75
    Height = 57
    Cursor = crHandPoint
    Caption = 'Limpiar'
    LookAndFeel.Kind = lfUltraFlat
    LookAndFeel.NativeStyle = True
    TabOrder = 8
    DragCursor = crHandPoint
    OnClick = cxButton12Click
  end
  object cxButton14: TcxButton
    Left = 251
    Top = 161
    Width = 75
    Height = 57
    Cursor = crHandPoint
    Caption = '<--'
    Enabled = False
    LookAndFeel.Kind = lfUltraFlat
    LookAndFeel.NativeStyle = True
    TabOrder = 4
    DragCursor = crHandPoint
  end
  object cxButton13: TcxButton
    Left = 8
    Top = 413
    Width = 318
    Height = 57
    Cursor = crHandPoint
    Caption = '&Salir'
    LookAndFeel.Kind = lfUltraFlat
    LookAndFeel.NativeStyle = True
    ModalResult = 2
    OptionsImage.Glyph.Data = {
      36030000424D3603000000000000360000002800000010000000100000000100
      18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF1313F10000F00000F000
      00F00000EE0000EE0000EC1212EDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFF1313F5191FF43B4BF83948F73746F73444F73342F63141F6131AF01717
      EEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF1313F71C22F84352F92328F81111F60E
      0EF50B0BF40808F4151AF43242F6131AF01717EEFFFFFFFFFFFFFFFFFF1313F9
      1E24F94957FA4146FAC8C8FC3A3AF81212F60F0FF53232F6C4C4FC2F34F63343
      F6131AF11717EEFFFFFFFFFFFF0000FA4E5CFC3136FACACAFDF1F1FEEAEAFD3A
      3AF83838F7E9E9FDF0F0FDC4C4FC171CF53242F60000EEFFFFFFFFFFFF0000FC
      515EFC2727FB4646FBEBEBFEF1F1FEEBEBFEEBEBFDF0F0FEE9E9FD3333F60A0A
      F43444F70000EEFFFFFFFFFFFF0000FC5461FD2B2BFC2828FB4747FBECECFEF1
      F1FEF1F1FEEBEBFD3939F81111F60E0EF53747F70000F0FFFFFFFFFFFF0000FC
      5663FD2F2FFC2C2CFC4A4AFBECECFEF1F1FEF1F1FEEBEBFE3C3CF81515F71212
      F63B4AF70000F0FFFFFFFFFFFF0000FE5966FD3232FD4F4FFCECECFEF2F2FEEC
      ECFEECECFEF1F1FEEBEBFD3D3DF91616F73E4DF80000F0FFFFFFFFFFFF0000FE
      5A67FE4246FDCECEFEF2F2FEECECFE4B4BFB4949FBEBEBFEF1F1FEC9C9FD292E
      F94150F90000F2FFFFFFFFFFFF1414FE252AFE5C69FE575AFECECEFE5151FD2E
      2EFC2B2BFC4A4AFBCBCBFD474BFA4856FA1C22F81414F5FFFFFFFFFFFFFFFFFF
      1414FE252AFE5C69FE4246FE3333FD3131FD2F2FFC2C2CFC373BFB4E5CFB1E24
      F91414F7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF1414FE252AFE5B68FE5A67FE59
      66FD5764FD5562FD5360FD2126FB0D0DFAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFF1313FE0000FE0000FE0000FE0000FC0000FC0000FC1313FCFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
    TabOrder = 15
    DragCursor = crHandPoint
    OnClick = cxButton13Click
  end
  object btnAceptar: TcxButton
    Left = 250
    Top = 287
    Width = 75
    Height = 120
    Cursor = crHandPoint
    Layout = blGlyphTop
    Caption = '&Aceptar'
    ModalResult = 1
    OptionsImage.Glyph.Data = {
      36030000424D3603000000000000360000002800000010000000100000000100
      18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE0EEE08FBF913A8C3E24
      79282476283A843E8FB991E0EBE0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFB5D9B6308E34419F5186C9999AD2AA9AD1AA82C6953C964B307B33B4D0
      B6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB5DBBA248E296CBD82A7DAB486CB9765
      BB7C63B97B85CA97A4D8B365B67C237126B4D0B6FFFFFFFFFFFFFFFFFFE1F2E4
      32A04371C186A7DAB15FBB765BB97258B76F58B46E57B46E5AB673A4D8B268B7
      7E307F34E0ECE1FFFFFFFFFFFF8FD29F4BAF63A9DCB363C0785EBD7074C484D3
      EBD889CC9855B56B57B46D5BB673A5D9B3409A4D8EBB90FFFFFFFFFFFF3EB45C
      90D19E8CD39963C27378C886F1F9F3FEFEFEFCFDFC85CA9556B66C5AB87184CB
      9686C6993A8A3EFFFFFFFFFFFF26AF48A5DBAE6FC97E72C97FEFF8F0FEFEFEEA
      F6ECFEFEFEFAFCFB87CC955AB87066BD7C9FD6AE227E25FFFFFFFFFFFF2DB650
      A6DCB071CB7F65C672AFE0B6D1EDD562C06FB7E2BEFEFEFEFAFCFB8BCF9868C0
      7DA0D6AD228325FFFFFFFFFFFF4AC46B94D6A090D69A68C87563C56E60C36D60
      C26E60C16EB8E3BFFEFEFEE2F3E58AD0988ACD9C3B983FFFFFFFFFFFFF9ADEAC
      56BE6FAEE0B66CCB7967C77164C66F62C46D61C36D62C370B5E2BD6EC67DABDE
      B447A85D8EC793FFFFFFFFFFFFE4F7E948C4657ECD8FADE0B46CCB7969C97567
      C77167C77367C7746AC878ABDEB375C38832A042E1F1E3FFFFFFFFFFFFFFFFFF
      BFECCA3CC25B7ECD8FAEE0B691D79C76CD8276CD8291D79CADE0B477C78A26A0
      3AB5DFBDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC0ECCB49C76858C17395D6A2A4
      DBADA4DBAD94D5A04FB86934B254B6E3C1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFE5F7E99EE2B054CA713ABF5B36BD5948C26A97DBAAE1F5E7FFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
    OptionsImage.Layout = blGlyphTop
    TabOrder = 12
    OnClick = btnAceptarClick
  end
  object txtCantidad: TJvSpinEdit
    Left = 8
    Top = 120
    Width = 317
    Height = 27
    CheckMinValue = True
    Alignment = taRightJustify
    TabOrder = 0
  end
end
