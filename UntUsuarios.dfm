object frmUsuarios: TfrmUsuarios
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu, biMinimize, biMaximize, biHelp]
  Caption = 'Usuarios'
  ClientHeight = 678
  ClientWidth = 1025
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Visible = True
  WindowState = wsMaximized
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object tlb3: TToolBar
    Left = 0
    Top = 0
    Width = 1025
    Height = 65
    BorderWidth = 2
    ButtonHeight = 52
    ButtonWidth = 51
    Caption = 'tlb1'
    Flat = False
    Images = frmMain.ilToolBar32
    ShowCaptions = True
    TabOrder = 0
    Transparent = False
    object btn10: TToolButton
      Left = 0
      Top = 0
      Action = dtstpr1
      ImageIndex = 3
    end
    object btnDataSetPrior1: TToolButton
      Left = 51
      Top = 0
      Action = dtstnxt1
      ImageIndex = 0
    end
    object btn14: TToolButton
      Left = 102
      Top = 0
      Width = 55
      Caption = 'btn5'
      ImageIndex = 5
      Style = tbsSeparator
    end
    object btnDataSetEdit1: TToolButton
      Left = 157
      Top = 0
      Action = act1
      ImageIndex = 9
    end
    object btn13: TToolButton
      Left = 208
      Top = 0
      Action = act3
      ImageIndex = 10
    end
    object btn12: TToolButton
      Left = 259
      Top = 0
      Action = act5
      ImageIndex = 8
    end
    object btn15: TToolButton
      Left = 310
      Top = 0
      Action = act4
      ImageIndex = 2
    end
    object btn11: TToolButton
      Left = 361
      Top = 0
      Action = act2
      ImageIndex = 5
    end
    object btn9: TToolButton
      Left = 412
      Top = 0
      Width = 55
      Caption = 'btn9'
      ImageIndex = 5
      Style = tbsSeparator
    end
    object btn16: TToolButton
      Left = 467
      Top = 0
      Action = wndwcls1
      Caption = 'C&errar'
      ImageIndex = 1
    end
  end
  object cxpgcntrl1: TcxPageControl
    Left = 0
    Top = 64
    Width = 1025
    Height = 606
    Align = alCustom
    Anchors = [akLeft, akTop, akRight, akBottom]
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlue
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
    Properties.ActivePage = tbsnUsuarios
    ClientRectBottom = 600
    ClientRectLeft = 3
    ClientRectRight = 1019
    ClientRectTop = 27
    object tbsnUsuarios: TcxTabSheet
      Caption = 'Usuarios'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ImageIndex = 0
      ParentFont = False
      object cxpgcntrl3: TcxPageControl
        Left = 0
        Top = 0
        Width = 1016
        Height = 573
        Align = alClient
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        Properties.ActivePage = tbsn7
        ClientRectBottom = 567
        ClientRectLeft = 3
        ClientRectRight = 1010
        ClientRectTop = 26
        object tbsn7: TcxTabSheet
          Caption = 'Listado'
          ImageIndex = 0
          object cxgrd3: TcxGrid
            Left = 0
            Top = 0
            Width = 1007
            Height = 541
            Align = alClient
            TabOrder = 0
            object cxgrdbtblvw2: TcxGridDBTableView
              OnCellDblClick = cxgrdbtblvw2CellDblClick
              DataController.DataSource = dsUsuarios
              DataController.Summary.DefaultGroupSummaryItems = <>
              DataController.Summary.FooterSummaryItems = <>
              DataController.Summary.SummaryGroups = <>
              OptionsSelection.CellSelect = False
              OptionsView.GroupByBox = False
              object Gridcxgrdbtblvw2Sucursal: TcxGridDBColumn
                DataBinding.FieldName = 'fkSucursal'
                FooterAlignmentHorz = taCenter
                GroupSummaryAlignment = taLeftJustify
                HeaderAlignmentHorz = taCenter
                Styles.Header = cxStyle1
                Width = 78
              end
              object Gridcxgrdbtblvw2Nombre: TcxGridDBColumn
                DataBinding.FieldName = 'Nombre'
                FooterAlignmentHorz = taCenter
                HeaderAlignmentHorz = taCenter
                Styles.Header = cxStyle1
                Width = 176
              end
              object Gridcxgrdbtblvw2Usuario: TcxGridDBColumn
                DataBinding.FieldName = 'Usuario'
                FooterAlignmentHorz = taCenter
                HeaderAlignmentHorz = taCenter
                Styles.Header = cxStyle1
                Width = 97
              end
              object Gridcxgrdbtblvw2Activo: TcxGridDBColumn
                DataBinding.FieldName = 'Activo'
                FooterAlignmentHorz = taCenter
                HeaderAlignmentHorz = taCenter
                Styles.Header = cxStyle1
                Width = 63
              end
              object Gridcxgrdbtblvw2IdAlmacen: TcxGridDBColumn
                DataBinding.FieldName = 'IdAlmacen'
                FooterAlignmentHorz = taCenter
                HeaderAlignmentHorz = taCenter
                Styles.Header = cxStyle1
                Width = 93
              end
              object Gridcxgrdbtblvw2Almacen: TcxGridDBColumn
                DataBinding.FieldName = 'Almacen'
                FooterAlignmentHorz = taCenter
                HeaderAlignmentHorz = taCenter
                Styles.Header = cxStyle1
                Width = 188
              end
              object Gridcxgrdbtblvw2GrupoID: TcxGridDBColumn
                DataBinding.FieldName = 'Grupo'
                FooterAlignmentHorz = taCenter
                HeaderAlignmentHorz = taCenter
                Styles.Header = cxStyle1
                Width = 173
              end
            end
            object cxgrdlvl2: TcxGridLevel
              GridView = cxgrdbtblvw2
            end
          end
        end
        object tbsn8: TcxTabSheet
          Caption = 'Registro'
          ImageIndex = 1
          object jvpnl2: TJvPanel
            Left = 0
            Top = 0
            Width = 1007
            Height = 541
            HotTrackFont.Charset = DEFAULT_CHARSET
            HotTrackFont.Color = clWindowText
            HotTrackFont.Height = -11
            HotTrackFont.Name = 'Tahoma'
            HotTrackFont.Style = []
            Align = alClient
            Alignment = taLeftJustify
            TabOrder = 0
            object jvlbl9: TJvLabel
              Left = 16
              Top = 5
              Width = 45
              Height = 13
              Caption = 'Usuario'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
              Transparent = True
              HotTrackFont.Charset = DEFAULT_CHARSET
              HotTrackFont.Color = clWindowText
              HotTrackFont.Height = -11
              HotTrackFont.Name = 'Tahoma'
              HotTrackFont.Style = []
            end
            object jvlbl10: TJvLabel
              Left = 215
              Top = 5
              Width = 46
              Height = 13
              Caption = 'Nombre'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
              Transparent = True
              HotTrackFont.Charset = DEFAULT_CHARSET
              HotTrackFont.Color = clWindowText
              HotTrackFont.Height = -11
              HotTrackFont.Name = 'Tahoma'
              HotTrackFont.Style = []
            end
            object jvlbl11: TJvLabel
              Left = 16
              Top = 53
              Width = 119
              Height = 13
              Caption = 'Descripci'#243'n Almac'#233'n'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
              Transparent = True
              HotTrackFont.Charset = DEFAULT_CHARSET
              HotTrackFont.Color = clWindowText
              HotTrackFont.Height = -11
              HotTrackFont.Name = 'Tahoma'
              HotTrackFont.Style = []
            end
            object jvlbl12: TJvLabel
              Left = 387
              Top = 53
              Width = 112
              Height = 13
              Caption = 'Descripci'#243'n de Caja'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
              Transparent = True
              HotTrackFont.Charset = DEFAULT_CHARSET
              HotTrackFont.Color = clWindowText
              HotTrackFont.Height = -11
              HotTrackFont.Name = 'Tahoma'
              HotTrackFont.Style = []
            end
            object jvlbl13: TJvLabel
              Left = 215
              Top = 99
              Width = 50
              Height = 13
              Caption = 'Sucursal'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
              Transparent = True
              HotTrackFont.Charset = DEFAULT_CHARSET
              HotTrackFont.Color = clWindowText
              HotTrackFont.Height = -11
              HotTrackFont.Name = 'Tahoma'
              HotTrackFont.Style = []
            end
            object jvlbl14: TJvLabel
              Left = 215
              Top = 53
              Width = 51
              Height = 13
              Caption = 'Almac'#233'n'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
              Transparent = True
              HotTrackFont.Charset = DEFAULT_CHARSET
              HotTrackFont.Color = clWindowText
              HotTrackFont.Height = -11
              HotTrackFont.Name = 'Tahoma'
              HotTrackFont.Style = []
            end
            object jvlbl15: TJvLabel
              Left = 16
              Top = 99
              Width = 35
              Height = 13
              Caption = 'Fecha'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
              Transparent = True
              HotTrackFont.Charset = DEFAULT_CHARSET
              HotTrackFont.Color = clWindowText
              HotTrackFont.Height = -11
              HotTrackFont.Name = 'Tahoma'
              HotTrackFont.Style = []
            end
            object jvlbl16: TJvLabel
              Left = 16
              Top = 155
              Width = 96
              Height = 13
              Caption = 'Grupo de Acceso'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
              Transparent = True
              HotTrackFont.Charset = DEFAULT_CHARSET
              HotTrackFont.Color = clWindowText
              HotTrackFont.Height = -11
              HotTrackFont.Name = 'Tahoma'
              HotTrackFont.Style = []
            end
            object cbb1: TcxDBLookupComboBox
              Left = 212
              Top = 72
              DataBinding.DataField = 'IdAlmacen'
              DataBinding.DataSource = dsUsuarios
              Properties.KeyFieldNames = 'Codigo_Almacen'
              Properties.ListColumns = <
                item
                  FieldName = 'Descripcion'
                end>
              Properties.ListSource = DatMain.dsAlmacenes
              StyleFocused.BorderColor = clRed
              StyleFocused.BorderStyle = ebsThick
              StyleFocused.Color = clGradientActiveCaption
              StyleHot.BorderColor = clRed
              TabOrder = 3
              Width = 169
            end
            object cbb3: TcxDBLookupComboBox
              Left = 16
              Top = 174
              DataBinding.DataField = 'Grupo.ID'
              DataBinding.DataSource = dsUsuarios
              Properties.KeyFieldNames = 'pIDReigstro'
              Properties.ListColumns = <
                item
                  FieldName = 'Nombre'
                end>
              Properties.ListSource = dsUsuariosGrupos
              StyleFocused.BorderColor = clRed
              StyleFocused.BorderStyle = ebsThick
              StyleHot.BorderColor = clRed
              StyleHot.Color = clGradientActiveCaption
              TabOrder = 7
              Width = 169
            end
            object cbb4: TcxDBLookupComboBox
              Left = 215
              Top = 118
              DataBinding.DataField = 'fkSucursal'
              DataBinding.DataSource = dsUsuarios
              Properties.KeyFieldNames = 'pkIdRegistro'
              Properties.ListColumns = <
                item
                  FieldName = 'Descripcion'
                end>
              Properties.ListSource = DatMain.dsSucursal
              StyleFocused.BorderColor = clRed
              StyleFocused.BorderStyle = ebsThick
              StyleFocused.Color = clGradientActiveCaption
              StyleFocused.TextColor = clBlack
              StyleHot.BorderColor = clRed
              TabOrder = 6
              Width = 330
            end
            object edt1: TcxDBTextEdit
              Left = 215
              Top = 26
              DataBinding.DataField = 'Nombre'
              DataBinding.DataSource = dsUsuarios
              Style.BorderStyle = ebsOffice11
              StyleFocused.BorderColor = clRed
              StyleFocused.BorderStyle = ebsThick
              StyleFocused.Color = clGradientActiveCaption
              StyleHot.BorderColor = clRed
              StyleHot.BorderStyle = ebsOffice11
              TabOrder = 1
              Width = 330
            end
            object edt3: TcxDBTextEdit
              Left = 16
              Top = 72
              DataBinding.DataField = 'DescripcionAlmacen'
              DataBinding.DataSource = dsUsuarios
              Style.BorderStyle = ebsOffice11
              StyleFocused.BorderColor = clRed
              StyleFocused.BorderStyle = ebsThick
              StyleFocused.Color = clGradientActiveCaption
              StyleHot.BorderColor = clRed
              StyleHot.BorderStyle = ebsOffice11
              TabOrder = 2
              Width = 193
            end
            object edt4: TcxDBTextEdit
              Left = 16
              Top = 26
              DataBinding.DataField = 'Usuario'
              DataBinding.DataSource = dsUsuarios
              Style.BorderStyle = ebsOffice11
              StyleFocused.BorderColor = clRed
              StyleFocused.BorderStyle = ebsThick
              StyleFocused.Color = clGradientActiveCaption
              StyleHot.BorderColor = clRed
              StyleHot.BorderStyle = ebsOffice11
              TabOrder = 0
              Width = 193
            end
            object edtDescripcionCaja: TcxDBTextEdit
              Left = 387
              Top = 72
              DataBinding.DataField = 'DescripcionCaja'
              DataBinding.DataSource = dsUsuarios
              Style.BorderStyle = ebsOffice11
              StyleFocused.BorderColor = clRed
              StyleFocused.BorderStyle = ebsThick
              StyleFocused.Color = clGradientActiveCaption
              StyleHot.BorderColor = clRed
              StyleHot.BorderStyle = ebsOffice11
              TabOrder = 4
              Width = 158
            end
            object edtFecha: TcxDBDateEdit
              Left = 16
              Top = 118
              DataBinding.DataField = 'Fecha'
              DataBinding.DataSource = dsUsuarios
              StyleFocused.BorderColor = clRed
              StyleFocused.BorderStyle = ebsThick
              StyleFocused.Color = clGradientActiveCaption
              StyleHot.BorderColor = clRed
              TabOrder = 5
              Width = 193
            end
            object chkActivo: TcxDBCheckBox
              Left = 215
              Top = 174
              Caption = 'Usuario Activo'
              DataBinding.DataField = 'Activo'
              DataBinding.DataSource = dsUsuarios
              StyleFocused.BorderColor = clRed
              StyleFocused.BorderStyle = ebsThick
              StyleHot.BorderColor = clRed
              TabOrder = 8
              Width = 121
            end
          end
        end
      end
    end
    object tbsnGrupos: TcxTabSheet
      Caption = 'Grupos'
      ImageIndex = 1
      object cxpgcntrl2: TcxPageControl
        Left = 0
        Top = 0
        Width = 1016
        Height = 573
        Align = alClient
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlue
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        Properties.ActivePage = tbsn1
        ClientRectBottom = 567
        ClientRectLeft = 3
        ClientRectRight = 1010
        ClientRectTop = 26
        object tbsn1: TcxTabSheet
          Caption = 'Listado'
          ImageIndex = 0
          object lbl1: TLabel
            Left = 508
            Top = 3
            Width = 72
            Height = 13
            Alignment = taCenter
            Caption = 'PRIVILEGIOS'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object cxgrd1: TcxGrid
            Left = 16
            Top = 3
            Width = 465
            Height = 535
            Align = alCustom
            Anchors = [akLeft, akTop, akBottom]
            TabOrder = 1
            object cxgrdbtblvw1: TcxGridDBTableView
              DataController.DataSource = dsUsuariosGrupos
              DataController.Summary.DefaultGroupSummaryItems = <>
              DataController.Summary.FooterSummaryItems = <>
              DataController.Summary.SummaryGroups = <>
              OptionsView.GroupByBox = False
              object Gridcxgrdbtblvw1Nombre: TcxGridDBColumn
                DataBinding.FieldName = 'Nombre'
                HeaderAlignmentHorz = taCenter
                Styles.Header = cxStyle1
                Width = 136
              end
              object Gridcxgrdbtblvw1Descripcion: TcxGridDBColumn
                DataBinding.FieldName = 'Descripcion'
                GroupSummaryAlignment = taCenter
                HeaderAlignmentHorz = taCenter
                Styles.Header = cxStyle1
                Width = 322
              end
            end
            object cxgrdlvl1: TcxGridLevel
              GridView = cxgrdbtblvw1
            end
          end
          object pnlPrivilegios: TPanel
            Left = 496
            Top = 27
            Width = 496
            Height = 511
            Align = alCustom
            Anchors = [akLeft, akTop, akBottom]
            BorderStyle = bsSingle
            Color = clWhite
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentBackground = False
            ParentFont = False
            ParentShowHint = False
            ShowCaption = False
            ShowHint = False
            TabOrder = 2
            VerticalAlignment = taAlignTop
            object lbl2: TLabel
              Left = 6
              Top = 1
              Width = 58
              Height = 13
              Caption = 'Asignados'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object lbl3: TLabel
              Left = 267
              Top = 1
              Width = 80
              Height = 13
              Caption = 'Not Asignados'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object lstAsignados: TcxListBox
              Left = 6
              Top = 17
              Width = 227
              Height = 480
              Hint = 'Doble-Click para remover Perfil'
              ItemHeight = 14
              ParentFont = False
              ParentShowHint = False
              ShowHint = True
              Sorted = True
              Style.Edges = [bLeft, bTop, bRight, bBottom]
              Style.Font.Charset = ANSI_CHARSET
              Style.Font.Color = clWindowText
              Style.Font.Height = -11
              Style.Font.Name = 'Courier New'
              Style.Font.Style = []
              Style.IsFontAssigned = True
              StyleFocused.BorderColor = clRed
              StyleFocused.BorderStyle = cbsThick
              StyleFocused.Color = clGradientActiveCaption
              StyleHot.BorderColor = clRed
              TabOrder = 0
              OnDblClick = lstAsignadosDblClick
            end
            object lstDisponibles: TcxListBox
              Left = 256
              Top = 17
              Width = 227
              Height = 480
              Hint = 'Doble-Click para Agregar Perfil'
              ItemHeight = 14
              ParentFont = False
              ParentShowHint = False
              ShowHint = True
              Sorted = True
              Style.Font.Charset = ANSI_CHARSET
              Style.Font.Color = clWindowText
              Style.Font.Height = -11
              Style.Font.Name = 'Courier New'
              Style.Font.Style = []
              Style.IsFontAssigned = True
              StyleFocused.BorderColor = clRed
              StyleFocused.BorderStyle = cbsThick
              StyleFocused.Color = clGradientActiveCaption
              StyleHot.BorderColor = clRed
              TabOrder = 1
              OnDblClick = lstDisponiblesDblClick
            end
          end
          object edtID: TcxDBTextEdit
            Left = 496
            Top = 0
            DataBinding.DataField = 'pIDReigstro'
            DataBinding.DataSource = dsUsuariosGrupos
            Properties.OnChange = edtIDPropertiesChange
            Style.BorderStyle = ebsOffice11
            StyleFocused.BorderColor = clRed
            StyleFocused.BorderStyle = ebsThick
            StyleFocused.Color = clGradientActiveCaption
            StyleHot.BorderColor = clRed
            StyleHot.BorderStyle = ebsOffice11
            TabOrder = 0
            Visible = False
            Width = 193
          end
        end
      end
    end
    object tbsnPrivilegios: TcxTabSheet
      Caption = 'Privilegios'
      ImageIndex = 2
      object cxpgcntrl4: TcxPageControl
        Left = 0
        Top = 0
        Width = 1016
        Height = 573
        Align = alClient
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        Properties.ActivePage = tbsn3
        ClientRectBottom = 567
        ClientRectLeft = 3
        ClientRectRight = 1010
        ClientRectTop = 26
        object tbsn3: TcxTabSheet
          Caption = 'Listado'
          ImageIndex = 0
          object cxgrd2: TcxGrid
            Left = 0
            Top = 0
            Width = 1007
            Height = 541
            Align = alClient
            TabOrder = 0
            object cxgrdbtblvw3: TcxGridDBTableView
              DataController.DataSource = dsPrivilegios
              DataController.Summary.DefaultGroupSummaryItems = <>
              DataController.Summary.FooterSummaryItems = <>
              DataController.Summary.SummaryGroups = <>
              object Gridcxgrdbtblvw3Codigo: TcxGridDBColumn
                DataBinding.FieldName = 'Codigo'
                HeaderAlignmentHorz = taCenter
                Styles.Header = cxStyle1
              end
              object Gridcxgrdbtblvw3Descripcion: TcxGridDBColumn
                DataBinding.FieldName = 'Descripcion'
                HeaderAlignmentHorz = taCenter
                Styles.Header = cxStyle1
              end
            end
            object cxgrdlvl3: TcxGridLevel
              GridView = cxgrdbtblvw3
            end
          end
        end
      end
    end
  end
  object cxStyleRepository1: TcxStyleRepository
    Left = 576
    Top = 16
    PixelsPerInch = 96
    object cxStyle1: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clActiveCaption
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
    end
    object cxstyl1: TcxStyle
      AssignedValues = [svColor]
      Color = clInactiveCaption
    end
  end
  object tblUsuariosGrupos: TADOTable
    Connection = DatMain.con1
    CursorType = ctStatic
    TableName = '[Usuarios.Grupos]'
    Left = 704
    Top = 184
    object atncfldUsuariosGrupospIDReigstro: TAutoIncField
      FieldName = 'pIDReigstro'
      ReadOnly = True
    end
    object wdstrngfldUsuariosGruposNombre: TWideStringField
      FieldName = 'Nombre'
      Size = 32
    end
    object wdstrngfldUsuariosGruposDescripcion: TWideStringField
      FieldName = 'Descripcion'
      Size = 128
    end
  end
  object dsUsuariosGrupos: TJvDataSource
    AutoEdit = False
    DataSet = tblUsuariosGrupos
    Left = 808
    Top = 184
  end
  object actmgr1: TActionManager
    Left = 488
    Top = 176
    StyleName = 'Platform Default'
    object dtstpr1: TDataSetPrior
      Category = 'dataset'
      Caption = '&Anterior'
      Hint = 'Prior'
      ImageIndex = 1
    end
    object dtstnxt1: TDataSetNext
      Category = 'dataset'
      Caption = '&Siguiente'
      Hint = 'Next'
      ImageIndex = 2
    end
    object act1: TDataSetEdit
      Category = 'dataset'
      Caption = '&Editar'
      Hint = 'Edit'
      ImageIndex = 6
    end
    object act3: TDataSetInsert
      Category = 'dataset'
      Caption = '&Agregar'
      Hint = 'Insert'
      ImageIndex = 4
    end
    object wndwcls1: TWindowClose
      Category = 'Window'
      Caption = 'C&lose'
      Enabled = False
      Hint = 'Close'
    end
    object act2: TDataSetCancel
      Category = 'Dataset'
      Caption = '&Cancelar'
      Hint = 'Cancel'
      ImageIndex = 8
    end
    object act4: TDataSetPost
      Category = 'Dataset'
      Caption = 'A&ceptar'
      Hint = 'Post'
      ImageIndex = 7
    end
    object act5: TDataSetDelete
      Category = 'Dataset'
      Caption = '&Eliminar'
      Hint = 'Delete'
      ImageIndex = 5
    end
  end
  object tblPrivilegios: TADOTable
    Connection = DatMain.con1
    CursorType = ctStatic
    TableName = 'Privilegios'
    Left = 704
    Top = 336
    object atncfldPrivilegiospIDRegistro: TAutoIncField
      FieldName = 'pIDRegistro'
      ReadOnly = True
    end
    object wdstrngfldPrivilegiosCodigo: TWideStringField
      FieldName = 'Codigo'
      Size = 32
    end
    object wdstrngfldPrivilegiosDescripcion: TWideStringField
      FieldName = 'Descripcion'
      Size = 128
    end
  end
  object dsPrivilegios: TJvDataSource
    AutoEdit = False
    DataSet = tblPrivilegios
    Left = 808
    Top = 336
  end
  object tblGrupos_Privilegios: TADOTable
    Connection = DatMain.con1
    CursorType = ctStatic
    IndexFieldNames = 'Grupo.ID'
    TableName = '[Grupos.Privilegios]'
    Left = 704
    Top = 256
    object atncfldGrupos_PrivilegiospIDRegistro: TAutoIncField
      FieldName = 'pIDRegistro'
      ReadOnly = True
    end
    object intgrfldGrupos_PrivilegiosGrupoID: TIntegerField
      FieldName = 'Grupo.ID'
    end
    object intgrfldGrupos_PrivilegiosPrivilegioID: TIntegerField
      FieldName = 'Privilegio.ID'
    end
    object strngfldGrupos_PrivilegiosGrupo: TStringField
      FieldKind = fkLookup
      FieldName = 'Grupo'
      LookupDataSet = tblUsuariosGrupos
      LookupKeyFields = 'pIDReigstro'
      LookupResultField = 'Nombre'
      KeyFields = 'Grupo.ID'
      Lookup = True
    end
    object strngfldGrupos_PrivilegiosPrivilegio: TStringField
      FieldKind = fkLookup
      FieldName = 'Privilegio'
      LookupDataSet = tblPrivilegios
      LookupKeyFields = 'pIDRegistro'
      LookupResultField = 'Codigo'
      KeyFields = 'Privilegio.ID'
      Lookup = True
    end
  end
  object dsGrupos_Privilegios: TJvDataSource
    AutoEdit = False
    DataSet = tblGrupos_Privilegios
    Left = 808
    Top = 256
  end
  object tblUsuarios: TADOTable
    Connection = DatMain.con1
    CursorType = ctStatic
    TableName = 'Usuarios'
    Left = 704
    Top = 112
    object atncfldUsuariospIdRegistro: TAutoIncField
      FieldName = 'pIdRegistro'
      ReadOnly = True
    end
    object intgrfldUsuariosfkSucursal: TIntegerField
      FieldName = 'fkSucursal'
    end
    object wdstrngfldUsuariosNombre: TWideStringField
      FieldName = 'Nombre'
    end
    object wdstrngfldUsuariosApellido1: TWideStringField
      FieldName = 'Apellido1'
    end
    object wdstrngfldUsuariosApellido2: TWideStringField
      FieldName = 'Apellido2'
    end
    object wdstrngfldUsuariosUsuario: TWideStringField
      FieldName = 'Usuario'
      Size = 10
    end
    object blnfldUsuariosActivo: TBooleanField
      FieldName = 'Activo'
    end
    object wdstrngfldUsuariosClave: TWideStringField
      FieldName = 'Clave'
      Size = 10
    end
    object dtmfldUsuariosFecha: TDateTimeField
      FieldName = 'Fecha'
    end
    object blnfldUsuariosBloqueo: TBooleanField
      FieldName = 'Bloqueo'
    end
    object wdstrngfldUsuariosDescripcionCaja: TWideStringField
      FieldName = 'DescripcionCaja'
    end
    object wdstrngfldUsuariosIdAlmacen: TWideStringField
      FieldName = 'IdAlmacen'
      Size = 50
    end
    object wdstrngfldUsuariosDescripcionAlmacen: TWideStringField
      FieldName = 'DescripcionAlmacen'
      Size = 50
    end
    object strngfldUsuariosConectado: TStringField
      FieldName = 'Conectado'
      Size = 1
    end
    object wdstrngfldUsuariosNombrePC: TWideStringField
      FieldName = 'NombrePC'
      Size = 90
    end
    object intgrfldUsuariosGrupoID: TIntegerField
      FieldName = 'Grupo.ID'
    end
    object strngfldUsuariosGrupo: TStringField
      FieldKind = fkLookup
      FieldName = 'Grupo'
      LookupDataSet = tblUsuariosGrupos
      LookupKeyFields = 'pIDReigstro'
      LookupResultField = 'Nombre'
      KeyFields = 'Grupo.ID'
      Lookup = True
    end
    object strngfldUsuariosAlmacen: TStringField
      FieldKind = fkLookup
      FieldName = 'Almacen'
      LookupDataSet = DatMain.qryAlmacenes
      LookupKeyFields = 'Codigo_Almacen'
      LookupResultField = 'Descripcion'
      KeyFields = 'IdAlmacen'
      Size = 64
      Lookup = True
    end
    object strngfldUsuariosSucursal: TStringField
      FieldKind = fkLookup
      FieldName = 'Sucursal'
      LookupDataSet = DatMain.qrySucursal
      LookupKeyFields = 'pkIdRegistro'
      LookupResultField = 'Descripcion'
      KeyFields = 'fkSucursal'
      Size = 64
      Lookup = True
    end
  end
  object dsUsuarios: TJvDataSource
    AutoEdit = False
    DataSet = tblUsuarios
    Left = 808
    Top = 112
  end
  object qryWork1: TADOQuery
    Connection = DatMain.con1
    Parameters = <>
    Left = 704
    Top = 416
  end
  object spActualizaGrupoPrivilegio: TADOStoredProc
    Connection = DatMain.con1
    ProcedureName = 'sp_ActualizaGrupoPrivilegio;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@Grupo_ID'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Privilegio_ID'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 16
        Value = Null
      end
      item
        Name = '@Action'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end>
    Left = 568
    Top = 200
  end
end
