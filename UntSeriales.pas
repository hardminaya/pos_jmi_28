unit UntSeriales;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit, dxSkinsCore,
  dxSkinsDefaultPainters, Vcl.Menus, cxButtons, cxTextEdit, cxMaskEdit,
  cxDropDownEdit, Data.DB, Data.Win.ADODB, cxStyles, dxSkinscxPCPainter,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxDBData,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGridLevel,
  cxClasses, cxGridCustomView, cxGrid, Datasnap.DBClient;

type
  TfrmSeriales = class(TForm)
    btnAceptar: TcxButton;
    cxButton1: TcxButton;
    qryWork1: TADOQuery;
    cxButton2: TcxButton;
    txtArticuloDescripcion: TLabel;
    sp_Validar_Serial: TADOStoredProc;
    lbl1: TLabel;
    cxgrdbtblvwGrid1DBTableView1: TcxGridDBTableView;
    cxgrdlvlGrid1Level1: TcxGridLevel;
    cxgrd1: TcxGrid;
    cxgrdbclmnSerial: TcxGridDBColumn;
    ds1: TClientDataSet;
    procedure cxButton2Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btnAceptarClick(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);

  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmSeriales: TfrmSeriales;

implementation

{$R *.dfm}

uses UntFacturacion, UntData, UntCantidad, UntConsultaArticulos;

procedure TfrmSeriales.btnAceptarClick(Sender: TObject);
begin
  frmFacturacion.acInsertaLinea.Execute;
end;

procedure TfrmSeriales.cxButton1Click(Sender: TObject);
begin
  Self.Close;
end;

procedure TfrmSeriales.cxButton2Click(Sender: TObject);
begin
  if MessageBox(Handle,'Limpiar todos los seriales?','Facturaci�n',MB_ICONQUESTION or MB_YESNO)=ID_YES then
  begin
    // Indicar commando para limpiar todos los seriales aqu�.
  end;
end;

procedure TfrmSeriales.FormClose(Sender: TObject; var Action: TCloseAction);
begin
//  Release;
end;

procedure TfrmSeriales.FormCreate(Sender: TObject);
begin
  txtArticuloDescripcion.Caption:= cArticulo.DescripcionArt;
  cxgrdbtblvwGrid1DBTableView1.DataController.Append;
end;

end.
