unit UntReporteVentas;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Vcl.Menus, dxSkinsCore, dxSkinsDefaultPainters,
  Vcl.StdCtrls, cxButtons, Vcl.Mask, JvExMask, JvToolEdit, JvMaskEdit,
  JvCheckedMaskEdit, JvDatePickerEdit, JvDBDatePickerEdit, Vcl.ComCtrls,
  JvExComCtrls, JvDateTimePicker, ppDB, ppDBPipe, ppComm, ppRelatv, ppProd,
  ppClass, ppReport, Data.DB, Data.Win.ADODB, ppBands, ppCache, ppDesignLayer,
  ppParameter, ppCtrls, ppPrnabl, ppVar, Vcl.CheckLst;

type
  TfrmReporteVentas = class(TForm)
    jvdtmpckrDesde: TJvDateTimePicker;
    jvdtmpckrHasta: TJvDateTimePicker;
    lbl1: TLabel;
    lbl2: TLabel;
    btnImprimir: TcxButton;
    btnSalir: TcxButton;
    qry1: TADOQuery;
    ds1: TDataSource;
    pdbpln1: TppDBPipeline;
    wdstrngfldqry1ClienteNombre: TWideStringField;
    QryArticuloqry1MontoGeneral: TFMTBCDField;
    strngfldqry1Nombre: TStringField;
    strngfldqry1Apellido: TStringField;
    wdstrngfldqry1Descripcion: TWideStringField;
    prprt1: TppReport;
    intgrfldqry1FacturaNo: TIntegerField;
    dtmfldqry1DocumentoFecha: TDateTimeField;
    strngfldqry1Fecha: TStringField;
    phdrbnd1: TppHeaderBand;
    pdbtxtDescripcion: TppDBText;
    plbl1: TppLabel;
    pln2: TppLine;
    pdtlbnd1: TppDetailBand;
    pdbtxtClienteNombre: TppDBText;
    pdbtxtMontoGeneral: TppDBText;
    pdbtxtFacturaNo: TppDBText;
    pftrbnd1: TppFooterBand;
    psystmvrbl1: TppSystemVariable;
    psmrybnd1: TppSummaryBand;
    pgrp1: TppGroup;
    pgrphdrbnd1: TppGroupHeaderBand;
    plblFecha: TppLabel;
    plbl2: TppLabel;
    plbl3: TppLabel;
    pdbtxtDocumentoFecha1: TppDBText;
    plbl4: TppLabel;
    pgrpftrbnd1: TppGroupFooterBand;
    pdbclcMontoGeneral: TppDBCalc;
    pln3: TppLine;
    pdsgnlyrs1: TppDesignLayers;
    pdsgnlyr1: TppDesignLayer;
    prmtrlst1: TppParameterList;
    prmtrSQL: TppParameter;
    pln1: TppLine;
    wdstrngfldqry1NombrePC: TWideStringField;
    pdbtxtNombrePC: TppDBText;
    plbl5: TppLabel;
    chklstCajas: TCheckListBox;
    qryWork1: TADOQuery;
    pdbtxtFecha: TppDBText;
    pdbclcMontoGeneral1: TppDBCalc;
    prmtr2: TppParameter;
    lbl3: TLabel;
    procedure btnImprimirClick(Sender: TObject);
    procedure btnSalirClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmReporteVentas: TfrmReporteVentas;

implementation

{$R *.dfm}

uses UntData, UntClientes;

procedure TfrmReporteVentas.btnImprimirClick(Sender: TObject);
var
  SQLText : String;
  Indice : Integer;
  Ordenado : string;
  Cajas : String;

begin
  //Cajas:=' AND NombrePC IN (';
  Ordenado:=' ORDER BY fecha,facturaNo';
  SQLText:='SELECT * FROM VW_RPT_VENTAS WHERE Fecha BETWEEN '+
  QuotedStr(DateToStr(jvdtmpckrDesde.Date))+ ' AND ' + QuotedStr(DateToStr(jvdtmpckrHasta.Date));

//// Define criterio de Cajas
  for indice:= 0 to (chklstCajas.Items.Count -1) do
  begin
    if chklstCajas.Checked[Indice] then
    begin
      Cajas:=Cajas+QuotedStr(chklstCajas.Items.Strings[indice])+',';
    end;
  end;
  if Length(Cajas) > 0 then
  begin
    Cajas:=' AND NombrePC IN ('+Cajas+')';
    Cajas:=StringReplace(Cajas,',)',')',[rfReplaceAll,rfIgnoreCase]);
  end;

  ////  SQLText:=SQLText+')';
  SQLText:=SQLText+Cajas+Ordenado;
  qry1.SQL.Text:=SQLText;
  //ShowMessage(SQLText);
  qry1.Close;
  qry1.Open;

  //prprt1.Template.FileName:='D:\vesslaBG\RELEASE\POSJMI\Win32\Debug\Reportes\VentasDetallado.rtm';
  //prprt1.Template.LoadFromFile;
  prprt1.Print;
end;

procedure TfrmReporteVentas.btnSalirClick(Sender: TObject);
begin
  Self.Close;
end;

procedure TfrmReporteVentas.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action:=caFree;
  frmReporteVentas:=nil;
end;

procedure TfrmReporteVentas.FormShow(Sender: TObject);
begin
  jvdtmpckrDesde.Date:=Now;
  jvdtmpckrHasta.Date:=Now;

  with qryWork1 do
  begin
    Open;
    First;
    while Not Eof do
    begin
      chklstCajas.Items.Add(FieldByName('NombrePC').Value);
       Next;
    end;
  end;
end;

end.
