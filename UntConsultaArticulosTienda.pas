unit UntConsultaArticulosTienda;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Vcl.Menus, dxSkinsCore, dxSkinsDefaultPainters,
  cxControls, cxStyles, dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, Data.DB, cxDBData, cxCheckBox, cxContainer, cxTextEdit,
  cxGridLevel, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxClasses, cxGridCustomView, cxGrid, Vcl.Buttons, Vcl.StdCtrls, cxButtons,
  Data.Win.ADODB, Vcl.ActnList, Vcl.ExtCtrls, cxCurrencyEdit, Vcl.Samples.Spin,
  cxMemo;

type
  TfrmConsultaArticulosTienda = class(TForm)
    btnBuscar: TBitBtn;
    cxgrd1: TcxGrid;
    cxgrdbtblvwMainView1: TcxGridDBTableView;
    cxgrdlvlGrid1Level1: TcxGridLevel;
    edtArticulo: TcxTextEdit;
    qryArticulos: TADOQuery;
    dsArticulos: TDataSource;
    btnSalir: TcxButton;
    GridMainView1CodigoArt: TcxGridDBColumn;
    GridMainView1DescripcionArt: TcxGridDBColumn;
    GridMainView1CodigoBarras: TcxGridDBColumn;
    GridMainView1EnMano: TcxGridDBColumn;
    GridMainView1AlDetalle: TcxGridDBColumn;
    GridMainView1Codigo_Almacen: TcxGridDBColumn;
    cxstylrpstry1: TcxStyleRepository;
    cxstyl1: TcxStyle;
    actlst1: TActionList;
    acBuscarArticulo: TAction;
    rg1: TRadioGroup;
    tmr1: TTimer;
    chkTimer: TCheckBox;
    GridMainView1AlPorMayor: TcxGridDBColumn;
    cxstyl2: TcxStyle;
    procedure btnSalirClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure acBuscarArticuloExecute(Sender: TObject);
    procedure btnBuscarClick(Sender: TObject);
    procedure edtArticuloKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure tmr1Timer(Sender: TObject);
    procedure edtArticuloKeyPress(Sender: TObject; var Key: Char);
    procedure chkTimerClick(Sender: TObject);

  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmConsultaArticulosTienda: TfrmConsultaArticulosTienda;

implementation

uses
  UntData;

{$R *.dfm}

procedure TfrmConsultaArticulosTienda.acBuscarArticuloExecute(Sender: TObject);
begin
  // Busqueda por C�digo de Barras
  qryArticulos.SQL.Text:='select * from vw_ConsultaArticulo where CodigoBarras = '+
  #39+cVariables.ArticuloaCriteriodeBusqueda + #39+
  ' OR CodigoArt Like '+#39+'%'+cVariables.ArticuloaCriteriodeBusqueda+ '%'+#39+
  ' OR DescripcionArt Like '+#39+'%'+cVariables.ArticuloaCriteriodeBusqueda+'%'+#39+
  'ORDER BY CodigoArt ASC';

  QryArticulos.Close;
  QryArticulos.Open;
//  edtArticulo.Text:=qryArticulos.SQL.Text;
//  ShowMessage(QryArticulos.SQL.Text);
end;

procedure TfrmConsultaArticulosTienda.btnBuscarClick(Sender: TObject);
begin
  cVariables.ArticuloaCriteriodeBusqueda:=edtArticulo.Text;
  acBuscarArticulo.Execute;
end;

procedure TfrmConsultaArticulosTienda.btnSalirClick(Sender: TObject);
begin
  self.Close();
end;

procedure TfrmConsultaArticulosTienda.chkTimerClick(Sender: TObject);
begin
  if chkTimer.Checked then begin
    tmr1.Enabled:=true;
  end
  else
  begin
    tmr1.Enabled:=False;
  end;
end;

procedure TfrmConsultaArticulosTienda.edtArticuloKeyPress(Sender: TObject;
  var Key: Char);
begin
  tmr1.Enabled:=False;
  tmr1.Interval:=60000;
  if chkTimer.Checked=true then tmr1.Enabled:=True;
end;

procedure TfrmConsultaArticulosTienda.edtArticuloKeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  if Key=VK_RETURN then begin
    btnBuscar.Click;
  end;

end;

procedure TfrmConsultaArticulosTienda.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action:=caFree;
  frmConsultaArticulosTienda:=nil;
end;

procedure TfrmConsultaArticulosTienda.FormShow(Sender: TObject);
begin
  //qryArticulos.Open;
end;

procedure TfrmConsultaArticulosTienda.tmr1Timer(Sender: TObject);
begin
  btnSalir.Click;
end;

end.
