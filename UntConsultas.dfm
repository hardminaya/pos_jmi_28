object frmConsultas: TfrmConsultas
  Left = 0
  Top = 0
  Caption = 'Consultas'
  ClientHeight = 584
  ClientWidth = 881
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Visible = True
  WindowState = wsMaximized
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object cxpgcntrl1: TcxPageControl
    Left = 26
    Top = 23
    Width = 797
    Height = 344
    TabOrder = 0
    Properties.ActivePage = cxtbsht1
    ClientRectBottom = 340
    ClientRectLeft = 4
    ClientRectRight = 793
    ClientRectTop = 24
    object cxtbsht1: TcxTabSheet
      Caption = 'Ventas'
      ImageIndex = 0
      object cxdbpvtgrd1: TcxDBPivotGrid
        Left = 0
        Top = 0
        Width = 789
        Height = 316
        Align = alClient
        DataSource = dsFlujoCajas
        Groups = <>
        TabOrder = 0
        object cxdbpvtgrdfldcxdbpvtgrd1ClienteNombre: TcxDBPivotGridField
          AreaIndex = 0
          DataBinding.FieldName = 'ClienteNombre'
          Visible = True
          UniqueName = 'ClienteNombre'
        end
        object cxdbpvtgrdfldcxdbpvtgrd1Fecha: TcxDBPivotGridField
          AreaIndex = 1
          DataBinding.FieldName = 'Fecha'
          Visible = True
          UniqueName = 'Fecha'
        end
        object cxdbpvtgrdfldcxdbpvtgrd1MontoGeneral: TcxDBPivotGridField
          AreaIndex = 2
          DataBinding.FieldName = 'MontoGeneral'
          Visible = True
          UniqueName = 'MontoGeneral'
        end
        object cxdbpvtgrdfldcxdbpvtgrd1Descripcion: TcxDBPivotGridField
          AreaIndex = 3
          DataBinding.FieldName = 'Descripcion'
          Visible = True
          UniqueName = 'Descripcion'
        end
        object cxdbpvtgrdfldcxdbpvtgrd1FacturaNo: TcxDBPivotGridField
          AreaIndex = 4
          DataBinding.FieldName = 'FacturaNo'
          Visible = True
          UniqueName = 'FacturaNo'
        end
        object cxdbpvtgrdfldcxdbpvtgrd1DocumentoFecha: TcxDBPivotGridField
          AreaIndex = 5
          DataBinding.FieldName = 'DocumentoFecha'
          Visible = True
          UniqueName = 'DocumentoFecha'
        end
        object cxdbpvtgrdfldcxdbpvtgrd1NombrePC: TcxDBPivotGridField
          AreaIndex = 6
          DataBinding.FieldName = 'NombrePC'
          Visible = True
          UniqueName = 'NombrePC'
        end
      end
    end
    object cxtbsht2: TcxTabSheet
      Caption = 'Inventario'
      ImageIndex = 1
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
    end
    object cxtbsht4: TcxTabSheet
      Caption = 'Flujo de Cajas'
      ImageIndex = 3
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object cxgrd2: TcxGrid
        Left = 0
        Top = 0
        Width = 788
        Height = 312
        Align = alClient
        TabOrder = 0
        object cxgrdbtblvwGrid1DBTableView2: TcxGridDBTableView
          DataController.DataSource = dsFlujoCajas
          DataController.Summary.DefaultGroupSummaryItems = <
            item
              Format = 'RD$: #,###,###.00'
              Kind = skSum
              FieldName = 'MontoGeneral'
              Column = ColGrid1DBTableView2Fecha
              DisplayText = 'Suma:'
            end
            item
              Format = 'Documentos :  #,###,###'
              Kind = skCount
              FieldName = 'MontoGeneral'
              Column = ColGrid1DBTableView2MontoGeneral
              DisplayText = 'Cuenta:'
            end>
          DataController.Summary.FooterSummaryItems = <
            item
              Format = '#,###,###.00'
              Kind = skSum
              FieldName = 'MontoGeneral'
              Column = ColGrid1DBTableView2Fecha
              DisplayText = 'Total Ventas:'
            end>
          DataController.Summary.SummaryGroups = <
            item
              Links = <
                item
                  Column = ColGrid1DBTableView2MontoGeneral
                end
                item
                end>
              SummaryItems = <
                item
                end>
            end>
          Styles.GroupSummary = cxstyl1
          object ColGrid1DBTableView2FacturaNo: TcxGridDBColumn
            Caption = 'Fact. No.'
            DataBinding.FieldName = 'FacturaNo'
            HeaderAlignmentHorz = taCenter
            Options.Editing = False
            Styles.GroupSummary = cxstyl2
            Styles.Header = cxstyl1
            Width = 73
          end
          object ColGrid1DBTableView2Fecha: TcxGridDBColumn
            Caption = 'Fecha Fact.'
            DataBinding.FieldName = 'Fecha'
            Visible = False
            GroupIndex = 0
            HeaderAlignmentHorz = taCenter
            Options.Editing = False
            SortIndex = 0
            SortOrder = soDescending
            Styles.GroupSummary = cxstyl2
            Styles.Header = cxstyl1
            Width = 113
          end
          object ColGrid1DBTableView2ClienteNombre: TcxGridDBColumn
            Caption = 'Cliente'
            DataBinding.FieldName = 'ClienteNombre'
            HeaderAlignmentHorz = taCenter
            Options.Editing = False
            Styles.Header = cxstyl1
            Width = 252
          end
          object ColGrid1DBTableView2NombrePC: TcxGridDBColumn
            Caption = 'Caja / Sucursal'
            DataBinding.FieldName = 'NombrePC'
            HeaderAlignmentHorz = taCenter
            Options.Editing = False
            Styles.Header = cxstyl1
            Width = 162
          end
          object ColGrid1DBTableView2MontoGeneral: TcxGridDBColumn
            Caption = 'Importe'
            DataBinding.FieldName = 'MontoGeneral'
            HeaderAlignmentHorz = taCenter
            Options.Editing = False
            Options.Grouping = False
            Styles.GroupSummary = cxstyl2
            Styles.Header = cxstyl1
            Width = 112
          end
        end
        object cxgrdlvlGrid1Level2: TcxGridLevel
          GridView = cxgrdbtblvwGrid1DBTableView2
        end
      end
    end
    object cxtbsht3: TcxTabSheet
      Caption = 'Articulos'
      ImageIndex = 3
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object cxdbpvtgrd2: TcxDBPivotGrid
        Left = 0
        Top = 0
        Width = 788
        Height = 312
        Align = alClient
        DataSource = ds2
        Groups = <>
        TabOrder = 0
        object cxdbpvtgrdfldcxdbpvtgrd2Field1: TcxDBPivotGridField
          AreaIndex = 0
          DataBinding.FieldName = 'ArticuloCantidad'
          Visible = True
          UniqueName = 'ArticuloCantidad'
        end
        object cxdbpvtgrdfldcxdbpvtgrd2Field2: TcxDBPivotGridField
          AreaIndex = 1
          DataBinding.FieldName = 'ArticuloDescripcion'
          Visible = True
          UniqueName = 'ArticuloDescripcion'
        end
        object cxdbpvtgrdfldcxdbpvtgrd2Field3: TcxDBPivotGridField
          AreaIndex = 2
          DataBinding.FieldName = 'ArticuloId'
          Visible = True
          UniqueName = 'ArticuloId'
        end
        object cxdbpvtgrdfldcxdbpvtgrd2Field4: TcxDBPivotGridField
          AreaIndex = 3
          DataBinding.FieldName = 'DocumentoFecha'
          Visible = True
          UniqueName = 'DocumentoFecha'
        end
        object cxdbpvtgrdfldcxdbpvtgrd2Field5: TcxDBPivotGridField
          AreaIndex = 4
          DataBinding.FieldName = 'fkCaja'
          Visible = True
          UniqueName = 'fkCaja'
        end
        object cxdbpvtgrdfldcxdbpvtgrd2Field6: TcxDBPivotGridField
          AreaIndex = 5
          DataBinding.FieldName = 'fkDocCabecera'
          Visible = True
          UniqueName = 'fkDocCabecera'
        end
      end
    end
  end
  object btnSalir: TcxButton
    Left = 637
    Top = 389
    Width = 186
    Height = 74
    Cursor = crHandPoint
    Align = alCustom
    Caption = '&Salir'
    LookAndFeel.NativeStyle = True
    OptionsImage.Glyph.Data = {
      36090000424D3609000000000000360000002800000018000000180000000100
      2000000000000009000000000000000000000000000000000000000000000000
      00000000000300000006000000090000000D0000001200000016000000190000
      00190000001800000015000000110000000C0000000800000005000000010000
      0000000000000000000000000000000000000000000000000000000000000000
      0002000000060000000D000000150000001E000000260000002F000000340000
      0036000000330000002D000000250000001C000000130000000B000000040000
      0001000000000000000000000000000000000000000000000000000000000000
      0002000000060000000E000000160000001F0000002800000030000000360000
      0038000000350000002F000000260000001E000000150000000C000000050000
      0001000000000000000000000000000000000000000000000000000000000000
      000100000003000000070000000B0000000F0000001400000019000000240000
      001F0000001C000000220303038F000000730000006F0000006D0000006A0000
      0069000000670000006600000066000000650000006500000040000000000000
      0000000000010000000100000002000000030000000500000007000000280008
      239E0000000B0000002D2A3136FF202427FE040505F8000000F3000000EE0000
      00EA000000E6000000E2000000DD000000D9000000D500000093000000000000
      0000000000000000000000000000000000000000000000000001000000220025
      94F1000F31970000002B414A50FF5D6A73FF49535BFE1F2226FB040404F20000
      00EB000000E6000000E2000000DE000000DA000000D700000094000000000000
      000000000000000000000000000000000000000000000000000000000021002D
      B1F30244DAFB071432A84B5359FF65737CFF637079FF5E6B74FF454E56FE1A1F
      22F7030304EA000000E3000000DF000000DC000000D800000095000000000000
      000000000000000000000000000000000000000000000000000000000021002C
      B0F3014CF8FF1F58D9FC2F3E59FF6F7D85FF6B7881FF65737CFF5E6B74FF5460
      6AFF292F35FB000000E4000000E1000000DD000000DB00000096000000210000
      47D600005BD600015DD6000561D6000966D6000D69D600116ED6001571D60032
      D2FC0049F6FF1C5FF8FF366ADDFF45546DFF718088FF6D7B83FF65727BFF5B67
      71FF2F363CFB000000E6000000E2000000DF000000DD00000097000000210000
      88F30000B8FF0001B9FF0009C2FF0011CAFF0019D2FF0021DBFF0028E3FF0034
      EBFF0044F4FF1057F8FF2F6DF9FF4674DEFF46566DFF73818AFF6C7982FF616E
      77FF32383EFC000000E7000000E4000000E1000000DF00000098000000210000
      88F30000B8FF0000B8FF0007BFFF000FC8FF0016D0FF001ED8FF0026E0FF002E
      E8FF003DF0FF004CF8FF1C60F8FF3873F9FF4876DDFF46566DFF717F88FF6775
      7EFF353C42FC000000E9000000E6000000E3000000E100000098000000210000
      88F30000B8FF0000B8FF0004BDFF000CC5FF0013CDFF001BD5FF0022DCFF0029
      E4FF0034EBFF0042F3FF0650F8FF1F62F8FF3571F9FF3F70DDFF42526DFF6C7A
      83FF384045FC000000EB000000E8000000E5000000E40000009A000000210E0E
      8FF44B4BCDFF4646CBFF4142CBFF3A41CFFF323FD4FF2B3ED9FF243EDEFF1B3C
      E2FF143CE8FF0C40EEFF0547F3FF044FF8FF175CF8FF2667F9FF2A4888FF6672
      79FF3C4348FD000000EC000000EA000000E8000000E60000009B000000211212
      90F46060D3FF5D5DD2FF5757D0FF4F52D1FF4750D5FF3E4CD8FF3448DCFF2B46
      E0FF2142E5FF1840E9FF0E41EDFF0443F1FF0048F6FF194095FF6B7A86FF7886
      8FFF3F454BFD000000EE000000EC000000EA000000E90000009B000000211313
      91F46E6ED7FF6B6BD6FF6363D4FF5959D1FF5055D4FF4650D6FF3C4BDAFF3247
      DCFF2843E0FF1F40E4FF143BE7FF0938EBFF1A3993FF798895FF8B9AA2FF7D8C
      94FF40484DFD000000F0000000EE000000EC000000EB0000009C000000210E0E
      8FF45252CFFF5454CFFF4E4ECEFF4747CCFF4041CCFF383FCFFF313DD3FF3040
      D7FF2D42DCFF233EDEFF1636E1FF1E3490FF92A0ABFF9DAEB5FF90A0A7FF8291
      99FF434C51FE000000F3000000F0000000EF000000EE0000009D000000100000
      1C7F00001E8100001E8100001E8100001E8100001E8100011F8100011D8E1923
      A0F7303FD6FF2136D8FF162681FF9DAAB4FFBFCDD2FFA8B8BEFF95A5ACFF8695
      9DFF454D53FE000000F5000000F3000000F1000000F00000009E000000000000
      000000000000000000000000000000000000000000000000000000000021191E
      99F42A35CFFF050E67DF6A737EFFCADADEFFC4D2D7FFB7C6CBFF9AABB2FF8999
      A0FF474F55FE000000F7000000F5000000F4000000F30000009F000000000000
      0000000000000000000000000000000000000000000000000000000000211213
      90F4070A63D900000853889598FFD1E0E4FFC9D8DCFFBECCD1FFACBABFFF909F
      A6FF495156FE000000F9000000F7000000F6000000F5000000A0000000000000
      0000000000000000000000000000000000000000000000000000000000210000
      3EDA00000733000000288E999CFFD6E6EAFFCDDCE1FFC1CFD4FFB3C1C6FFA5B2
      B8FF51595DFF000000FB000000FA000000F8000000F8000000A1000000000000
      0000000000000000000000000000000000000000000000000000000000190000
      052F0000000000000028929FA2FFDCECF0FFD1E0E4FFC3D2D6FFB5C2C8FFA7B3
      B9FF535C61FF000000FE000000FC000000FB000000FA000000A2000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000002897A3A6FFE0F1F4FFD2E2E6FFC4D2D7FFB6C3C9FFA7B4
      BAFF545C61FF000000FE000000FE000000FD000000FD000000A2000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000002899A5A8FFE2F1F5FFD3E2E6FFC4D3D7FFB6C3C9FFA7B4
      BAFF545C61FF000000FE000000FE000000FE000000FE000000A3000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000172A2E2FD3333738D02E3334D02A2E30D0262A2CD02226
      27D016191ACC000000A3000000A3000000A3000000A300000075}
    OptionsImage.ImageIndex = 1
    TabOrder = 1
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    OnClick = btnSalirClick
  end
  object qryWork1: TADOQuery
    Connection = DatMain.con1
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    Parameters = <>
    SQL.Strings = (
      'select top 1000  * from [vw_rpt_ventas]')
    Left = 64
    Top = 384
    object wdstrngfldWork1ClienteNombre: TWideStringField
      DisplayWidth = 42
      FieldName = 'ClienteNombre'
      Size = 100
    end
    object dtmfldWork1DocumentoFecha: TDateTimeField
      DisplayWidth = 12
      FieldName = 'DocumentoFecha'
    end
    object QryArticuloWork1MontoGeneral: TFMTBCDField
      DisplayWidth = 12
      FieldName = 'MontoGeneral'
      DisplayFormat = '##,###,###.00'
      Precision = 19
      Size = 6
    end
    object strngfldWork1Nombre: TStringField
      FieldName = 'Nombre'
      Size = 50
    end
    object strngfldWork1Apellido: TStringField
      FieldName = 'Apellido'
      Size = 50
    end
  end
  object ds1: TDataSource
    DataSet = qryWork1
    Left = 120
    Top = 384
  end
  object qryFlujoCajas: TADOQuery
    Connection = DatMain.con1
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    Parameters = <>
    SQL.Strings = (
      'SELECT TOP 1000 *'
      '  FROM [DB_PUNTO_VENTA].[dbo].[vw_Qry_Flujo_Cajas]'
      'ORDER BY Fecha desc')
    Left = 64
    Top = 448
    object wdstrngfldFlujoCajasClienteNombre: TWideStringField
      FieldName = 'ClienteNombre'
      Size = 100
    end
    object wdstrngfldFlujoCajasFecha: TWideStringField
      FieldName = 'Fecha'
      ReadOnly = True
      Size = 10
    end
    object QryArticuloFlujoCajasMontoGeneral: TFMTBCDField
      FieldName = 'MontoGeneral'
      DisplayFormat = '#,###,###.00'
      Precision = 19
      Size = 6
    end
    object wdstrngfldFlujoCajasDescripcion: TWideStringField
      FieldName = 'Descripcion'
      Size = 50
    end
    object intgrfldFlujoCajasFacturaNo: TIntegerField
      FieldName = 'FacturaNo'
    end
    object dtmfldFlujoCajasDocumentoFecha: TDateTimeField
      FieldName = 'DocumentoFecha'
    end
    object wdstrngfldFlujoCajasNombrePC: TWideStringField
      FieldName = 'NombrePC'
      Size = 90
    end
  end
  object dsFlujoCajas: TDataSource
    DataSet = qryFlujoCajas
    Left = 120
    Top = 448
  end
  object cxstylrpstry1: TcxStyleRepository
    Left = 56
    Top = 504
    PixelsPerInch = 96
    object cxstyl1: TcxStyle
      AssignedValues = [svFont]
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
    end
    object cxstyl2: TcxStyle
      AssignedValues = [svFont, svTextColor]
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      TextColor = clHotLight
    end
  end
  object qry1: TADOQuery
    Connection = DatMain.con1
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    Parameters = <>
    SQL.Strings = (
      'select * from vw_qry_articulos_ventas')
    Left = 384
    Top = 392
  end
  object ds2: TDataSource
    DataSet = qry1
    Left = 440
    Top = 392
  end
end
