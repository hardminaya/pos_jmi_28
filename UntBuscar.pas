unit UntBuscar;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, dxSkinsCore,
  dxSkinsDefaultPainters, cxStyles, dxSkinscxPCPainter, cxCustomData, cxFilter,
  cxData, cxDataStorage, Data.DB, cxDBData, Data.Win.ADODB, cxGridLevel,
  cxClasses, cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, cxTextEdit, Vcl.StdCtrls, Vcl.Menus, cxButtons,
  Vcl.ComCtrls, Vcl.ToolWin, cxPCdxBarPopupMenu, cxPC, JvMenus, Vcl.ActnList,
  cxGroupBox, cxRadioGroup, cxDBEdit, Vcl.Mask, Vcl.DBCtrls, Vcl.DBActns,
  JvExMask, JvToolEdit, JvMaskEdit, JvDBControls, cxMaskEdit, Vcl.ExtCtrls,
  cxDropDownEdit, dxSkinBlack, dxSkinBlue, dxSkinBlueprint, dxSkinCaramel,
  dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinDevExpressDarkStyle,
  dxSkinDevExpressStyle, dxSkinFoggy, dxSkinGlassOceans, dxSkinHighContrast,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black, dxSkinOffice2007Blue,
  dxSkinOffice2007Green, dxSkinOffice2007Pink, dxSkinOffice2007Silver,
  dxSkinOffice2010Black, dxSkinOffice2010Blue, dxSkinOffice2010Silver,
  dxSkinPumpkin, dxSkinSeven, dxSkinSevenClassic, dxSkinSharp, dxSkinSharpPlus,
  dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinTheAsphaltWorld, dxSkinValentine, dxSkinVS2010, dxSkinWhiteprint,
  dxSkinXmas2008Blue;

type
  TfrmBuscar = class(TForm)
    qryClientes: TADOQuery;
    dsClientes: TDataSource;
    cxpgcntrl1: TcxPageControl;
    cxtbsht1: TcxTabSheet;
    cxtbsht2: TcxTabSheet;
    lbl1: TLabel;
    edttexto: TcxTextEdit;
    cxgrd1: TcxGrid;
    cxgrdbtblvwGrid1DBTableView1: TcxGridDBTableView;
    GridGrid1DBTableView1ClienteID: TcxGridDBColumn;
    GridGrid1DBTableView1Nombre: TcxGridDBColumn;
    GridGrid1DBTableView1RNC: TcxGridDBColumn;
    cxgrdlvlGrid1Level1: TcxGridLevel;
    actlst1: TActionList;
    acAgregarCliente: TAction;
    lbl2: TLabel;
    lblNombre: TLabel;
    lblTelefono: TLabel;
    wdstrngfldClientesClienteID: TWideStringField;
    wdstrngfldClientesNombre: TWideStringField;
    strngfldClientesTipoCliente: TStringField;
    wdstrngfldClientesDireccion: TWideStringField;
    wdstrngfldClientesRNC: TWideStringField;
    strngfldClientesGenerico: TStringField;
    wdstrngfldClientesTelefono1: TWideStringField;
    btnPost: TcxButton;
    btnSalir: TcxButton;
    lblRNC: TLabel;
    actInsert: TDataSetInsert;
    actPost: TDataSetPost;
    actCancel: TDataSetCancel;
    edtNombre: TcxDBTextEdit;
    edtRNC: TcxDBTextEdit;
    edtTelefono: TcxDBMaskEdit;
    edtEmail: TcxDBTextEdit;
    lbl3: TLabel;
    wdstrngfldClientesEmail1: TWideStringField;
    cxdbrdgrp1: TcxDBRadioGroup;
    procedure edttextoKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure SeleccionaCliente(Sender:TObject);
    procedure cxgrdbtblvwGrid1DBTableView1CellClick(
      Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure edttextoKeyPress(Sender: TObject; var Key: Char);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure acAgregarClienteExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnInsertarClick(Sender: TObject);
    procedure btnSalirClick(Sender: TObject);
    procedure cxtbsht2Show(Sender: TObject);
    procedure CrearCliente;
    procedure cxpgcntrl1Click(Sender: TObject);
    procedure cxtbsht1Show(Sender: TObject);
    procedure btnPostClick(Sender: TObject);

  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmBuscar: TfrmBuscar;

implementation

{$R *.dfm}

uses UntData, UntFacturacion, UntMain, UntClientes, UntCustomLogin;

procedure TfrmBuscar.acAgregarClienteExecute(Sender: TObject);
begin
  ShowMessage('Agregar Cliente');
end;

procedure TfrmBuscar.btnInsertarClick(Sender: TObject);
begin
  // Inserta Cliente al Por Mayor
  qryClientes.Insert;
  //qryClientes.FieldByName('TipoCliente').Value:='20';
end;

procedure TfrmBuscar.btnPostClick(Sender: TObject);
begin
  cVariables.Privilegio:='CREARCLIENTE';
  cVariables.PrivilegioElevado:=False;

  if fn_ValidarPrivilegio(CUsuario.Usuario,CUsuario.Clave,cVariables.Privilegio)=1 then begin
    CrearCliente;
    //frmBuscar.Close;
  end
  else begin
    if not Assigned (frmCustomLogin) then frmCustomLogin:=TfrmCustomLogin.Create(nil);
    frmCustomLogin.ShowModal;
    if cVariables.PrivilegioElevado=TRUE then begin
      CrearCliente;
      //frmBuscar.Close;
    end
    else begin
      MessageBox(Handle,'ERROR de Privilegios, Operacion NO fue completada','Creacion Cliente',MB_ICONERROR or MB_OK);
    end;
  end;
end;

procedure TfrmBuscar.btnSalirClick(Sender: TObject);
begin
  self.Close;
end;

procedure TfrmBuscar.cxgrdbtblvwGrid1DBTableView1CellClick(
  Sender: TcxCustomGridTableView; ACellViewInfo: TcxGridTableDataCellViewInfo;
  AButton: TMouseButton; AShift: TShiftState; var AHandled: Boolean);
begin
  SeleccionaCliente(Sender);
end;

procedure TfrmBuscar.cxpgcntrl1Click(Sender: TObject);
begin
  //edtNombre.SetFocus;
end;

procedure TfrmBuscar.cxtbsht1Show(Sender: TObject);
begin
  qryClientes.Cancel;
end;

procedure TfrmBuscar.cxtbsht2Show(Sender: TObject);
begin
  qryClientes.Insert;
//  cxdbrdgrp1.ItemIndex:=20;
  qryClientes.FieldByName('TipoCliente').Value:='20';
  edtNombre.SetFocus;
end;

procedure TfrmBuscar.edttextoKeyPress(Sender: TObject; var Key: Char);
begin
  if Ord(Key) = VK_RETURN then
  begin
    SeleccionaCliente(Sender);
  end;
end;

procedure TfrmBuscar.edttextoKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  vl_SQLText : string;
begin
  vl_SQLText:='Select * from vw_Clientes where clienteID like '+' ''%'+
  edttexto.Text + '%'''+' or Nombre like '+' ''%'+edttexto.Text + '%'''+
  ' or RNC Like '+' ''%'+edttexto.Text + '%'''+
  ' or Telefono1 Like '+' ''%'+edttexto.Text + '%''';
  qryClientes.SQL.Text:=vl_SQLText;
  qryClientes.Close;
  qryClientes.Open;

//  QryArticulo.SQL.Text:='select * from v_articulos where (codigoart like '+QuotedStr(cVariables.ArticuloaBuscarDescripcion)
//   + ') and (Codigo_Almacen=' + QuotedStr(var_Almacen_ID) + 'and PlanID='+IntToStr(cFacturaD.PlanID)+')';
end;


procedure TfrmBuscar.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action:=caFree;
  frmBuscar:=nil;
end;

procedure TfrmBuscar.FormCreate(Sender: TObject);
begin
  qryClientes.Open;
end;

procedure TfrmBuscar.FormShow(Sender: TObject);
begin
  qryClientes.Open;
  edttexto.SetFocus;
end;

procedure TfrmBuscar.SeleccionaCliente(Sender: TObject);
begin
  qryClientes.Open;
  cCliente.Codigo:=qryClientes.FieldByName('ClienteID').AsString;
  cCliente.Nombre:=qryClientes.FieldByName('Nombre').AsString;
  cCliente.RNC:=qryClientes.FieldByName('RNC').AsString;
  cCliente.TipoCliente:=qryClientes.FieldByName('TipoCliente').AsString;
  //cCliente.Descuento:=qryClientes.FieldByName('Descuento').AsInteger;
  cCliente.Descuento:=0;

  with dsClientes.DataSet do
  begin
    frmFacturacion.edtClienteCodigo.Text:=FieldByName('clienteID').AsString;
    frmFacturacion.edtClienteNombre.Text:=FieldByName('Nombre').AsString;
    frmFacturacion.edtClienteRNC.Text:=FieldByName('RNC').AsString;
    cCliente.Generico:=FieldByName('Generico').AsString;
    if cCliente.Generico='N' then begin
      frmFacturacion.edtClienteNombre.Enabled:=False;
      frmFacturacion.edtClienteRNC.Enabled:=False;
    end
    else begin
      frmFacturacion.edtClienteNombre.Enabled:=true;
      frmFacturacion.edtClienteRNC.Enabled:=True;
    end;
  end;

    ModalResult:=mrOk;
  //end;
end;

procedure TfrmBuscar.CrearCliente;
var
  var_Mensaje : String;
begin
  Try
    qryClientes.Post;
  Except
    on E:Exception do begin
      var_Mensaje:='ATENCION. CLIENTE NO HA SIDO CREADO'+#13+e.Message;
      MessageDlg(var_Mensaje,mtError,[mbOK],0);
      fn_RegistrodeCambio(CUsuario.Usuario,'Facturacion','ERROR Creado Cliente ' +
      'Nombre: '+qryClientes.FieldByName('Nombre').Value+
      ' Tel�fono: '+edtTelefono.Text);
      Exit;
    end;
  End;
  fn_RegistrodeCambio(CUsuario.Usuario,'Facturacion','Cliente Creado Nombre: '+
  +qryClientes.FieldByName('Nombre').Value);
  MessageBox(Handle,'Usuario Creado Satisfactoriamente','Cliente',
  MB_ICONINFORMATION or MB_OK);
  frmBuscar.Close;
end;

end.
