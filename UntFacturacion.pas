unit UntFacturacion;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Vcl.Menus, dxSkinsCore, dxSkinsDefaultPainters,
  dxdbtrel, Vcl.StdCtrls, cxButtons, Vcl.ExtCtrls, cxControls, cxContainer,
  cxEdit, cxLabel, cxTextEdit, cxGroupBox, cxMaskEdit, cxDropDownEdit,
  cxLookupEdit, cxDBLookupEdit, cxDBExtLookupComboBox, cxStyles,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage, Data.DB,
  cxDBData, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxGridLevel, cxClasses, cxGridCustomView, cxGrid, cxDBLookupComboBox,
  Data.Win.ADODB, cxPCdxBarPopupMenu, cxPC, Vcl.ComCtrls, Vcl.Buttons,
  Vcl.ActnList, Vcl.DBCtrls, Datasnap.DBClient, Vcl.Mask, Datasnap.Provider,Math,
  cxCalc, cxButtonEdit, cxBlobEdit, cxDBEdit, Vcl.Samples.Spin, cxListBox,
  Data.Bind.EngExt, Vcl.Bind.DBEngExt, System.Rtti, System.Bindings.Outputs,
  Vcl.Bind.Editors, Data.Bind.Components, cxCurrencyEdit,
  dxSkinsdxStatusBarPainter, dxStatusBar, cxSpinEdit, JvExStdCtrls, JvCombobox,
  JvDBCombobox, JvExMask, JvToolEdit, JvDBLookup, JvExControls, JvButton,
  JvTransparentButton, cxRadioGroup, JvDBLookupComboEdit, JvSwitch, dxSkinBlack,
  dxSkinBlue, dxSkinBlueprint, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom,
  dxSkinDarkSide, dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinFoggy,
  dxSkinGlassOceans, dxSkinHighContrast, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSevenClassic, dxSkinSharp, dxSkinSharpPlus, dxSkinSilver,
  dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008, dxSkinTheAsphaltWorld,
  dxSkinValentine, dxSkinVS2010, dxSkinWhiteprint, dxSkinXmas2008Blue;

//type TArticulo = record
//  CodigoArt : String;
//  DescripcionArt : String;
//  Precio : Double;
//  EnMano : Integer;
//  Cantidad : Integer;
//end;

type
  TfrmFacturacion = class(TForm)
    GroupBox1: TGroupBox;
    btnCancelarFactura: TcxButton;
    cxButton6: TcxButton;
    GroupBox2: TGroupBox;
    cxLabel1: TcxLabel;
    cxLabel2: TcxLabel;
    cxLabel3: TcxLabel;
    cxLabel4: TcxLabel;
    grpInformacionCliente: TGroupBox;
    cxLabel6: TcxLabel;
    cxLabel7: TcxLabel;
    cxLabel8: TcxLabel;
    cxLabel9: TcxLabel;
    edtArticulo: TcxTextEdit;
    cxLabel19: TcxLabel;
    cxMaskEdit1: TcxMaskEdit;
    cxStyleRepository1: TcxStyleRepository;
    cxStyle1: TcxStyle;
    btnPago: TcxButton;
    qryFactura_Cabecera: TADOQuery;
    dsFactura_Cabecera: TDataSource;
    btnBorrarLinea: TcxButton;
    QryArticulo: TADOQuery;
    QryArticuloCodigoArt: TWideStringField;
    QryArticuloDescripcionArt: TWideStringField;
    QryArticuloCodigoBarras: TWideStringField;
    QryArticuloEnMano: TBCDField;
    QryArticuloPrecio: TBCDField;
    QryArticuloCodigo_Almacen: TWideStringField;
    cbbTipoNCF: TcxComboBox;
    PageControl1: TPageControl;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    cxGrid1: TcxGrid;
    MainView: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    Seriales: TcxTabSheet;
    BitBtn1: TBitBtn;
    ActionList1: TActionList;
    acBuscarArticulo: TAction;
    qryWork1: TADOQuery;
    cxStyle2: TcxStyle;
    acInsertaLinea: TAction;
    qryNoProximaFactura: TADOQuery;
    spInsertaFactura: TADOStoredProc;
    ADOTable1: TADOTable;
    dsFacturaD: TDataSource;
    DataSetProvider1: TDataSetProvider;
    TFacturaD: TClientDataSet;
    TFacturaDfkDocCabecera: TIntegerField;
    TFacturaDpIdRegistro: TAutoIncField;
    TFacturaDSecSerial: TIntegerField;
    TFacturaDNo_Linea: TIntegerField;
    TFacturaDArticuloId: TWideStringField;
    TFacturaDArticuloDescripcion: TWideStringField;
    TFacturaDArticuloCosto: TFMTBCDField;
    TFacturaDArticuloPrecio: TFMTBCDField;
    TFacturaDArticuloPrecioDesc: TBCDField;
    TFacturaDArticuloCantidad: TBCDField;
    TFacturaDArticuloMonto: TFMTBCDField;
    TFacturaDArticuloPorcientoDesc: TBCDField;
    TFacturaDArticuloPrecioImpuesto: TFMTBCDField;
    TFacturaDArticuloTotalImpuesto: TFMTBCDField;
    TFacturaDArticuloImporte: TFMTBCDField;
    TFacturaDArticuloImporteNeto: TFMTBCDField;
    TFacturaDfkUsuario: TIntegerField;
    TFacturaDCodigo_Almacen: TWideStringField;
    TFacturaDPlanID: TIntegerField;
    TFacturaDManejaSeriales: TStringField;
    TFacturaDfkSucursal: TWideStringField;
    MainViewArticuloId: TcxGridDBColumn;
    MainViewArticuloDescripcion: TcxGridDBColumn;
    MainViewArticuloPrecio: TcxGridDBColumn;
    MainViewArticuloCantidad: TcxGridDBColumn;
    MainViewArticuloMonto: TcxGridDBColumn;
    MainViewArticuloImporte: TcxGridDBColumn;
    ADODataSet1: TADODataSet;
    btnImprimirFactura: TcxButton;
    cxLabel5: TcxLabel;
    Timer1: TTimer;
    lblFecha: TcxLabel;
    txtImpuestos: TEdit;
    txtDescuentos: TEdit;
    txtSubTotal: TEdit;
    ADODataSet1pIdRegistro: TAutoIncField;
    ADODataSet1fkDocCabecera: TIntegerField;
    ADODataSet1SecSerial: TIntegerField;
    ADODataSet1No_Linea: TIntegerField;
    ADODataSet1ArticuloId: TWideStringField;
    ADODataSet1ArticuloDescripcion: TWideStringField;
    ADODataSet1ArticuloCosto: TFMTBCDField;
    ADODataSet1ArticuloPrecio: TFMTBCDField;
    ADODataSet1ArticuloPrecioDesc: TBCDField;
    ADODataSet1ArticuloCantidad: TBCDField;
    ADODataSet1ArticuloMonto: TFMTBCDField;
    ADODataSet1ArticuloPorcientoDesc: TBCDField;
    ADODataSet1ArticuloPrecioImpuesto: TFMTBCDField;
    ADODataSet1ArticuloTotalImpuesto: TFMTBCDField;
    ADODataSet1ArticuloImporte: TFMTBCDField;
    ADODataSet1ArticuloImporteNeto: TFMTBCDField;
    ADODataSet1fkUsuario: TIntegerField;
    ADODataSet1Codigo_Almacen: TWideStringField;
    ADODataSet1PlanID: TIntegerField;
    ADODataSet1ManejaSeriales: TStringField;
    ADODataSet1fkSucursal: TWideStringField;
    ADODataSet1ArticuloPorcientoImpuesto: TFMTBCDField;
    TFacturaDArticuloPorcientoImpuesto: TFMTBCDField;
    acRecalcular: TAction;
    ADODataSet1ArticuloTotalDescuento: TFMTBCDField;
    TFacturaDArticuloTotalDescuento: TFMTBCDField;
    cxStyle3: TcxStyle;
    cxStyle4: TcxStyle;
    QryArticuloManejaSeriales: TWideStringField;
    btnSalir: TcxButton;
    acCancelar: TAction;
    spInsertaFacturaDetalle: TADOStoredProc;
    cxlkndflcntrlr1: TcxLookAndFeelController;
    cxlbl5: TcxLabel;
    cxgrdbclmnItbis: TcxGridDBColumn;
    cxgrdbclmnporcientoDescuento: TcxGridDBColumn;
    txttotal: TcxCurrencyEdit;
    qryArticulosSeriales: TADOQuery;
    actReImprimir: TAction;
    cxgrd1: TcxGrid;
    cxgrdbtblvwGrid2DBTableView1: TcxGridDBTableView;
    cxgrdlvlGrid2Level1: TcxGridLevel;
    dsetSeriales: TClientDataSet;
    strngfldds2ArticuloID: TStringField;
    strngfldds2ArticuloSerial: TStringField;
    dsSeriales: TDataSource;
    cxgrdbclmnArticuloID: TcxGridDBColumn;
    cxgrdbclmnArticuloSerial: TcxGridDBColumn;
    cxgrdbclmnAccion: TcxGridDBColumn;
    acInsertaLineaSerial: TAction;
    spInsertaFacturaDetalleSerial: TADOStoredProc;
    intgrfldSerialesArticuloLinea: TIntegerField;
    cxgrdbclmnSeriales: TcxGridDBColumn;
    acRemoverLineaSerial: TAction;
    acAplicarDescuento: TAction;
    acCambiarPrecio: TAction;
    dxstsbr1: TdxStatusBar;
    qryVendedores: TADOQuery;
    dsVendedores: TDataSource;
    edtClienteNombre: TcxTextEdit;
    edtClienteCodigo: TcxButtonEdit;
    grp1: TGroupBox;
    btn2: TcxButton;
    cxmskdt1: TcxMaskEdit;
    pgc1: TPageControl;
    jvswtchDescuentos: TJvSwitch;
    jvswtchPrecios: TJvSwitch;
    cxgrdbclmnCosto: TcxGridDBColumn;
    QryArticuloCosto: TFMTBCDField;
    acValidaCantidad: TAction;
    strngfldQryArticuloInventarioArt: TStringField;
    spActualizaEstadoFactura: TADOStoredProc;
    acBuscarArticuloSeriales: TAction;
    acAplicarTarjetaDescuento: TAction;
    spConsultaRNC: TADOStoredProc;
    acConsultaRNC: TAction;
    lblVendedorNombre: TcxLabel;
    cbbVendedor: TcxLookupComboBox;
    edtClienteRNC: TcxButtonEdit;
    strngfldQryArticuloAcepta_Pago_TarjetaCredito: TStringField;
    tblFactura_CabecerapIdRegistro: TIntegerField;
    tblFactura_CabeceraClienteId: TWideStringField;
    tblFactura_CabeceraClienteNombre: TWideStringField;
    tblFactura_CabeceraClienteRnc: TWideStringField;
    tblFactura_CabeceraDocumentoFecha: TDateTimeField;
    tblFactura_CabeceraDocumentoServidor: TStringField;
    tblFactura_CabecerafkSucursal: TIntegerField;
    tblFactura_CabecerafkUsuario: TIntegerField;
    tblFactura_CabecerafkCaja: TIntegerField;
    tblFactura_CabecerafkRegistroCaja: TIntegerField;
    tblFactura_CabeceraFormaPago: TStringField;
    fmtbcdfldFactura_CabeceraSubTotal: TFMTBCDField;
    fmtbcdfldFactura_CabeceraTotalDesc: TFMTBCDField;
    fmtbcdfldFactura_CabeceraTotalItbis: TFMTBCDField;
    fmtbcdfldFactura_CabeceraEfectivo: TFMTBCDField;
    fmtbcdfldFactura_CabeceraCheque: TFMTBCDField;
    tblFactura_CabeceraNoCheque: TBCDField;
    fmtbcdfldFactura_CabeceraTarjeta: TFMTBCDField;
    tblFactura_CabeceraNoTarjeta: TBCDField;
    fmtbcdfldFactura_CabeceraNcredito: TFMTBCDField;
    tblFactura_CabeceraCodigo_Almacen: TWideStringField;
    fmtbcdfldFactura_CabeceraMontoGeneral: TFMTBCDField;
    tblFactura_CabeceraCodigo_Vendedor: TWideStringField;
    tblFactura_CabeceraNombrePC: TWideStringField;
    tblFactura_CabecerafkTipoNCF: TIntegerField;
    tblFactura_CabeceraMOntoEfectivo: TBCDField;
    tblFactura_CabeceraMontoDevuelta: TBCDField;
    tblFactura_CabeceraPagoEfectivo: TBCDField;
    tblFactura_CabeceraPagoCheque: TBCDField;
    tblFactura_CabeceraPagoTarjetaCredito: TBCDField;
    tblFactura_CabeceraPagoTarjetaDebito: TBCDField;
    tblFactura_CabeceraPagoNotaCredito: TBCDField;
    mm1: TMainMenu;
    OrdenDespacho1: TMenuItem;
    QryArticuloITBIS: TStringField;
    acCalcularTotales: TAction;
    procedure edtArticuloKeyPress(Sender: TObject; var Key: Char);
    procedure FormCreate(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure acBuscarArticuloExecute(Sender: TObject);
    procedure btnPagoClick(Sender: TObject);
    procedure acInsertaLineaExecute(Sender: TObject);
    procedure btnBorrarLineaClick(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure acRecalcularExecute(Sender: TObject);
    procedure cxComboBox1PropertiesChange(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure btnSalirClick(Sender: TObject);
    procedure acCancelarExecute(Sender: TObject);
    Procedure InsertaFactura(Sender: TObject);
    procedure InsertaFacturaDetalle (Sender: TObject);
    Procedure InsertaFacturaDetalleSerial(Sender: TObject);
    Procedure ControlaFormulario(Sender: TObject);
    procedure btnCancelarFacturaClick(Sender: TObject);
    procedure btnBuscarClienteClick(Sender: TObject);
    procedure btnImprimirFacturaClick(Sender: TObject);
    procedure actReImprimirExecute(Sender: TObject);
    procedure acInsertaLineaSerialExecute(Sender: TObject);
    procedure dsetSerialesAfterInsert(DataSet: TDataSet);
    procedure TFacturaDAfterInsert(DataSet: TDataSet);
    procedure TFacturaDAfterPost(DataSet: TDataSet);
    procedure cxgrdbclmnAccionPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure acRemoverLineaSerialExecute(Sender: TObject);
    procedure acCambiarDescuentoExecute(Sender: TObject; CodigoArticulo:string;Descuento:Integer);
    procedure acCambiarPrecioExecute(Sender: TObject; CodigoArticulo:string;Precio:Currency);
    procedure MainViewArticuloCantidadPropertiesChange(Sender: TObject);
    procedure MainViewArticuloPrecioPropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
    procedure btn3PropertiesButtonClick(Sender: TObject; AButtonIndex: Integer);
    procedure jvswtchPreciosOn(Sender: TObject);
    procedure jvswtchPreciosOff(Sender: TObject);
    procedure jvswtchDescuentosDragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure jvswtchDescuentosOn(Sender: TObject);
    procedure jvswtchDescuentosOff(Sender: TObject);
    procedure MainViewArticuloCantidadPropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
    procedure MainViewArticuloCantidadPropertiesEditValueChanged(
      Sender: TObject);
    procedure cxgrdbclmnporcientoDescuentoPropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
    procedure acBuscarArticuloSerialesExecute(Sender: TObject);
    procedure acAplicarTarjetaDescuentoExecute(Sender: TObject);
    procedure acConsultaRNCExecute(Sender: TObject);
    procedure btn1Click(Sender: TObject);
    procedure cbbVendedorPropertiesChange(Sender: TObject);
    procedure edt1PropertiesButtonClick(Sender: TObject; AButtonIndex: Integer);
    procedure edtClienteRNCPropertiesChange(Sender: TObject);
    procedure edtClienteRNCPropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
    //procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure OrdenDespacho1Click(Sender: TObject);
    procedure acCalcularTotalesExecute(Sender: TObject);

//    procedure BuscarCliente(Sender:TObject);

//    function fnArticuloRetornaPrecio(IDarticulo,IDAlmacen,IDPlan:string) : TArticulo;

  private
    { Private declarations }
  public

    var_codigo : string;
    var_descripcion : String;
    var_Cantidad : Integer;
    var_Precio : Double;
    var_Monto : Double;
    var_EnMano : Integer;
    var_Importe : Double;
    var_VendedorID : String;
    var_ClienteID : String;
    var_ClienteNombre : String;
    var_No_Factura : Integer;

  { Public declarations }

  end;

var
  frmFacturacion: TfrmFacturacion;
  function fn_ValidarCantidad(param_CodigoArt,param_Codigo_Almacen : string) : Integer;

implementation

{$R *.dfm}

uses UntConsultaArticulos, UntData, UntPago, UntSeriales, UntBuscar,
  UntVendedores, UntCustomLogin, UntMain, UntCantidad, UntConsultaSeriales,
  UntPreview;

function fn_ValidarCantidad(param_CodigoArt,param_Codigo_Almacen : string) : Integer;
begin
  with datmain.qryWork1 do
  begin
    Close;
    datmain.qryWork1.SQL.Text:='SELECT enmano as Cantidad FROM articulos WHERE CodigoArt='+
    #39+param_CodigoArt+#39+' and Codigo_Almacen ='+#39+param_Codigo_Almacen+#39;
//    ShowMessage(sql.Text);
    Open;
    Result:=FieldByName('Cantidad').AsInteger;
  end;
end;

procedure TfrmFacturacion.acAplicarTarjetaDescuentoExecute(Sender: TObject);
begin
  with DatMain.qryPOS do
  begin
    SQL.Text:='SELECT * FROM VW_TarjetaDescuento_Selecciona WHERE ID=' +#39+edtArticulo.Text+#39;
    //ShowMessage(SQL.Text);
    Open;
    cCliente.TarjetaID:=FieldByName('ID').AsString;
    cCliente.Descuento:=FieldByName('Descuento').AsInteger;
    cCliente.TarjetaID:=cCliente.TarjetaID;
    if RecordCount>0 then
      begin
        with TFacturaD do
        begin
          First;
          while not eof do
          begin
            TFacturaD.Edit;
            TFacturaDArticuloPorcientoDesc.Text:=IntToStr(cCliente.Descuento);
            Next;
          end;
          //TFacturaD.Post;
          acRecalcular.Execute;
        end;
      end
      else
      begin
        MessageBox(Handle,'Tarjeta Invalida, ver Servicio al Cliente','Tarjeta',MB_ICONERROR or MB_OK);
      end;
    Open;
  end;
end;

procedure TfrmFacturacion.acBuscarArticuloExecute(Sender: TObject);
begin
  //Screen.Cursor:=crSQLWait;
// Busca Tarjeta de Descuento
  if (Pos(cSucursal.PrefijoTarjetaDescuento,edtArticulo.Text)>0) then
  begin
    acAplicarTarjetaDescuento.Execute;
    Exit;
  end;

  // Busqueda por C�digo de Barras
  QryArticulo.SQL.Text:='select * from v_articulos where (CodigoBarras = '+#39+cVariables.ArticuloaCriteriodeBusqueda + #39
  + ') and (Codigo_Almacen=' + QuotedStr(CUsuario.ID_Almacen) + 'and PlanID='+cCliente.TipoCliente+')';
  QryArticulo.Close;
  QryArticulo.Open;
  //ShowMessage(QryArticulo.SQL.Text);

  // Busqueda por C�digo
  if QryArticulo.RecordCount = 0 then begin
  QryArticulo.SQL.Text:='select * from v_articulos where (codigoart like '+'''%'+cVariables.ArticuloaCriteriodeBusqueda + '%'''
   + ') and (Codigo_Almacen=' + QuotedStr(CUsuario.ID_Almacen) + 'and PlanID='+cCliente.TipoCliente+')';
  QryArticulo.Close;
  QryArticulo.Open;
  //ShowMessage(QryArticulo.SQL.Text);
  end;

  // Busqueda Por Descripcion
  if QryArticulo.RecordCount = 0 then
  begin
    QryArticulo.SQL.Text:='select * from v_articulos where DescripcionArt like '+'''%'+cVariables.ArticuloaCriteriodeBusqueda+ '%'''
    + ' and (Codigo_Almacen=' + QuotedStr(CUsuario.ID_Almacen) + 'and PlanID='+cCliente.TipoCliente+')';
    QryArticulo.Close;
    QryArticulo.Open;
  end;

  // Busqueda Por Serial
  if QryArticulo.RecordCount = 0 then
  begin
    acBuscarArticuloSeriales.Execute;
    cVariables.ArticuloCantidadEncontrada:=QryArticulo.RecordCount;
  end
  else
    begin
      if not assigned(frmConsultaArticulo) then
      begin
        frmConsultaArticulo := TfrmConsultaArticulo.Create(frmFacturacion);
      end;
      frmConsultaArticulo.ShowModal;
      //if frmConsultaArticulo.Visible=False then frmConsultaArticulo.ShowModal;
    end;
    Screen.Cursor:=crDefault;
end;

procedure TfrmFacturacion.acBuscarArticuloSerialesExecute(Sender: TObject);
begin
  with qryArticulosSeriales do begin
    SQL.Text:='Select * from vw_ArticulosSeriales where SerialNo ='+
    QuotedStr(edtArticulo.Text)+' and (Codigo_Almacen=' + QuotedStr(CUsuario.ID_Almacen) +
    ' and PlanID='+cCliente.TipoCliente+')' + 'and Estatus='+QuotedStr('D');
//    ShowMessage(qryArticulosSeriales.SQL.Text);
    Close;
    Open;
    begin
      if RecordCount>0 then begin
        with cArticulo do begin
          CodigoArt:=FieldByName('fkIdArticulo').AsString;
          DescripcionArt:=FieldByName('Descripcionart').AsString;
          cArticulo.Precio:=FieldByName('Precio').AsCurrency;
          Cantidad:=1;
          Factura:=FieldByName('FkFacturaVenta').AsInteger;
          ManejaSeriales:='Y';
         end;

//        if FieldByName('Estatus').Value='U' then begin
//        ShowMessage('Articulo NO Disponible, Vendido en Factura No.:'+
//        IntToStr(cArticulo.Factura));
//          Exit;
//        end;
        cArticulo.Serial:=edtArticulo.Text;
        acInsertaLineaSerial.Execute;
        Exit;
      end;
    if qryArticulosSeriales.RecordCount=0 then ShowMessage('Articulo NO Disponible');
    Exit;
    end;
  end;
end;

procedure TfrmFacturacion.BitBtn1Click(Sender: TObject);
begin
  cVariables.ArticuloaCriteriodeBusqueda:=edtArticulo.Text;
  if fn_CajaEstado(cCaja.Nombre)<>1 then begin
    MessageDlg('CAJA NO esta abierta',mtError,[mbOK],0);
    Exit;
  end;

  if Length(edtClienteCodigo.Text)=0 then begin
    cCliente.TipoCliente:='9999';
    btnBuscarClienteClick(Sender);
  end;

  if Length(cbbVendedor.Text)=0 then
  begin
    MessageDlg('Debe Seleccionar el Vendedor para continuar',mtError,[mbOK],0);
    Exit;
  end;

  if cFactura.TipoNCF=2 then
  begin
    if Length(edtClienteRNC.Text)=0 then begin
      MessageDlg('RNC No debe quedar en Blanco',mtError,[mbOK],0);
      Exit;
    end;
  end;
  cVariables.ArticuloaCriteriodeBusqueda:=edtArticulo.Text;
  acBuscarArticulo.Execute;
end;

procedure TfrmFacturacion.btn1Click(Sender: TObject);
begin
  acConsultaRNC.Execute;
end;

procedure TfrmFacturacion.btn3PropertiesButtonClick(Sender: TObject;
  AButtonIndex: Integer);
begin
  frmBuscar:=TfrmBuscar.Create(frmFacturacion);
  frmBuscar.ShowModal;
end;

procedure TfrmFacturacion.btnPagoClick(Sender: TObject);
begin
  //ShowMessage(IntToStr(cFactura.TipoNCF));

  if cFactura.Total>0 then
  begin
    if not assigned(frmPago) then frmpago:=TfrmPago.Create(frmFacturacion);
    frmPago.ShowModal;
    exit;
    var_No_Factura:=0;
  end;
end;

procedure TfrmFacturacion.btnSalirClick(Sender: TObject);
begin
  if cFactura.Total>0 then
  begin
    if MessageBox(Handle,'Los Datos no han sido guardados, se perderan, est� Seguro de Salir?','Caja',MB_ICONQUESTION or MB_YESNO)=ID_YES then
    begin
      dsFacturaD.DataSet.Cancel;
    end
    else begin
      exit;
    end;
  end;
  acCancelar.Execute;
  self.Close();
end;

procedure TfrmFacturacion.cbbVendedorPropertiesChange(Sender: TObject);
begin
  with cbbVendedor.Properties.Grid.DataController do begin
    cVendedor.CodVendedor:= Values[FocusedRecordIndex,0];
    cVendedor.Nombre:=Values[FocusedRecordIndex,1];
    lblVendedorNombre.Caption:=cVendedor.Nombre;
  end;
end;

procedure TfrmFacturacion.btnCancelarFacturaClick(Sender: TObject);
begin
  if MessageBox(Handle,'Cancelar Venta y Limpiar Factura?','Facturaci�n',MB_ICONQUESTION or MB_YESNO) =IDYES then
  begin
    acCancelar.Execute;
  end;
end;

procedure TfrmFacturacion.btnBorrarLineaClick(Sender: TObject);
begin
  if TFacturaD.FieldByName('ManejaSeriales').Value='N' then
  begin
    if  MessageBox(Handle,'Seguro que desea eliminar esta linea completa?','Facturaci�n',MB_ICONQUESTION or MB_YESNO) =IDYES then
    begin
      TFacturaD.Delete;
      acRecalcular.Execute;
    end;
  end
  else begin
    MessageDlg('No puede eliminar esta linea,  debe remover cada serial',mtWarning,[mbOK],0);
  end;
end;

procedure TfrmFacturacion.btnBuscarClienteClick(Sender: TObject);
begin
  if not Assigned (frmBuscar) then
  begin
    frmBuscar:=TfrmBuscar.Create(frmFacturacion);
  end;
  frmBuscar.ShowModal;
end;


procedure TfrmFacturacion.btnImprimirFacturaClick(Sender: TObject);
begin
  cVariables.Privilegio:='REIMPRIMIR';
  cVariables.PrivilegioElevado:=False;

  if fn_ValidarPrivilegio(CUsuario.Usuario,CUsuario.Clave,cVariables.Privilegio)=1 then
  begin
    actReImprimir.Execute;
  end
  else begin
    if not Assigned (frmCustomLogin) then frmCustomLogin:=TfrmCustomLogin.Create(nil);
    frmCustomLogin.ShowModal;
    if cVariables.PrivilegioElevado=TRUE then begin
      actReImprimir.Execute;
    end;
  end;
end;

procedure TfrmFacturacion.cxComboBox1PropertiesChange(Sender: TObject);
begin
  cFactura.TipoNCF:=StrToInt(Copy(cbbTipoNCF.Text,1,1));
//  if cFactura.TipoNCF=2 then
//  begin
//    edtClienteRNC.Properties.ReadOnly:=True;
//    acConsultaRNC.Execute;
//  end
//  else begin
//    edtClienteRNC.Properties.ReadOnly:=False;
//  end;

end;

procedure TfrmFacturacion.cxgrdbclmnAccionPropertiesButtonClick(Sender: TObject;
  AButtonIndex: Integer);
begin
    acRemoverLineaSerial.Execute;
end;

procedure TfrmFacturacion.cxgrdbclmnporcientoDescuentoPropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
  if TFacturaD.FieldByName('ArticuloPrecio').AsCurrency*(1-(DisplayValue)/100)<= TFacturaD.FieldByName('ArticuloCosto').AsCurrency then begin
    MessageDlg('Precio NO Aceptado, es menor al Precio Minino del articulo',mtError,[mbOK],0);
    TFacturaD.Cancel;
    Exit;
  end;
  acCambiarDescuentoExecute(Self,cArticulo.CodigoArt,DisplayValue);
end;

procedure TfrmFacturacion.dsetSerialesAfterInsert(DataSet: TDataSet);
begin
  cArticulo.Cantidad:=1;
  frmFacturacion.acInsertaLinea.Execute;
end;

procedure TfrmFacturacion.edt1PropertiesButtonClick(Sender: TObject;
  AButtonIndex: Integer);
begin
  acConsultaRNC.Execute;
end;

procedure TfrmFacturacion.edtArticuloKeyPress(Sender: TObject;
  var Key: Char);
begin
  if Ord(Key) = VK_RETURN then
  begin
    BitBtn1Click(Sender);
  end;
end;

procedure TfrmFacturacion.edtClienteRNCPropertiesChange(Sender: TObject);
begin
  //cCliente.RNC:=edtClienteRNC.Text;
end;

procedure TfrmFacturacion.edtClienteRNCPropertiesValidate(Sender: TObject;
  var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
begin
  cCliente.RNC:=edtClienteRNC.Text;
  //ShowMessage(cCliente.RNC);
end;

procedure TfrmFacturacion.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action:=caFree;
  frmFacturacion:=nil;
end;

procedure TfrmFacturacion.FormCreate(Sender: TObject);
begin
   if TFacturaD.Active=false then TFacturaD.Active:=true;
  //qryFactura_Cabecera.open;
  QryArticulo.Open;
  qryVendedores.Open;
end;

procedure TfrmFacturacion.FormShow(Sender: TObject);
begin
 cbbTipoNCF.ItemIndex:=0;
 edtArticulo.SetFocus;
 if (cCaja.EmiteCoprobantes=true) and (cValoresIni.ComprobantesTipo<>'7') then cbbTipoNCF.Enabled:=True;

//  With qryWork1 do
//  begin
//    qryWork1.SQL.Text:='Select * from vw_Clientes';
//    //cbbClientes.Properties.Items.Clear;
//    Open;
//  while not Eof do
//    begin
//      var_texto:=(Format('%-8s',[FieldByName('ClienteID').AsString]))+' - ' + Format('%-50s',[FieldByName('Nombre').AsString]);
//      //cbbClientes.Properties.Items.Add(var_texto);
//      Next;
//    end;
////    cbVendedor.Properties.Items.Add('<Agregar Nuevo>');
  QryArticulo.Active;
//  end;
end;

procedure TfrmFacturacion.TFacturaDAfterInsert(DataSet: TDataSet);
begin
//  edtArticulo.Clear;
end;

procedure TfrmFacturacion.TFacturaDAfterPost(DataSet: TDataSet);
begin
  edtArticulo.Clear;
end;

procedure TfrmFacturacion.Timer1Timer(Sender: TObject);
begin
  lblFecha.Caption:=FormatDateTime('yyyy/mm/dd hh:mm:ss am/pm',Now);
end;

procedure TfrmFacturacion.acInsertaLineaExecute(Sender: TObject);                         // Inserta Linea Detalle
begin
// Valida si el articulo ya existe en el GRID
  if TFacturaD.Locate('ArticuloID',cArticulo.CodigoArt,[
     loCaseInsensitive]) then begin
     TFacturaD.Edit;
     TFacturaDArticuloCantidad.AsInteger:=TFacturaDArticuloCantidad.AsInteger+
      cArticulo.Cantidad;
     TFacturaD.Post;
     acRecalcular.Execute;
     Exit;
   end;

  TFacturaD.Append;

  With cFacturaD do begin
//    Codigo:=StrToInt(cArticulo.CodigoArt);
    Codigo:=cArticulo.CodigoArt;
    Descripcion:=cArticulo.DescripcionArt;
    Cantidad:=cArticulo.Cantidad;
    PorCientoDescuento:=cCliente.Descuento;
    PrecioDesc:=Precio*(1-(PorCientoDescuento/100));

    if (cArticulo.Itbis='N') then
    begin
    //Linea sin Sacar TTBIS
      Precio:=cArticulo.Precio;
      PorCientoImpuestos:=0;
      PrecioImpuestos:=Precio;
      TotalImpuesto:=0;
    end
    else begin
    //Linea sacando ITBIS
      Precio:=(cArticulo.Precio)/(cVariables.Impuestos/100+1);
      PorCientoImpuestos:=cVariables.Impuestos;
      PrecioImpuestos:=Precio*(cVariables.Impuestos/100);
      TotalImpuesto:=PrecioImpuestos*Cantidad;
    end;

        Monto:=Precio*Cantidad;
        Importe:=monto+TotalImpuesto;
        TotalDescuento:=0;

    ManejaSeriales:=cArticulo.ManejaSeriales;
    Costo:=cArticulo.Costo;
  end;

  TFacturaDArticuloId.AsString:=cFacturaD.Codigo;
  TFacturaDArticuloDescripcion.AsString:=cFacturaD.Descripcion;
  TFacturaDArticuloCantidad.AsInteger:=cFacturaD.Cantidad;
  TFacturaDArticuloPrecio.AsFloat:=cFacturaD.Precio;
  TFacturaDArticuloPorcientoDesc.AsInteger:=cCliente.Descuento;
  TFacturaDArticuloMonto.AsFloat:= cFacturaD.Monto;
  TFacturaDArticuloTotalDescuento.AsFloat:=cFacturaD.TotalDescuento;
  TFacturaDArticuloTotalImpuesto.AsFloat:=cFacturaD.TotalImpuesto;
  TFacturaDArticuloImporte.AsFloat:=cFacturad.Importe;
  TFacturaDManejaSeriales.AsString:=cArticulo.ManejaSeriales;
  TFacturaDArticuloCosto.AsFloat:=cArticulo.Costo;

  TFacturaD.Post;
  acRecalcular.Execute;
end;

procedure TfrmFacturacion.acInsertaLineaSerialExecute(Sender: TObject);
begin
// Valida si el Serial ya existe en el GRID
  if not dsetSeriales.locate('ArticuloSerial',cArticulo.Serial,[
     loCaseInsensitive]) then
   begin
    with dsSeriales do begin
      DataSet.Insert;
      DataSet.FieldByName('ArticuloID').AsString:=cArticulo.CodigoArt;
      DataSet.FieldByName('ArticuloSerial').AsString:=cArticulo.Serial;
      DataSet.FieldByName('ArticuloLinea').Value:=TFacturaD.RecNo;
      DataSet.Post;
    end;
  end;
end;

procedure TfrmFacturacion.acRecalcularExecute(Sender: TObject);
begin
  FormatSettings.CurrencyDecimals:=2;
  with TFacturaD do
  begin
    TFacturaD.First;

    cFactura.SubTotal:=0;
    cFactura.Impuestos:=0;
    cFactura.Descuentos:=0;
    cFactura.Total:=0;

    while not tFacturaD.Eof do
    begin
// Calcula los Detalles de Factura Linea por Linea
      with cFacturaD do
      begin
        Codigo:=TFacturaDArticuloId.AsString;
        Descripcion:=TFacturaDArticuloDescripcion.AsString;
        Cantidad:=TFacturaDArticuloCantidad.AsInteger;
        Precio:=TFacturaDArticuloPrecio.AsFloat;
        Monto:=precio * Cantidad;
        Importe:=TFacturaDArticuloImporte.AsFloat;

        PorCientoDescuento:=TFacturaDArticuloPorcientoDesc.AsInteger;
        PrecioDesc:=precio*(1-(PorCientoDescuento/100));
        TotalDescuento:=Monto*(PorCientoDescuento/100);

        if(TFacturaDArticuloTotalImpuesto.AsInteger=0) then
        begin
          PorCientoImpuestos:=0;
          PrecioImpuestos:=PrecioDesc;
          TotalImpuesto:=0;
        end
        else begin
          PorCientoImpuestos:=cVariables.Impuestos;
          PrecioImpuestos:=PrecioDesc*(cVariables.Impuestos/100);
          TotalImpuesto:=PrecioImpuestos*Cantidad;

        end;

//        PorCientoImpuestos:=cVariables.Impuestos;
//        PrecioImpuestos:=PrecioDesc*(cVariables.Impuestos/100);
//        TotalImpuesto:=PrecioImpuestos*Cantidad;

        Importe:=Monto+TotalImpuesto-TotalDescuento;

        TFacturaD.Edit;
        TFacturaDArticuloPrecioDesc.AsFloat:=cFacturaD.PrecioDesc;
        TFacturaDArticuloPrecioImpuesto.AsFloat:=cFacturaD.PrecioImpuestos;
        TFacturaDArticuloPorcientoDesc.AsFloat:=cFacturaD.PorCientoDescuento;
        TFacturaDArticuloTotalImpuesto.AsFloat:=cFacturaD.TotalImpuesto;
        TFacturaDArticuloImporte.AsFloat:=cFacturad.Importe;
//        TFacturaD.Post;
      end;

// Postea de nuevo los datos
//    TFacturaD.Edit;
    TFacturaDArticuloId.AsString:=cFacturaD.Codigo;
    TFacturaDArticuloDescripcion.AsString:=cFacturaD.Descripcion;
    TFacturaDArticuloCantidad.AsInteger:=cFacturaD.Cantidad;
    //TFacturaDArticuloPrecio.AsFloat:=cFacturaD.Precio;
    TFacturaDArticuloMonto.AsFloat:= cFacturaD.Monto;
    TFacturaDArticuloTotalDescuento.AsFloat:=cFacturaD.TotalDescuento;
    TFacturaDArticuloPorcientoImpuesto.AsFloat:=cFacturaD.PorCientoImpuestos;
    TFacturaD.Post;
    TFacturaD.Next;
    end;
   end;
  acCalcularTotales.Execute;
  ControlaFormulario(Sender);
end;

procedure TfrmFacturacion.acRemoverLineaSerialExecute(Sender: TObject);                  // Remueve Linea Seriales
begin
  if  MessageBox(Handle,'Seguro que desea eliminar este serial?','Caja',MB_ICONQUESTION or MB_YESNO)=ID_YES then
  begin
    cArticulo.CodigoArt:=dsSeriales.DataSet.FieldByName('ArticuloID').AsString;
    dsSeriales.DataSet.Delete;
  end;
  if TFacturaD.Locate('ArticuloID',cArticulo.CodigoArt,[loCaseInsensitive]) then
   begin
     TFacturaD.Edit;
     TFacturaDArticuloCantidad.AsInteger:=TFacturaDArticuloCantidad.AsInteger-cArticulo.Cantidad;

     if TFacturaDArticuloCantidad.AsInteger=0 then
     begin
      TFacturaD.Delete;
      acRecalcular.Execute;
      Exit;
     end;
     TFacturaD.Post;
     acRecalcular.Execute;
//     Exit;
   end;
   acRecalcular.Execute;
end;

procedure TfrmFacturacion.actReImprimirExecute(Sender: TObject);                         // Imprimie la Factura
var
    FacturaNo : String;
begin
  //cVariables.NombreTicketReporte:='OrdenPedido';
  cVariables.Privilegio:='Facturacion.Imprimir';
  FacturaNo:=InputBox('Buscar Factura','Introduzca N�mero de Factura','0');
  datmain.qryTicket.Parameters.ParamByName('@PIDRegistro').Value:=StrToInt(FacturaNo);

  datmain.qryTicket.Close;
  datmain.qryTicket.Open;

  if (DatMain.qryTicket.FieldByName('fkTipoNCF').Value=6) then begin
    EscribirOrdenPedido(FacturaNo);
    MessageBox(Handle,'Buscar Orden de Pedido','Impresi�n de Ticket',MB_ICONINFORMATION or MB_OK);
    Exit;
  end;

  if datmain.qryTicket.RecordCount=0 then begin
    MessageDlg('Factura NO Existe',mtInformation,[mbOK],0);
    Exit;
  end;
  with frmPreview1 do begin
    prprtTicket.PrinterSetup.Copies:=1;
    prprtTicket.Template.DatabaseSettings.Name:=cVariables.NombreTicketReporte;
    prprtTicket.Template.LoadFromDatabase;
    plblTitulo.Text:='FACTURA(REIMPRESA)';

//    prprtTicket.DeviceType:='Screen';
//    prprtTicket.Print;
  end;
  cVariables.LargoTicket:=cValoresIni.PapelTamano;

// Definir tama�o del paper de acuerdo a la cantidad de registro en la factura
//  cVariables.LargoTicket:=cValoresIni.PapelTamano+(datmain.qryTicket.RecordCount*cValoresIni.RegistroTamano);

  frmPreview1.prprtTicket.PrinterSetup.PaperHeight:=cVariables.LargoTicket;

  fn_RegistrodeCambio(cVariables.PrivilegioElevadoUsuario,'Facturaci�n','Factura No.: '+FacturaNo+' Visualizada');

  if not Assigned(frmPreview1) then frmPreview1:=TfrmPreview1.Create(frmFacturacion);
  //ShowMessage(FloatToStr(cVariables.LargoTicket));
  frmPreview1.btnImprimir.Visible:=True;
  frmPreview1.ShowModal;
end;

procedure TfrmFacturacion.acCalcularTotalesExecute(Sender: TObject);
begin
  FormatSettings.CurrencyDecimals:=2;
  with TFacturaD do
  begin
    TFacturaD.First;

    cFactura.SubTotal:=0;
    cFactura.Impuestos:=0;
    cFactura.Descuentos:=0;
    cFactura.Total:=0;

    while not tFacturaD.Eof do
    begin
// Calcula los Totales
      cFactura.SubTotal:=(TFacturaDArticuloMonto.AsFloat+cFactura.SubTotal);
      cFactura.Descuentos:=TFacturaDArticuloTotalDescuento.AsFloat+cFactura.Descuentos;
      cFactura.Impuestos:=(TFacturaDArticuloTotalImpuesto.AsFloat+cFactura.Impuestos);
      cFactura.Total:=SimpleRoundTo((cFactura.SubTotal-cFactura.Descuentos+cFactura.Impuestos),-2);
      TFacturaD.Next;
  end;
// Redondeo
      txtSubTotal.Text:=Format('%m',[cFactura.SubTotal]);
      txtDescuentos.Text:=format('%m',[cFactura.Descuentos]);
      txtImpuestos.Text:=format('%m',[cFactura.Impuestos]);
      //txttotal.Value:=cFactura.Total;
      //txtTotal.Text:=format('%m',[cFactura.Total]);
      //cFactura.Total:=SimpleRoundTo(cFactura.Total,-2);
      txttotal.Value:=cfactura.Total;
      //ShowMessage('cfactura.total : '+floattostr(cfactura.Total));
  end;
end;

procedure TfrmFacturacion.acCambiarDescuentoExecute                                         // Cambia Precio de Articulo
(Sender: TObject;CodigoArticulo:string;Descuento:Integer);
begin
  TFacturaD.FieldByName('ArticuloPorcientoDesc').Value:=Descuento;
  TFacturaD.Post;
  fn_RegistrodeCambio(cVariables.PrivilegioElevadoUsuario,'Facturaci�n','Aplicar Descuento Articulo No.'
  + TFacturaD.FieldByName('ArticuloID').AsString + ' Descuento : '
  + TFacturaD.FieldByName('ArticuloPorcientoDesc').AsString+'%');

  acRecalcularExecute(Sender);
end;

procedure TfrmFacturacion.acCambiarPrecioExecute                                         // Cambia Precio de Articulo
(Sender: TObject;CodigoArticulo:string;Precio:Currency);
var
  Precio_Nuevo : Currency;

begin
  if (TFacturaD.FieldByName('ArticuloTotalImpuesto').AsInteger>0) then
  begin
    Precio_Nuevo:=precio/(cVariables.Impuestos/100+1);
//    TFacturaD.FieldByName('ArticuloPrecio').Value:=Precio_Nuevo;
  end
  else begin
      Precio_Nuevo:=precio;
  end;

//  Precio_Nuevo:=precio/(cVariables.Impuestos/100+1);
  TFacturaD.FieldByName('ArticuloPrecio').Value:=Precio_Nuevo;

  TFacturaD.Post;
  fn_RegistrodeCambio(cVariables.PrivilegioElevadoUsuario,'Facturaci�n','Cambio de Precio Articulo No.'
  +CodigoArticulo+' Precio Nuevo RD$'+FormatFloat ('###,###,##0.00',(Precio_Nuevo)));

  acRecalcularExecute(Sender);
end;

procedure TfrmFacturacion.acCancelarExecute(Sender: TObject);                            //Cancela Ventas / Limpia Formulario
begin
  TFacturaD.EmptyDataSet;
  dsetSeriales.EmptyDataSet;
  acRecalcular.Execute;
  edtClienteCodigo.Clear;
  edtClienteNombre.Clear;
  edtClienteRNC.Clear;
  edtArticulo.SetFocus;
  cCliente.Descuento:=0;
  cCliente.TarjetaID:='';
  cbbTipoNCF.ItemIndex:=0;
  cFactura.TipoNCF:=1;
  cFactura.Acepta_Pago_TarjetaCredito:='S';
  //jvswtchPrecios.StateOn:=False;
  //jvswtchDescuentos.StateOn:=False;

end;

procedure TfrmFacturacion.acConsultaRNCExecute(Sender: TObject);
var
  var_RNC : String;
begin
  with spConsultaRNC do
  begin
    Close;
    var_RNC:= InputBox('Consulta RNC','Indique N�mero RNC','');
    Parameters.ParamByName('@RNC').Value:=var_RNC;
    Open;
    if  RecordCount>0 then
    begin
      edtClienteNombre.Text:=FieldByName('Nombre').Value;
      //edtClienteRNC.Text:=var_RNC;
    end
    else
    begin
      ShowMessage('Registro NO Encontrado');
      edtClienteRNC.Clear;
    end;
  end;
end;

Procedure TfrmFacturacion.InsertaFactura(Sender: TObject);                               // Inserta Factura
var
  respuesta: string;

begin
//  ShowMessage('Poner C?digo para Insertar Factura');
  With spInsertaFactura.Parameters do
  begin
    ParamByName('@PidRegistro').Value:=var_No_Factura;
    ParamByName('@ClienteId').Value:=edtClienteCodigo.Text;
    ParamByName('@ClienteNombre').Value:=edtClienteNombre.Text;
    ParamByName('@ClienteRnc').Value:=edtClienteRNC.Text;
    ParamByName('@DocumentoFecha').Value:=FormatDateTime('YYYY-MM-DD',Date);
    ParamByName('@DocumentoServidor').Value:='N';

    ParamByName('@fkSucursal').Value:=cvariables.SucursalID;
    ParamByName('@fkUsuario').Value:=CUsuario.UsuarioID;
    ParamByName('@fkCaja').Value:=1;
    ParamByName('@fkRegistroCaja').Value:=1;
    ParamByName('@FormaPago').Value:='E';

    ParamByName('@SubTotal').Value:=cFactura.SubTotal;
    ParamByName('@TotalDesc').Value:=cFactura.Descuentos;
    ParamByName('@TotalItbis').Value:=cFactura.Impuestos;
    ParamByName('@Efectivo').Value:=cPago.Efectivo;
    ParamByName('@Cheque').Value:=cPago.Cheque;
    ParamByName('@NoCheque').Value:=cPago.NotaCreditoNumero;
    ParamByName('@Tarjeta').Value:=cPago.TarjetaCredito+cpago.TarjetaDebito;
    ParamByName('@NoTarjeta').Value:=0;
    ParamByName('@Ncredito').Value:=cPago.NotaCreditoMonto;

    ParamByName('@Codigo_Almacen').Value:=CUsuario.ID_Almacen;
    ParamByName('@MontoGeneral').Value:=cFactura.Total;
    ParamByName('@Codigo_Vendedor').Value:=cbbVendedor.Text;
    ParamByName('@NombrePC').Value:=cCaja.Nombre;
    ParamByName('@fkTipoNCF').Value:=cFactura.TipoNCF;
    ParamByName('@MOntoEfectivo').Value:=0;
    ParamByName('@MontoDevuelta').Value:=cPago.Devuelta;

    ParamByName('@PagoEfectivo').Value:=0;
    ParamByName('@PagoCheque').Value:=0;
    ParamByName('@PagoTarjetaCredito').Value:=0;
    ParamByName('@PagoTarjetaDebito').Value:=0;
    ParamByName('@PagoNotaCredito').Value:=0;

    repeat
      respuesta:='SI';
      Try
        spInsertaFactura.ExecProc;

      Except
      on E: Exception do
        begin
         respuesta:='NO';
         ShowMessage(e.Message);
         if MessageDlg('Error envio de factura, Enviarla de nuevo?',
          mtError,mbYesNo,0)=mrNo then exit;
        end;
     end;
    cFactura.Numero:=spInsertaFactura.Parameters.ParamByName('@return_Value').Value;
    //MessageDlg('Ticket: '+inttostr(cfactura.Numero)+' Generado',mtInformation,[mbOK],0);
    until Respuesta='SI';
  end;
end;

// ============================================================//
// Procedimiento para insertar detalle factura linea por linea //
// ============================================================//
Procedure TfrmFacturacion.InsertaFacturaDetalle(Sender: TObject);
var
  no_linea:Integer;
begin
  no_linea:=0;
//  ShowMessage('Poner C?digo para Insertar Factura');
  dsFacturaD.DataSet.First;
  with dsFacturaD do
  begin
    while not dataset.Eof do
    begin
      spInsertaFacturaDetalle.Parameters.ParamByName('@fkDocCabecera').Value:=cFactura.Numero;
      spInsertaFacturaDetalle.Parameters.ParamByName('@SecSerial').Value:=-1;
      spInsertaFacturaDetalle.Parameters.ParamByName('@No_Linea').value:=no_linea;
      spInsertaFacturaDetalle.Parameters.ParamByName('@ArticuloID').Value:=dataset.FieldByName('ArticuloID').AsString;
      spInsertaFacturaDetalle.Parameters.ParamByName('@ArticuloDescripcion').Value:=dataset.FieldByName('ArticuloDescripcion').AsString;
      spInsertaFacturaDetalle.Parameters.ParamByName('@ArticuloCosto').Value:=dataset.FieldByName('ArticuloCosto').AsFloat;
      spInsertaFacturaDetalle.Parameters.ParamByName('@ArticuloPrecio').Value:=dataset.FieldByName('ArticuloPrecio').AsFloat;
      spInsertaFacturaDetalle.Parameters.ParamByName('@ArticuloPrecioDesc').Value:=dataset.FieldByName('ArticuloPrecioDesc').AsFloat;
      spInsertaFacturaDetalle.Parameters.ParamByName('@ArticuloCantidad').Value:=dataset.FieldByName('ArticuloCantidad').AsFloat;
      spInsertaFacturaDetalle.Parameters.ParamByName('@ArticuloMonto').Value:=dataset.FieldByName('ArticuloMonto').AsFloat;
      spInsertaFacturaDetalle.Parameters.ParamByName('@ArticuloPorcientoDesc').Value:=dataset.FieldByName('ArticuloPorcientoDesc').AsFloat;
      spInsertaFacturaDetalle.Parameters.ParamByName('@ArticuloTotalDescuento').Value:=dataset.FieldByName('ArticuloTotalDescuento').AsFloat;
      spInsertaFacturaDetalle.Parameters.ParamByName('@ArticuloPorcientoImpuesto').Value:=dataset.FieldByName('ArticuloPorcientoImpuesto').AsFloat;
      spInsertaFacturaDetalle.Parameters.ParamByName('@ArticuloPrecioImpuesto').Value:=dataset.FieldByName('ArticuloPrecioImpuesto').AsFloat;
      spInsertaFacturaDetalle.Parameters.ParamByName('@ArticuloTotalImpuesto').Value:=dataset.FieldByName('ArticuloTotalImpuesto').AsFloat;
//      spInsertaFacturaDetalle.Parameters.ParamByName('@ArticuloTotal)
      spInsertaFacturaDetalle.Parameters.ParamByName('@ArticuloImporte').Value:=dataset.FieldByName('ArticuloImporte').AsFloat;
      spInsertaFacturaDetalle.Parameters.ParamByName('@ArticuloImporteNeto').Value:=dataset.FieldByName('ArticuloImporteNeto').AsFloat;
      spInsertaFacturaDetalle.Parameters.ParamByName('@fkUsuario').Value:=
        CUsuario.UsuarioID;
      spInsertaFacturaDetalle.Parameters.ParamByName('@Codigo_Almacen').Value:=CUsuario.ID_Almacen;
      spInsertaFacturaDetalle.Parameters.ParamByName('@PlanID').Value:=cFacturaD.PlanID;
      spInsertaFacturaDetalle.Parameters.ParamByName('@ManejaSeriales').Value:=
        cArticulo.ManejaSeriales;
//      spInsertaFacturaDetalle.Parameters.ParamByName('@FKSucursal').Value:=cVariables.SucursalID;
      spInsertaFacturaDetalle.Parameters.ParamByName('@FKSucursal').Value:=cUsuario.ID_Almacen;
      spInsertaFacturaDetalle.Parameters.ParamByName('@Estatus').Value:='N';

      spInsertaFacturaDetalle.ExecProc;

      dsFacturaD.DataSet.Next;
      no_linea:=no_linea+1;
    end;

  end;
end;

// ==============================================================//
// Procedimiento para insertar Serial de Factura Linea por Linea //
// ==============================================================//
Procedure TfrmFacturacion.InsertaFacturaDetalleSerial(Sender: TObject);                  // Inserta Seriales
var
  no_linea:Integer;
begin
  //no_linea:=0;
  dsSeriales.DataSet.First;
  with dsSeriales do
  begin
    while not dataset.Eof do
    begin
      cArticulo.CodigoArt:=dsSeriales.DataSet.FieldByName(
        'ArticuloID').AsString;
      frmFacturacion.TFacturaD.Locate('ArticuloID',cArticulo.CodigoArt,[
        loCaseInsensitive]);
       no_linea:=frmFacturacion.TFacturaD.RecNo-1;

      spInsertaFacturaDetalleSerial.Parameters.ParamByName('@Secuencia').Value:=-1;
      spInsertaFacturaDetalleSerial.Parameters.ParamByName('@FKDocCabecera').Value:=
        cFactura.Numero;
      spInsertaFacturaDetalleSerial.Parameters.ParamByName('@No_Linea').value:=
        no_linea;
      spInsertaFacturaDetalleSerial.Parameters.ParamByName('@FKArticulo').Value:=
        dsSeriales.DataSet.FieldByName('ArticuloID').AsString;
      spInsertaFacturaDetalleSerial.Parameters.ParamByName('@Serie').Value:=
        dsSeriales.DataSet.FieldByName('ArticuloSerial').AsString;
      spInsertaFacturaDetalleSerial.Parameters.ParamByName(
        '@Codigo_Almacen').Value:=CUsuario.ID_Almacen;
      spInsertaFacturaDetalleSerial.Parameters.ParamByName('@FKUsuario').Value:=
        CUsuario.UsuarioID;
      spInsertaFacturaDetalleSerial.Parameters.ParamByName('@Estatus').Value:=0;
      spInsertaFacturaDetalleSerial.Parameters.ParamByName('@FKSucursal').Value:=
        cUsuario.ID_Almacen;
//              spInsertaFacturaDetalleSerial.Parameters.ParamByName('@FKSucursal').Value:=
 //       cVariables.SucursalID;

      spInsertaFacturaDetalleSerial.ExecProc;

      dsSeriales.DataSet.Next;

    end;
  end;
end;

procedure TfrmFacturacion.jvswtchDescuentosDragDrop(Sender, Source: TObject; X,
  Y: Integer);
begin
  MainViewArticuloPrecio.Options.Editing:=True;
end;

procedure TfrmFacturacion.jvswtchDescuentosOn(Sender: TObject);
begin
  cVariables.Privilegio:='DESCUENTO';
  cVariables.PrivilegioElevado:=FALSE;
  cVariables.PrivilegioElevadoUsuario:='';

  if not Assigned(frmCustomLogin) then frmCustomLogin:=TfrmCustomLogin.Create(nil);
  frmCustomLogin.ShowModal;
  if cVariables.PrivilegioElevado=TRUE then
  begin
    cxgrdbclmnporcientoDescuento.Options.Editing:=True;
  end
  else
  begin
    jvswtchDescuentos.ToggleSwitch;
  end;

end;
procedure TfrmFacturacion.jvswtchDescuentosOff(Sender: TObject);
begin
  cxgrdbclmnporcientoDescuento.Options.Editing:=False;
end;

procedure TfrmFacturacion.jvswtchPreciosOn(Sender: TObject);
begin
  cVariables.Privilegio:='PRECIO';
  cVariables.PrivilegioElevado:=FALSE;
  cVariables.PrivilegioElevadoUsuario:='';

  if not Assigned(frmCustomLogin) then frmCustomLogin:=TfrmCustomLogin.Create(nil);
  frmCustomLogin.ShowModal;
  if cVariables.PrivilegioElevado=TRUE then
  begin
    MainViewArticuloPrecio.Options.Editing:=True;
  end
  else
  begin
    jvswtchPrecios.ToggleSwitch;
  end;
end;

procedure TfrmFacturacion.jvswtchPreciosOff(Sender: TObject);
begin
  MainViewArticuloPrecio.Options.Editing:=False;
end;

Procedure TfrmFacturacion.ControlaFormulario(Sender: TObject);
begin
//  ShowMessage('ControlaFormulario');
// Activa Boton de Pago
    if cFactura.Total> 0 then
    begin
      btnPago.Enabled:=true;
      btnBorrarLinea.Enabled:=True;
      btnCancelarFactura.Enabled:=True;
      btnImprimirFactura.Enabled:=False;

      grpInformacionCliente.Enabled:=false;
    end
    else
    begin
      btnPago.Enabled:=false;
      btnBorrarLinea.Enabled:=false;
      btnCancelarFactura.Enabled:=False;
      btnImprimirFactura.Enabled:=True;

      grpInformacionCliente.Enabled:=True;
    end;
end;

procedure TfrmFacturacion.MainViewArticuloCantidadPropertiesChange(  Sender: TObject);
begin
  if TFacturaD.FieldByName('ManejaSeriales').Value='Y' then
  Begin
    MessageDlg('Debe Seleccionar los seriales',mtError,[mbOK],0);
    //TFacturaD.Cancel;
  End;
end;

procedure TfrmFacturacion.MainViewArticuloCantidadPropertiesEditValueChanged(
  Sender: TObject);
begin
    acRecalcularExecute(Sender);
end;

procedure TfrmFacturacion.MainViewArticuloPrecioPropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
  if DisplayValue <= TFacturaD.FieldByName('ArticuloCosto').AsCurrency then
  begin
    MessageDlg('Precio NO Aceptado, es menor al costo del articulo',mtError,[mbOK],0);
    TFacturaD.Cancel;
    Exit;
  end;
  cArticulo.CodigoArt:=TFacturaD.FieldByName('ArticuloID').AsString;
  acCambiarPrecioExecute(Self,cArticulo.CodigoArt,DisplayValue);
end;

procedure TfrmFacturacion.OrdenDespacho1Click(Sender: TObject);
var
  var_pregunta : string;
begin
  var_pregunta:=InputBox('Autorizaci�n', #31'Clave', '');;
  if var_pregunta=cValoresIni.ClaveOrdenPedido then
  begin
    MessageBox(Handle,'AUTORIZADA','Orden de Pedido',MB_ICONINFORMATION or MB_OK);
    cFactura.TipoNCF:=6;
    //cVariables.NombreTicketReporte:='OrdenPedido';
    cVariables.ImprimirWebPos:=False;
    cVariables.ImprimirTicket:=False;
  end
  else begin
    cVariables.NombreTicketReporte:='Ticket25';
    cVariables.ImprimirWebPos:=True;
    MessageBox(Handle,'Clave Incorrecta','Orden de Pedido',MB_ICONERROR or MB_OK);
    end;

  try
    fn_RegistrodeCambio(frmMain.usrs1.CurrentUser.UserName,'Activar Pedido de Mercancia','Facturacion');

      Except
        on E:Exception do begin
          ShowMessage('Error'+e.Message);
        end;
      end;
end;

procedure TfrmFacturacion.MainViewArticuloCantidadPropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
var
  var_EnMano : Integer;
begin
  var_EnMano:=fn_ValidarCantidad(TFacturaD.FieldByName('ArticuloID').AsString,cUsuario.ID_Almacen);
  if DisplayValue>var_EnMano Then
    Begin
      MessageDlg('Cantidad Mayor al existente en Almacen. Existencia= '+IntToStr(var_EnMano),mtError,[mbOK],0);
      TFacturaD.Cancel;
      //Exit;
    End;
end;

end.

