unit UntConsultaFlujodeCaja;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, dxSkinsCore, dxSkinsDefaultPainters,
  dxSkinscxPCPainter, cxPCdxBarPopupMenu, Vcl.Menus, Vcl.ActnList, Data.DB,
  Data.Win.ADODB, JvComponentBase, JvDBGridExport, Vcl.StdCtrls, JvExStdCtrls,
  JvCombobox, Vcl.ComCtrls, JvExComCtrls, JvDateTimePicker, cxButtons,
  JvExControls, JvXPCore, JvXPButtons, Vcl.Grids, Vcl.DBGrids, JvExDBGrids,
  JvDBGrid, cxPC, Datasnap.DBClient;

type
  TfrmConsultaFlujodeCaja = class(TForm)
    lbl2: TLabel;
    lbl1: TLabel;
    lbl3: TLabel;
    cxpgcntrl1: TcxPageControl;
    cxtbsht1: TcxTabSheet;
    jvdbgrd1: TJvDBGrid;
    btn1: TJvXPToolButton;
    btnExportarExcel: TcxButton;
    jvdtmpckrHasta: TJvDateTimePicker;
    jvdtmpckrDesde: TJvDateTimePicker;
    cbbCaja: TJvComboBox;
    btnSalir: TcxButton;
    btnRefrescar: TcxButton;
    jvdbgrdxclxprt1: TJvDBGridExcelExport;
    sp1: TADOStoredProc;
    ds1: TDataSource;
    flsvdlg1: TFileSaveDialog;
    actlst1: TActionList;
    acRefrescar: TAction;
    acExportar: TAction;
    dtmfldsp1FechaDocumento: TDateTimeField;
    intgrfldsp1FacturaNoPOS: TIntegerField;
    wdstrngfldsp1ClienteId: TWideStringField;
    wdstrngfldsp1ClienteNombre: TWideStringField;
    wdstrngfldsp1ClienteRnc: TWideStringField;
    bcdfldsp1MontoGeneral: TBCDField;
    bcdfldsp1Ncredito: TBCDField;
    wdstrngfldsp1NombrePC: TWideStringField;
    lbl4: TLabel;
    dset2: TClientDataSet;
    lblTotal: TLabel;
    QryArticulosp1Total: TFMTBCDField;
    procedure btnExportarExcelClick(Sender: TObject);
    procedure acRefrescarExecute(Sender: TObject);
    procedure acExportarExecute(Sender: TObject);
    procedure btnRefrescarClick(Sender: TObject);
    procedure btnSalirClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure cbbCajaChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);

  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmConsultaFlujodeCaja: TfrmConsultaFlujodeCaja;

implementation

uses
  UntData;

{$R *.dfm}

procedure TfrmConsultaFlujodeCaja.acExportarExecute(Sender: TObject);
var
  archivo : String;
begin
  flsvdlg1.Execute;
  archivo:=flsvdlg1.FileName;
  if Length(archivo)>0 then begin
    jvdbgrdxclxprt1.FileName:=archivo;
    jvdbgrdxclxprt1.ExportGrid;
  end;
end;

procedure TfrmConsultaFlujodeCaja.acRefrescarExecute(Sender: TObject);
begin
  if cbbCaja.Text<>'' then
  begin
    btnExportarExcel.Enabled:=True;
  end
  else
  begin
    btnExportarExcel.Enabled:=False;
  end;
  sp1.Close;

  sp1.Parameters.ParamByName('@NombrePC').Value:=cbbCaja.Text;
  sp1.Parameters.ParamByName('@Fecha_Desde').Value:=FormatDateTime('YYYY-MM-DD',jvdtmpckrDesde.Date);
  sp1.Parameters.ParamByName('@Fecha_Hasta').Value:=FormatDateTime('YYYY-MM-DD',jvdtmpckrHasta.Date);

  sp1.Open;
  lblTotal.Caption:= FormatFloat('#,###,###0.00;-1#,###,###0.00;0',sp1.FieldByName('Total').AsFloat);
end;

procedure TfrmConsultaFlujodeCaja.btnExportarExcelClick(Sender: TObject);
begin
  acExportar.Execute;
end;

procedure TfrmConsultaFlujodeCaja.btnRefrescarClick(Sender: TObject);
begin
  fn_RegistrodeCambio(CUsuario.Usuario,'Consulta','Flujo de Caja del '+
  FormatDateTime('YYYY-MM-DD',jvdtmpckrDesde.Date)+' al '+
  FormatDateTime('YYYY-MM-DD',jvdtmpckrHasta.Date)+ ' Caja: '+
  cbbCaja.Text);
  acRefrescar.Execute;
end;

procedure TfrmConsultaFlujodeCaja.btnSalirClick(Sender: TObject);
begin
  Self.Close;
end;

procedure TfrmConsultaFlujodeCaja.cbbCajaChange(Sender: TObject);
begin
  btnRefrescar.Enabled:=True;
end;


procedure TfrmConsultaFlujodeCaja.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action:=caFree;
  frmConsultaFlujodeCaja:=nil;
end;

procedure TfrmConsultaFlujodeCaja.FormCreate(Sender: TObject);
begin
  Try
    datmain.conComun.Open;
   except
    on E:Exception do
    begin
      MessageDlg('Error Base de Datos (COMUN)NO Disponible '+ e.Message,mtError,[mbOK],0);
      Exit;
    end;
  End;

  with DatMain.qryCOMUN do
  begin
    SQL.Text:='SELECT DISTINCT nombrepc FROM [facturapos.cabecera] WHERE nombrepc'+
              ' IS NOT null ORDER BY nombrepc';
    Open;
    First;
    while not Eof do
    begin
      cbbCaja.Items.Add(FieldByName('NombrePC').AsString);
      Next;
    end;
    Close;
  end;
end;

procedure TfrmConsultaFlujodeCaja.FormShow(Sender: TObject);
begin
  jvdtmpckrDesde.Date:=Now;
  jvdtmpckrHasta.Date:=Now;
end;

end.
