object DatReportes: TDatReportes
  OldCreateOrder = False
  Height = 559
  Width = 897
  object pdbplnCuadreDeCaja: TppDBPipeline
    DataSource = dsCuadre_De_Caja
    RefreshAfterPost = True
    UserName = 'pdbplnCuadreDeCaja'
    Left = 64
    Top = 152
    object pfldCuadreDeCajappField1: TppField
      FieldAlias = 'CAJA'
      FieldName = 'CAJA'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 0
      Searchable = False
      Sortable = False
    end
    object pfldCuadreDeCajappField2: TppField
      FieldAlias = 'APERTURA'
      FieldName = 'APERTURA'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 1
      Searchable = False
      Sortable = False
    end
    object pfldCuadreDeCajappField3: TppField
      FieldAlias = 'EFECTIVO'
      FieldName = 'EFECTIVO'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 2
      Searchable = False
      Sortable = False
    end
    object pfldCuadreDeCajappField4: TppField
      FieldAlias = 'TARJETA'
      FieldName = 'TARJETA'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 3
      Searchable = False
      Sortable = False
    end
    object pfldCuadreDeCajappField5: TppField
      FieldAlias = 'CHEQUE'
      FieldName = 'CHEQUE'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 4
      Searchable = False
      Sortable = False
    end
    object pfldCuadreDeCajappField6: TppField
      FieldAlias = 'NCREDITO'
      FieldName = 'NCREDITO'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 5
      Searchable = False
      Sortable = False
    end
    object pfldCuadreDeCajappField7: TppField
      FieldAlias = 'TCAJA'
      FieldName = 'TCAJA'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 6
      Searchable = False
      Sortable = False
    end
    object pfldCuadreDeCajappField8: TppField
      FieldAlias = 'ESTADODECAJA'
      FieldName = 'ESTADODECAJA'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 7
      Searchable = False
      Sortable = False
    end
    object pfldCuadreDeCajappField9: TppField
      FieldAlias = 'MONTODECIERRE'
      FieldName = 'MONTODECIERRE'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 8
      Searchable = False
      Sortable = False
    end
    object pfldCuadreDeCajappField10: TppField
      FieldAlias = 'DIFERENCIA'
      FieldName = 'DIFERENCIA'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 9
      Searchable = False
      Sortable = False
    end
  end
  object dsCuadre_De_Caja: TDataSource
    AutoEdit = False
    DataSet = spRPTCuadreDeCaja
    Left = 64
    Top = 80
  end
  object prprtReport: TppReport
    AutoStop = False
    DataPipeline = pdbplnCuadreDeCaja
    PrinterSetup.BinName = 'Default'
    PrinterSetup.DocumentName = 'Report'
    PrinterSetup.PaperName = 'Letter'
    PrinterSetup.PrinterName = 'Default'
    PrinterSetup.SaveDeviceSettings = False
    PrinterSetup.mmMarginBottom = 6350
    PrinterSetup.mmMarginLeft = 6350
    PrinterSetup.mmMarginRight = 6350
    PrinterSetup.mmMarginTop = 6350
    PrinterSetup.mmPaperHeight = 279400
    PrinterSetup.mmPaperWidth = 215900
    PrinterSetup.PaperSize = 1
    Template.DatabaseSettings.DataPipeline = DatMain.pdbplnReportes
    Template.DatabaseSettings.Name = 'cuadre de Caja'
    Template.DatabaseSettings.NameField = 'Name'
    Template.DatabaseSettings.TemplateField = 'Template'
    Template.FileName = 
      'D:\vesslaBG\0.-SOURCE CODES\JMI\POS-nuevo\DATABASE Changes\Repor' +
      'te Cuadre de Caja V4.rtm'
    Template.SaveTo = stDatabase
    Template.Format = ftASCII
    ArchiveFileName = '($MyDocuments)\ReportArchive.raf'
    CachePages = True
    DeviceType = 'Screen'
    DefaultFileDeviceType = 'PDF'
    EmailSettings.ReportFormat = 'PDF'
    LanguageID = 'Default'
    OpenFile = False
    OutlineSettings.CreateNode = True
    OutlineSettings.CreatePageNodes = True
    OutlineSettings.Enabled = True
    OutlineSettings.Visible = True
    ThumbnailSettings.Enabled = True
    ThumbnailSettings.Visible = True
    ThumbnailSettings.DeadSpace = 30
    PDFSettings.EmbedFontOptions = [efUseSubset]
    PDFSettings.EncryptSettings.AllowCopy = True
    PDFSettings.EncryptSettings.AllowInteract = True
    PDFSettings.EncryptSettings.AllowModify = True
    PDFSettings.EncryptSettings.AllowPrint = True
    PDFSettings.EncryptSettings.Enabled = False
    PDFSettings.EncryptSettings.KeyLength = kl40Bit
    PDFSettings.FontEncoding = feAnsi
    PDFSettings.ImageCompressionLevel = 25
    PreviewFormSettings.WindowState = wsMaximized
    RTFSettings.DefaultFont.Charset = DEFAULT_CHARSET
    RTFSettings.DefaultFont.Color = clWindowText
    RTFSettings.DefaultFont.Height = -13
    RTFSettings.DefaultFont.Name = 'Arial'
    RTFSettings.DefaultFont.Style = []
    TextFileName = '($MyDocuments)\Report.pdf'
    TextSearchSettings.DefaultString = '<FindText>'
    TextSearchSettings.Enabled = True
    XLSSettings.AppName = 'ReportBuilder'
    XLSSettings.Author = 'ReportBuilder'
    XLSSettings.Subject = 'Report'
    XLSSettings.Title = 'Report'
    Left = 64
    Top = 216
    Version = '15.01'
    mmColumnWidth = 0
    DataPipelineName = 'pdbplnCuadreDeCaja'
    object ptlbnd1: TppTitleBand
      Background.Brush.Style = bsClear
      mmBottomOffset = 0
      mmHeight = 23548
      mmPrintPosition = 0
      object plbl1: TppLabel
        UserName = 'plbl1'
        Caption = 'CUADRE DIARIO DE CAJA'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 14
        Font.Style = []
        TextAlignment = taCentered
        Transparent = True
        mmHeight = 5821
        mmLeft = 68225
        mmTop = 5556
        mmWidth = 71438
        BandType = 1
        LayerName = BandLayer3
      end
      object plblfecha: TppLabel
        UserName = 'plblfecha'
        Caption = 'Fecha desde Hasta'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        Transparent = True
        mmHeight = 4233
        mmLeft = 6350
        mmTop = 13494
        mmWidth = 29898
        BandType = 1
        LayerName = BandLayer3
      end
      object pln1: TppLine
        UserName = 'pln1'
        Style = lsDouble
        Weight = 0.750000000000000000
        mmHeight = 3969
        mmLeft = 6350
        mmTop = 18785
        mmWidth = 190765
        BandType = 1
        LayerName = BandLayer3
      end
    end
    object phdrbnd1: TppHeaderBand
      Background.Brush.Style = bsClear
      mmBottomOffset = 0
      mmHeight = 14817
      mmPrintPosition = 0
      object plbl3: TppLabel
        UserName = 'plbl3'
        Caption = 'Informe Cuadre Caja.:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlue
        Font.Name = 'Arial'
        Font.Size = 12
        Font.Style = [fsBold, fsUnderline]
        Transparent = True
        mmHeight = 5027
        mmLeft = 6350
        mmTop = 1588
        mmWidth = 43921
        BandType = 0
        LayerName = BandLayer3
      end
      object pdbtxtCAJA: TppDBText
        UserName = 'pdbtxtCAJA'
        DataField = 'CAJA'
        DataPipeline = pdbplnCuadreDeCaja
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 12
        Font.Style = [fsBold]
        Transparent = True
        DataPipelineName = 'pdbplnCuadreDeCaja'
        mmHeight = 4763
        mmLeft = 52917
        mmTop = 1588
        mmWidth = 42069
        BandType = 0
        LayerName = BandLayer3
      end
      object plbl2: TppLabel
        UserName = 'plbl12'
        Caption = 'Responsable de Caja.:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlue
        Font.Name = 'Arial'
        Font.Size = 12
        Font.Style = [fsBold, fsUnderline]
        Transparent = True
        mmHeight = 5027
        mmLeft = 6350
        mmTop = 8202
        mmWidth = 45508
        BandType = 0
        LayerName = BandLayer3
      end
      object plblEstadodeCaja: TppLabel
        UserName = 'plbEstadodeCaja'
        AutoSize = False
        Caption = 'ESTADODECAJA'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 18
        Font.Style = [fsBold]
        TextAlignment = taCentered
        Transparent = True
        VerticalAlignment = avCenter
        mmHeight = 10394
        mmLeft = 102659
        mmTop = 1588
        mmWidth = 94683
        BandType = 0
        LayerName = BandLayer3
      end
      object plbl13: TppLabel
        UserName = 'plblfecha1'
        Caption = 'Fecha desde Hasta'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        Transparent = True
        mmHeight = 4233
        mmLeft = 6350
        mmTop = 13758
        mmWidth = 29898
        BandType = 0
        LayerName = BandLayer3
      end
    end
    object pdtlbnd1: TppDetailBand
      Background1.Brush.Style = bsClear
      Background2.Brush.Style = bsClear
      PrintHeight = phDynamic
      mmBottomOffset = 0
      mmHeight = 212725
      mmPrintPosition = 0
      object plbl7: TppLabel
        UserName = 'plbl7'
        AutoSize = False
        Caption = 'Ventas Efectivo'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 12
        Font.Style = []
        Transparent = True
        mmHeight = 4763
        mmLeft = 47096
        mmTop = 11642
        mmWidth = 52388
        BandType = 4
        LayerName = BandLayer3
      end
      object pdbtxtEFECTIVO: TppDBText
        UserName = 'pdbtxtEFECTIVO'
        DataField = 'EFECTIVO'
        DataPipeline = pdbplnCuadreDeCaja
        DisplayFormat = '#,0.00;(#,0.00)'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 12
        Font.Style = []
        TextAlignment = taRightJustified
        Transparent = True
        DataPipelineName = 'pdbplnCuadreDeCaja'
        mmHeight = 4763
        mmLeft = 159809
        mmTop = 11642
        mmWidth = 34396
        BandType = 4
        LayerName = BandLayer3
      end
      object plbl8: TppLabel
        UserName = 'plbl8'
        AutoSize = False
        Caption = 'Apertura de Caja'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 12
        Font.Style = []
        Transparent = True
        mmHeight = 4763
        mmLeft = 46831
        mmTop = 5292
        mmWidth = 52652
        BandType = 4
        LayerName = BandLayer3
      end
      object pdbtxtAPERTURA: TppDBText
        UserName = 'pdbtxtAPERTURA'
        DataField = 'APERTURA'
        DataPipeline = pdbplnCuadreDeCaja
        DisplayFormat = '#,0.00;(#,0.00)'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 12
        Font.Style = []
        TextAlignment = taRightJustified
        Transparent = True
        DataPipelineName = 'pdbplnCuadreDeCaja'
        mmHeight = 4763
        mmLeft = 159809
        mmTop = 5292
        mmWidth = 34396
        BandType = 4
        LayerName = BandLayer3
      end
      object plbl6: TppLabel
        UserName = 'plbl9'
        AutoSize = False
        Caption = 'Ventas Cheque'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 12
        Font.Style = []
        Transparent = True
        mmHeight = 4763
        mmLeft = 46831
        mmTop = 17198
        mmWidth = 52388
        BandType = 4
        LayerName = BandLayer3
      end
      object pdbtxtTARJETA: TppDBText
        UserName = 'pdbtxtTARJETA'
        DataField = 'TARJETA'
        DataPipeline = pdbplnCuadreDeCaja
        DisplayFormat = '#,0.00;(#,0.00)'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 12
        Font.Style = []
        TextAlignment = taRightJustified
        Transparent = True
        DataPipelineName = 'pdbplnCuadreDeCaja'
        mmHeight = 4763
        mmLeft = 159809
        mmTop = 23019
        mmWidth = 34018
        BandType = 4
        LayerName = BandLayer3
      end
      object plbl9: TppLabel
        UserName = 'plbl10'
        AutoSize = False
        Caption = 'Ventas Tarjeta'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 12
        Font.Style = []
        Transparent = True
        mmHeight = 4763
        mmLeft = 46831
        mmTop = 23019
        mmWidth = 52652
        BandType = 4
        LayerName = BandLayer3
      end
      object pdbtxtNCREDITO: TppDBText
        UserName = 'pdbtxtNCREDITO'
        DataField = 'NCREDITO'
        DataPipeline = pdbplnCuadreDeCaja
        DisplayFormat = '(#,0.00);(#,0.00)'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Name = 'Arial'
        Font.Size = 12
        Font.Style = []
        TextAlignment = taRightJustified
        Transparent = True
        DataPipelineName = 'pdbplnCuadreDeCaja'
        mmHeight = 4763
        mmLeft = 159279
        mmTop = 28575
        mmWidth = 34774
        BandType = 4
        LayerName = BandLayer3
      end
      object plbl10: TppLabel
        UserName = 'plbl11'
        AutoSize = False
        Caption = 'Notas de Credito'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 12
        Font.Style = []
        Transparent = True
        mmHeight = 4763
        mmLeft = 46567
        mmTop = 28575
        mmWidth = 52652
        BandType = 4
        LayerName = BandLayer3
      end
      object pdbtxtTCAJA: TppDBText
        UserName = 'pdbtxtTCAJA'
        Border.Color = clAqua
        Color = clRed
        DataField = 'TCAJA'
        DataPipeline = pdbplnCuadreDeCaja
        DisplayFormat = '#,0.00;(#,0.00)'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 12
        Font.Style = [fsBold]
        TextAlignment = taRightJustified
        Transparent = True
        DataPipelineName = 'pdbplnCuadreDeCaja'
        mmHeight = 4763
        mmLeft = 159317
        mmTop = 40217
        mmWidth = 34774
        BandType = 4
        LayerName = BandLayer3
      end
      object plbl4: TppLabel
        UserName = 'plbl4'
        AutoSize = False
        Caption = 'Total caja RD$ ----------->'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 12
        Font.Style = [fsBold]
        Transparent = True
        mmHeight = 4763
        mmLeft = 100164
        mmTop = 40217
        mmWidth = 51783
        BandType = 4
        LayerName = BandLayer3
      end
      object pdbtxtCHEQUE: TppDBText
        UserName = 'pdbtxtCHEQUE'
        DataField = 'CHEQUE'
        DataPipeline = pdbplnCuadreDeCaja
        DisplayFormat = '#,0.00;(#,0.00)'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 12
        Font.Style = []
        TextAlignment = taRightJustified
        Transparent = True
        DataPipelineName = 'pdbplnCuadreDeCaja'
        mmHeight = 4763
        mmLeft = 159809
        mmTop = 17198
        mmWidth = 34396
        BandType = 4
        LayerName = BandLayer3
      end
      object plbl5: TppLabel
        UserName = 'plbl5'
        AutoSize = False
        Caption = 'Detalle Recibo de Caja'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 12
        Font.Style = []
        Transparent = True
        mmHeight = 4763
        mmLeft = 6350
        mmTop = 56621
        mmWidth = 51594
        BandType = 4
        LayerName = BandLayer3
      end
      object pln2: TppLine
        UserName = 'pln2'
        Style = lsDouble
        Weight = 0.750000000000000000
        mmHeight = 3969
        mmLeft = 6237
        mmTop = 61913
        mmWidth = 188422
        BandType = 4
        LayerName = BandLayer3
      end
      object prchtxt1: TppRichText
        UserName = 'prchtxt1'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 12
        Font.Style = []
        Caption = 'prchtxt1'
        RichText = 
          '{\rtf1\ansi\ansicpg1252\deff0\deflang1033{\fonttbl{\f0\fnil\fcha' +
          'rset0 Century Gothic;}{\f1\fnil\fcharset0 Arial;}{\f2\fnil Arial' +
          ';}}'#13#10'{\colortbl ;\red0\green0\blue0;}'#13#10'\viewkind4\uc1\pard\fi-56' +
          '0\li560\qj\cf1\f0\fs34 _________\tab de 2,000 \tab\tab RD$ = ___' +
          '_____\par'#13#10'\pard\qj _________ \tab de 1,000\tab\tab\tab RD$= ___' +
          '______\par'#13#10'_________\tab de    500\tab\tab\tab RD$= _________\p' +
          'ar'#13#10'_________\tab de    200\tab\tab\tab RD$= _________\par'#13#10'____' +
          '_____\tab de    100\tab\tab\tab RD$= _________\par'#13#10'_________\ta' +
          'b de      50 \tab\tab RD$= _________\par'#13#10'_________ \tab de     ' +
          ' 20 \tab\tab RD$= _________\par'#13#10'\tab\tab\tab Monedas\tab\tab RD' +
          '$= _________\par'#13#10'\tab\tab\par'#13#10'\tab\tab Total Efectivo \tab\tab' +
          ' RD$= _________\par'#13#10'\tab\tab Total Tarjetas\tab\tab\tab RD$= __' +
          '_______\par'#13#10'\tab\tab Total Cheques\tab\tab RD$= _________\par'#13#10 +
          '\tab\tab Total Gastos\tab\tab\tab RD$= _________\par'#13#10'\tab\tab T' +
          'otal Transf. Bco.\tab\tab RD$= _________\par'#13#10'\tab\tab\tab\par'#13#10 +
          '\tab\tab Total Seg\'#39'fan Caja\tab\tab RD$= _________\par'#13#10'\tab\ta' +
          'b Diferencia (+ \'#39'f3 -)\tab\tab RD$= _________\par'#13#10'\par'#13#10'\par'#13#10 +
          '\f1\par'#13#10'\par'#13#10'\par'#13#10'\pard\f2\par'#13#10'}'#13#10#0
        RemoveEmptyLines = False
        Stretch = True
        Transparent = True
        mmHeight = 116228
        mmLeft = 6350
        mmTop = 70909
        mmWidth = 188913
        BandType = 4
        LayerName = BandLayer3
        mmBottomOffset = 0
        mmOverFlowOffset = 0
        mmStopPosition = 0
        mmMinHeight = 0
      end
      object prchtxt2: TppRichText
        UserName = 'prchtxt2'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 12
        Font.Style = []
        Caption = 'prchtxt2'
        RichText = 
          '{\rtf1\ansi\ansicpg1252\deff0\deflang1033{\fonttbl{\f0\fnil\fcha' +
          'rset0 Century Gothic;}{\f1\fnil\fcharset0 Arial;}{\f2\fnil Arial' +
          ';}}'#13#10'{\colortbl ;\red0\green0\blue0;}'#13#10'\viewkind4\uc1\pard\qj\cf' +
          '1\f0\fs28 ____________________\tab\tab ____________________\tab\' +
          'tab ____________________\par'#13#10'CAJERO\tab (A)\tab\tab\tab REVISAD' +
          'O POR\tab\tab\tab RECIBIDO POR\par'#13#10'\f1\par'#13#10'\pard\f2\fs24\par'#13#10 +
          '}'#13#10#0
        RemoveEmptyLines = False
        Transparent = True
        mmHeight = 13229
        mmLeft = 6350
        mmTop = 197644
        mmWidth = 186796
        BandType = 4
        LayerName = BandLayer3
        mmBottomOffset = 0
        mmOverFlowOffset = 0
        mmStopPosition = 0
        mmMinHeight = 0
      end
      object plbl15: TppLabel
        UserName = 'plbl15'
        AutoSize = False
        Caption = 'Total Cierre RD$ -------->'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 12
        Font.Style = [fsBold]
        Transparent = True
        mmHeight = 4763
        mmLeft = 100542
        mmTop = 47361
        mmWidth = 51594
        BandType = 4
        LayerName = BandLayer3
      end
      object plbl11: TppLabel
        UserName = 'plbl16'
        AutoSize = False
        Caption = 'Diferencia RD$ ---------->'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 12
        Font.Style = [fsBold]
        Transparent = True
        mmHeight = 4763
        mmLeft = 100542
        mmTop = 53975
        mmWidth = 51594
        BandType = 4
        LayerName = BandLayer3
      end
      object pdbtxtDIFERENCIA: TppDBText
        UserName = 'pdbtxtDIFERENCIA'
        Border.Color = clAqua
        Color = clRed
        DataField = 'DIFERENCIA'
        DataPipeline = pdbplnCuadreDeCaja
        DisplayFormat = '#,0.00;(#,0.00)'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Name = 'Arial'
        Font.Size = 12
        Font.Style = [fsBold]
        TextAlignment = taRightJustified
        Transparent = True
        DataPipelineName = 'pdbplnCuadreDeCaja'
        mmHeight = 4763
        mmLeft = 158750
        mmTop = 53711
        mmWidth = 34963
        BandType = 4
        LayerName = BandLayer3
      end
      object pdbtxtTCIERRE: TppDBText
        UserName = 'pdbtxtTCIERRE'
        Border.Color = clAqua
        Color = clRed
        DataField = 'MONTODECIERRE'
        DataPipeline = pdbplnCuadreDeCaja
        DisplayFormat = '#,0.00;(#,0.00)'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 12
        Font.Style = [fsBold]
        TextAlignment = taRightJustified
        Transparent = True
        DataPipelineName = 'pdbplnCuadreDeCaja'
        mmHeight = 4763
        mmLeft = 159317
        mmTop = 47096
        mmWidth = 34774
        BandType = 4
        LayerName = BandLayer3
      end
    end
    object pftrbnd1: TppFooterBand
      Background.Brush.Style = bsClear
      mmBottomOffset = 0
      mmHeight = 13229
      mmPrintPosition = 0
      object psystmvrbl1: TppSystemVariable
        UserName = 'psystmvrbl1'
        VarType = vtDateTime
        DisplayFormat = 'yyyy/MM/dd h:nn:ss AM/PM'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        Transparent = True
        mmHeight = 3704
        mmLeft = 5292
        mmTop = 2646
        mmWidth = 30691
        BandType = 8
        LayerName = BandLayer3
      end
      object psystmvrbl2: TppSystemVariable
        UserName = 'psystmvrbl2'
        VarType = vtDocumentName
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        Transparent = True
        mmHeight = 3704
        mmLeft = 5292
        mmTop = 8467
        mmWidth = 73819
        BandType = 8
        LayerName = BandLayer3
      end
    end
    object pdsgnlyrs1: TppDesignLayers
      object pdsgnlyr1: TppDesignLayer
        UserName = 'BandLayer3'
        LayerType = ltBanded
        Index = 0
      end
    end
    object prmtrlst1: TppParameterList
    end
  end
  object spRPTCuadreDeCaja: TADOStoredProc
    Connection = DatMain.conComun
    CursorType = ctStatic
    ProcedureName = 'sp_RPTCUADREDECAJA;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@i_date'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@terminal'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end>
    Left = 64
    Top = 24
    object strngfldRPTCuadreDeCajaCAJA: TStringField
      FieldName = 'CAJA'
      ReadOnly = True
      Size = 50
    end
    object QryArticuloRPTCuadreDeCajaAPERTURA: TFMTBCDField
      FieldName = 'APERTURA'
      ReadOnly = True
      Precision = 38
      Size = 2
    end
    object QryArticuloRPTCuadreDeCajaEFECTIVO: TFMTBCDField
      FieldName = 'EFECTIVO'
      ReadOnly = True
      Precision = 38
      Size = 4
    end
    object QryArticuloRPTCuadreDeCajaTARJETA: TFMTBCDField
      FieldName = 'TARJETA'
      ReadOnly = True
      Precision = 38
      Size = 2
    end
    object QryArticuloRPTCuadreDeCajaCHEQUE: TFMTBCDField
      FieldName = 'CHEQUE'
      ReadOnly = True
      Precision = 38
      Size = 2
    end
    object QryArticuloRPTCuadreDeCajaNCREDITO: TFMTBCDField
      FieldName = 'NCREDITO'
      ReadOnly = True
      Precision = 38
      Size = 2
    end
    object QryArticuloRPTCuadreDeCajaTCAJA: TFMTBCDField
      FieldName = 'TCAJA'
      ReadOnly = True
      Precision = 38
      Size = 2
    end
    object strngfldRPTCuadreDeCajaESTADODECAJA: TStringField
      FieldName = 'ESTADODECAJA'
      ReadOnly = True
      FixedChar = True
      Size = 1
    end
    object bcdfldDatReportesMONTODECIERRE: TBCDField
      FieldName = 'MONTODECIERRE'
      ReadOnly = True
      Precision = 18
      Size = 2
    end
    object QryArticuloRPTCuadreDeCajaDIFERENCIA: TFMTBCDField
      FieldName = 'DIFERENCIA'
      ReadOnly = True
      Precision = 38
      Size = 2
    end
  end
  object sp_RPT_NC_Conciliadas: TADOStoredProc
    Active = True
    Connection = DatMain.con1
    CursorType = ctStatic
    ProcedureName = 'sp_RPT_NC_Conciliadas;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@Fecha_Conciliacion_Desde'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = 42370d
      end
      item
        Name = '@Fecha_Conciliacion_Hasta'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = 42735d
      end
      item
        Name = '@Conciliacion_UsuarioID'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 67
      end>
    Left = 600
    Top = 16
    object dtmfld_RPT_NC_ConciliadasFecha: TDateTimeField
      FieldName = 'Fecha'
    end
    object intgrfld_RPT_NC_ConciliadasNumeroReq: TIntegerField
      FieldName = 'NumeroReq'
    end
    object intgrfld_RPT_NC_ConciliadasNumeroNC: TIntegerField
      FieldName = 'NumeroNC'
    end
    object wdstrngfld_RPT_NC_ConciliadasArticuloID: TWideStringField
      FieldName = 'ArticuloID'
    end
    object wdstrngfld_RPT_NC_ConciliadasArticuloDescripcion: TWideStringField
      FieldName = 'ArticuloDescripcion'
      Size = 100
    end
    object wdstrngfld_RPT_NC_ConciliadasSerie: TWideStringField
      FieldName = 'Serie'
      Size = 32
    end
    object wdstrngfld_RPT_NC_ConciliadasRazon: TWideStringField
      FieldName = 'Razon'
      Size = 250
    end
    object wdstrngfld_RPT_NC_ConciliadasNombre: TWideStringField
      FieldName = 'Nombre'
    end
    object dtmfld_RPT_NC_ConciliadasFecha_Conciliacion: TDateTimeField
      FieldName = 'Fecha_Conciliacion'
    end
  end
  object dsRPT_NC_Conciliadas: TDataSource
    AutoEdit = False
    DataSet = sp_RPT_NC_Conciliadas
    Left = 600
    Top = 80
  end
  object pdbplnNC_Conciliadas: TppDBPipeline
    DataSource = dsRPT_NC_Conciliadas
    SkipWhenNoRecords = False
    UserName = 'NC_Conciliadas'
    Left = 600
    Top = 152
    object pfldNC_ConciliadasppField1: TppField
      FieldAlias = 'Fecha'
      FieldName = 'Fecha'
      FieldLength = 0
      DataType = dtDateTime
      DisplayWidth = 18
      Position = 0
    end
    object pfldNC_ConciliadasppField2: TppField
      Alignment = taRightJustify
      FieldAlias = 'NumeroReq'
      FieldName = 'NumeroReq'
      FieldLength = 0
      DataType = dtInteger
      DisplayWidth = 10
      Position = 1
    end
    object pfldNC_ConciliadasppField3: TppField
      Alignment = taRightJustify
      FieldAlias = 'NumeroNC'
      FieldName = 'NumeroNC'
      FieldLength = 0
      DataType = dtInteger
      DisplayWidth = 10
      Position = 2
    end
    object pfldNC_ConciliadasppField4: TppField
      FieldAlias = 'ArticuloID'
      FieldName = 'ArticuloID'
      FieldLength = 20
      DisplayWidth = 20
      Position = 3
    end
    object pfldNC_ConciliadasppField5: TppField
      FieldAlias = 'ArticuloDescripcion'
      FieldName = 'ArticuloDescripcion'
      FieldLength = 100
      DisplayWidth = 100
      Position = 4
    end
    object pfldNC_ConciliadasppField6: TppField
      FieldAlias = 'Serie'
      FieldName = 'Serie'
      FieldLength = 32
      DisplayWidth = 32
      Position = 5
    end
    object pfldNC_ConciliadasppField7: TppField
      FieldAlias = 'Razon'
      FieldName = 'Razon'
      FieldLength = 250
      DisplayWidth = 250
      Position = 6
    end
    object pfldNC_ConciliadasppField8: TppField
      FieldAlias = 'Nombre'
      FieldName = 'Nombre'
      FieldLength = 20
      DisplayWidth = 20
      Position = 7
    end
    object pfldNC_ConciliadasppField9: TppField
      FieldAlias = 'Fecha_Conciliacion'
      FieldName = 'Fecha_Conciliacion'
      FieldLength = 0
      DataType = dtDateTime
      DisplayWidth = 18
      Position = 8
    end
  end
  object RptNC_Conciliadas: TppReport
    AutoStop = False
    DataPipeline = pdbplnNC_Conciliadas
    PrinterSetup.BinName = 'Default'
    PrinterSetup.DocumentName = 'Report'
    PrinterSetup.Orientation = poLandscape
    PrinterSetup.PaperName = 'Letter'
    PrinterSetup.PrinterName = 'Default'
    PrinterSetup.SaveDeviceSettings = False
    PrinterSetup.mmMarginBottom = 6350
    PrinterSetup.mmMarginLeft = 6350
    PrinterSetup.mmMarginRight = 6350
    PrinterSetup.mmMarginTop = 6350
    PrinterSetup.mmPaperHeight = 215900
    PrinterSetup.mmPaperWidth = 279400
    PrinterSetup.PaperSize = 1
    Template.DatabaseSettings.DataPipeline = DatMain.pdbplnReportes
    Template.DatabaseSettings.Name = 'NC Conciliadas'
    Template.DatabaseSettings.NameField = 'Name'
    Template.DatabaseSettings.TemplateField = 'Template'
    Template.FileName = 
      'C:\VesslaBG\0.-SOURCE CODES\0.-JMI\POS\VERSION_2.8 (Posible)\NC ' +
      'Conciliadas.rtm'
    Template.SaveTo = stDatabase
    Template.Format = ftASCII
    ArchiveFileName = '($MyDocuments)\ReportArchive.raf'
    CachePages = True
    DeviceType = 'Screen'
    DefaultFileDeviceType = 'PDF'
    EmailSettings.ReportFormat = 'PDF'
    LanguageID = 'Default'
    OpenFile = False
    OutlineSettings.CreateNode = True
    OutlineSettings.CreatePageNodes = True
    OutlineSettings.Enabled = True
    OutlineSettings.Visible = True
    ThumbnailSettings.Enabled = True
    ThumbnailSettings.Visible = True
    ThumbnailSettings.DeadSpace = 30
    PDFSettings.EmbedFontOptions = [efUseSubset]
    PDFSettings.EncryptSettings.AllowCopy = True
    PDFSettings.EncryptSettings.AllowInteract = True
    PDFSettings.EncryptSettings.AllowModify = True
    PDFSettings.EncryptSettings.AllowPrint = True
    PDFSettings.EncryptSettings.Enabled = False
    PDFSettings.EncryptSettings.KeyLength = kl40Bit
    PDFSettings.FontEncoding = feAnsi
    PDFSettings.ImageCompressionLevel = 25
    PreviewFormSettings.WindowState = wsMaximized
    RTFSettings.DefaultFont.Charset = DEFAULT_CHARSET
    RTFSettings.DefaultFont.Color = clWindowText
    RTFSettings.DefaultFont.Height = -13
    RTFSettings.DefaultFont.Name = 'Arial'
    RTFSettings.DefaultFont.Style = []
    TextFileName = '($MyDocuments)\Report.pdf'
    TextSearchSettings.DefaultString = '<FindText>'
    TextSearchSettings.Enabled = True
    XLSSettings.AppName = 'ReportBuilder'
    XLSSettings.Author = 'ReportBuilder'
    XLSSettings.Subject = 'Report'
    XLSSettings.Title = 'Report'
    Left = 600
    Top = 208
    Version = '15.01'
    mmColumnWidth = 0
    DataPipelineName = 'pdbplnNC_Conciliadas'
    object phdrbnd2: TppHeaderBand
      Background.Brush.Style = bsClear
      mmBottomOffset = 0
      mmHeight = 24606
      mmPrintPosition = 0
      object plbl14: TppLabel
        UserName = 'plbl1'
        Caption = 'Reporte de Conciliacion de Notas de Cr'#233'ditos  '
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        Transparent = True
        mmHeight = 3704
        mmLeft = 44450
        mmTop = 4233
        mmWidth = 59002
        BandType = 0
        LayerName = BandLayer4
      end
      object plbl16: TppLabel
        UserName = 'plbl2'
        Caption = 'Mercancia entregada al Almac'#233'n'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        Transparent = True
        mmHeight = 3704
        mmLeft = 44450
        mmTop = 8731
        mmWidth = 59267
        BandType = 0
        LayerName = BandLayer4
      end
      object plbl17: TppLabel
        UserName = 'plbl3'
        AutoSize = False
        Caption = 'Fecha'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        Transparent = True
        mmHeight = 4763
        mmLeft = 4233
        mmTop = 19050
        mmWidth = 12700
        BandType = 0
        LayerName = BandLayer4
      end
      object plbl18: TppLabel
        UserName = 'Label1'
        AutoSize = False
        Caption = 'NumeroReq'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        Transparent = True
        mmHeight = 4763
        mmLeft = 24871
        mmTop = 19050
        mmWidth = 23283
        BandType = 0
        LayerName = BandLayer4
      end
      object plbl19: TppLabel
        UserName = 'Label2'
        AutoSize = False
        Caption = 'ArticuloID'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        Transparent = True
        mmHeight = 4763
        mmLeft = 49742
        mmTop = 19050
        mmWidth = 18785
        BandType = 0
        LayerName = BandLayer4
      end
      object plbl20: TppLabel
        UserName = 'Label5'
        AutoSize = False
        Caption = 'ArticuloDescripcion'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        Transparent = True
        mmHeight = 4763
        mmLeft = 80963
        mmTop = 19315
        mmWidth = 33073
        BandType = 0
        LayerName = BandLayer4
      end
      object plbl21: TppLabel
        UserName = 'plbl21'
        AutoSize = False
        Caption = 'Serie'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        Transparent = True
        mmHeight = 4763
        mmLeft = 114829
        mmTop = 19315
        mmWidth = 11113
        BandType = 0
        LayerName = BandLayer4
      end
      object plbl12: TppLabel
        UserName = 'Label9'
        AutoSize = False
        Caption = 'Razon'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        Transparent = True
        mmHeight = 4763
        mmLeft = 143934
        mmTop = 19050
        mmWidth = 12700
        BandType = 0
        LayerName = BandLayer4
      end
    end
    object pdtlbnd2: TppDetailBand
      Background1.Brush.Style = bsClear
      Background2.Brush.Style = bsClear
      mmBottomOffset = 0
      mmHeight = 14552
      mmPrintPosition = 0
      object pdbtxtFecha: TppDBText
        UserName = 'pdbtxtFecha'
        DataField = 'Fecha'
        DataPipeline = pdbplnNC_Conciliadas
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        ParentDataPipeline = False
        Transparent = True
        DataPipelineName = 'pdbplnNC_Conciliadas'
        mmHeight = 4763
        mmLeft = 4233
        mmTop = 1852
        mmWidth = 17992
        BandType = 4
        LayerName = BandLayer4
      end
      object pdbtxtNumeroReq: TppDBText
        UserName = 'DBText1'
        DataField = 'NumeroReq'
        DataPipeline = pdbplnNC_Conciliadas
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        ParentDataPipeline = False
        Transparent = True
        DataPipelineName = 'pdbplnNC_Conciliadas'
        mmHeight = 4763
        mmLeft = 25929
        mmTop = 1852
        mmWidth = 22490
        BandType = 4
        LayerName = BandLayer4
      end
      object pdbtxtArticuloID: TppDBText
        UserName = 'DBText2'
        DataField = 'ArticuloID'
        DataPipeline = pdbplnNC_Conciliadas
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        ParentDataPipeline = False
        Transparent = True
        DataPipelineName = 'pdbplnNC_Conciliadas'
        mmHeight = 4763
        mmLeft = 50006
        mmTop = 1323
        mmWidth = 64558
        BandType = 4
        LayerName = BandLayer4
      end
      object pdbtxtArticuloDescripcion3: TppDBText
        UserName = 'DBText6'
        DataField = 'ArticuloDescripcion'
        DataPipeline = pdbplnNC_Conciliadas
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        ParentDataPipeline = False
        Transparent = True
        DataPipelineName = 'pdbplnNC_Conciliadas'
        mmHeight = 4763
        mmLeft = 50006
        mmTop = 7938
        mmWidth = 64029
        BandType = 4
        LayerName = BandLayer4
      end
      object pdbtxtSerie1: TppDBText
        UserName = 'DBText8'
        DataField = 'Serie'
        DataPipeline = pdbplnNC_Conciliadas
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        ParentDataPipeline = False
        Transparent = True
        DataPipelineName = 'pdbplnNC_Conciliadas'
        mmHeight = 4763
        mmLeft = 115094
        mmTop = 1852
        mmWidth = 27781
        BandType = 4
        LayerName = BandLayer4
      end
      object pdbtxtRazon: TppDBText
        UserName = 'DBText10'
        CharWrap = True
        DataField = 'Razon'
        DataPipeline = pdbplnNC_Conciliadas
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        ParentDataPipeline = False
        Transparent = True
        WordWrap = True
        DataPipelineName = 'pdbplnNC_Conciliadas'
        mmHeight = 11377
        mmLeft = 144992
        mmTop = 1323
        mmWidth = 116681
        BandType = 4
        LayerName = BandLayer4
      end
    end
    object pftrbnd2: TppFooterBand
      Background.Brush.Style = bsClear
      mmBottomOffset = 0
      mmHeight = 61913
      mmPrintPosition = 0
      object plbl25: TppLabel
        UserName = 'Label8'
        AutoSize = False
        Caption = 'Nombre'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        Transparent = True
        mmHeight = 4763
        mmLeft = 11906
        mmTop = 21696
        mmWidth = 15875
        BandType = 8
        LayerName = BandLayer4
      end
      object pdbtxtNombre: TppDBText
        UserName = 'DBText9'
        DataField = 'Nombre'
        DataPipeline = pdbplnNC_Conciliadas
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        ParentDataPipeline = False
        Transparent = True
        DataPipelineName = 'pdbplnNC_Conciliadas'
        mmHeight = 4763
        mmLeft = 11906
        mmTop = 15610
        mmWidth = 64558
        BandType = 8
        LayerName = BandLayer4
      end
    end
    object pdsgnlyrs2: TppDesignLayers
      object pdsgnlyr2: TppDesignLayer
        UserName = 'BandLayer4'
        LayerType = ltBanded
        Index = 0
      end
    end
    object prmtrlst2: TppParameterList
    end
  end
  object RptEtiqueta: TppReport
    AutoStop = False
    DataPipeline = pdbplnEtiqueta
    PrinterSetup.BinName = 'Default'
    PrinterSetup.DocumentName = 'Report'
    PrinterSetup.PaperName = 'Custom'
    PrinterSetup.PrinterName = 'Default'
    PrinterSetup.SaveDeviceSettings = False
    PrinterSetup.mmMarginBottom = 0
    PrinterSetup.mmMarginLeft = 0
    PrinterSetup.mmMarginRight = 0
    PrinterSetup.mmMarginTop = 0
    PrinterSetup.mmPaperHeight = 19050
    PrinterSetup.mmPaperWidth = 31750
    PrinterSetup.PaperSize = 256
    Template.DatabaseSettings.DataPipeline = DatMain.pdbplnReportes
    Template.DatabaseSettings.Name = 'Etiqueta'
    Template.DatabaseSettings.NameField = 'Name'
    Template.DatabaseSettings.TemplateField = 'Template'
    Template.FileName = 
      'C:\Users\Amaury Minaya\OneDrive\_Personales\TIENDA Mister Tech\e' +
      'tiqueta.rtm'
    Template.SaveTo = stDatabase
    Template.Format = ftASCII
    ArchiveFileName = '($MyDocuments)\ReportArchive.raf'
    DeviceType = 'Screen'
    DefaultFileDeviceType = 'PDF'
    EmailSettings.ReportFormat = 'PDF'
    LanguageID = 'Default'
    OpenFile = False
    OutlineSettings.CreateNode = True
    OutlineSettings.CreatePageNodes = True
    OutlineSettings.Enabled = True
    OutlineSettings.Visible = True
    ThumbnailSettings.Enabled = True
    ThumbnailSettings.Visible = True
    ThumbnailSettings.DeadSpace = 30
    PDFSettings.EmbedFontOptions = [efUseSubset]
    PDFSettings.EncryptSettings.AllowCopy = True
    PDFSettings.EncryptSettings.AllowInteract = True
    PDFSettings.EncryptSettings.AllowModify = True
    PDFSettings.EncryptSettings.AllowPrint = True
    PDFSettings.EncryptSettings.Enabled = False
    PDFSettings.EncryptSettings.KeyLength = kl40Bit
    PDFSettings.FontEncoding = feAnsi
    PDFSettings.ImageCompressionLevel = 25
    RTFSettings.DefaultFont.Charset = DEFAULT_CHARSET
    RTFSettings.DefaultFont.Color = clWindowText
    RTFSettings.DefaultFont.Height = -13
    RTFSettings.DefaultFont.Name = 'Arial'
    RTFSettings.DefaultFont.Style = []
    TextFileName = '($MyDocuments)\Report.pdf'
    TextSearchSettings.DefaultString = '<FindText>'
    TextSearchSettings.Enabled = True
    XLSSettings.AppName = 'ReportBuilder'
    XLSSettings.Author = 'ReportBuilder'
    XLSSettings.Subject = 'Report'
    XLSSettings.Title = 'Report'
    Left = 384
    Top = 360
    Version = '15.01'
    mmColumnWidth = 0
    DataPipelineName = 'pdbplnEtiqueta'
    object phdrbnd3: TppHeaderBand
      Visible = False
      Background.Brush.Style = bsClear
      PrintHeight = phDynamic
      mmBottomOffset = 0
      mmHeight = 3704
      mmPrintPosition = 0
    end
    object pdtlbnd3: TppDetailBand
      Background1.Brush.Style = bsClear
      Background2.Brush.Style = bsClear
      mmBottomOffset = 0
      mmHeight = 19050
      mmPrintPosition = 0
      object pdbrcdCodigoBarras: TppDBBarCode
        UserName = 'pdbrcdCodigoBarras'
        AlignBarCode = ahCenter
        AutoEncode = True
        BarCodeType = bcCode128
        BarColor = clBlack
        DataField = 'CodigoBarras'
        DataPipeline = pdbplnEtiqueta
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 5
        Font.Style = []
        Transparent = True
        DataPipelineName = 'pdbplnEtiqueta'
        mmHeight = 9259
        mmLeft = 7673
        mmTop = 10349
        mmWidth = 17017
        BandType = 4
        LayerName = Foreground
        mmBarWidth = 254
        mmWideBarRatio = 76200
      end
      object pshp1: TppShape
        UserName = 'pshp1'
        Brush.Color = clBlack
        Shape = stRoundRect
        mmHeight = 4233
        mmLeft = 1058
        mmTop = 1058
        mmWidth = 13758
        BandType = 4
        LayerName = Foreground
      end
      object plbl22: TppLabel
        UserName = 'plbl1'
        Caption = 'Mister Tech'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Name = 'Calibri Light'
        Font.Size = 7
        Font.Style = []
        Transparent = True
        mmHeight = 2910
        mmLeft = 2381
        mmTop = 1852
        mmWidth = 11906
        BandType = 4
        LayerName = Foreground
      end
      object plbl23: TppLabel
        UserName = 'plbl2'
        HyperlinkColor = clBlack
        Caption = 'www.mistertech.do'
        Color = clBlack
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Calibri Light'
        Font.Size = 5
        Font.Style = []
        Transparent = True
        mmHeight = 2381
        mmLeft = 794
        mmTop = 5292
        mmWidth = 15081
        BandType = 4
        LayerName = Foreground
      end
      object pdbtxtPrecio: TppDBText
        UserName = 'pdbtxtPrecio'
        DataField = 'Precio'
        DataPipeline = pdbplnEtiqueta
        DisplayFormat = '$#,0.00;-$#,0.00'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = [fsBold]
        Transparent = True
        DataPipelineName = 'pdbplnEtiqueta'
        mmHeight = 3704
        mmLeft = 15610
        mmTop = 1058
        mmWidth = 15610
        BandType = 4
        LayerName = Foreground
      end
      object pdbtxtDescripcionEtiqueta: TppDBText
        UserName = 'pdbtxtDescripcionEtiqueta'
        DataField = 'DescripcionEtiqueta'
        DataPipeline = pdbplnEtiqueta
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 5
        Font.Style = []
        Transparent = True
        WordWrap = True
        DataPipelineName = 'pdbplnEtiqueta'
        mmHeight = 4763
        mmLeft = 794
        mmTop = 7938
        mmWidth = 30427
        BandType = 4
        LayerName = Foreground
      end
    end
    object pftrbnd3: TppFooterBand
      Visible = False
      Background.Brush.Style = bsClear
      mmBottomOffset = 0
      mmHeight = 1852
      mmPrintPosition = 0
    end
    object pdsgnlyrs3: TppDesignLayers
      object pdsgnlyr3: TppDesignLayer
        UserName = 'Foreground'
        LayerType = ltBanded
        Index = 0
      end
    end
    object prmtrlst3: TppParameterList
    end
  end
  object pdbplnEtiqueta: TppDBPipeline
    DataSource = frmEtiquetas.dsEtiqueta
    UserName = 'pdbplnEtiqueta'
    Left = 296
    Top = 360
  end
end
