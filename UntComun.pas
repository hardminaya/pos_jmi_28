unit UntComun;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, dxSkinsCore, dxSkinsDefaultPainters,
  dxSkinscxPCPainter, cxPCdxBarPopupMenu, Data.DB, Data.Win.ADODB, Vcl.Grids,
  Vcl.DBGrids, JvExDBGrids, JvDBGrid, cxPC, JvComponentBase, JvDBGridExport,
  Vcl.Menus, Vcl.StdCtrls, Vcl.ComCtrls, JvExComCtrls, JvDateTimePicker,
  cxButtons, Vcl.Mask, JvExMask, JvToolEdit, JvMaskEdit, JvCheckedMaskEdit,
  JvDatePickerEdit, JvDialogs, JvExStdCtrls, JvCombobox, JvExControls, JvXPCore,
  JvXPButtons, Vcl.ActnList, cxNavigator, cxDBNavigator, cxContainer, cxEdit,
  cxTextEdit, cxMaskEdit, cxDropDownEdit, cxLookupEdit, cxDBLookupEdit,
  cxDBLookupComboBox, Vcl.ToolWin, Vcl.DBActns, Vcl.StdActns,
  Vcl.PlatformDefaultStyleActnCtrls, Vcl.ActnMan;

type
  TfrmComun = class(TForm)
    jvdbgrdxclxprt1: TJvDBGridExcelExport;
    cxpgcntrl1: TcxPageControl;
    cxtbsht1: TcxTabSheet;
    jvdbgrd1: TJvDBGrid;
    sp1: TADOStoredProc;
    ds1: TDataSource;
    btnExportarExcel: TcxButton;
    jvdtmpckrHasta: TJvDateTimePicker;
    lbl2: TLabel;
    jvdtmpckrDesde: TJvDateTimePicker;
    lbl1: TLabel;
    flsvdlg1: TFileSaveDialog;
    JvXPToolButton1: TJvXPToolButton;
    lbl3: TLabel;
    btnRefrescar: TcxButton;
    dtmfldsp1DocumentoFecha: TDateTimeField;
    wdstrngfldsp1NumeroNCF: TWideStringField;
    wdstrngfldsp1ClienteNombre: TWideStringField;
    wdstrngfldsp1ClienteRNC: TWideStringField;
    QryArticulosp1Monto: TFMTBCDField;
    QryArticulosp1ITBIS: TFMTBCDField;
    QryArticulosp1MontoGeneral: TFMTBCDField;
    cxdbnvgtr1: TcxDBNavigator;
    cbbTipoNCF: TcxLookupComboBox;
    dsNCFTipos: TDataSource;
    lblTipoNCF: TLabel;
    actmgr1: TActionManager;
    dtstpr1: TDataSetPrior;
    dtstnxt1: TDataSetNext;
    act1: TDataSetEdit;
    act3: TDataSetInsert;
    wndwcls1: TWindowClose;
    act2: TDataSetCancel;
    act4: TDataSetPost;
    act5: TDataSetDelete;
    acExportarExcel: TAction;
    tlb3: TToolBar;
    btnExportarExcel1: TToolButton;
    strngfldsp1NCF_MODIFICADO: TStringField;
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnSalirClick(Sender: TObject);
    procedure btnRefrescarClick(Sender: TObject);
    procedure Refrescar;
    procedure cbbTipoNCFPropertiesChange(Sender: TObject);
    procedure jvdtmpckrDesdeChange(Sender: TObject);
    procedure jvdtmpckrHastaChange(Sender: TObject);
    procedure btnExportarExcelClick(Sender: TObject);

  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmComun: TfrmComun;
  var_TipoNCF : String;

implementation

{$R *.dfm}

uses UntData;



procedure TfrmComun.btnExportarExcelClick(Sender: TObject);
var
  archivo : String;
begin
  if jvdbgrd1.DataSource.DataSet.RecordCount= 0 then begin
    MessageBox(Handle,'No hay datos para Exportar, verifique el criterio de seleccion','ATENCION NCF',MB_ICONWARNING or MB_OK);
    Exit;
  end;

  flsvdlg1.Execute;
  archivo:=flsvdlg1.FileName;
  if Length(archivo)>0 then begin
    jvdbgrdxclxprt1.FileName:=archivo;
    jvdbgrdxclxprt1.ExportGrid;
  end;
end;

procedure TfrmComun.btnRefrescarClick(Sender: TObject);
begin
  Refrescar;
end;

procedure TfrmComun.btnSalirClick(Sender: TObject);
begin
  Self.Close;
end;

procedure TfrmComun.cbbTipoNCFPropertiesChange(Sender: TObject);
begin
  with cbbTipoNCF.Properties.DataController do
  begin
    var_TipoNCF:=Values[FocusedRecordIndex,1];
  end;
  Refrescar;
end;

procedure TfrmComun.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action:=caFree;
  frmComun:=nil;
end;

procedure TfrmComun.FormShow(Sender: TObject);
begin
  jvdtmpckrDesde.Date:=Now;
  jvdtmpckrHasta.Date:=Now;
end;

procedure TfrmComun.jvdtmpckrDesdeChange(Sender: TObject);
begin
  Refrescar;
end;

procedure TfrmComun.jvdtmpckrHastaChange(Sender: TObject);
begin
  Refrescar;
end;

procedure TfrmComun.Refrescar;
begin
  if cbbTipoNCF.Text='' then begin
    MessageBox(Handle,'Debe seleccionar un tipo de Comprobante','ATENCION NCF',MB_ICONWARNING or MB_OK);
    Exit;
  end;

  fn_RegistrodeCambio(CUsuario.Usuario,'Consulta','NCF del '+

  FormatDateTime('YYYY-MM-DD',jvdtmpckrDesde.Date)+' al '+
  FormatDateTime('YYYY-MM-DD',jvdtmpckrHasta.Date)+ ' Tipo: '+
  cbbTipoNCF.Text);

  lblTipoNCF.Caption:=cbbTipoNCF.Text;

  with sp1 do begin
    Close;
    Parameters.ParamByName('@TipoNCF').Value:=var_TipoNCF;
    Parameters.ParamByName('@Fecha_Desde').Value:=FormatDateTime('YYYY-MM-DD',jvdtmpckrDesde.Date);
    Parameters.ParamByName('@Fecha_Hasta').Value:=FormatDateTime('YYYY-MM-DD',jvdtmpckrHasta.Date);
    Open;
  end;
end;

end.
