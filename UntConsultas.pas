unit UntConsultas;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxClasses, cxCustomData, cxStyles, cxEdit, dxSkinsCore,
  dxSkinsDefaultPainters, cxCustomPivotGrid, cxDBPivotGrid, Data.DB,
  Data.Win.ADODB, dxSkinscxPCPainter, cxPCdxBarPopupMenu, cxPC, Vcl.ComCtrls,
  Vcl.Grids, Vcl.DBGrids, JvExDBGrids, JvDBGrid, Vcl.StdCtrls, JvComponentBase,
  JvDBGridExport, cxFilter, cxData, cxDataStorage, cxDBData, cxGridLevel,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGridCustomView,
  cxGrid, Vcl.Menus, cxButtons, users, dxSkinBlack, dxSkinBlue, dxSkinBlueprint,
  dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide,
  dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinFoggy,
  dxSkinGlassOceans, dxSkinHighContrast, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSevenClassic, dxSkinSharp, dxSkinSharpPlus, dxSkinSilver,
  dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008, dxSkinTheAsphaltWorld,
  dxSkinValentine, dxSkinVS2010, dxSkinWhiteprint, dxSkinXmas2008Blue;

type
  TfrmConsultas = class(TForm)
    qryWork1: TADOQuery;
    ds1: TDataSource;
    cxpgcntrl1: TcxPageControl;
    cxtbsht1: TcxTabSheet;
    cxtbsht2: TcxTabSheet;
    cxdbpvtgrd1: TcxDBPivotGrid;
    wdstrngfldWork1ClienteNombre: TWideStringField;
    dtmfldWork1DocumentoFecha: TDateTimeField;
    QryArticuloWork1MontoGeneral: TFMTBCDField;
    strngfldWork1Nombre: TStringField;
    strngfldWork1Apellido: TStringField;
    btnSalir: TcxButton;
    cxtbsht4: TcxTabSheet;
    qryFlujoCajas: TADOQuery;
    dsFlujoCajas: TDataSource;
    cxgrdbtblvwGrid1DBTableView2: TcxGridDBTableView;
    cxgrdlvlGrid1Level2: TcxGridLevel;
    cxgrd2: TcxGrid;
    ColGrid1DBTableView2ClienteNombre: TcxGridDBColumn;
    ColGrid1DBTableView2MontoGeneral: TcxGridDBColumn;
    wdstrngfldFlujoCajasClienteNombre: TWideStringField;
    wdstrngfldFlujoCajasFecha: TWideStringField;
    QryArticuloFlujoCajasMontoGeneral: TFMTBCDField;
    wdstrngfldFlujoCajasDescripcion: TWideStringField;
    intgrfldFlujoCajasFacturaNo: TIntegerField;
    dtmfldFlujoCajasDocumentoFecha: TDateTimeField;
    wdstrngfldFlujoCajasNombrePC: TWideStringField;
    ColGrid1DBTableView2Fecha: TcxGridDBColumn;
    ColGrid1DBTableView2FacturaNo: TcxGridDBColumn;
    ColGrid1DBTableView2NombrePC: TcxGridDBColumn;
    cxstylrpstry1: TcxStyleRepository;
    cxstyl1: TcxStyle;
    cxstyl2: TcxStyle;
    cxdbpvtgrdfldcxdbpvtgrd1ClienteNombre: TcxDBPivotGridField;
    cxdbpvtgrdfldcxdbpvtgrd1Fecha: TcxDBPivotGridField;
    cxdbpvtgrdfldcxdbpvtgrd1MontoGeneral: TcxDBPivotGridField;
    cxdbpvtgrdfldcxdbpvtgrd1Descripcion: TcxDBPivotGridField;
    cxdbpvtgrdfldcxdbpvtgrd1FacturaNo: TcxDBPivotGridField;
    cxdbpvtgrdfldcxdbpvtgrd1DocumentoFecha: TcxDBPivotGridField;
    cxdbpvtgrdfldcxdbpvtgrd1NombrePC: TcxDBPivotGridField;
    cxtbsht3: TcxTabSheet;
    qry1: TADOQuery;
    ds2: TDataSource;
    cxdbpvtgrd2: TcxDBPivotGrid;
    cxdbpvtgrdfldcxdbpvtgrd2Field1: TcxDBPivotGridField;
    cxdbpvtgrdfldcxdbpvtgrd2Field2: TcxDBPivotGridField;
    cxdbpvtgrdfldcxdbpvtgrd2Field3: TcxDBPivotGridField;
    cxdbpvtgrdfldcxdbpvtgrd2Field4: TcxDBPivotGridField;
    cxdbpvtgrdfldcxdbpvtgrd2Field5: TcxDBPivotGridField;
    cxdbpvtgrdfldcxdbpvtgrd2Field6: TcxDBPivotGridField;
    procedure btnSalirClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmConsultas: TfrmConsultas;

implementation

{$R *.dfm}

uses UntData, UntMain;

procedure TfrmConsultas.btnSalirClick(Sender: TObject);
begin
  Self.Close;
end;

procedure TfrmConsultas.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action:=caFree;
  frmConsultas:=nil;
end;

procedure TfrmConsultas.FormShow(Sender: TObject);
begin
  qry1.Open;
end;

end.
