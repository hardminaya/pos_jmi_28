object frmNotasdeCredito: TfrmNotasdeCredito
  Left = 0
  Top = 0
  Caption = 'Notas de Cr'#233'dito'
  ClientHeight = 668
  ClientWidth = 1011
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Visible = True
  WindowState = wsMaximized
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object tlb1: TToolBar
    Left = 0
    Top = 0
    Width = 1011
    Height = 85
    ButtonHeight = 60
    ButtonWidth = 57
    Caption = 'tlb1'
    Images = frmMain.ilToolBar32
    ShowCaptions = True
    TabOrder = 0
    object btnRegistro: TToolButton
      Left = 0
      Top = 0
      Caption = 'Registro'
      DropdownMenu = jvpmn1
      ImageIndex = 9
      PopupMenu = jvpmn1
      Style = tbsDropDown
      Visible = False
    end
    object btnAnterior: TToolButton
      Left = 76
      Top = 0
      Caption = 'Anterior'
      ImageIndex = 3
      Visible = False
    end
    object btnSiguiente: TToolButton
      Left = 133
      Top = 0
      Caption = 'Siguiente'
      ImageIndex = 0
      Visible = False
    end
    object btnGuardar: TToolButton
      Left = 190
      Top = 0
      Caption = 'Guardar'
      ImageIndex = 2
      Visible = False
    end
    object btnCancelar: TToolButton
      Left = 247
      Top = 0
      Caption = 'Cancelar'
      ImageIndex = 5
      Visible = False
    end
    object btn2: TToolButton
      Left = 304
      Top = 0
      Width = 55
      Caption = 'btn5'
      ImageIndex = 5
      Style = tbsSeparator
      Visible = False
    end
    object btnSalir: TToolButton
      Left = 359
      Top = 0
      Caption = 'Salir'
      ImageIndex = 1
      OnClick = btnSalirClick
    end
    object rg1: TRadioGroup
      Left = 416
      Top = 0
      Width = 165
      Height = 60
      Caption = 'Visualizar'
      Color = clCream
      Ctl3D = True
      Items.Strings = (
        'Todas'
        'Pendientes NC SAP')
      ParentBackground = False
      ParentColor = False
      ParentCtl3D = False
      TabOrder = 0
      OnClick = rg1Click
    end
  end
  object cxpgcntrl1: TcxPageControl
    Left = 0
    Top = 85
    Width = 1011
    Height = 583
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    Properties.ActivePage = cxtbsht3
    ClientRectBottom = 577
    ClientRectLeft = 3
    ClientRectRight = 1005
    ClientRectTop = 26
    object cxtbsht1: TcxTabSheet
      Caption = 'Listado SNC'
      ImageIndex = 0
      object cxgrd1: TcxGrid
        Left = 0
        Top = 0
        Width = 1002
        Height = 551
        Align = alClient
        Images = il1
        TabOrder = 0
        object cxgrdbtblvwGrid1DBTableView1: TcxGridDBTableView
          OnDblClick = cxgrdbtblvwGrid1DBTableView1DblClick
          DataController.DataSource = ds1
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          Images = il1
          object cxgrdbclmnseleccion: TcxGridDBColumn
            Caption = 'Sel'
            DataBinding.ValueType = 'Boolean'
            PropertiesClassName = 'TcxCheckBoxProperties'
            MinWidth = 30
            Options.IgnoreTimeForFiltering = False
            Options.IncSearch = False
            Options.GroupFooters = False
            Options.Grouping = False
            Options.HorzSizing = False
            Options.Moving = False
            Options.Sorting = False
            Styles.Header = cxstyl1
            Width = 30
          end
          object GridColAccion: TcxGridDBColumn
            Caption = 'Acci'#243'n'
            PropertiesClassName = 'TcxButtonEditProperties'
            Properties.Buttons = <
              item
                Action = acConciliarNC
                Default = True
                Kind = bkText
              end
              item
                Action = acEliminarNC
                Glyph.Data = {
                  36090000424D3609000000000000360000002800000018000000180000000100
                  2000000000000009000000000000000000000000000000000000000000000000
                  0000000000000100383801002424000000000000000000000000000000000000
                  0000000000000000000000000000000000000000000000000000000000000000
                  00000000000001002C420607EEFF010066A20000000000000000000000000000
                  00001316E1E1243CFFFF1D2EFFFF0706B8BA0000000000000000000000000000
                  0000000000000000000000000000000000000000000000000000000000000000
                  0000000000000100CDF01720FFFF01007ECB0000000000000000000000000904
                  A7A73357FFFF2E51FFFF2A4AFFFF233EFFFF0502C8CD00000000000000000000
                  0000000000000000000000000000000000000000000000000000000000000000
                  000003006E910909FFFF0B13FFFF010068A5000000000000000000000000201E
                  FFFF325BFFFF2C4FFFFF2948FFFF233FFFFF1628FFFF0200565A000000000000
                  0000000000000000000000000000000000000000000000000000000000000100
                  17200200EAFF1414FFFF0509EFFF0100395B000000000000000000000000231F
                  FFFF4E76FFFF2B4DFFFF2948FFFF233DFFFF203CFFFF080AE9EF000000000000
                  0000000000000000000000000000000000000000000000000000000000000300
                  BBDD100CFFFF1414FFFF0000D0FF00000B110000000000000000000000000400
                  C9C97190FFFF284BFFFF2847FFFF233DFFFF1F3AFFFF182BFFFF0200767F0000
                  0000000000000000000000000000000000000000000000000000040083A50500
                  FFFF100CFFFF0708FFFF010076B7000000000000000000000000000000000300
                  5C5C4248FFFF5275FFFF2645FFFF233DFFFF1F39FFFF1D34FFFF0B10FBFF0000
                  0C0D0000000000000000000000000000000000000000020045580500FDFF0F07
                  FFFF100CFFFF0200D9FF01002435000000000000000000000000000000000000
                  00000100CDCD697AFFFF3454FFFF213CFFFF1F39FFFF1C32FFFF1728FFFF0400
                  BECB000000000000000000000000000000000200495A0500F8FF0F08FFFF0E07
                  FFFF0602FFFF030088CA00000000000000000000000000000000000000000000
                  0000000000000400FFFF7F98FFFF213DFFFF1E38FFFF1C32FFFF192CFFFF0C15
                  FFFF0300626D000000000000000001001A1F0601FBFF0F09FFFF0E07FFFF0F08
                  FFFF0400CEFF0100253500000000000000000000000000000000000000000000
                  00000000000001003232110EFFFF7591FFFF1D38FFFF1B31FFFF192BFFFF1824
                  FFFF0708F5FF000007080100181C0702F4FF110DFFFF0F09FFFF0E07FFFF0500
                  F8FF020069910000000000000000000000000000000000000000000000000000
                  0000000000000000000002007E802625FDFF607DFFFF172DFFFF192BFFFF1825
                  FFFF131CFFFF0501D1E50807F4F9100FFFFF110DFFFF100AFFFF0C06FFFF0400
                  B8F0000000000000000000000000000000000000000000000000000000000000
                  00000000000000000000000000000300A1A6414AFFFF5570FFFF1728FFFF1825
                  FFFF1820FFFF1317FFFF1616FFFF1210FFFF120EFFFF120CFFFF0300DBFF0100
                  2834000000000000000000000000000000000000000000000000000000000000
                  0000000000000000000000000000000000000300D7E0474EFEFF5568FFFF1723
                  FFFF1920FFFF171CFFFF1616FFFF1310FFFF1211FFFF0803EBFF030167810000
                  0000000000000000000000000000000000000000000000000000000000000000
                  000000000000000000000000000000000000000000000300D9E6444CFFFF2937
                  FFFF1B23FFFF191CFFFF1817FFFF1513FFFF0C08F6FF060596B8000000000000
                  0000000000000000000000000000000000000000000000000000000000000000
                  000000000000000000000000000000000000211D8F922E3AFFFF2638FFFF1B25
                  FFFF1D24FFFF1B1FFFFF1C1CFFFF1816FFFF0D09E9FF0201232A000000000000
                  0000000000000000000000000000000000000000000000000000000000000000
                  0000000000000000000019155D5D545BFFFF637AFFFF6279FFFF5F70FFFF5F6C
                  FFFF4952FFFF4B4FFFFF4142FFFF2524FFFF2624FFFF130DEAFF01001A210000
                  0000000000000000000000000000000000000000000000000000000000000000
                  0000120F3A3A474ACECE6A7EFFFF6C85FFFF687EFFFF6378FFFF6172FFFF5D6A
                  FFFF6972FFFF9A9CFFFFA7A8FFFF6F6CFFFF5853FFFF5B56FFFF4F48F7FF1513
                  414C000000000000000000000000000000000000000000000000000000003330
                  9A9A6F83FFFF7894FFFF738DFFFF6F85FFFF6C81FFFF667BFFFF6373FFFF7D8A
                  FFFF8485FCFF4B44D9E95B54EFFF9F9DFFFF8F8DFFFF5B55FFFF5B52FFFF514B
                  F8FF211F627500000000000000000000000000000000000000002C2780807D94
                  FFFF809CFFFF7B94FFFF7790FFFF7389FFFF6F84FFFF667CFFFF95A3FFFF8989
                  FCFF4B45D9E500000000131135395751ECFF807DF7FFAAA6FFFF7770FFFF5C56
                  FFFF5754FFFF3B39A8C1000000000000000000000000000000007175FFFF89A5
                  FFFF839EFFFF7F98FFFF7C94FFFF768BFFFF788DFFFFA1B0FFFF7978FAFF3E39
                  A9B101010202000000000000000000000000413FB1C46965EDFF9A98FEFFA7A0
                  FFFF7D7BFFFF6060FFFF5757ECFF1C1B506400000000000000009093FFFF9EB6
                  FFFF839FFFFF8098FFFF7D95FFFF95A5FFFFA9B3FFFF706CFBFF2E2B777C0000
                  0000000000000000000000000000000000000000000017163940524FCCE77873
                  EDFFA19BFDFFAAACFFFF979DFFFF7680FBFF484CBFD60B0A1D25665FECECB1BD
                  FFFFBCCDFFFFB4C5FFFFBDCBFFFF979CFDFF6862F3F719173E40000000000000
                  0000000000000000000000000000000000000000000000000000000000002A27
                  606D5351C6E76C68E2FF8C8DF2FF9FA7FAFFC5D3FFFF797EE6FF11112A2A5D56
                  DADA7D78FFFF827FFFFF6D67FEFF4D48B8B90000010100000000000000000000
                  0000000000000000000000000000000000000000000000000000000000000000
                  000000000000131229303A377E96504EB4D94E4BAFD748479EC5000000000000
                  0000000000000000000000000000000000000000000000000000000000000000
                  0000000000000000000000000000000000000000000000000000000000000000
                  0000000000000000000000000000000000000000000000000000}
                Kind = bkText
              end>
            HeaderAlignmentHorz = taCenter
            Options.ShowEditButtons = isebAlways
            Styles.Content = cxstyl4
            Styles.Header = cxstyl1
            Width = 116
          end
          object ColGrid1DBTableView1NumeroReq: TcxGridDBColumn
            Caption = 'No. Req.'
            DataBinding.FieldName = 'NumeroReq'
            HeaderAlignmentHorz = taCenter
            Styles.Header = cxstyl1
            Width = 59
          end
          object ColGrid1DBTableView1NumeroNC: TcxGridDBColumn
            Caption = 'No. NC SAP'
            DataBinding.FieldName = 'NumeroNC'
            HeaderAlignmentHorz = taCenter
            Styles.Header = cxstyl1
            Width = 86
          end
          object ColGrid1DBTableView1Fecha: TcxGridDBColumn
            DataBinding.FieldName = 'Fecha'
            HeaderAlignmentHorz = taCenter
            Styles.Header = cxstyl1
            Width = 66
          end
          object ColGrid1DBTableView1Ticket: TcxGridDBColumn
            Caption = 'No. Ticket'
            DataBinding.FieldName = 'Ticket'
            HeaderAlignmentHorz = taCenter
            Options.Editing = False
            Options.ShowEditButtons = isebAlways
            Styles.Content = cxstyl5
            Styles.Header = cxstyl1
            Width = 67
          end
          object ColArticuloDescripcion: TcxGridDBColumn
            Caption = 'Articulo'
            DataBinding.FieldName = 'DescripcionArt'
            HeaderAlignmentHorz = taCenter
            Styles.Header = cxstyl1
            Width = 242
          end
          object ColGrid1DBTableView1Monto: TcxGridDBColumn
            Caption = 'Monto RD$'
            DataBinding.FieldName = 'Monto'
            HeaderAlignmentHorz = taCenter
            Styles.Header = cxstyl1
            Width = 70
          end
          object ColGrid1DBTableView1Balance: TcxGridDBColumn
            Caption = 'Balance RD$'
            DataBinding.FieldName = 'Balance'
            HeaderAlignmentHorz = taCenter
            Styles.Header = cxstyl1
            Width = 91
          end
          object ColEstado: TcxGridDBColumn
            DataBinding.FieldName = 'Estado'
            HeaderAlignmentHorz = taCenter
            Styles.Header = cxstyl1
            Width = 43
          end
          object ColSerie: TcxGridDBColumn
            DataBinding.FieldName = 'Serie'
            HeaderAlignmentHorz = taCenter
            Styles.Header = cxstyl1
            Width = 112
          end
          object ColFacturaUtilizado: TcxGridDBColumn
            Caption = 'Ticket Pago'
            DataBinding.FieldName = 'TicketUtilizado'
            HeaderAlignmentHorz = taCenter
            Styles.Header = cxstyl1
            Width = 88
          end
          object cxgrdbclmnGrid1DBTableView1Column1: TcxGridDBColumn
            Caption = 'NCF NC'
            DataBinding.FieldName = 'NCF_NC'
            HeaderAlignmentHorz = taCenter
            Styles.Header = cxstyl1
          end
        end
        object cxgrdlvlGrid1Level1: TcxGridLevel
          GridView = cxgrdbtblvwGrid1DBTableView1
        end
      end
    end
    object cxtbsht3: TcxTabSheet
      Caption = 'Solicitud (Tickets)'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ImageIndex = 2
      ParentFont = False
      OnShow = cxtbsht3Show
      object grpInformacionCliente: TGroupBox
        Left = 16
        Top = 43
        Width = 865
        Height = 110
        Caption = 'Informaci'#243'n General'
        Color = clGradientInactiveCaption
        Ctl3D = True
        ParentBackground = False
        ParentColor = False
        ParentCtl3D = False
        TabOrder = 1
        object lbl1: TLabel
          Left = 232
          Top = 112
          Width = 16
          Height = 13
          Caption = 'lbl1'
        end
        object cxlbl1: TcxLabel
          Left = 23
          Top = 18
          Caption = 'Fecha'
          ParentFont = False
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -13
          Style.Font.Name = 'Tahoma'
          Style.Font.Style = []
          Style.IsFontAssigned = True
        end
        object edtFecha: TcxDBTextEdit
          Left = 23
          Top = 36
          DataBinding.DataField = 'DocumentoFecha'
          DataBinding.DataSource = dsTicket
          StyleFocused.BorderColor = clRed
          StyleFocused.Color = clGradientActiveCaption
          StyleHot.BorderColor = clRed
          TabOrder = 1
          Width = 121
        end
        object edtClientNombre: TcxDBTextEdit
          Left = 150
          Top = 36
          DataBinding.DataField = 'ClienteNombre'
          DataBinding.DataSource = dsTicket
          StyleFocused.BorderColor = clRed
          StyleFocused.Color = clGradientActiveCaption
          StyleFocused.TextStyle = [fsBold]
          StyleHot.BorderColor = clRed
          TabOrder = 2
          Width = 290
        end
        object edtFacturaTotal: TcxDBTextEdit
          Left = 446
          Top = 36
          DataBinding.DataField = 'MontoGeneral'
          DataBinding.DataSource = dsTicket
          StyleFocused.BorderColor = clRed
          StyleFocused.Color = clGradientActiveCaption
          StyleFocused.TextStyle = [fsBold]
          StyleHot.BorderColor = clRed
          TabOrder = 5
          Width = 121
        end
        object cxlbl2: TcxLabel
          Left = 150
          Top = 18
          Caption = 'Cliente Nombre'
          ParentFont = False
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -13
          Style.Font.Name = 'Tahoma'
          Style.Font.Style = []
          Style.IsFontAssigned = True
        end
        object cxlbl4: TcxLabel
          Left = 446
          Top = 18
          Caption = 'Factura Total'
          ParentFont = False
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -13
          Style.Font.Name = 'Tahoma'
          Style.Font.Style = []
          Style.IsFontAssigned = True
        end
        object edtTipoComprobante: TcxDBTextEdit
          Left = 150
          Top = 78
          DataBinding.DataField = 'NCFDescripcion'
          DataBinding.DataSource = dsTicket
          StyleFocused.BorderColor = clRed
          StyleFocused.Color = clGradientActiveCaption
          StyleFocused.TextStyle = [fsBold]
          StyleHot.BorderColor = clRed
          TabOrder = 7
          Width = 228
        end
        object cxlbl3: TcxLabel
          Left = 150
          Top = 60
          Caption = 'Tipo Comprobante'
          ParentFont = False
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -13
          Style.Font.Name = 'Tahoma'
          Style.Font.Style = []
          Style.IsFontAssigned = True
        end
        object edtVendedor: TcxDBTextEdit
          Left = 23
          Top = 78
          DataBinding.DataField = 'Vendedor'
          DataBinding.DataSource = dsTicket
          StyleFocused.BorderColor = clRed
          StyleFocused.Color = clGradientActiveCaption
          StyleFocused.TextStyle = [fsBold]
          StyleHot.BorderColor = clRed
          TabOrder = 9
          Width = 121
        end
        object cxlbl5: TcxLabel
          Left = 23
          Top = 60
          Caption = 'Vendedor'
          ParentFont = False
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -13
          Style.Font.Name = 'Tahoma'
          Style.Font.Style = []
          Style.IsFontAssigned = True
        end
        object edtNumeroNCF: TcxDBTextEdit
          Left = 392
          Top = 78
          DataBinding.DataField = 'NumeroNCF'
          DataBinding.DataSource = dsTicket
          StyleFocused.BorderColor = clRed
          StyleFocused.Color = clGradientActiveCaption
          StyleFocused.TextStyle = [fsBold]
          StyleHot.BorderColor = clRed
          TabOrder = 10
          Width = 175
        end
        object lbl4: TcxLabel
          Left = 392
          Top = 60
          Caption = 'Numero NCF'
          ParentFont = False
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -13
          Style.Font.Name = 'Tahoma'
          Style.Font.Style = []
          Style.IsFontAssigned = True
        end
        object btnSolicitar: TcxButton
          Left = 587
          Top = 63
          Width = 86
          Height = 44
          Cursor = crHandPoint
          Action = acSolicitarNC
          OptionsImage.Images = frmMain.ilToolBar32
          TabOrder = 12
        end
        object btnLimpiar: TcxButton
          Left = 679
          Top = 63
          Width = 165
          Height = 44
          Cursor = crHandPoint
          Caption = 'Limpiar/Cancelar'
          OptionsImage.ImageIndex = 8
          OptionsImage.Images = frmMain.ilToolBar32
          TabOrder = 13
          OnClick = btnLimpiarClick
        end
        object cxLabel1: TcxLabel
          Left = 587
          Top = 18
          Caption = 'Cliente RNC'
          ParentFont = False
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -13
          Style.Font.Name = 'Tahoma'
          Style.Font.Style = []
          Style.IsFontAssigned = True
        end
        object edtClienteRNC: TcxDBTextEdit
          Left = 587
          Top = 36
          DataBinding.DataField = 'RNC'
          DataBinding.DataSource = dsTicket
          StyleFocused.BorderColor = clRed
          StyleFocused.Color = clGradientActiveCaption
          StyleFocused.TextStyle = [fsBold]
          StyleHot.BorderColor = clRed
          TabOrder = 15
          Width = 175
        end
      end
      object cxspndtTicket: TcxSpinEdit
        Left = 16
        Top = 16
        Hint = 'Indique N'#250'mero de Factura'
        ParentShowHint = False
        Properties.Alignment.Horz = taRightJustify
        Properties.AssignedValues.MinValue = True
        Properties.BeepOnError = True
        Properties.DisplayFormat = '######;0'
        Properties.EditFormat = '######;0'
        Properties.MaxValue = 9999999.000000000000000000
        Properties.Nullstring = '0'
        Properties.SpinButtons.Visible = False
        Properties.UseDisplayFormatWhenEditing = True
        Properties.OnEditValueChanged = cxspndtTicketPropertiesEditValueChanged
        Properties.OnValidate = cxspndtTicketPropertiesValidate
        ShowHint = True
        Style.TextStyle = [fsBold]
        StyleFocused.BorderColor = clRed
        StyleFocused.BorderStyle = ebsThick
        StyleFocused.Color = clGradientActiveCaption
        StyleFocused.ButtonStyle = btsDefault
        StyleHot.BorderColor = clRed
        TabOrder = 0
        Width = 121
      end
      object grp1: TGroupBox
        Left = 16
        Top = 159
        Width = 865
        Height = 346
        Caption = 'Detalle de Factura'
        Color = clGradientInactiveCaption
        Ctl3D = True
        ParentBackground = False
        ParentColor = False
        ParentCtl3D = False
        TabOrder = 3
        object cxLabel3: TcxLabel
          Left = 544
          Top = 292
          Caption = 'Total  Nota de Cr'#233'dito'
          ParentFont = False
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -16
          Style.Font.Name = 'Tahoma'
          Style.Font.Style = []
          Style.IsFontAssigned = True
        end
      end
      object cxlbl6: TcxLabel
        Left = 143
        Top = 17
        AutoSize = False
        Caption = '<-Indique N'#250'mero de Factura'
        ParentFont = False
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = clBlue
        Style.Font.Height = -13
        Style.Font.Name = 'Tahoma'
        Style.Font.Style = [fsItalic]
        Style.IsFontAssigned = True
        StyleFocused.TextStyle = []
        Transparent = True
        Height = 20
        Width = 194
      end
      object cxGrid1: TcxGrid
        Left = 37
        Top = 200
        Width = 823
        Height = 241
        Align = alCustom
        TabOrder = 4
        object cxGridDBTableView1: TcxGridDBTableView
          DataController.DataSource = dsNotaCreditoD
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          OptionsSelection.InvertSelect = False
          OptionsSelection.MultiSelect = True
          OptionsSelection.CellMultiSelect = True
          OptionsView.GroupByBox = False
          object cxGridDBTableView1Column1: TcxGridDBColumn
            Caption = 'Acci'#243'n'
            PropertiesClassName = 'TcxButtonEditProperties'
            Properties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.OnButtonClick = cxGridDBTableView1Column1PropertiesButtonClick
            HeaderAlignmentHorz = taCenter
            MinWidth = 64
            Options.Editing = False
            Options.Filtering = False
            Options.FilteringFilteredItemsList = False
            Options.FilteringMRUItemsList = False
            Options.FilteringPopup = False
            Options.IgnoreTimeForFiltering = False
            Options.IncSearch = False
            Options.ShowEditButtons = isebAlways
            Options.GroupFooters = False
            Options.Grouping = False
            Options.HorzSizing = False
            Options.Moving = False
            Options.Sorting = False
            Styles.Header = cxstyl1
          end
          object cxGridDBArticuloId: TcxGridDBColumn
            Caption = 'C'#243'digo'
            DataBinding.FieldName = 'ArticuloId'
            MinWidth = 64
            Options.Editing = False
            Options.Filtering = False
            Options.FilteringFilteredItemsList = False
            Options.FilteringMRUItemsList = False
            Options.FilteringPopup = False
            Options.FilteringPopupMultiSelect = False
            Options.IgnoreTimeForFiltering = False
            Options.IncSearch = False
            Options.GroupFooters = False
            Options.Grouping = False
            Options.HorzSizing = False
            Options.Moving = False
            Styles.Header = cxstyl1
            Width = 64
          end
          object cxGridDBArticuloDescripcion1: TcxGridDBColumn
            Caption = 'Descripci'#243'n'
            DataBinding.FieldName = 'ArticuloDescripcion'
            MinWidth = 237
            Options.Editing = False
            Options.Filtering = False
            Options.FilteringFilteredItemsList = False
            Options.FilteringMRUItemsList = False
            Options.FilteringPopup = False
            Options.FilteringPopupMultiSelect = False
            Options.IgnoreTimeForFiltering = False
            Options.IncSearch = False
            Options.GroupFooters = False
            Options.Grouping = False
            Options.HorzSizing = False
            Options.Moving = False
            Styles.Header = cxstyl1
            Width = 237
          end
          object cxGridDBArticuloCantidad: TcxGridDBColumn
            Caption = 'Cantidad'
            DataBinding.FieldName = 'ArticuloCantidad'
            HeaderAlignmentHorz = taRightJustify
            MinWidth = 118
            Options.Filtering = False
            Options.FilteringFilteredItemsList = False
            Options.FilteringMRUItemsList = False
            Options.FilteringPopup = False
            Options.FilteringPopupMultiSelect = False
            Options.IgnoreTimeForFiltering = False
            Options.IncSearch = False
            Options.GroupFooters = False
            Options.Grouping = False
            Options.HorzSizing = False
            Options.Moving = False
            Styles.Header = cxstyl1
          end
          object cxGridDBImportePorArticulo: TcxGridDBColumn
            Caption = 'Precio Unitario'
            DataBinding.FieldName = 'ImporteporArticulo'
            HeaderAlignmentHorz = taRightJustify
            MinWidth = 111
            Options.Editing = False
            Options.Filtering = False
            Options.FilteringFilteredItemsList = False
            Options.FilteringMRUItemsList = False
            Options.FilteringPopup = False
            Options.FilteringPopupMultiSelect = False
            Options.IgnoreTimeForFiltering = False
            Options.IncSearch = False
            Options.GroupFooters = False
            Options.Grouping = False
            Options.HorzSizing = False
            Options.Moving = False
            Styles.Header = cxstyl1
            Width = 111
          end
          object cxGridDBArticuloImporte: TcxGridDBColumn
            Caption = 'Total Importe'
            DataBinding.FieldName = 'ArticuloImporte'
            HeaderAlignmentHorz = taRightJustify
            MinWidth = 124
            Options.Editing = False
            Options.Filtering = False
            Options.FilteringFilteredItemsList = False
            Options.FilteringMRUItemsList = False
            Options.FilteringPopup = False
            Options.FilteringPopupMultiSelect = False
            Options.Focusing = False
            Options.IgnoreTimeForFiltering = False
            Options.IncSearch = False
            Options.GroupFooters = False
            Options.Grouping = False
            Options.HorzSizing = False
            Options.Moving = False
            Options.Sorting = False
            Styles.Header = cxstyl1
          end
        end
        object cxGridLevel1: TcxGridLevel
          GridView = cxGridDBTableView1
        end
      end
      object edtTotal: TEdit
        Left = 728
        Top = 447
        Width = 132
        Height = 27
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        ReadOnly = True
        TabOrder = 5
        Text = '0.00'
      end
    end
  end
  object ds1: TJvDataSource
    AutoEdit = False
    DataSet = dset1
    Left = 704
  end
  object actlst1: TActionList
    Left = 256
    Top = 64
    object acSalir: TAction
      Caption = 'acSalir'
    end
    object acSolicitarNC: TAction
      Caption = 'Solicitar'
      OnExecute = acSolicitarNCExecute
    end
    object acConciliarNC: TAction
      Caption = 'Conciliar'
      OnExecute = acConciliarNCExecute
    end
    object acEliminarNC: TAction
      Caption = 'Eliminar'
      OnExecute = acEliminarNCExecute
    end
    object acRecalcularNC: TAction
      Caption = 'acRecalcularNC'
      OnExecute = acRecalcularNCExecute
    end
    object acRecalcularTotal: TAction
      Caption = 'acRecalcularTotal'
      OnExecute = acRecalcularTotalExecute
    end
  end
  object jvpmn1: TJvPopupMenu
    Images = frmMain.ilToolBar32
    ImageMargin.Left = 0
    ImageMargin.Top = 0
    ImageMargin.Right = 0
    ImageMargin.Bottom = 0
    ImageSize.Height = 0
    ImageSize.Width = 0
    Left = 552
    Top = 56
    object mniAgregar1: TMenuItem
      Caption = 'Agregar'
      ImageIndex = 10
    end
    object mniModificar1: TMenuItem
      Caption = 'Modificar'
      ImageIndex = 9
    end
    object mniEliminar1: TMenuItem
      Caption = 'Eliminar'
      ImageIndex = 8
    end
  end
  object cxstylrpstry1: TcxStyleRepository
    Left = 552
    Top = 104
    PixelsPerInch = 120
    object cxstyl1: TcxStyle
      AssignedValues = [svFont]
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
    end
    object cxstyl2: TcxStyle
      AssignedValues = [svFont]
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
    end
    object cxstyl3: TcxStyle
    end
    object cxstyl4: TcxStyle
      AssignedValues = [svFont, svTextColor]
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -11
      Font.Name = 'Segoe UI'
      Font.Style = [fsBold, fsUnderline]
      TextColor = clBlue
    end
    object cxstyl5: TcxStyle
      AssignedValues = [svFont, svTextColor]
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsUnderline]
      TextColor = clHotLight
    end
  end
  object qry1: TADOQuery
    Connection = DatMain.con1
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'SELECT Top 200 * FROM [vw_NC] '
      'order by numeroReq desc')
    Left = 640
    object intgrfldqry1NumeroReq: TIntegerField
      FieldName = 'NumeroReq'
    end
    object intgrfldqry1NumeroNC: TIntegerField
      FieldName = 'NumeroNC'
    end
    object dtmfldqry1Fecha: TDateTimeField
      FieldName = 'Fecha'
    end
    object intgrfldqry1Ticket: TIntegerField
      FieldName = 'Ticket'
    end
    object bcdfldqry1Monto: TBCDField
      FieldName = 'Monto'
      Precision = 19
    end
    object bcdfldqry1Balance: TBCDField
      FieldName = 'Balance'
      Precision = 19
    end
    object strngfldqry1Estado: TStringField
      FieldName = 'Estado'
      FixedChar = True
      Size = 1
    end
    object wdstrngfldqry1Serie: TWideStringField
      FieldName = 'Serie'
      Size = 32
    end
    object wdstrngfldqry1Razon: TWideStringField
      FieldName = 'Razon'
      Size = 250
    end
    object wdstrngfldqry1ArticuloID: TWideStringField
      FieldName = 'ArticuloID'
    end
    object intgrfldqry1RegistroDetalle: TIntegerField
      FieldName = 'RegistroDetalle'
    end
    object wdstrngfldqry1DescripcionArt: TWideStringField
      FieldName = 'DescripcionArt'
      Size = 100
    end
    object wdstrngfldqry1AlmacenID: TWideStringField
      FieldName = 'AlmacenID'
      Size = 50
    end
    object intgrfldqry1TicketUtilizado: TIntegerField
      FieldName = 'TicketUtilizado'
    end
    object wdstrngfldqry1NCF_NC: TWideStringField
      FieldName = 'NCF_NC'
    end
  end
  object qryTickets: TADOQuery
    Connection = DatMain.con1
    CursorType = ctStatic
    Parameters = <
      item
        Name = '@ticket'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 970000
      end>
    SQL.Strings = (
      'SELECT * FROM [vw_Ticket_NC]'
      'WHERE'
      'pidRegistro=:@ticket')
    Left = 800
    Top = 8
    object intgrfldTicketspIdRegistro: TIntegerField
      FieldName = 'pIdRegistro'
    end
    object wdstrngfldTicketsClienteId: TWideStringField
      FieldName = 'ClienteId'
    end
    object wdstrngfldTicketsClienteNombre: TWideStringField
      FieldName = 'ClienteNombre'
      Size = 100
    end
    object wdstrngfldTicketsClienteRnc: TWideStringField
      FieldName = 'ClienteRnc'
    end
    object dtmfldTicketsDocumentoFecha: TDateTimeField
      FieldName = 'DocumentoFecha'
    end
    object wdstrngfldTicketsDescripcion: TWideStringField
      FieldName = 'Descripcion'
      Size = 50
    end
    object wdstrngfldTicketsDireccion: TWideStringField
      FieldName = 'Direccion'
      Size = 50
    end
    object wdstrngfldfactra: TWideStringField
      FieldName = 'Telefono'
      Size = 15
    end
    object wdstrngfldTicketsRNC: TWideStringField
      FieldName = 'RNC'
    end
    object wdstrngfldTicketsArticuloId: TWideStringField
      FieldName = 'ArticuloId'
    end
    object wdstrngfldTicketsArticuloDescripcion: TWideStringField
      FieldName = 'ArticuloDescripcion'
      Size = 100
    end
    object QryArticuloTicketsArticuloPrecio: TFMTBCDField
      FieldName = 'ArticuloPrecio'
      currency = True
      Precision = 19
      Size = 6
    end
    object bcdfldTicketsArticuloCantidad: TBCDField
      FieldName = 'ArticuloCantidad'
      Precision = 18
      Size = 0
    end
    object bcdfldTicketsPrecioRealArticulo: TBCDField
      FieldName = 'PrecioRealArticulo'
      ReadOnly = True
      Precision = 19
      Size = 2
    end
    object bcdfldTicketsArticuloPorcientoImpuesto: TBCDField
      FieldName = 'ArticuloPorcientoImpuesto'
      Precision = 18
      Size = 2
    end
    object QryArticuloTicketsArticuloMonto: TFMTBCDField
      FieldName = 'ArticuloMonto'
      currency = True
      Precision = 19
      Size = 6
    end
    object bcdfldTicketsArticuloPorcientoDesc: TBCDField
      FieldName = 'ArticuloPorcientoDesc'
      Precision = 18
      Size = 2
    end
    object QryArticuloTicketsArticuloImporte: TFMTBCDField
      FieldName = 'ArticuloImporte'
      currency = True
      Precision = 19
      Size = 6
    end
    object wdstrngfldTicketsNumeroNCF: TWideStringField
      FieldName = 'NumeroNCF'
    end
    object wdstrngfldTicketsNCFDescripcion: TWideStringField
      FieldName = 'NCFDescripcion'
      Size = 50
    end
    object bcdfldTicketsMontoEfectivo: TBCDField
      FieldName = 'MontoEfectivo'
      currency = True
      Precision = 19
    end
    object bcdfldTicketsMontoDevuelta: TBCDField
      FieldName = 'MontoDevuelta'
      currency = True
      Precision = 19
    end
    object strngfldTicketsVendedor: TStringField
      FieldName = 'Vendedor'
      ReadOnly = True
      Size = 101
    end
    object QryArticuloTicketsMontoGeneral: TFMTBCDField
      FieldName = 'MontoGeneral'
      currency = True
      Precision = 19
      Size = 6
    end
    object intgrfldTicketsfkTipoNCF: TIntegerField
      FieldName = 'fkTipoNCF'
    end
    object QryArticuloTicketsSubTotal: TFMTBCDField
      FieldName = 'SubTotal'
      Precision = 19
      Size = 6
    end
    object QryArticuloTicketsTotalDesc: TFMTBCDField
      FieldName = 'TotalDesc'
      Precision = 19
      Size = 6
    end
    object QryArticuloTicketsTotalItbis: TFMTBCDField
      FieldName = 'TotalItbis'
      Precision = 19
      Size = 6
    end
    object QryArticuloTicketsEfectivo: TFMTBCDField
      FieldName = 'Efectivo'
      currency = True
      Precision = 19
      Size = 6
    end
    object QryArticuloTicketsCheque: TFMTBCDField
      FieldName = 'Cheque'
      Precision = 19
      Size = 6
    end
    object QryArticuloTicketsTarjeta: TFMTBCDField
      FieldName = 'Tarjeta'
      Precision = 19
      Size = 6
    end
    object QryArticuloTicketsNcredito: TFMTBCDField
      FieldName = 'Ncredito'
      Precision = 19
      Size = 6
    end
    object wdstrngfldTicketsNombrePC: TWideStringField
      FieldName = 'NombrePC'
      Size = 90
    end
    object strngfldTicketsNombre: TStringField
      FieldName = 'Nombre'
      Size = 50
    end
    object wdstrngfldTicketsExpr1: TWideStringField
      FieldName = 'Expr1'
      Size = 100
    end
    object QryArticuloTicketsArticuloTotalDescuento: TFMTBCDField
      FieldName = 'ArticuloTotalDescuento'
      Precision = 19
      Size = 6
    end
    object QryArticuloTicketsArticuloPrecioImpuesto: TFMTBCDField
      FieldName = 'ArticuloPrecioImpuesto'
      Precision = 19
      Size = 6
    end
    object QryArticuloTicketsArticuloTotalImpuesto: TFMTBCDField
      FieldName = 'ArticuloTotalImpuesto'
      Precision = 19
      Size = 6
    end
    object wdstrngfldTicketsCodigo_Almacen: TWideStringField
      FieldName = 'Codigo_Almacen'
      Size = 8
    end
    object QryArticuloTicketsArticuloPrecioDesc: TFMTBCDField
      FieldName = 'ArticuloPrecioDesc'
      Precision = 19
      Size = 6
    end
    object QryArticuloTicketsArticuloImporteNeto: TFMTBCDField
      FieldName = 'ArticuloImporteNeto'
      Precision = 19
      Size = 6
    end
    object wdstrngfldTicketsCodigo_Vendedor: TWideStringField
      FieldName = 'Codigo_Vendedor'
      FixedChar = True
      Size = 10
    end
    object wdstrngfldTicketsSerie: TWideStringField
      FieldName = 'Serie'
      Size = 32
    end
    object wdstrngfldTicketsUsuario: TWideStringField
      FieldName = 'Usuario'
      Size = 10
    end
    object wdstrngfldTicketsManejaSeriales: TWideStringField
      FieldName = 'ManejaSeriales'
      FixedChar = True
      Size = 1
    end
    object smlntfldTicketsNCFControl: TSmallintField
      FieldName = 'NCFControl'
    end
    object intgrfldTicketsNo_Linea: TIntegerField
      FieldName = 'No_Linea'
    end
    object intgrfldTicketsSecuencia: TIntegerField
      FieldName = 'Secuencia'
      ReadOnly = True
    end
    object intgrfldTicketsRegistroDetalle: TIntegerField
      FieldName = 'RegistroDetalle'
    end
    object blnfldTicketsComprobante: TBooleanField
      FieldName = 'Comprobante'
    end
    object QryArticuloTicketsImporteporArticulo: TFMTBCDField
      FieldName = 'ImporteporArticulo'
      ReadOnly = True
      DisplayFormat = '##,###,###.00'
      Precision = 38
      Size = 25
    end
    object bcdfldTicketsCantidad: TBCDField
      FieldName = 'Cantidad'
      ReadOnly = True
      Precision = 18
      Size = 0
    end
  end
  object dsTicket: TJvDataSource
    DataSet = qryTickets
    Left = 848
    Top = 8
  end
  object spInsertaNC: TADOStoredProc
    Connection = DatMain.con1
    ProcedureName = 'sp_InsertaNC;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 14836
      end
      item
        Name = '@NumeroReq'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroNC'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Monto'
        Attributes = [paNullable]
        DataType = ftBCD
        Precision = 19
        Value = Null
      end
      item
        Name = '@Balance'
        Attributes = [paNullable]
        DataType = ftBCD
        Precision = 19
        Value = Null
      end
      item
        Name = '@Ticket'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Fecha'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 10
        Value = Null
      end
      item
        Name = '@Estado'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@AlmacenID'
        DataType = ftWideString
        Size = -1
        Value = Null
      end
      item
        Name = '@Razon'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 250
        Value = Null
      end
      item
        Name = '@NCF_NC'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 20
        Value = Null
      end>
    Left = 456
    Top = 64
  end
  object spVerificaNC: TADOStoredProc
    Connection = DatMain.con1
    ProcedureName = 'sp_VerificaNC;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@Ticket'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@RegistroDetalle'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Serie'
        Attributes = [paNullable]
        DataType = ftString
        Size = -1
        Value = Null
      end>
    Left = 392
    Top = 64
  end
  object spConciliarNC: TADOStoredProc
    Connection = DatMain.con1
    ProcedureName = 'sp_CONCILIARNC;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroReq'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroNC'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Conciliacion_UsuarioID'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 320
    Top = 64
  end
  object il1: TImageList
    Left = 8
    Top = 56
    Bitmap = {
      494C010102000800380110001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000001000000001002000000000000010
      000000000000000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC7C6FD00FFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFF0607EE005D5CC100FFFFFFFF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF3357FF002E51FF00233E
      FF003532F800FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFF6F6DDA000B13FF005A59C000FFFFFFFF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF325BFF002C4FFF00233F
      FF001628FF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFDE
      F4000200EA000509EF00A4A3DB00FFFFFFFF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDCDAD500FFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7190FF00284BFF00233D
      FF001F3AFF00807FF400FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0500
      FF00100CFF004747BC00FFFFFFFFFFFFFFFF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDCD8D300CA8B1F00B094
      6200FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF4248FF005275FF00233D
      FF001F39FF000B10FB00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA8A6EA000F07
      FF00100CFF00CAC9EC00FFFFFFFFFFFFFFFF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCEC7BC00ED990600FCA3
      0100B1966200FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0400FF00213D
      FF001E38FF00192CFF000C15FF00FFFFFFFFFFFFFFFF0601FB000F09FF000F08
      FF000400CE00FFFFFFFFFFFFFFFFFFFFFFFF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFFFFB99D7C00C2935A00C293
      5A00C2955A00C2965A00C2975A00C2985A00C29A5A00BC914A00FCA00100FFA5
      0000FCA60100B2966200FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCCCCFD007591
      FF001D38FF00192BFF001824FF00FFFFFFFFFFFFFFFF110DFF000F09FF000500
      F8006F6DD500FFFFFFFFFFFFFFFFFFFFFFFF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFFFFC2884400FF8B0000FF8D
      0000FF900000FF930000FF960000FF990000FF9C0000FF9F0000FFA20000FFA5
      0000FFA80000FCA80100B8A07100FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF5B58
      F800414AFF001728FF001825FF001317FF001616FF00120EFF00120CFF00CBCA
      F100FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFFFFC2884400FF8B0000FF8D
      0000FF900000FF930000FF960000FF990000FF9C0000FF9F0000FFA20000FFA5
      0000FFA80000FCA80100B8A07100FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFF201EF4005568FF001723FF00171CFF001616FF001211FF000803EB00FFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFFFFB99D7C00C2935A00C293
      5A00C2955A00C2965A00C2975A00C2985A00C29A5A00BC914A00FCA00100FFA5
      0000FCA60100B2966200FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFF8D89FA002638FF001B25FF001B1FFF001C1CFF000D09E900D5D4F600FFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCEC7BC00ED990600FCA3
      0100B1966200FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF545B
      FF00637AFF005F70FF005F6CFF004B4FFF004142FF002624FF00130DEA00FFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDCD8D300CA8B1F00B094
      6200FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6F83FF007894FF006F85
      FF006C81FF006373FF007D8AFF005F58EC005B54EF008F8DFF005B55FF00514B
      F800A9A8EA00FFFFFFFFFFFFFFFFFFFFFFFF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDCDAD500FFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFAAA5FD00809CFF007B94FF007389
      FF006F84FF0095A3FF008989FC00FFFFFFFFD7D5F900807DF700AAA6FF005C56
      FF005754FF00FFFFFFFFFFFFFFFFFFFFFFFF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9093FF00839FFF008098FF0095A5
      FF00A9B3FF00B0ACF800FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD5D4F6007873
      ED00A19BFD00979DFF007680FB00E3E2F5000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7770FD00BCCDFF00B4C5FF00979C
      FD006E68F900FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBAB7
      F0006A67DC008C8DF2009FA7FA00797EE6000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000100000000100010000000000800000000000000000000000
      000000000000000000000000FFFFFF0000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000}
  end
  object dset1: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'dtstprvdr1'
    Left = 704
    Top = 48
    object intgrflddset1NumeroReq: TIntegerField
      FieldName = 'NumeroReq'
    end
    object intgrflddset1NumeroNC: TIntegerField
      FieldName = 'NumeroNC'
    end
    object bcdflddset1Monto: TBCDField
      FieldName = 'Monto'
      DisplayFormat = '#,###,###0.00'
      Precision = 19
    end
    object bcdflddset1Balance: TBCDField
      FieldName = 'Balance'
      DisplayFormat = '#,###,###0.00'
      Precision = 19
    end
    object intgrflddset1Ticket: TIntegerField
      FieldName = 'Ticket'
    end
    object strngflddset1Estado: TStringField
      FieldName = 'Estado'
      FixedChar = True
      Size = 1
    end
    object wdstrngflddset1Serie: TWideStringField
      FieldName = 'Serie'
      Size = 32
    end
    object dtmflddset1Fecha: TDateTimeField
      FieldName = 'Fecha'
    end
    object wdstrngflddset1DescripcionArt: TWideStringField
      FieldName = 'DescripcionArt'
      Size = 100
    end
    object intgrflddset1TicketUtilizado: TIntegerField
      FieldName = 'TicketUtilizado'
    end
    object wdstrngflddset1NCF_NC: TWideStringField
      FieldName = 'NCF_NC'
    end
  end
  object dtstprvdr1: TDataSetProvider
    DataSet = qry1
    Left = 640
    Top = 48
  end
  object usrsrg1: TUsersReg
    FormName = 'frmNotasdeCredito'
    FormCaption = 'Notas de Cr'#233'dito'
    ComponentList.Strings = (
      'cxpgcntrl1=Planillas=None'
      'cxpgcntrl1Parent=frmNotasdeCredito'
      'cxtbsht1=Listado SNC=None'
      'cxtbsht1Parent=cxpgcntrl1'
      'cxtbsht2=Solicitud (Abierto)=None'
      'cxtbsht2Parent=cxpgcntrl1'
      'cxtbsht3=Solicitud (Tickets)=None'
      'cxtbsht3Parent=cxpgcntrl1')
    SecurityComponent = frmMain.usrs1
    IsRepositoryForm = False
    AutoApplySecurity = True
    Left = 912
    Top = 328
  end
  object dsCllientes: TDataSource
    DataSet = tblClientes
    Left = 912
    Top = 392
  end
  object tblClientes: TADOTable
    Connection = DatMain.con1
    CursorType = ctStatic
    TableName = 'Clientes'
    Left = 912
    Top = 440
  end
  object dsAlmacenes: TDataSource
    DataSet = qryAlmacenes
    Left = 712
    Top = 112
  end
  object qryAlmacenes: TADOQuery
    Connection = DatMain.con1
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select * from almacenes')
    Left = 640
    Top = 112
  end
  object sp2: TADOStoredProc
    Connection = DatMain.con1
    Parameters = <>
    Left = 912
    Top = 264
  end
  object cdsNotaCredito: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'DataSetProvider1'
    AfterDelete = cdsNotaCreditoAfterDelete
    Left = 896
    Top = 592
    object cdsNotaCreditopIdRegistro: TIntegerField
      FieldName = 'pIdRegistro'
    end
    object cdsNotaCreditoClienteId: TWideStringField
      FieldName = 'ClienteId'
    end
    object cdsNotaCreditoClienteNombre: TWideStringField
      FieldName = 'ClienteNombre'
      Size = 100
    end
    object cdsNotaCreditoClienteRnc: TWideStringField
      FieldName = 'ClienteRnc'
    end
    object cdsNotaCreditoDocumentoFecha: TDateTimeField
      FieldName = 'DocumentoFecha'
    end
    object cdsNotaCreditoDescripcion: TWideStringField
      FieldName = 'Descripcion'
      Size = 50
    end
    object cdsNotaCreditoDireccion: TWideStringField
      FieldName = 'Direccion'
      Size = 50
    end
    object cdsNotaCreditoTelefono: TWideStringField
      FieldName = 'Telefono'
      Size = 15
    end
    object cdsNotaCreditoRNC: TWideStringField
      FieldName = 'RNC'
    end
    object cdsNotaCreditoArticuloId: TWideStringField
      FieldName = 'ArticuloId'
    end
    object cdsNotaCreditoArticuloDescripcion: TWideStringField
      FieldName = 'ArticuloDescripcion'
      Size = 100
    end
    object cdsNotaCreditoArticuloPrecio: TFMTBCDField
      FieldName = 'ArticuloPrecio'
      DisplayFormat = '#,###.00'
      EditFormat = '#,###.00'
      MaxValue = '9999999'
      MinValue = '1'
      Precision = 19
      Size = 6
    end
    object cdsNotaCreditoArticuloCantidad: TBCDField
      FieldName = 'ArticuloCantidad'
      OnChange = cdsNotaCreditoArticuloCantidadChange
      OnValidate = cdsNotaCreditoArticuloCantidadValidate
      MaxValue = 99999.000000000000000000
      MinValue = 1.000000000000000000
      Precision = 18
      Size = 0
    end
    object cdsNotaCreditoPrecioRealArticulo: TBCDField
      FieldName = 'PrecioRealArticulo'
      ReadOnly = True
      Precision = 19
      Size = 2
    end
    object cdsNotaCreditoArticuloPorcientoImpuesto: TBCDField
      FieldName = 'ArticuloPorcientoImpuesto'
      Precision = 18
      Size = 2
    end
    object cdsNotaCreditoArticuloMonto: TFMTBCDField
      FieldName = 'ArticuloMonto'
      Precision = 19
      Size = 6
    end
    object cdsNotaCreditoArticuloPorcientoDesc: TBCDField
      FieldName = 'ArticuloPorcientoDesc'
      Precision = 18
      Size = 2
    end
    object cdsNotaCreditoArticuloImporte: TFMTBCDField
      FieldName = 'ArticuloImporte'
      DisplayFormat = '#,###.00'
      EditFormat = '#,###.00'
      Precision = 19
      Size = 6
    end
    object cdsNotaCreditoNumeroNCF: TWideStringField
      FieldName = 'NumeroNCF'
    end
    object cdsNotaCreditoNCFDescripcion: TWideStringField
      FieldName = 'NCFDescripcion'
      Size = 50
    end
    object cdsNotaCreditoMontoEfectivo: TBCDField
      FieldName = 'MontoEfectivo'
      Precision = 19
    end
    object cdsNotaCreditoMontoDevuelta: TBCDField
      FieldName = 'MontoDevuelta'
      Precision = 19
    end
    object cdsNotaCreditoVendedor: TStringField
      FieldName = 'Vendedor'
      ReadOnly = True
      Size = 101
    end
    object cdsNotaCreditoMontoGeneral: TFMTBCDField
      FieldName = 'MontoGeneral'
      Precision = 19
      Size = 6
    end
    object cdsNotaCreditofkTipoNCF: TIntegerField
      FieldName = 'fkTipoNCF'
    end
    object cdsNotaCreditoSubTotal: TFMTBCDField
      FieldName = 'SubTotal'
      Precision = 19
      Size = 6
    end
    object cdsNotaCreditoTotalDesc: TFMTBCDField
      FieldName = 'TotalDesc'
      Precision = 19
      Size = 6
    end
    object cdsNotaCreditoTotalItbis: TFMTBCDField
      FieldName = 'TotalItbis'
      Precision = 19
      Size = 6
    end
    object cdsNotaCreditoEfectivo: TFMTBCDField
      FieldName = 'Efectivo'
      Precision = 19
      Size = 6
    end
    object cdsNotaCreditoCheque: TFMTBCDField
      FieldName = 'Cheque'
      Precision = 19
      Size = 6
    end
    object cdsNotaCreditoTarjeta: TFMTBCDField
      FieldName = 'Tarjeta'
      Precision = 19
      Size = 6
    end
    object cdsNotaCreditoNcredito: TFMTBCDField
      FieldName = 'Ncredito'
      Precision = 19
      Size = 6
    end
    object cdsNotaCreditoNombrePC: TWideStringField
      FieldName = 'NombrePC'
      Size = 90
    end
    object cdsNotaCreditoNombre: TStringField
      FieldName = 'Nombre'
      Size = 50
    end
    object cdsNotaCreditoExpr1: TWideStringField
      FieldName = 'Expr1'
      Size = 100
    end
    object cdsNotaCreditoArticuloTotalDescuento: TFMTBCDField
      FieldName = 'ArticuloTotalDescuento'
      Precision = 19
      Size = 6
    end
    object cdsNotaCreditoArticuloPrecioImpuesto: TFMTBCDField
      FieldName = 'ArticuloPrecioImpuesto'
      Precision = 19
      Size = 6
    end
    object cdsNotaCreditoArticuloTotalImpuesto: TFMTBCDField
      FieldName = 'ArticuloTotalImpuesto'
      Precision = 19
      Size = 6
    end
    object cdsNotaCreditoCodigo_Almacen: TWideStringField
      FieldName = 'Codigo_Almacen'
      Size = 8
    end
    object cdsNotaCreditoArticuloPrecioDesc: TFMTBCDField
      FieldName = 'ArticuloPrecioDesc'
      Precision = 19
      Size = 6
    end
    object cdsNotaCreditoArticuloImporteNeto: TFMTBCDField
      FieldName = 'ArticuloImporteNeto'
      Precision = 19
      Size = 6
    end
    object cdsNotaCreditoCodigo_Vendedor: TWideStringField
      FieldName = 'Codigo_Vendedor'
      FixedChar = True
      Size = 10
    end
    object cdsNotaCreditoSerie: TWideStringField
      FieldName = 'Serie'
      Size = 32
    end
    object cdsNotaCreditoUsuario: TWideStringField
      FieldName = 'Usuario'
      Size = 10
    end
    object cdsNotaCreditoManejaSeriales: TWideStringField
      FieldName = 'ManejaSeriales'
      FixedChar = True
      Size = 1
    end
    object cdsNotaCreditoNCFControl: TSmallintField
      FieldName = 'NCFControl'
    end
    object cdsNotaCreditoNo_Linea: TIntegerField
      FieldName = 'No_Linea'
    end
    object cdsNotaCreditoSecuencia: TIntegerField
      FieldName = 'Secuencia'
      ReadOnly = True
    end
    object cdsNotaCreditoRegistroDetalle: TIntegerField
      FieldName = 'RegistroDetalle'
    end
    object cdsNotaCreditoComprobante: TBooleanField
      FieldName = 'Comprobante'
    end
    object cdsNotaCreditoImporteporArticulo: TFMTBCDField
      FieldName = 'ImporteporArticulo'
      DisplayFormat = '##,###,###.00'
      EditFormat = '##,###,###.00'
      MaxValue = '99999999'
      MinValue = '1'
      Precision = 38
      Size = 25
    end
    object cdsNotaCreditoCantidad: TBCDField
      FieldName = 'Cantidad'
      ReadOnly = True
      Precision = 18
      Size = 0
    end
  end
  object dsNotaCreditoD: TDataSource
    DataSet = cdsNotaCredito
    Left = 904
    Top = 504
  end
  object DataSetProvider1: TDataSetProvider
    DataSet = qryTickets
    Left = 960
    Top = 552
  end
  object spInsertaNCCabecera: TADOStoredProc
    Connection = DatMain.con1
    ProcedureName = 'sp_InsertaNC_Cabecera;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroReq'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroNC'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Monto'
        Attributes = [paNullable]
        DataType = ftBCD
        Precision = 19
        Value = Null
      end
      item
        Name = '@Balance'
        Attributes = [paNullable]
        DataType = ftBCD
        Precision = 19
        Value = Null
      end
      item
        Name = '@Ticket'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Fecha'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 10
        Value = Null
      end
      item
        Name = '@Estado'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@Razon'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 250
        Value = Null
      end
      item
        Name = '@NCF_NC'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 20
        Value = Null
      end>
    Left = 896
    Top = 112
  end
end
