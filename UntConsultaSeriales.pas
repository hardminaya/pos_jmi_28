unit UntConsultaSeriales;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.Grids, Vcl.DBGrids, Data.DB,
  Data.Win.ADODB, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Vcl.Menus,
  dxSkinsCore, dxSkinsDefaultPainters, Vcl.StdCtrls, cxButtons, Vcl.CheckLst,
  Vcl.ExtCtrls, dxGDIPlusClasses;

type
  TfrmConsultaSeriales = class(TForm)
    qrySeriales: TADOQuery;
    dsSeriales: TDataSource;
    wdstrngfldSerialesSerialNo: TWideStringField;
    atncfldSerialespIdRegistro: TAutoIncField;
    intgrfldSerialesNo_Linea: TIntegerField;
    wdstrngfldSerialesfkIdArticulo: TWideStringField;
    intgrfldSerialesfkEntrada: TIntegerField;
    strngfldSerialesEstatus: TStringField;
    intgrfldSerialesfkFacturaVenta: TIntegerField;
    wdstrngfldSerialesCodigo_Almacen: TWideStringField;
    intgrfldSerialesCantidad: TIntegerField;
    btn1: TcxButton;
    chklstSerialesDisponibles: TCheckListBox;
    chklstSerialesSeleccionados: TCheckListBox;
    lbl1: TLabel;
    lbl2: TLabel;
    btn2: TcxButton;
    img1: TImage;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure dbgrd1DblClick(Sender: TObject);
    procedure chklstSerialesDisponiblesClickCheck(Sender: TObject);
    procedure chklstSerialesSeleccionadosClickCheck(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btn1Click(Sender: TObject);

  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmConsultaSeriales: TfrmConsultaSeriales;

implementation

uses
  UntData, UntFacturacion;

{$R *.dfm}

procedure TfrmConsultaSeriales.btn1Click(Sender: TObject);
var
  i : Integer;
begin
  with chklstSerialesSeleccionados do begin
    for i := 0 to Count-1 do
    begin
      frmFacturacion.edtArticulo.Text:=chklstSerialesSeleccionados.Items[i];
      frmFacturacion.acBuscarArticuloSeriales.Execute;
    end;
  end;

end;

procedure TfrmConsultaSeriales.chklstSerialesDisponiblesClickCheck(
  Sender: TObject);
begin
  chklstSerialesSeleccionados.Items.Add(chklstSerialesDisponibles.Items[chklstSerialesDisponibles.ItemIndex]);
  chklstSerialesDisponibles.Items.Delete(chklstSerialesDisponibles.ItemIndex);
end;

procedure TfrmConsultaSeriales.chklstSerialesSeleccionadosClickCheck(
  Sender: TObject);
begin
  chklstSerialesDisponibles.Items.Add(chklstSerialesSeleccionados.Items[chklstSerialesSeleccionados.ItemIndex]);
  chklstSerialesSeleccionados.Items.Delete(chklstSerialesSeleccionados.ItemIndex);
end;

procedure TfrmConsultaSeriales.dbgrd1DblClick(Sender: TObject);
begin
  frmFacturacion.edtArticulo.Text:=qrySeriales.FieldByName('SerialNo').AsString;
  frmFacturacion.acBuscarArticuloSeriales.Execute;
  self.ModalResult:=mrNone;
  Exit;
end;

procedure TfrmConsultaSeriales.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action:=caFree;
  frmConsultaSeriales:=nil;
end;

procedure TfrmConsultaSeriales.FormShow(Sender: TObject);
begin
  with chklstSerialesDisponibles do begin
    qrySeriales.Parameters.ParamByName('Almacen').Value:=CUsuario.ID_Almacen;
    qrySeriales.Parameters.ParamByName('Articulo').Value:=cArticulo.CodigoArt;
    qrySeriales.Open;
    qrySeriales.First;

    while not qrySeriales.eof do begin
      Items.Add(qrySeriales.FieldByName('SerialNo').AsString);
      qrySeriales.Next;
    end;
  end;
end;
end.
