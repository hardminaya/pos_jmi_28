unit UntClientes;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, dxSkinsCore, dxSkinsDefaultPainters,
  dxSkinscxPCPainter, cxPCdxBarPopupMenu, cxStyles, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxEdit, Data.DB, cxDBData, Vcl.Menus, cxClasses,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, Vcl.StdCtrls,
  cxButtons, cxGridLevel, cxGridCustomView, cxGrid, cxPC, cxContainer,
  cxMaskEdit, cxDropDownEdit, cxCalendar, cxDBEdit, cxLabel, cxTextEdit,
  Vcl.ExtCtrls, Vcl.DBCtrls, cxVGrid, cxDBVGrid, cxInplaceContainer, Vcl.ImgList,
  dxSkinsdxNavBarPainter, dxNavBarCollns, dxNavBarBase, dxNavBar, cxCheckBox,
  Vcl.Mask, cxGroupBox, cxRadioGroup, dxSkinsdxStatusBarPainter, dxStatusBar,
  cxNavigator, cxDBNavigator, Vcl.ComCtrls, Vcl.ToolWin, Vcl.DBActns,
  Vcl.StdActns, Vcl.ActnList, Vcl.PlatformDefaultStyleActnCtrls, Vcl.ActnMan,
  Data.Win.ADODB;

type
  TfrmClientes = class(TForm)
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    dsClientes: TDataSource;
    cxStyleRepository1: TcxStyleRepository;
    cxStyle1: TcxStyle;
    cxStyle2: TcxStyle;
    cxGrid1DBTableView1ClienteID: TcxGridDBColumn;
    cxGrid1DBTableView1Nombre: TcxGridDBColumn;
    cxGrid1DBTableView1TipoCliente: TcxGridDBColumn;
    cxGrid1DBTableView1RNC: TcxGridDBColumn;
    cxGrid1DBTableView1TarjetaNo: TcxGridDBColumn;
    cxGrid1DBTableView1Descuento: TcxGridDBColumn;
    cxtbshtEstadisticas: TcxTabSheet;
    cxtbshtEstadisticasDetalle: TcxTabSheet;
    cxstyl1: TcxStyle;
    lblNombre: TLabel;
    lblDireccion: TLabel;
    lblTRNC: TLabel;
    cxdbrdgrpTipoCliente: TcxDBRadioGroup;
    cxdbnvgtr1: TcxDBNavigator;
    lbl1: TLabel;
    lbl2: TLabel;
    lbl3: TLabel;
    edt2: TcxDBMaskEdit;
    edt3: TcxDBTextEdit;
    edt4: TcxDBTextEdit;
    edt5: TcxDBTextEdit;
    actmgr1: TActionManager;
    dtstpr1: TDataSetPrior;
    dtstnxt1: TDataSetNext;
    act1: TDataSetEdit;
    act3: TDataSetInsert;
    wndwcls1: TWindowClose;
    act2: TDataSetCancel;
    act4: TDataSetPost;
    act5: TDataSetDelete;
    tlb3: TToolBar;
    btn10: TToolButton;
    btnDataSetPrior1: TToolButton;
    btn14: TToolButton;
    btnDataSetEdit1: TToolButton;
    btn13: TToolButton;
    btn12: TToolButton;
    btn15: TToolButton;
    btn11: TToolButton;
    btn9: TToolButton;
    btnSalir: TToolButton;
    cxdbtxtdt1: TcxDBTextEdit;
    lblEmail: TLabel;
    edt6: TcxDBTextEdit;
    cxtbsht1: TcxTabSheet;
    edt1: TcxDBTextEdit;
    lbl4: TLabel;
    tblClientes: TADOTable;
    edt7: TcxDBTextEdit;
    edt8: TcxDBTextEdit;
    dbnvgr1: TDBNavigator;
    atncfldClientespIdRegistro: TAutoIncField;
    wdstrngfldClientesClienteID: TWideStringField;
    wdstrngfldClientesNombre: TWideStringField;
    strngfldClientesTipoCliente: TStringField;
    wdstrngfldClientesDireccion: TWideStringField;
    wdstrngfldClientesRNC: TWideStringField;
    strngfldClientesGenerico: TStringField;
    wdstrngfldClientesTelefono1: TWideStringField;
    wdstrngfldClientesEmail1: TWideStringField;
    procedure btnSalirClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure cxGrid1DBTableView1CellDblClick(Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure act5Execute(Sender: TObject);
    procedure wndwcls1Execute(Sender: TObject);
    procedure act1Execute(Sender: TObject);
    procedure act3Execute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmClientes: TfrmClientes;

implementation

{$R *.dfm}

uses UntData;

procedure TfrmClientes.act1Execute(Sender: TObject);
begin
  cxPageControl1.ActivePageIndex:=1;
  dsClientes.DataSet.Edit;
end;

procedure TfrmClientes.act3Execute(Sender: TObject);
begin
  cxPageControl1.ActivePageIndex:=1;
  try
    dsClientes.DataSet.Insert;
    except
    on E: Exception do begin
      ShowMessage(E.Message);
    end;
  end;

end;

procedure TfrmClientes.act5Execute(Sender: TObject);
begin
  if MessageBox(Handle,'Seguro desear ELIMINAR el registro?','Cliente',MB_ICONQUESTION or MB_YESNO)=ID_YES then
  begin
    dsClientes.DataSet.Delete;
  end;
end;

procedure TfrmClientes.btnSalirClick(Sender: TObject);
begin
  if dsClientes.State=dsEdit then
  begin
    if MessageBox(Handle,'Los Datos no han sido guardados, se perderan, est� Seguro de Salir?','Caja',MB_ICONQUESTION or MB_YESNO)=ID_YES then
    begin
      dsClientes.DataSet.Cancel;
    end
    else
    begin
      exit;
    end;
  end;
  self.Close();
end;

procedure TfrmClientes.cxGrid1DBTableView1CellDblClick(
  Sender: TcxCustomGridTableView; ACellViewInfo: TcxGridTableDataCellViewInfo;
  AButton: TMouseButton; AShift: TShiftState; var AHandled: Boolean);
begin
  cxPageControl1.ActivePageIndex:=1;
end;

procedure TfrmClientes.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action:=caFree;
  frmClientes:=nil;
end;

procedure TfrmClientes.FormShow(Sender: TObject);
begin
  datmain.qryClientes.Open;
  tblClientes.Open;
end;

procedure TfrmClientes.wndwcls1Execute(Sender: TObject);
begin
  if dsClientes.State=dsEdit then
  begin
    if MessageBox(Handle,'Los Datos no han sido guardados, se perderan, est� Seguro de Salir?','Caja',MB_ICONQUESTION or MB_YESNO)=ID_YES then
    begin
      dsClientes.DataSet.Cancel;
    end
    else
    begin
      exit;
    end;
  end;
  self.Close();
end;

end.
