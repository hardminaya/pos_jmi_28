object frmCajas: TfrmCajas
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu]
  Caption = 'Cajas'
  ClientHeight = 494
  ClientWidth = 675
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Visible = True
  WindowState = wsMaximized
  OnClose = FormClose
  OnShow = FormShow
  DesignSize = (
    675
    494)
  PixelsPerInch = 96
  TextHeight = 13
  object cxPageControl1: TcxPageControl
    Left = 8
    Top = 71
    Width = 659
    Height = 410
    Anchors = [akLeft, akTop, akRight, akBottom]
    TabOrder = 1
    Properties.ActivePage = cxTabSheet1
    ClientRectBottom = 404
    ClientRectLeft = 3
    ClientRectRight = 653
    ClientRectTop = 26
    object cxTabSheet1: TcxTabSheet
      Caption = 'Listado'
      ImageIndex = 0
      object cxGrid1: TcxGrid
        Left = 0
        Top = 0
        Width = 650
        Height = 378
        Align = alClient
        TabOrder = 0
        object cxGrid1DBTableView1: TcxGridDBTableView
          OnCellDblClick = cxGrid1DBTableView1CellDblClick
          DataController.DataSource = dsCajas
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          object ColID: TcxGridDBColumn
            DataBinding.FieldName = 'ID'
            HeaderAlignmentHorz = taCenter
            Styles.Content = cxStyle1
            Styles.Header = cxstyl1
          end
          object ColNombrePC: TcxGridDBColumn
            DataBinding.FieldName = 'NombrePC'
            HeaderAlignmentHorz = taCenter
            Styles.Content = cxStyle1
            Styles.Header = cxstyl1
            Width = 267
          end
          object ColSupervisor: TcxGridDBColumn
            DataBinding.FieldName = 'Supervisor'
            HeaderAlignmentHorz = taCenter
            Styles.Content = cxStyle1
            Styles.Header = cxstyl1
            Width = 148
          end
          object ColImprimeCopia: TcxGridDBColumn
            Caption = 'Copia'
            DataBinding.FieldName = 'ImprimieCopia'
            PropertiesClassName = 'TcxCheckBoxProperties'
            Properties.ValueChecked = '1'
            Properties.ValueUnchecked = '0'
            HeaderAlignmentHorz = taCenter
            Styles.Header = cxstyl1
          end
        end
        object cxGrid1Level1: TcxGridLevel
          GridView = cxGrid1DBTableView1
        end
      end
    end
    object cxtbshtEstadisticas1: TcxTabSheet
      Caption = 'Detalle'
      ImageIndex = 1
      object cxlbl6: TcxLabel
        Left = 23
        Top = 27
        Caption = 'ID Caja'
        ParentFont = False
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'Tahoma'
        Style.Font.Style = [fsBold]
        Style.IsFontAssigned = True
      end
      object grp1: TGroupBox
        Left = 23
        Top = 85
        Width = 458
        Height = 84
        Caption = 'Funciones'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 6
        object cxdbspndt1: TcxDBSpinEdit
          Left = 16
          Top = 33
          DataBinding.DataField = 'ImprimieCopia'
          DataBinding.DataSource = dsCajas
          Properties.MaxValue = 10.000000000000000000
          TabOrder = 2
          Width = 121
        end
        object cxlbl3: TcxLabel
          Left = 16
          Top = 19
          Caption = 'Cantidad de Copias'
          ParentFont = False
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -11
          Style.Font.Name = 'Tahoma'
          Style.Font.Style = [fsBold]
          Style.IsFontAssigned = True
        end
        object chkSeleccionesSeriales: TcxDBCheckBox
          Left = 161
          Top = 17
          Caption = 'Selecciones Seriales'
          DataBinding.DataField = 'SeleccionaSeriales'
          DataBinding.DataSource = dsCajas
          TabOrder = 0
          Width = 160
        end
        object chkEmiteNCF: TcxDBCheckBox
          Left = 161
          Top = 42
          Caption = 'Emite NCF'
          DataBinding.DataField = 'EmiteComprobantes'
          DataBinding.DataSource = dsCajas
          TabOrder = 3
          Width = 160
        end
      end
      object cxlbl1: TcxLabel
        Left = 135
        Top = 27
        Caption = 'Nombre de Caja'
        ParentFont = False
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'Tahoma'
        Style.Font.Style = [fsBold]
        Style.IsFontAssigned = True
      end
      object cxlbl2: TcxLabel
        Left = 302
        Top = 27
        Caption = 'Supervisor'
        ParentFont = False
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'Tahoma'
        Style.Font.Style = [fsBold]
        Style.IsFontAssigned = True
      end
      object edtIDCaja: TcxDBMaskEdit
        Left = 23
        Top = 42
        DataBinding.DataField = 'ID'
        DataBinding.DataSource = dsCajas
        Properties.MaxLength = 50
        TabOrder = 3
        Width = 106
      end
      object edtNombreCaja: TcxDBMaskEdit
        Left = 135
        Top = 42
        DataBinding.DataField = 'NombrePC'
        DataBinding.DataSource = dsCajas
        Properties.MaxLength = 90
        TabOrder = 4
        Width = 161
      end
      object edtSupervisor: TcxDBMaskEdit
        Left = 302
        Top = 42
        DataBinding.DataField = 'Supervisor'
        DataBinding.DataSource = dsCajas
        Properties.MaxLength = 50
        TabOrder = 5
        Width = 179
      end
    end
  end
  object tlb1: TToolBar
    Left = 0
    Top = 0
    Width = 675
    Height = 65
    ButtonHeight = 60
    ButtonWidth = 57
    Caption = 'tlb1'
    Images = frmMain.ilToolBar32
    ShowCaptions = True
    TabOrder = 0
    object btnRegistro: TToolButton
      Left = 0
      Top = 0
      Caption = 'Registro'
      DropdownMenu = jvpmn1
      ImageIndex = 9
      Style = tbsDropDown
    end
    object btnAnterior: TToolButton
      Left = 72
      Top = 0
      Caption = 'Anterior'
      ImageIndex = 3
      OnClick = btnAnteriorClick
    end
    object btnSiguiente: TToolButton
      Left = 129
      Top = 0
      Caption = 'Siguiente'
      ImageIndex = 0
      OnClick = btnSiguienteClick
    end
    object btnGuardar: TToolButton
      Left = 186
      Top = 0
      Caption = 'Guardar'
      ImageIndex = 2
      Visible = False
      OnClick = btnGuardarClick
    end
    object btnCancelar: TToolButton
      Left = 243
      Top = 0
      Caption = 'Cancelar'
      ImageIndex = 5
      OnClick = btnCancelarClick
    end
    object btn2: TToolButton
      Left = 300
      Top = 0
      Width = 55
      Caption = 'btn5'
      ImageIndex = 5
      Style = tbsSeparator
    end
    object btnSalir: TToolButton
      Left = 355
      Top = 0
      Caption = 'Salir'
      ImageIndex = 1
      OnClick = btnSalirClick
    end
  end
  object qry_Cajas: TADOQuery
    Connection = DatMain.con1
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'SELECT * FROM CAJA')
    Left = 72
    Top = 304
    object atncfld_CajasID: TAutoIncField
      FieldName = 'ID'
      ReadOnly = True
    end
    object wdstrngfld_CajasNombrePC: TWideStringField
      FieldName = 'NombrePC'
      Size = 90
    end
    object wdstrngfld_CajasSupervisor: TWideStringField
      FieldName = 'Supervisor'
      Size = 50
    end
    object intgrfld_CajasImprimieCopia: TIntegerField
      FieldName = 'ImprimieCopia'
    end
    object blnfld_CajasSeleccionaSeriales: TBooleanField
      FieldName = 'SeleccionaSeriales'
    end
    object blnfld_CajasEmiteComprobantes: TBooleanField
      FieldName = 'EmiteComprobantes'
    end
  end
  object dsCajas: TDataSource
    AutoEdit = False
    DataSet = dsetCajas
    Left = 304
    Top = 288
  end
  object cxStyleRepository1: TcxStyleRepository
    Left = 480
    Top = 8
    PixelsPerInch = 96
    object cxStyle1: TcxStyle
    end
    object cxStyle2: TcxStyle
      AssignedValues = [svFont]
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
    end
    object cxstyl1: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clActiveCaption
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
    end
    object cxstyl2: TcxStyle
    end
  end
  object dsetCajas: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'dtstprvdrCajas'
    Left = 208
    Top = 296
  end
  object dtstprvdrCajas: TDataSetProvider
    DataSet = qry_Cajas
    Left = 144
    Top = 296
  end
  object jvpmn1: TJvPopupMenu
    Images = frmMain.ilToolBar32
    ImageMargin.Left = 0
    ImageMargin.Top = 0
    ImageMargin.Right = 0
    ImageMargin.Bottom = 0
    ImageSize.Height = 0
    ImageSize.Width = 0
    Left = 576
    Top = 8
    object mniBuscar1: TMenuItem
      Caption = '&Buscar'
      Enabled = False
      ImageIndex = 11
    end
    object mniN1: TMenuItem
      Caption = '-'
    end
    object mniAgregar1: TMenuItem
      Caption = '&Agregar'
      ImageIndex = 10
      OnClick = mniAgregar1Click
    end
    object mniN2: TMenuItem
      Caption = '-'
    end
    object mniModificar1: TMenuItem
      Caption = '&Modificar'
      ImageIndex = 9
      OnClick = mniModificar1Click
    end
    object mniN3: TMenuItem
      Caption = '-'
    end
    object mniEliminar1: TMenuItem
      Caption = '&Eliminar'
      ImageIndex = 8
      OnClick = mniEliminar1Click
    end
  end
end
