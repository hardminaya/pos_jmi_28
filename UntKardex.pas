unit UntKardex;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Vcl.Menus, dxSkinsCore, dxSkinsDefaultPainters,
  Vcl.StdCtrls, cxButtons, cxControls, cxStyles, dxSkinscxPCPainter,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, Data.DB, cxDBData,
  cxMemo, cxCheckBox, Data.Win.ADODB, cxClasses, Datasnap.DBClient,
  JvDataSource, Datasnap.Provider, cxGridLevel, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGridCustomView, cxGrid;

type
  TfrmKardex = class(TForm)
    btnSalir: TcxButton;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnSalirClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmKardex: TfrmKardex;

implementation

{$R *.dfm}

procedure TfrmKardex.btnSalirClick(Sender: TObject);
begin
  self.Close;
end;

procedure TfrmKardex.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action:=caFree;
  frmKardex:=nil;
end;

end.
