unit UntAbrirCaja;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, dxSkinsCore,
  dxSkinsDefaultPainters, Vcl.StdCtrls, cxMaskEdit, cxDropDownEdit, cxCalendar,
  cxTextEdit, cxCurrencyEdit, Vcl.ImgList, Vcl.ExtCtrls, Vcl.ComCtrls,
  dxGDIPlusClasses, JvExControls, JvXPCore, JvXPButtons, JvComponentBase,
  JvCaptionButton;

type
  TfrmAbrirCaja = class(TForm)
    lbl1: TLabel;
    lbl2: TLabel;
    lbl3: TLabel;
    lbl4: TLabel;
    edtMonto: TcxCurrencyEdit;
    edtDescripcion: TcxTextEdit;
    edtResponsable: TcxTextEdit;
    dtpFecha: TDateTimePicker;
    btnSalir: TJvXPButton;
    btnAbrirCaja: TJvXPButton;
    procedure FormShow(Sender: TObject);
    procedure btnAbrirCajaClick(Sender: TObject);
    procedure btnSalirClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmAbrirCaja: TfrmAbrirCaja;

implementation

{$R *.dfm}

uses UntMain, UntData;

procedure TfrmAbrirCaja.btnAbrirCajaClick(Sender: TObject);
begin
  cCaja.MontoApertura:=edtMonto.Value;
  frmMain.actAbrirCaja.Execute;
//  fn_RegistrodeCambio(CUsuario.Usuario,'Abrir Caja','Caja Abierta: '+ccaja.Nombre+', Monto: '+CurrToStr(ccaja.MontoApertura));
  fn_RegistrodeCambio(CUsuario.Usuario,'Abrir Caja','Caja Abierta: '+ccaja.Nombre+', Monto RD$: '+ FormatFloat ('###,###,##0.00',ccaja.MontoApertura));
end;


procedure TfrmAbrirCaja.btnSalirClick(Sender: TObject);
begin
  self.Close;
end;

procedure TfrmAbrirCaja.FormShow(Sender: TObject);
begin
   dtpFecha.DateTime:=Now;
  edtDescripcion.Text:=cCaja.Nombre;
  edtResponsable.Text:=cCaja.Responsable;
  edtMonto.SetFocus;

end;

end.
