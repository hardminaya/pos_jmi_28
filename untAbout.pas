unit untAbout;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, jpeg, dxGDIPlusClasses, Vcl.Imaging.GIFImg,
  Vcl.ComCtrls, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxStyles, dxSkinsCore, dxSkinsDefaultPainters, dxSkinscxPCPainter,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, Data.DB, cxDBData,
  cxGridLevel, cxClasses, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, JvExStdCtrls, JvListBox;

type
  TfrmAbout = class(TForm)
    pgc1: TPageControl;
    ts1: TTabSheet;
    ts2: TTabSheet;
    img3: TImage;
    lbl2: TLabel;
    lblversion: TLabel;
    lbl3: TLabel;
    lbl4: TLabel;
    bvl1: TBevel;
    lbl5: TLabel;
    lbl6: TLabel;
    img1: TImage;
    lbl1: TLabel;
    lst1: TJvListBox;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);

  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmAbout: TfrmAbout;

implementation

uses UntMain, UntData;

{$R *.dfm}

procedure TfrmAbout.FormCreate(Sender: TObject);
begin
  lblversion.Caption := 'Version: '+ GetFileVersion(ParamStr(0));
end;



procedure TfrmAbout.FormShow(Sender: TObject);
begin
  with lst1.Items do
  begin
    Clear;
    Add('Directory WorkSpsace= '+cVariables.DirectoryWorkspace);
    Add('Caja.Imprime Copia='+IntToStr(cCaja.ImprimeCopia));
    Add('Caja.SeleccionaSeriales= '+BoolToStr(cCaja.SeleccionaSeriales));
    Add('Caja.EmiteComprobantes= '+BoolToStr(cCaja.EmiteCoprobantes));
    Add('Impresion.TicketAuto= '+BoolToStr(cValoresIni.TicketAuto));
    Add('Impresion.PapelTamano= '+floatToStr(cValoresIni.PapelTamano));
    Add('Impresion.RegistroTamano= '+floatToStr(cValoresIni.RegistroTamano));
    Add('---- end ---');
  end;
end;

end.
