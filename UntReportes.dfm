object frmReportes: TfrmReportes
  Left = 0
  Top = 0
  Caption = 'frmReportes'
  ClientHeight = 528
  ClientWidth = 948
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object pgscrlr1: TPageScroller
    Left = 384
    Top = 440
    Width = 150
    Height = 45
    TabOrder = 1
  end
  object cxpgcntrl1: TcxPageControl
    Left = 8
    Top = 8
    Width = 932
    Height = 512
    TabOrder = 0
    Properties.ActivePage = cxtbsht1
    ClientRectBottom = 506
    ClientRectLeft = 3
    ClientRectRight = 926
    ClientRectTop = 26
    object cxtbsht1: TcxTabSheet
      Caption = 'Ventas'
      ImageIndex = 0
      ExplicitLeft = 43
      ExplicitTop = 0
      ExplicitWidth = 280
      ExplicitHeight = 161
      object cxdbpvtgrd1: TcxDBPivotGrid
        Left = 0
        Top = 0
        Width = 923
        Height = 480
        Align = alClient
        Groups = <>
        TabOrder = 0
        ExplicitLeft = -262
        ExplicitTop = 24
        ExplicitWidth = 889
        ExplicitHeight = 257
        object cxdbpvtgrdfldcxdbpvtgrd1Field1: TcxDBPivotGridField
          AreaIndex = 0
          Visible = True
          UniqueName = 'cxdbpvtgrdfldcxdbpvtgrd1Field1'
        end
      end
    end
    object cxtbsht2: TcxTabSheet
      Caption = 'Inventario'
      ImageIndex = 1
      ExplicitWidth = 280
      ExplicitHeight = 161
    end
  end
  object qryWork1: TADOQuery
    Connection = datmain.con1
    Parameters = <>
    SQL.Strings = (
      'use db_punto_venta'
      'select top 1000  * from [vw_rpt_ventas]')
    Left = 56
    Top = 352
  end
  object ds1: TDataSource
    DataSet = datmain.ADOQuery1
    Left = 104
    Top = 352
  end
end
