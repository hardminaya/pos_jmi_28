unit UntRNC;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB, Data.Win.ADODB, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxStyles, dxSkinsCore,
  dxSkinsDefaultPainters, dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxDBData, Vcl.ComCtrls, Vcl.ToolWin, cxGridLevel,
  cxClasses, cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, cxContainer, Vcl.StdCtrls, cxMaskEdit, cxTextEdit,
  cxDBEdit, cxLabel, Vcl.Menus, JvMenus, Datasnap.Provider, Datasnap.DBClient,
  cxButtonEdit, cxNavigator, cxDBNavigator, Vcl.DBActns, Vcl.StdActns,
  Vcl.ActnList, Vcl.PlatformDefaultStyleActnCtrls, Vcl.ActnMan, Vcl.ExtCtrls,
  Vcl.DBCtrls, Vcl.Grids, Vcl.DBGrids, cxPCdxBarPopupMenu, cxPC;

type
  TfrmRNC = class(TForm)
    qryRNC: TADOQuery;
    dsRNC: TDataSource;
    atncfldRNCpidregistro: TAutoIncField;
    wdstrngfldRNCRNC: TWideStringField;
    wdstrngfldRNCNombre: TWideStringField;
    wdstrngfldRNCNombreComercial: TWideStringField;
    wdstrngfldRNCOcupacion: TWideStringField;
    wdstrngfldRNCDireccionCalle: TWideStringField;
    wdstrngfldRNCDireccionNumero: TWideStringField;
    wdstrngfldRNCDireccionSector: TWideStringField;
    wdstrngfldRNCTelefono: TWideStringField;
    wdstrngfldRNCFecha: TWideStringField;
    wdstrngfldRNCEstatus: TWideStringField;
    wdstrngfldRNCRegimenPago: TWideStringField;
    jvpmn1: TJvPopupMenu;
    mniAgregar1: TMenuItem;
    mniModificar1: TMenuItem;
    mniEliminar1: TMenuItem;
    cxdbnvgtr1: TcxDBNavigator;
    mniN1: TMenuItem;
    mniN2: TMenuItem;
    actmgr1: TActionManager;
    dtstprPrior: TDataSetPrior;
    dtstnxtNext: TDataSetNext;
    dtstdtedit: TDataSetEdit;
    dtstnsrtadd: TDataSetInsert;
    wndwclsclose: TWindowClose;
    dtstcnclcancel: TDataSetCancel;
    dtstpstAccept: TDataSetPost;
    dtstdltDelete: TDataSetDelete;
    tlb3: TToolBar;
    btnclose: TToolButton;
    cxpgcntrlListado: TcxPageControl;
    cxtbshtListado: TcxTabSheet;
    cxgrd1: TcxGrid;
    cxgrdbtblvwGrid1DBTableView1: TcxGridDBTableView;
    cxgrdlvlGrid1Level1: TcxGridLevel;
    cxtbshtDetalle: TcxTabSheet;
    grp1: TGroupBox;
    cxlbllbl2: TcxLabel;
    edtcxdbtxtdt1: TcxDBTextEdit;
    edtcxdbtxtdt4: TcxDBTextEdit;
    edtcxdbtxtdt5: TcxDBTextEdit;
    edtcxdbtxtdt7: TcxDBTextEdit;
    edtcxdbtxtdt8: TcxDBTextEdit;
    edtcxdbtxtdt9: TcxDBTextEdit;
    cxlbllblcxlbl1: TcxLabel;
    edtcxdbtxtdt2: TcxDBTextEdit;
    cxlbllbl1: TcxLabel;
    cxlbllbl3: TcxLabel;
    cxlbllbl4: TcxLabel;
    cxlbllbl5: TcxLabel;
    cxlbllbl6: TcxLabel;
    cxlbllbl7: TcxLabel;
    cxlbllbl8: TcxLabel;
    edtcxdbtxtdt10: TcxDBTextEdit;
    cxlbllbl10: TcxLabel;
    cxlbllbl11: TcxLabel;
    edtcxdbtxtdt11: TcxDBTextEdit;
    btnSupervisorID1: TcxDBButtonEdit;
    cxgrdbclmnGrid1DBTableView1RNC: TcxGridDBColumn;
    cxgrdbclmnGrid1DBTableView1Nombre: TcxGridDBColumn;
    cxgrdbclmnGrid1DBTableView1NombreComercial: TcxGridDBColumn;
    cxgrdbclmnGrid1DBTableView1Estatus: TcxGridDBColumn;
    cxtbshtAgregar: TcxTabSheet;
    grp2: TGroupBox;
    cxlbl1: TcxLabel;
    edt1: TcxDBTextEdit;
    edt2: TcxDBTextEdit;
    edt3: TcxDBTextEdit;
    edt4: TcxDBTextEdit;
    edt5: TcxDBTextEdit;
    edt6: TcxDBTextEdit;
    edt7: TcxDBTextEdit;
    cxlbl2: TcxLabel;
    edt8: TcxDBTextEdit;
    cxlbl3: TcxLabel;
    cxlbl4: TcxLabel;
    cxlbl5: TcxLabel;
    cxlbl6: TcxLabel;
    cxlbl7: TcxLabel;
    cxlbl8: TcxLabel;
    cxlbl9: TcxLabel;
    edt9: TcxDBTextEdit;
    cxlbl10: TcxLabel;
    cxlbl11: TcxLabel;
    edt10: TcxDBTextEdit;
    edt11: TcxDBTextEdit;
    qryRNC2: TADOQuery;
    atncfld1: TAutoIncField;
    wdstrngfld1: TWideStringField;
    wdstrngfld2: TWideStringField;
    wdstrngfld3: TWideStringField;
    wdstrngfld4: TWideStringField;
    wdstrngfld5: TWideStringField;
    wdstrngfld6: TWideStringField;
    wdstrngfld7: TWideStringField;
    wdstrngfld8: TWideStringField;
    wdstrngfld9: TWideStringField;
    wdstrngfld10: TWideStringField;
    wdstrngfld11: TWideStringField;
    dsRNC2: TDataSource;
    tlb1: TToolBar;
    btnedit1: TToolButton;
    btnadd1: TToolButton;
    btnDelete: TToolButton;
    btnAccept1: TToolButton;
    btncancel1: TToolButton;
    btn1: TToolButton;
    btnNombre: TcxDBButtonEdit;
    procedure btnSalirClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnSupervisorID1PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure mniEliminar1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure dtstdltDeleteExecute(Sender: TObject);
    procedure dtstprPriorExecute(Sender: TObject);
    procedure dtstnxtNextExecute(Sender: TObject);
    procedure wndwclscloseExecute(Sender: TObject);
    procedure qryRNC2AfterPost(DataSet: TDataSet);
    procedure qryRNC2AfterEdit(DataSet: TDataSet);
    procedure qryRNC2BeforeDelete(DataSet: TDataSet);
    procedure btnNombrePropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmRNC: TfrmRNC;
  var_Posteado : Boolean;

implementation

uses
  UntData, UntMain;

{$R *.dfm}

procedure TfrmRNC.btnNombrePropertiesButtonClick(Sender: TObject;
  AButtonIndex: Integer);
var
  var_texto:string;
begin
  Application.ProcessMessages;
  Cursor:=crSQLWait;
  with qryRNC do begin
    Close;
    var_texto:=InputBox('Consulta RNC','Indique Nombre','');
    SQL.Text:='SELECT TOP 50 * FROM vw_rnc WHERE nombre ='+#39+var_texto+#39+
    ' OR [Nombre.Comercial] LIKE '+#39+'%'+var_texto+'%'+#39;
    //ShowMessage(sql.Text);
    //Close;
    //dsetRNC.Refresh or requery

    dsRNC.DataSet.Close;;
    dsRNC.DataSet.Open;
    Cursor:=crDefault;
  end;
end;

procedure TfrmRNC.btnSalirClick(Sender: TObject);
begin
  self.Close;
end;

procedure TfrmRNC.btnSupervisorID1PropertiesButtonClick(Sender: TObject;
  AButtonIndex: Integer);
var
  var_texto:string;
begin
  Application.ProcessMessages;
  Cursor:=crSQLWait;
  with qryRNC do begin
    Close;
    var_texto:=InputBox('Consulta RNC','Indique N�mero RNC','');
    SQL.Text:='SELECT TOP 50 * FROM vw_rnc WHERE RNC ='+#39+var_texto+#39;
    //' OR [Nombre.Comercial] LIKE '+#39+'%'+var_texto+'%'+#39;
    //ShowMessage(sql.Text);
    //Close;
    //dsetRNC.Refresh or requery

    dsRNC.DataSet.Close;;
    dsRNC.DataSet.Open;
    Cursor:=crDefault;
  end;
end;

procedure TfrmRNC.dtstdltDeleteExecute(Sender: TObject);
begin
  if MessageBox(Handle,'Seguro desear ELIMINAR el registro?','RNC',MB_ICONQUESTION or MB_YESNO)=ID_YES then
  begin
    dsRNC2.DataSet.Delete;
  end;
end;

procedure TfrmRNC.dtstnxtNextExecute(Sender: TObject);
begin
  qryRNC.Next;
end;

procedure TfrmRNC.dtstprPriorExecute(Sender: TObject);
begin
  qryRNC.Prior;
end;

procedure TfrmRNC.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action:=caFree;
  frmRNC:=nil;
end;

procedure TfrmRNC.FormCreate(Sender: TObject);
begin
  qryRNC.Open;
  qryRNC2.Open
end;

procedure TfrmRNC.mniEliminar1Click(Sender: TObject);
begin
  if MessageBox(Handle,'Seguro desear ELIMINAR el registro?','RNC',MB_ICONQUESTION or MB_YESNO)=ID_YES then
  begin
    dsRNC.DataSet.Delete;
  end;
end;

procedure TfrmRNC.qryRNC2AfterEdit(DataSet: TDataSet);
begin
  cxpgcntrlListado.ActivePageIndex:=2;
end;

procedure TfrmRNC.qryRNC2AfterPost(DataSet: TDataSet);
begin
  qryRNC.Close;
  qryRNC.Open;
  cxpgcntrlListado.ActivePageIndex:=0;
end;

procedure TfrmRNC.qryRNC2BeforeDelete(DataSet: TDataSet);
begin
  cxpgcntrlListado.ActivePageIndex:=0;
end;

procedure TfrmRNC.wndwclscloseExecute(Sender: TObject);
begin
 if (dsRNC2.State=dsInsert) or (dsRNC2.State=dsEdit)  then
  begin
    if MessageBox(Handle,'Los Datos no han sido guardados, se perderan, est� Seguro de Salir?','RNC',MB_ICONQUESTION or MB_YESNO)=ID_YES then
    begin
      dsRNC2.DataSet.Cancel;
    end
    else
    begin
      exit;
    end;
  end;
  self.Close();
end;

end.
