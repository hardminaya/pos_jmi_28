object frmEstadodeCajas: TfrmEstadodeCajas
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu]
  Caption = 'Estado de Cajas'
  ClientHeight = 417
  ClientWidth = 906
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Visible = True
  WindowState = wsMaximized
  OnClose = FormClose
  OnShow = FormShow
  DesignSize = (
    906
    417)
  PixelsPerInch = 96
  TextHeight = 13
  object tlb1: TToolBar
    Left = 0
    Top = 0
    Width = 906
    Height = 57
    ButtonHeight = 52
    ButtonWidth = 54
    Caption = 'tlb1'
    Images = frmMain.ilToolBar32
    ShowCaptions = True
    TabOrder = 0
    object btnSalir: TToolButton
      Left = 0
      Top = 0
      Caption = '&Salir'
      ImageIndex = 1
      OnClick = btnSalirClick
    end
    object btn5: TToolButton
      Left = 54
      Top = 0
      Width = 8
      Caption = 'btn5'
      ImageIndex = 3
      Style = tbsSeparator
    end
    object btnRefrescar: TToolButton
      Left = 62
      Top = 0
      Caption = '&Refrescar'
      ImageIndex = 13
      OnClick = btnRefrescarClick
    end
    object btnModificar: TToolButton
      Left = 116
      Top = 0
      Caption = '&Modificar'
      ImageIndex = 9
      OnClick = btnModificarClick
    end
    object btn2: TToolButton
      Left = 170
      Top = 0
      Width = 8
      Caption = 'btn2'
      ImageIndex = 2
      Style = tbsSeparator
    end
  end
  object cxgrd1: TcxGrid
    Left = 8
    Top = 72
    Width = 890
    Height = 337
    Anchors = [akLeft, akTop, akRight, akBottom]
    TabOrder = 1
    object cxgrdbtblvwGrid1DBTableView1: TcxGridDBTableView
      DataController.DataSource = ds2
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      object GridGrid1DBTableView1fkUsuario: TcxGridDBColumn
        Caption = 'Usuario'
        DataBinding.FieldName = 'fkUsuario'
        HeaderAlignmentHorz = taCenter
        Options.Editing = False
        Styles.Header = cxstyl1
        Width = 61
      end
      object GridGrid1DBTableView1FechaApertura: TcxGridDBColumn
        Caption = 'Apertura'
        DataBinding.FieldName = 'FechaApertura'
        HeaderAlignmentHorz = taCenter
        Options.Editing = False
        Styles.Header = cxstyl1
        Width = 155
      end
      object GridGrid1DBTableView1FechaCierre: TcxGridDBColumn
        Caption = 'Cierre'
        DataBinding.FieldName = 'FechaCierre'
        HeaderAlignmentHorz = taCenter
        Options.Editing = False
        Styles.Header = cxstyl1
        Width = 162
      end
      object GridGrid1DBTableView1MontoApertura: TcxGridDBColumn
        Caption = 'Apertura'
        DataBinding.FieldName = 'MontoApertura'
        HeaderAlignmentHorz = taCenter
        Options.Editing = False
        Styles.Header = cxstyl1
        Width = 105
      end
      object GridGrid1DBTableView1MontoCierre: TcxGridDBColumn
        Caption = 'Cierre'
        DataBinding.FieldName = 'MontoCierre'
        HeaderAlignmentHorz = taCenter
        Options.Editing = False
        Styles.Header = cxstyl1
        Width = 110
      end
      object GridGrid1DBTableView1EstadoCaja: TcxGridDBColumn
        Caption = 'Estado'
        DataBinding.FieldName = 'EstadoCaja'
        HeaderAlignmentHorz = taCenter
        Options.Editing = False
        Styles.Header = cxstyl1
        Width = 90
      end
      object GridGrid1DBTableView1NombrePC: TcxGridDBColumn
        Caption = 'Caja'
        DataBinding.FieldName = 'NombrePC'
        HeaderAlignmentHorz = taCenter
        Options.Editing = False
        Styles.Header = cxstyl1
        Width = 312
      end
    end
    object cxgrdlvlGrid1Level1: TcxGridLevel
      GridView = cxgrdbtblvwGrid1DBTableView1
    end
  end
  object ds2: TDataSource
    DataSet = dset1
    Left = 392
    Top = 22
  end
  object dtstprvdr1: TDataSetProvider
    DataSet = qry1
    Left = 296
    Top = 16
  end
  object dset1: TClientDataSet
    Active = True
    Aggregates = <>
    Params = <>
    ProviderName = 'dtstprvdr1'
    Left = 352
    Top = 16
    object atncflddset1pIdRegistro: TAutoIncField
      FieldName = 'pIdRegistro'
      ReadOnly = True
    end
    object intgrflddset1fkUsuario: TIntegerField
      FieldName = 'fkUsuario'
    end
    object dtmflddset1FechaApertura: TDateTimeField
      FieldName = 'FechaApertura'
    end
    object dtmflddset1FechaCierre: TDateTimeField
      FieldName = 'FechaCierre'
    end
    object bcdflddset1MontoApertura: TBCDField
      FieldName = 'MontoApertura'
      DisplayFormat = '#,###,###.00'
      Precision = 18
      Size = 2
    end
    object bcdflddset1MontoCierre: TBCDField
      FieldName = 'MontoCierre'
      DisplayFormat = '#,###,###.00'
      Precision = 18
      Size = 2
    end
    object strngflddset1EstadoCaja: TStringField
      FieldName = 'EstadoCaja'
      FixedChar = True
      Size = 1
    end
    object wdstrngflddset1NombrePC: TWideStringField
      FieldName = 'NombrePC'
      Size = 90
    end
  end
  object qry1: TADOQuery
    Connection = DatMain.conComun
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      
        'select top 100 *  from [cajas.estado] order by fechaapertura des' +
        'c')
    Left = 248
    Top = 16
  end
  object cxstylrpstry1: TcxStyleRepository
    Left = 472
    Top = 16
    PixelsPerInch = 96
    object cxstyl1: TcxStyle
      AssignedValues = [svFont]
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
    end
  end
  object usrsrg1: TUsersReg
    FormName = 'frmEstadodeCajas'
    FormCaption = 'Estado de Cajas'
    ComponentList.Strings = (
      'tlb1=tlb1=None'
      'tlb1Parent=frmEstadodeCajas'
      'btnModificar=Modificar=None'
      'btnModificarParent=tlb1')
    SecurityComponent = frmMain.usrs1
    IsRepositoryForm = False
    AutoApplySecurity = True
    Left = 528
    Top = 56
  end
end
