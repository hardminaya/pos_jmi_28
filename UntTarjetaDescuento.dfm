object frmTarjetaDescuento: TfrmTarjetaDescuento
  Left = 0
  Top = 0
  Caption = 'Tarjeta de Descuento'
  ClientHeight = 430
  ClientWidth = 819
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object cxgrd1: TcxGrid
    Left = 0
    Top = 65
    Width = 819
    Height = 365
    Align = alClient
    TabOrder = 1
    object cxgrdbtblvwGrid1DBTableView1: TcxGridDBTableView
      DataController.DataSource = dsTarjetas
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      object GridGrid1DBTableView1ID: TcxGridDBColumn
        Caption = 'Tarjeta N'#250'mero'
        DataBinding.FieldName = 'ID'
        HeaderAlignmentHorz = taCenter
        Styles.Header = cxstyl1
        Width = 233
      end
      object GridGrid1DBTableView1ClienteID: TcxGridDBColumn
        Caption = 'Cliente C'#243'digo'
        DataBinding.FieldName = 'ClienteID'
        HeaderAlignmentHorz = taCenter
        Styles.Header = cxstyl1
        Width = 148
      end
      object GridGrid1DBTableView1Descuento: TcxGridDBColumn
        DataBinding.FieldName = 'Descuento'
        HeaderAlignmentHorz = taCenter
        Styles.Header = cxstyl1
        Width = 77
      end
      object GridGrid1DBTableView1Fecha_Expiracion: TcxGridDBColumn
        Caption = 'Fecha de Expiracion'
        DataBinding.FieldName = 'Fecha_Expiracion'
        PropertiesClassName = 'TcxDateEditProperties'
        Properties.DisplayFormat = 'yyyy/mm/dd'
        Properties.EditFormat = 'yyyy/mm/dd'
        HeaderAlignmentHorz = taCenter
        Styles.Header = cxstyl1
        Width = 142
      end
      object GridGrid1DBTableView1Fecha_Utilizada: TcxGridDBColumn
        Caption = 'Fecha Uso Reciente'
        DataBinding.FieldName = 'Fecha_Utilizada'
        PropertiesClassName = 'TcxDateEditProperties'
        Properties.DisplayFormat = 'yyyy/mm/dd'
        Properties.EditFormat = 'yyyy/mm/dd'
        HeaderAlignmentHorz = taCenter
        Options.Editing = False
        Styles.Header = cxstyl1
        Width = 128
      end
    end
    object cxgrdlvlGrid1Level1: TcxGridLevel
      GridView = cxgrdbtblvwGrid1DBTableView1
    end
  end
  object tlb1: TToolBar
    Left = 0
    Top = 0
    Width = 819
    Height = 65
    ButtonHeight = 60
    ButtonWidth = 57
    Caption = 'tlb1'
    Images = frmMain.ilToolBar32
    ShowCaptions = True
    TabOrder = 0
    object btnRegistro: TToolButton
      Left = 0
      Top = 0
      Caption = 'Registro'
      DropdownMenu = jvpmn1
      ImageIndex = 9
      Style = tbsDropDown
    end
    object btnAnterior: TToolButton
      Left = 72
      Top = 0
      Caption = 'Anterior'
      ImageIndex = 3
      OnClick = btnAnteriorClick
    end
    object btnSiguiente: TToolButton
      Left = 129
      Top = 0
      Caption = 'Siguiente'
      ImageIndex = 0
      OnClick = btnSiguienteClick
    end
    object btnGuardar: TToolButton
      Left = 186
      Top = 0
      Caption = 'Guardar'
      ImageIndex = 2
      Visible = False
      OnClick = btnGuardarClick
    end
    object btnCancelar: TToolButton
      Left = 243
      Top = 0
      Caption = 'Cancelar'
      ImageIndex = 5
      Visible = False
      OnClick = btnCancelarClick
    end
    object btn2: TToolButton
      Left = 300
      Top = 0
      Width = 55
      Caption = 'btn5'
      ImageIndex = 5
      Style = tbsSeparator
    end
    object btnSalir: TToolButton
      Left = 355
      Top = 0
      Caption = 'Salir'
      ImageIndex = 1
      OnClick = btnSalirClick
    end
  end
  object qryTarjetas: TADOQuery
    Connection = DatMain.con1
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'SELECT * FROM TarjetaDescuento')
    Left = 440
    Top = 8
    object atncfldTarjetaspIDRegistro: TAutoIncField
      FieldName = 'pIDRegistro'
      ReadOnly = True
    end
    object wdstrngfldTarjetasID: TWideStringField
      FieldName = 'ID'
      Size = 50
    end
    object wdstrngfldTarjetasClienteID: TWideStringField
      FieldName = 'ClienteID'
      Size = 50
    end
    object intgrfldTarjetasDescuento: TIntegerField
      FieldName = 'Descuento'
    end
    object wdstrngfldTarjetasFecha_Expiracion: TWideStringField
      FieldName = 'Fecha_Expiracion'
      EditMask = '!99/99/00;1;_'
      Size = 10
    end
    object wdstrngfldTarjetasFecha_Utilizada: TWideStringField
      FieldName = 'Fecha_Utilizada'
      Size = 10
    end
  end
  object dsetTarjetas: TClientDataSet
    Aggregates = <>
    CommandText = 'SELECT * FROM TarjetaDescuento'
    Params = <>
    ProviderName = 'dtstprvdrTarjetas'
    OnPostError = dsetTarjetasPostError
    Left = 592
    Top = 8
    object atncfldTarjetaspIDRegistro1: TAutoIncField
      FieldName = 'pIDRegistro'
      ReadOnly = True
    end
    object wdstrngfldTarjetasID1: TWideStringField
      FieldName = 'ID'
      Size = 50
    end
    object wdstrngfldTarjetasClienteID1: TWideStringField
      FieldName = 'ClienteID'
      Size = 50
    end
    object intgrfldTarjetasDescuento1: TIntegerField
      FieldName = 'Descuento'
    end
    object wdstrngfldTarjetasFecha_Expiracion1: TWideStringField
      FieldName = 'Fecha_Expiracion'
      Size = 10
    end
    object wdstrngfldTarjetasFecha_Utilizada1: TWideStringField
      FieldName = 'Fecha_Utilizada'
      Size = 10
    end
  end
  object dtstprvdrTarjetas: TDataSetProvider
    DataSet = qryTarjetas
    Options = [poAllowCommandText, poUseQuoteChar]
    OnUpdateError = dtstprvdrTarjetasUpdateError
    Left = 528
    Top = 8
  end
  object dsTarjetas: TDataSource
    AutoEdit = False
    DataSet = dsetTarjetas
    Left = 640
    Top = 8
  end
  object jvpmn1: TJvPopupMenu
    Images = frmMain.ilToolBar32
    ImageMargin.Left = 0
    ImageMargin.Top = 0
    ImageMargin.Right = 0
    ImageMargin.Bottom = 0
    ImageSize.Height = 0
    ImageSize.Width = 0
    Left = 696
    Top = 8
    object mniBuscar1: TMenuItem
      Caption = '&Buscar'
      Enabled = False
    end
    object mniAgregar1: TMenuItem
      Caption = 'Agregar'
      ImageIndex = 10
      OnClick = mniAgregar1Click
    end
    object mniModificar1: TMenuItem
      Caption = 'Modificar'
      ImageIndex = 9
      OnClick = mniModificar1Click
    end
    object mniEliminar1: TMenuItem
      Caption = 'Eliminar'
      ImageIndex = 8
      OnClick = mniEliminar1Click
    end
  end
  object cxstylrpstry1: TcxStyleRepository
    Left = 752
    Top = 8
    PixelsPerInch = 96
    object cxstyl1: TcxStyle
      AssignedValues = [svFont]
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
    end
  end
end
