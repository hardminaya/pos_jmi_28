unit UntPreview;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, ppComm, ppRelatv, ppProd, ppClass,
  ppReport, Vcl.ExtCtrls, ppViewr, ppDesignLayer, ppCtrls, ppBands, ppStrtch,
  ppMemo, ppVar, ppPrnabl, ppCache, ppParameter, Vcl.StdCtrls, Vcl.ComCtrls,
  JvExComCtrls, JvStatusBar, JvToolBar, Vcl.ActnMan,
  Vcl.Ribbon, Vcl.RibbonLunaStyleActnCtrls, cxGraphics,
  cxLookAndFeels, cxLookAndFeelPainters, dxSkinsCore, dxSkinsDefaultPainters,
  cxButtons, JvExControls, JvxSlider, dxGDIPlusClasses,ppTypes, Vcl.Menus,
  dxSkinBlack, dxSkinBlue, dxSkinBlueprint, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkRoom, dxSkinDarkSide, dxSkinDevExpressDarkStyle,
  dxSkinDevExpressStyle, dxSkinFoggy, dxSkinGlassOceans, dxSkinHighContrast,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black, dxSkinOffice2007Blue,
  dxSkinOffice2007Green, dxSkinOffice2007Pink, dxSkinOffice2007Silver,
  dxSkinOffice2010Black, dxSkinOffice2010Blue, dxSkinOffice2010Silver,
  dxSkinPumpkin, dxSkinSeven, dxSkinSevenClassic, dxSkinSharp, dxSkinSharpPlus,
  dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinTheAsphaltWorld, dxSkinValentine, dxSkinVS2010, dxSkinWhiteprint,
  dxSkinXmas2008Blue;

type
  TfrmPreview1 = class(TForm)
    pvwr1: TppViewer;
    prprtTicket2: TppReport;
    phdrbnd1: TppHeaderBand;
    pdbtxtDireccion: TppDBText;
    pdbtxtRNC: TppDBText;
    pdbtxtTelefono: TppDBText;
    plblTitulo: TppLabel;
    pdbtxtDescripcion: TppDBText;
    pdbtxtpIdRegistro: TppDBText;
    psystmvrbl1: TppSystemVariable;
    plbl7: TppLabel;
    plbl8: TppLabel;
    plbl9: TppLabel;
    pdbtxtClienteNombre: TppDBText;
    plbl13: TppLabel;
    plbl14: TppLabel;
    plbl15: TppLabel;
    plbl2: TppLabel;
    plbl3: TppLabel;
    psystmvrbl2: TppSystemVariable;
    pdtlbnd1: TppDetailBand;
    pdbtxtSerie: TppDBText;
    pftrbnd1: TppFooterBand;
    psmrybnd1: TppSummaryBand;
    plbl29: TppLabel;
    plbl30: TppLabel;
    plbl31: TppLabel;
    pdbtxtMontoGeneral2: TppDBText;
    plbl32: TppLabel;
    pdbtxtSubTotal2: TppDBText;
    pdbtxtTotalItbis2: TppDBText;
    pdbtxtTotalDesc2: TppDBText;
    plbl33: TppLabel;
    plbl34: TppLabel;
    plbl35: TppLabel;
    plbl36: TppLabel;
    plbl37: TppLabel;
    pdbtxtEfectivo: TppDBText;
    pdbtxtCheque: TppDBText;
    pdbtxtTarjeta: TppDBText;
    pdbtxtNcredito: TppDBText;
    plbl4: TppLabel;
    pdbtxtMontoDevuelta: TppDBText;
    pdbtxtVendedor: TppDBText;
    plbl6: TppLabel;
    plbl10: TppLabel;
    pm1: TppMemo;
    pgrp1: TppGroup;
    pgrphdrbnd1: TppGroupHeaderBand;
    pdbtxtArticuloDescripcion: TppDBText;
    pdbtxtArticuloCantidad: TppDBText;
    plbl5: TppLabel;
    pdbtxtArticuloImporte: TppDBText;
    pdbtxtArticuloPrecio: TppDBText;
    pdbtxtArticuloPrecioImpuesto: TppDBText;
    pgrpftrbnd1: TppGroupFooterBand;
    pln4: TppLine;
    pdsgnlyrs1: TppDesignLayers;
    pdsgnlyr1: TppDesignLayer;
    prmtrlst1: TppParameterList;
    btnAnterior: TcxButton;
    btnProximo: TcxButton;
    btnSalir: TcxButton;
    jvxsldr1: TJvxSlider;
    btnImprimir: TcxButton;
    prprtTicket: TppReport;
    procedure FormShow(Sender: TObject);
    procedure btnAnteriorClick(Sender: TObject);
    procedure btnProximoClick(Sender: TObject);
    procedure btnSalirClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnAcercarClick(Sender: TObject);
    procedure jvxsldr1Changed(Sender: TObject);
    procedure btnImprimirClick(Sender: TObject);
    procedure prprtTicketBeforePrint(Sender: TObject);
  private
    procedure ToggleButtons;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmPreview1: TfrmPreview1;

implementation

uses
  UntData, UntMain;

{$R *.dfm}

procedure TfrmPreview1.btnProximoClick(Sender: TObject);
begin
  pvwr1.NextPage;
  ToggleButtons;
end;

procedure TfrmPreview1.btnSalirClick(Sender: TObject);
begin
  ModalResult:=mrClose;
end;

procedure TfrmPreview1.btnAcercarClick(Sender: TObject);
begin
  pvwr1.ZoomPercentage:=200;
end;

procedure TfrmPreview1.btnAnteriorClick(Sender: TObject);
begin
  pvwr1.PriorPage;
  ToggleButtons;
end;

procedure TfrmPreview1.btnImprimirClick(Sender: TObject);
var
  FacturaNo : String;
//  Paginas : TStrings;
begin
  with prprtTicket do begin
    FacturaNo:=datmain.qryTicket.Parameters.ParamByName('@PIDRegistro').Value;
    DeviceType:='Printer';
    PrinterSetup.Copies:=1;
    frmPreview1.plblTitulo.Text:='FACTURA(REIMPRESA)';

    fn_RegistrodeCambio(cVariables.PrivilegioElevadoUsuario,'Facturación','Factura No.: '+FacturaNo+' Re-Impresa');
    PrinterSetup.PrinterName:='Default';
    ShowPrintDialog:=False;
    ShowCancelDialog:=False;
    //Print;
    PrintReport;
  end;
  ModalResult:=mrOk;
  Exit;
end;

procedure TfrmPreview1.FormClose(Sender: TObject; var Action: TCloseAction);
begin
//  Action:=caFree;
//  frmMain:=nil;
end;

procedure TfrmPreview1.FormShow(Sender: TObject);
begin
  prprtTicket.PrintToDevices;
  pvwr1.Show;
end;

procedure TfrmPreview1.jvxsldr1Changed(Sender: TObject);
begin
  pvwr1.ZoomPercentage:=jvxsldr1.Value;
end;

procedure TfrmPreview1.prprtTicketBeforePrint(Sender: TObject);
begin
  prprtTicket.PrinterDevice.PageRequest.PageSetting:=psPageList;
  prprtTicket.PrinterDevice.PageRequest.PageList.Add('0');
end;

procedure TfrmPreview1.ToggleButtons;
begin
  if (TppReport(pvwr1.Report).PassSetting=psTwoPass) and
     (pvwr1.AbsolutePageNo=TppReport(pvwr1.Report).AbsolutePageCount) then
      btnProximo.Enabled:=False
  else
    btnProximo.Enabled:=True;

  if (pvwr1.AbsolutePageNo =1) then
    btnAnterior.Enabled:=False
  else
    btnAnterior.Enabled:=True;

  end;
end.
