unit UntCantidad;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Vcl.Menus, dxSkinsCore, dxSkinsDefaultPainters,
  cxControls, cxContainer, cxEdit, cxTextEdit, cxMaskEdit, cxDropDownEdit,
  cxCalc, Vcl.StdCtrls, Vcl.Mask, cxButtons, Vcl.Touch.Keyboard, cxPC,
  dxDockControl, dxDockPanel, JvExMask, JvSpin, Vcl.Samples.Spin, dxSkinBlack,
  dxSkinBlue, dxSkinBlueprint, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom,
  dxSkinDarkSide, dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinFoggy,
  dxSkinGlassOceans, dxSkinHighContrast, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSevenClassic, dxSkinSharp, dxSkinSharpPlus, dxSkinSilver,
  dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008, dxSkinTheAsphaltWorld,
  dxSkinValentine, dxSkinVS2010, dxSkinWhiteprint, dxSkinXmas2008Blue;

type
  TfrmCantidad = class(TForm)
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    cxButton3: TcxButton;
    cxButton4: TcxButton;
    cxButton5: TcxButton;
    cxButton6: TcxButton;
    cxButton7: TcxButton;
    cxButton8: TcxButton;
    cxButton9: TcxButton;
    cxButton10: TcxButton;
    cxButton11: TcxButton;
    cxButton12: TcxButton;
    cxButton14: TcxButton;
    txtArticuloDescripcion: TLabel;
    cxButton13: TcxButton;
    btnAceptar: TcxButton;
    txtCantidad: TJvSpinEdit;
    procedure cxButton7Click(Sender: TObject);
    procedure cxButton8Click(Sender: TObject);
    procedure cxButton9Click(Sender: TObject);
    procedure cxButton4Click(Sender: TObject);
    procedure cxButton5Click(Sender: TObject);
    procedure cxButton6Click(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
    procedure cxButton3Click(Sender: TObject);
    procedure cxButton10Click(Sender: TObject);
    procedure cxButton11Click(Sender: TObject);
    procedure cxButton12Click(Sender: TObject);
    procedure btnAceptarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure txtCantidadKeyPress(Sender: TObject; var Key: Char);
    procedure cxButton13Click(Sender: TObject);
    procedure btn1Click(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);

  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmCantidad: TfrmCantidad;

implementation

{$R *.dfm}

uses UntFacturacion, UntData, UntSeriales, UntConsultaArticulos;

procedure TfrmCantidad.btn1Click(Sender: TObject);
begin
  cArticulo.Cantidad:=StrToInt(txtCantidad.Text);
// Valida que la cantidad sea mayor de 0
  if cArticulo.Cantidad=0 then
  begin
    MessageDlg('Debe especificar Cantidad',mtError,[mbOK],0);
    self.ModalResult:=mrNone;
    Exit;
  end;
end;

procedure TfrmCantidad.btnAceptarClick(Sender: TObject);
begin
  cArticulo.Cantidad:=StrToInt(txtCantidad.Text);
// Valida que la cantidad sea mayor de 0
  if cArticulo.Cantidad=0 then
  begin
    MessageDlg('Debe especificar Cantidad',mtError,[mbOK],0);
    self.ModalResult:=mrNone;
    Exit;
  end;

// Valida Cantidad requerida sea menor que la cantidad EnMano
if  cValoresIni.PermiteVentaArticuloSinInventario=false then begin
  if (cArticulo.Cantidad > cArticulo.EnMano) or (cValoresIni.PermiteVentaArticuloSinInventario=true) then begin
    MessageDlg('Cantidad Mayor al existente en Almacen. Existencia= '+IntToStr(cArticulo.EnMano),mtError,[mbOK],0);
    self.ModalResult:=mrNone;
  Exit;
  end;
end;

//Valida el Manejo de Seriales
  if cArticulo.ManejaSeriales='Y' then
  begin
    if not assigned(frmSeriales) then frmSeriales:=TfrmSeriales.Create(frmCantidad);
    frmSeriales.ShowModal;
  end
  else
  begin
    frmFacturacion.acInsertaLinea.Execute;
//    frmFacturacion.edtArticulo.Clear;
  end;
    frmFacturacion.edtArticulo.SetFocus;
    self.Close;

end;

procedure TfrmCantidad.cxButton10Click(Sender: TObject);
begin
  txtCantidad.Text:=txtCantidad.Text+'0'
end;

procedure TfrmCantidad.cxButton11Click(Sender: TObject);
begin
   txtCantidad.Text:=txtCantidad.Text+'.'
end;

procedure TfrmCantidad.cxButton12Click(Sender: TObject);
begin
  txtCantidad.Text:='';
end;

procedure TfrmCantidad.cxButton13Click(Sender: TObject);
begin
  Self.Close;
end;

procedure TfrmCantidad.cxButton1Click(Sender: TObject);
begin
  txtCantidad.Text:=txtCantidad.Text+'7'
end;

procedure TfrmCantidad.cxButton2Click(Sender: TObject);
begin
  txtCantidad.Text:=txtCantidad.Text+'8';
end;

procedure TfrmCantidad.cxButton3Click(Sender: TObject);
begin
  txtCantidad.Text:=txtCantidad.Text+'9';
end;

procedure TfrmCantidad.cxButton4Click(Sender: TObject);
begin
  txtCantidad.Text:=txtCantidad.Text+'4';
end;

procedure TfrmCantidad.cxButton5Click(Sender: TObject);
begin
  txtCantidad.Text:=txtCantidad.Text+'5';
end;

procedure TfrmCantidad.cxButton6Click(Sender: TObject);
begin
  txtCantidad.Text:=txtCantidad.Text+'6';
end;

procedure TfrmCantidad.cxButton7Click(Sender: TObject);
begin
  txtCantidad.Text:=txtCantidad.Text+'1';
end;

procedure TfrmCantidad.cxButton8Click(Sender: TObject);
begin
  txtCantidad.Text:=txtCantidad.Text+'2';
end;

procedure TfrmCantidad.cxButton9Click(Sender: TObject);
begin
   txtCantidad.Text:=txtCantidad.Text+'3';
end;

procedure TfrmCantidad.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action:=caFree;
  frmCantidad:=nil;
end;

procedure TfrmCantidad.FormKeyPress(Sender: TObject; var Key: Char);
begin
  ShowMessage('presionado');
end;

procedure TfrmCantidad.FormShow(Sender: TObject);
begin
//  frmConsultaArticulo.Close;
  txtArticuloDescripcion.Caption:= cArticulo.DescripcionArt;
  txtCantidad.Text:='';
  txtCantidad.SetFocus;
  cArticulo.Cantidad:=0;
//  txtCantidad.Text:=inttostr(cArticulo.Cantidad);
end;

procedure TfrmCantidad.txtCantidadKeyPress(Sender: TObject; var Key: Char);
begin
  if Key=#13 then
  begin
    btnAceptar.SetFocus;
  end;
end;

end.
