object frmRNC: TfrmRNC
  Left = 0
  Top = 0
  Caption = 'RNC'
  ClientHeight = 501
  ClientWidth = 825
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poMainFormCenter
  Visible = True
  WindowState = wsMaximized
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object cxdbnvgtr1: TcxDBNavigator
    Left = 8
    Top = 70
    Width = 178
    Height = 19
    Buttons.First.Visible = False
    Buttons.PriorPage.Visible = False
    Buttons.Prior.Visible = False
    Buttons.Next.Visible = False
    Buttons.NextPage.Visible = False
    Buttons.Last.Visible = False
    Buttons.Insert.Visible = False
    Buttons.Delete.Visible = False
    Buttons.Edit.Visible = False
    Buttons.Post.Visible = False
    Buttons.Cancel.Visible = False
    Buttons.Refresh.Visible = False
    Buttons.SaveBookmark.Visible = False
    Buttons.GotoBookmark.Visible = False
    Buttons.Filter.Visible = False
    DataSource = dsRNC
    InfoPanel.DisplayMask = 'Registros Encontrados [RecordIndex] de [RecordCount]'
    InfoPanel.Font.Charset = DEFAULT_CHARSET
    InfoPanel.Font.Color = clWindowText
    InfoPanel.Font.Height = -11
    InfoPanel.Font.Name = 'Tahoma'
    InfoPanel.Font.Style = [fsBold]
    InfoPanel.ParentFont = False
    InfoPanel.Visible = True
    LookAndFeel.NativeStyle = True
    TabOrder = 1
  end
  object tlb3: TToolBar
    Left = 0
    Top = 0
    Width = 825
    Height = 65
    BorderWidth = 2
    ButtonHeight = 52
    ButtonWidth = 39
    Caption = 'tlb1'
    Flat = False
    Images = frmMain.ilToolBar32
    ShowCaptions = True
    TabOrder = 0
    Transparent = False
    object btnclose: TToolButton
      Left = 0
      Top = 0
      Action = wndwclsclose
      Caption = 'Sa&lir'
      ImageIndex = 1
    end
  end
  object cxpgcntrlListado: TcxPageControl
    Left = 8
    Top = 95
    Width = 804
    Height = 403
    Align = alCustom
    TabOrder = 2
    Properties.ActivePage = cxtbshtListado
    ClientRectBottom = 397
    ClientRectLeft = 3
    ClientRectRight = 798
    ClientRectTop = 26
    object cxtbshtListado: TcxTabSheet
      Caption = '&Listado'
      ImageIndex = 0
      object cxgrd1: TcxGrid
        Left = 0
        Top = 0
        Width = 795
        Height = 371
        Align = alClient
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        LookAndFeel.Kind = lfOffice11
        LookAndFeel.NativeStyle = False
        object cxgrdbtblvwGrid1DBTableView1: TcxGridDBTableView
          DataController.DataSource = dsRNC
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          OptionsView.GroupByBox = False
          object cxgrdbclmnGrid1DBTableView1RNC: TcxGridDBColumn
            DataBinding.FieldName = 'RNC'
            HeaderAlignmentHorz = taCenter
            Styles.Header = frmMain.cxstylGridHeader
            Width = 126
          end
          object cxgrdbclmnGrid1DBTableView1Nombre: TcxGridDBColumn
            DataBinding.FieldName = 'Nombre'
            HeaderAlignmentHorz = taCenter
            Styles.Header = frmMain.cxstylGridHeader
            Width = 200
          end
          object cxgrdbclmnGrid1DBTableView1NombreComercial: TcxGridDBColumn
            Caption = 'Nombre Comercial'
            DataBinding.FieldName = 'Nombre.Comercial'
            HeaderAlignmentHorz = taCenter
            Styles.Header = frmMain.cxstylGridHeader
            Width = 225
          end
          object cxgrdbclmnGrid1DBTableView1Estatus: TcxGridDBColumn
            Caption = 'Estado'
            DataBinding.FieldName = 'Estatus'
            HeaderAlignmentHorz = taCenter
            Styles.Header = frmMain.cxstylGridHeader
            Width = 205
          end
        end
        object cxgrdlvlGrid1Level1: TcxGridLevel
          GridView = cxgrdbtblvwGrid1DBTableView1
        end
      end
    end
    object cxtbshtDetalle: TcxTabSheet
      Caption = '&Detalle'
      ImageIndex = 1
      object grp1: TGroupBox
        Left = 16
        Top = 17
        Width = 625
        Height = 290
        Caption = 'Registro'
        TabOrder = 0
        object cxlbllbl2: TcxLabel
          Left = 149
          Top = 12
          Caption = 'Nombre'
        end
        object edtcxdbtxtdt1: TcxDBTextEdit
          Left = 22
          Top = 120
          DataBinding.DataField = 'Ocupacion'
          DataBinding.DataSource = dsRNC
          StyleFocused.BorderColor = clRed
          StyleFocused.BorderStyle = ebsThick
          StyleFocused.Color = clGradientActiveCaption
          StyleHot.BorderColor = clRed
          TabOrder = 7
          Width = 587
        end
        object edtcxdbtxtdt4: TcxDBTextEdit
          Left = 22
          Top = 163
          DataBinding.DataField = 'Direccion.Calle'
          DataBinding.DataSource = dsRNC
          StyleFocused.BorderColor = clRed
          StyleFocused.BorderStyle = ebsThick
          StyleFocused.Color = clGradientActiveCaption
          StyleHot.BorderColor = clRed
          TabOrder = 9
          Width = 587
        end
        object edtcxdbtxtdt5: TcxDBTextEdit
          Left = 22
          Top = 206
          DataBinding.DataField = 'Direccion.Numero'
          DataBinding.DataSource = dsRNC
          StyleFocused.BorderColor = clRed
          StyleFocused.BorderStyle = ebsThick
          StyleFocused.Color = clGradientActiveCaption
          StyleHot.BorderColor = clRed
          TabOrder = 13
          Width = 121
        end
        object edtcxdbtxtdt7: TcxDBTextEdit
          Left = 149
          Top = 248
          DataBinding.DataField = 'Fecha'
          DataBinding.DataSource = dsRNC
          StyleFocused.BorderColor = clRed
          StyleFocused.BorderStyle = ebsThick
          StyleFocused.Color = clGradientActiveCaption
          StyleHot.BorderColor = clRed
          TabOrder = 20
          Width = 121
        end
        object edtcxdbtxtdt8: TcxDBTextEdit
          Left = 22
          Top = 248
          DataBinding.DataField = 'Telefono'
          DataBinding.DataSource = dsRNC
          StyleFocused.BorderColor = clRed
          StyleFocused.BorderStyle = ebsThick
          StyleFocused.Color = clGradientActiveCaption
          StyleHot.BorderColor = clRed
          TabOrder = 19
          Width = 121
        end
        object edtcxdbtxtdt9: TcxDBTextEdit
          Left = 283
          Top = 248
          DataBinding.DataField = 'Estatus'
          DataBinding.DataSource = dsRNC
          StyleFocused.BorderColor = clRed
          StyleFocused.BorderStyle = ebsThick
          StyleFocused.Color = clGradientActiveCaption
          StyleHot.BorderColor = clRed
          TabOrder = 21
          Width = 121
        end
        object cxlbllblcxlbl1: TcxLabel
          Left = 22
          Top = 12
          Caption = 'RNC / C'#233'dula'
        end
        object edtcxdbtxtdt2: TcxDBTextEdit
          Left = 149
          Top = 206
          DataBinding.DataField = 'Direccion.Sector'
          DataBinding.DataSource = dsRNC
          StyleFocused.BorderColor = clRed
          StyleFocused.BorderStyle = ebsThick
          StyleFocused.Color = clGradientActiveCaption
          StyleHot.BorderColor = clRed
          TabOrder = 14
          Width = 255
        end
        object cxlbllbl1: TcxLabel
          Left = 410
          Top = 190
          Caption = 'Regimen Pago'
        end
        object cxlbllbl3: TcxLabel
          Left = 22
          Top = 101
          Caption = 'Ocupaci'#243'n'
        end
        object cxlbllbl4: TcxLabel
          Left = 22
          Top = 147
          Caption = 'Direcci'#243'n Calle'
        end
        object cxlbllbl5: TcxLabel
          Left = 22
          Top = 190
          Caption = 'Direcci'#243'n N'#250'mero'
        end
        object cxlbllbl6: TcxLabel
          Left = 22
          Top = 233
          Caption = 'Tel'#233'fono'
        end
        object cxlbllbl7: TcxLabel
          Left = 149
          Top = 233
          Caption = 'Fecha'
        end
        object cxlbllbl8: TcxLabel
          Left = 283
          Top = 233
          Caption = 'Estatus'
        end
        object edtcxdbtxtdt10: TcxDBTextEdit
          Left = 410
          Top = 206
          DataBinding.DataField = 'RegimenPago'
          DataBinding.DataSource = dsRNC
          StyleFocused.BorderColor = clRed
          StyleFocused.BorderStyle = ebsThick
          StyleFocused.Color = clGradientActiveCaption
          StyleHot.BorderColor = clRed
          TabOrder = 15
          Width = 121
        end
        object cxlbllbl10: TcxLabel
          Left = 149
          Top = 190
          Caption = 'Sector'
        end
        object cxlbllbl11: TcxLabel
          Left = 22
          Top = 54
          Caption = 'Nombre Comercial'
        end
        object edtcxdbtxtdt11: TcxDBTextEdit
          Left = 22
          Top = 74
          DataBinding.DataField = 'Nombre.Comercial'
          DataBinding.DataSource = dsRNC
          StyleFocused.BorderColor = clRed
          StyleFocused.BorderStyle = ebsThick
          StyleFocused.Color = clGradientActiveCaption
          StyleHot.BorderColor = clRed
          TabOrder = 5
          Width = 587
        end
        object btnSupervisorID1: TcxDBButtonEdit
          Left = 22
          Top = 27
          DataBinding.DataField = 'RNC'
          DataBinding.DataSource = dsRNC
          Properties.Buttons = <
            item
              Default = True
              Kind = bkEllipsis
            end>
          Properties.OnButtonClick = btnSupervisorID1PropertiesButtonClick
          StyleFocused.BorderColor = clRed
          StyleFocused.BorderStyle = ebsThick
          StyleFocused.Color = clGradientActiveCaption
          StyleHot.BorderColor = clRed
          TabOrder = 2
          Width = 121
        end
        object btnNombre: TcxDBButtonEdit
          Left = 149
          Top = 27
          DataBinding.DataField = 'Nombre'
          DataBinding.DataSource = dsRNC
          Properties.Buttons = <
            item
              Default = True
              Kind = bkEllipsis
            end>
          Properties.OnButtonClick = btnNombrePropertiesButtonClick
          StyleFocused.BorderColor = clRed
          StyleFocused.BorderStyle = ebsThick
          StyleFocused.Color = clGradientActiveCaption
          StyleHot.BorderColor = clRed
          TabOrder = 3
          Width = 460
        end
      end
    end
    object cxtbshtAgregar: TcxTabSheet
      Caption = '&Mantenimiento'
      ImageIndex = 2
      object grp2: TGroupBox
        Left = 16
        Top = 78
        Width = 625
        Height = 290
        Caption = 'Registro'
        TabOrder = 1
        object cxlbl1: TcxLabel
          Left = 149
          Top = 12
          Caption = 'Nombre'
        end
        object edt1: TcxDBTextEdit
          Left = 149
          Top = 27
          DataBinding.DataField = 'Nombre'
          DataBinding.DataSource = dsRNC2
          StyleFocused.BorderColor = clRed
          StyleFocused.BorderStyle = ebsThick
          StyleFocused.Color = clGradientActiveCaption
          StyleHot.BorderColor = clRed
          TabOrder = 3
          Width = 460
        end
        object edt2: TcxDBTextEdit
          Left = 22
          Top = 120
          DataBinding.DataField = 'Ocupacion'
          DataBinding.DataSource = dsRNC2
          StyleFocused.BorderColor = clRed
          StyleFocused.BorderStyle = ebsThick
          StyleFocused.Color = clGradientActiveCaption
          StyleHot.BorderColor = clRed
          TabOrder = 7
          Width = 587
        end
        object edt3: TcxDBTextEdit
          Left = 22
          Top = 163
          DataBinding.DataField = 'Direccion.Calle'
          DataBinding.DataSource = dsRNC2
          StyleFocused.BorderColor = clRed
          StyleFocused.BorderStyle = ebsThick
          StyleFocused.Color = clGradientActiveCaption
          StyleHot.BorderColor = clRed
          TabOrder = 9
          Width = 587
        end
        object edt4: TcxDBTextEdit
          Left = 22
          Top = 206
          DataBinding.DataField = 'Direccion.Numero'
          DataBinding.DataSource = dsRNC2
          StyleFocused.BorderColor = clRed
          StyleFocused.BorderStyle = ebsThick
          StyleFocused.Color = clGradientActiveCaption
          StyleHot.BorderColor = clRed
          TabOrder = 13
          Width = 121
        end
        object edt5: TcxDBTextEdit
          Left = 149
          Top = 248
          DataBinding.DataField = 'Fecha'
          DataBinding.DataSource = dsRNC2
          StyleFocused.BorderColor = clRed
          StyleFocused.BorderStyle = ebsThick
          StyleFocused.Color = clGradientActiveCaption
          StyleHot.BorderColor = clRed
          TabOrder = 20
          Width = 121
        end
        object edt6: TcxDBTextEdit
          Left = 22
          Top = 248
          DataBinding.DataField = 'Telefono'
          DataBinding.DataSource = dsRNC2
          StyleFocused.BorderColor = clRed
          StyleFocused.BorderStyle = ebsThick
          StyleFocused.Color = clGradientActiveCaption
          StyleHot.BorderColor = clRed
          TabOrder = 19
          Width = 121
        end
        object edt7: TcxDBTextEdit
          Left = 287
          Top = 248
          DataBinding.DataField = 'Estatus'
          DataBinding.DataSource = dsRNC2
          StyleFocused.BorderColor = clRed
          StyleFocused.BorderStyle = ebsThick
          StyleFocused.Color = clGradientActiveCaption
          StyleHot.BorderColor = clRed
          TabOrder = 21
          Width = 121
        end
        object cxlbl2: TcxLabel
          Left = 22
          Top = 12
          Caption = 'RNC / C'#233'dula'
        end
        object edt8: TcxDBTextEdit
          Left = 149
          Top = 206
          DataBinding.DataField = 'Direccion.Sector'
          DataBinding.DataSource = dsRNC2
          StyleFocused.BorderColor = clRed
          StyleFocused.BorderStyle = ebsThick
          StyleFocused.Color = clGradientActiveCaption
          StyleHot.BorderColor = clRed
          TabOrder = 14
          Width = 255
        end
        object cxlbl3: TcxLabel
          Left = 410
          Top = 190
          Caption = 'Regimen Pago'
        end
        object cxlbl4: TcxLabel
          Left = 22
          Top = 101
          Caption = 'Ocupaci'#243'n'
        end
        object cxlbl5: TcxLabel
          Left = 22
          Top = 147
          Caption = 'Direcci'#243'n Calle'
        end
        object cxlbl6: TcxLabel
          Left = 22
          Top = 190
          Caption = 'Direcci'#243'n N'#250'mero'
        end
        object cxlbl7: TcxLabel
          Left = 22
          Top = 233
          Caption = 'Tel'#233'fono'
        end
        object cxlbl8: TcxLabel
          Left = 149
          Top = 233
          Caption = 'Fecha'
        end
        object cxlbl9: TcxLabel
          Left = 283
          Top = 233
          Caption = 'Estatus'
        end
        object edt9: TcxDBTextEdit
          Left = 410
          Top = 206
          DataBinding.DataField = 'RegimenPago'
          DataBinding.DataSource = dsRNC2
          StyleFocused.BorderColor = clRed
          StyleFocused.BorderStyle = ebsThick
          StyleFocused.Color = clGradientActiveCaption
          StyleHot.BorderColor = clRed
          TabOrder = 15
          Width = 121
        end
        object cxlbl10: TcxLabel
          Left = 149
          Top = 190
          Caption = 'Sector'
        end
        object cxlbl11: TcxLabel
          Left = 22
          Top = 54
          Caption = 'Nombre Comercial'
        end
        object edt10: TcxDBTextEdit
          Left = 22
          Top = 74
          DataBinding.DataField = 'Nombre.Comercial'
          DataBinding.DataSource = dsRNC2
          StyleFocused.BorderColor = clRed
          StyleFocused.BorderStyle = ebsThick
          StyleFocused.Color = clGradientActiveCaption
          StyleHot.BorderColor = clRed
          TabOrder = 5
          Width = 587
        end
        object edt11: TcxDBTextEdit
          Left = 22
          Top = 27
          DataBinding.DataField = 'RNC'
          DataBinding.DataSource = dsRNC2
          StyleFocused.BorderColor = clRed
          StyleFocused.BorderStyle = ebsThick
          StyleFocused.Color = clGradientActiveCaption
          StyleHot.BorderColor = clRed
          TabOrder = 2
          Width = 121
        end
      end
      object tlb1: TToolBar
        Left = 0
        Top = 0
        Width = 795
        Height = 65
        BorderWidth = 2
        ButtonHeight = 52
        ButtonWidth = 49
        Caption = 'tlb1'
        Flat = False
        Images = frmMain.ilToolBar32
        ShowCaptions = True
        TabOrder = 0
        Transparent = False
        object btnadd1: TToolButton
          Left = 0
          Top = 0
          Action = dtstnsrtadd
          ImageIndex = 10
        end
        object btnedit1: TToolButton
          Left = 49
          Top = 0
          Action = dtstdtedit
          ImageIndex = 9
        end
        object btnDelete: TToolButton
          Left = 98
          Top = 0
          Action = dtstdltDelete
          ImageIndex = 8
        end
        object btn1: TToolButton
          Left = 147
          Top = 0
          Width = 49
          Caption = 'btn1'
          ImageIndex = 6
          Style = tbsSeparator
        end
        object btnAccept1: TToolButton
          Left = 196
          Top = 0
          Action = dtstpstAccept
          ImageIndex = 2
        end
        object btncancel1: TToolButton
          Left = 245
          Top = 0
          Action = dtstcnclcancel
          ImageIndex = 5
        end
      end
    end
  end
  object qryRNC: TADOQuery
    Active = True
    Connection = DatMain.con1
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'SELECT TOP 50 *  FROM vw_RNC')
    Left = 336
    Top = 64
    object atncfldRNCpidregistro: TAutoIncField
      DisplayWidth = 12
      FieldName = 'pidregistro'
      ReadOnly = True
    end
    object wdstrngfldRNCRNC: TWideStringField
      DisplayWidth = 10
      FieldName = 'RNC'
      Size = 50
    end
    object wdstrngfldRNCNombre: TWideStringField
      DisplayWidth = 300
      FieldName = 'Nombre'
      Size = 250
    end
    object wdstrngfldRNCNombreComercial: TWideStringField
      DisplayWidth = 300
      FieldName = 'Nombre.Comercial'
      Size = 250
    end
    object wdstrngfldRNCOcupacion: TWideStringField
      DisplayWidth = 300
      FieldName = 'Ocupacion'
      Size = 250
    end
    object wdstrngfldRNCDireccionCalle: TWideStringField
      DisplayWidth = 300
      FieldName = 'Direccion.Calle'
      Size = 250
    end
    object wdstrngfldRNCDireccionNumero: TWideStringField
      DisplayWidth = 300
      FieldName = 'Direccion.Numero'
      Size = 250
    end
    object wdstrngfldRNCDireccionSector: TWideStringField
      DisplayWidth = 300
      FieldName = 'Direccion.Sector'
      Size = 250
    end
    object wdstrngfldRNCTelefono: TWideStringField
      DisplayWidth = 60
      FieldName = 'Telefono'
      Size = 50
    end
    object wdstrngfldRNCFecha: TWideStringField
      DisplayWidth = 60
      FieldName = 'Fecha'
      Size = 50
    end
    object wdstrngfldRNCEstatus: TWideStringField
      DisplayWidth = 60
      FieldName = 'Estatus'
      Size = 50
    end
    object wdstrngfldRNCRegimenPago: TWideStringField
      DisplayWidth = 60
      FieldName = 'RegimenPago'
      Size = 50
    end
  end
  object dsRNC: TDataSource
    AutoEdit = False
    DataSet = qryRNC
    Left = 400
    Top = 64
  end
  object jvpmn1: TJvPopupMenu
    Images = frmMain.ilToolBar32
    ImageMargin.Left = 0
    ImageMargin.Top = 0
    ImageMargin.Right = 0
    ImageMargin.Bottom = 0
    ImageSize.Height = 0
    ImageSize.Width = 0
    Left = 464
    Top = 64
    object mniAgregar1: TMenuItem
      Action = dtstnsrtadd
    end
    object mniN1: TMenuItem
      Caption = '-'
    end
    object mniModificar1: TMenuItem
      Action = dtstdtedit
    end
    object mniN2: TMenuItem
      Caption = '-'
    end
    object mniEliminar1: TMenuItem
      Action = dtstdltDelete
    end
  end
  object actmgr1: TActionManager
    Left = 264
    Top = 64
    StyleName = 'Platform Default'
    object dtstprPrior: TDataSetPrior
      Category = 'dataset'
      Caption = '&Anterior'
      Hint = 'Prior'
      ImageIndex = 1
      OnExecute = dtstprPriorExecute
    end
    object dtstnxtNext: TDataSetNext
      Category = 'dataset'
      Caption = '&Siguiente'
      Hint = 'Next'
      ImageIndex = 2
      OnExecute = dtstnxtNextExecute
    end
    object dtstdtedit: TDataSetEdit
      Category = 'dataset'
      Caption = '&Editar'
      Hint = 'Edit'
      ImageIndex = 6
    end
    object dtstnsrtadd: TDataSetInsert
      Category = 'dataset'
      Caption = '&Agregar'
      Hint = 'Insert'
      ImageIndex = 4
    end
    object wndwclsclose: TWindowClose
      Category = 'Window'
      Caption = 'C&lose'
      Enabled = False
      Hint = 'Close'
      OnExecute = wndwclscloseExecute
    end
    object dtstcnclcancel: TDataSetCancel
      Category = 'Dataset'
      Caption = '&Cancelar'
      Hint = 'Cancel'
      ImageIndex = 8
    end
    object dtstpstAccept: TDataSetPost
      Category = 'Dataset'
      Caption = 'A&ceptar'
      Hint = 'Post'
      ImageIndex = 7
    end
    object dtstdltDelete: TDataSetDelete
      Category = 'Dataset'
      Caption = '&Eliminar'
      Hint = 'Delete'
      ImageIndex = 5
      OnExecute = dtstdltDeleteExecute
    end
  end
  object qryRNC2: TADOQuery
    Connection = DatMain.con1
    CursorType = ctStatic
    AfterEdit = qryRNC2AfterEdit
    AfterPost = qryRNC2AfterPost
    BeforeDelete = qryRNC2BeforeDelete
    Parameters = <>
    SQL.Strings = (
      'SELECT TOP 50 *  FROM RNC2')
    Left = 576
    Top = 64
    object atncfld1: TAutoIncField
      DisplayWidth = 12
      FieldName = 'pidregistro'
      ReadOnly = True
    end
    object wdstrngfld1: TWideStringField
      DisplayWidth = 10
      FieldName = 'RNC'
      Size = 50
    end
    object wdstrngfld2: TWideStringField
      DisplayWidth = 300
      FieldName = 'Nombre'
      Size = 250
    end
    object wdstrngfld3: TWideStringField
      DisplayWidth = 300
      FieldName = 'Nombre.Comercial'
      Size = 250
    end
    object wdstrngfld4: TWideStringField
      DisplayWidth = 300
      FieldName = 'Ocupacion'
      Size = 250
    end
    object wdstrngfld5: TWideStringField
      DisplayWidth = 300
      FieldName = 'Direccion.Calle'
      Size = 250
    end
    object wdstrngfld6: TWideStringField
      DisplayWidth = 300
      FieldName = 'Direccion.Numero'
      Size = 250
    end
    object wdstrngfld7: TWideStringField
      DisplayWidth = 300
      FieldName = 'Direccion.Sector'
      Size = 250
    end
    object wdstrngfld8: TWideStringField
      DisplayWidth = 60
      FieldName = 'Telefono'
      Size = 50
    end
    object wdstrngfld9: TWideStringField
      DisplayWidth = 60
      FieldName = 'Fecha'
      Size = 50
    end
    object wdstrngfld10: TWideStringField
      DisplayWidth = 60
      FieldName = 'Estatus'
      Size = 50
    end
    object wdstrngfld11: TWideStringField
      DisplayWidth = 60
      FieldName = 'RegimenPago'
      Size = 50
    end
  end
  object dsRNC2: TDataSource
    AutoEdit = False
    DataSet = qryRNC2
    Left = 624
    Top = 64
  end
end
